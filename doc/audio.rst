.. contents:: Table of Contents


.. _audio:

Table audio
===========

Audio is an attempt to embed audio for multiple platform using the same interface in `Lua <https://www.lua.org/>`__ by the engine.

Unfortunately, this is still time consuming and not every platform is easy to handle with a common built-in music library. The main focus of the engine is *graphics*, so, for now, we will keep those libraries. ``PortAudio`` is a potential cross-platform API to be used however the codecs might demand some time to incorporate, so, for now, it is available over ``Linux`` only.

For :guilabel:`Windows`  it is used `Audiere <http://audiere.sourceforge.net>`__.

For :guilabel:`Linux` it is used `PortAudio <http://www.portaudio.com/>`__.

For :guilabel:`Android` it is used native audio.

.. Important:: For `Android` only add audio in the asset folder. If you try to read audio from external device such as SD card, it has to modify the Java code for that.



audio Methods
-------------

audio new
^^^^^^^^^

.. data:: new(string file_name, boolean * in_memory, boolean * play, boolean * loop)

   Create a new instance of a :ref:`audio <audio>`.

   :param string: **file name** of sound.
   :param boolean: **in memory** , default is ``false``.
   :param boolean: **play** to play immediately, default is ``false``.
   :param boolean: **loop** to keep in loop, default is ``false``.
   :return: :ref:`audio <audio>` *table*.


audio play
^^^^^^^^^^

.. data:: play(boolean * loop)

   :param boolean: **loop** to keep in loop, default is ``false``.
   :return: ``boolean`` *result*

.. data:: isPlaying()

   :return: ``boolean`` *result*


audio pause
^^^^^^^^^^^

.. data:: pause()

   :return: ``boolean`` *result*

.. data:: isPaused()

   :return: ``boolean`` *result*

audio stop
^^^^^^^^^^

.. data:: stop()

   :return: ``boolean`` *result*


audio volume
^^^^^^^^^^^^

.. data:: setVolume(number volume)

   :param number: **volume** of sound (usually 0 to 1).
   :return: ``boolean`` *result*

.. data:: getVolume()

   :return: ``number`` *volume*

audio pitch
^^^^^^^^^^^

.. data:: setPitch(number pitch)

   :param number: **pitch** of sound.
   :return: ``boolean`` *result*

.. data:: getPitch()

   :return: ``number`` *pitch*

audio pan
^^^^^^^^^

.. data:: setPan(number pan)

   :param number: **pan** of sound.
   :return: ``boolean`` *result*

.. data:: getPan()

   :return: ``number`` *pan*

audio pan
^^^^^^^^^

.. data:: setPan(number pan)

   :param number: **pan** of sound.
   :return: ``boolean`` *result*

.. data:: getPan()

   :return: ``number`` *pan*

audio reset
^^^^^^^^^^^

.. data:: reset()

   :return: ``boolean`` *result*


audio len
^^^^^^^^^

.. data:: getLen()

   :return: ``number`` *length* of sound.

audio setPosition
^^^^^^^^^^^^^^^^^

.. data:: setPan(number position)

   :param number: **position** of sound as ``integer``.
   :return: ``boolean`` *result*


audio pan
^^^^^^^^^

.. data:: setPan(number pan)

   :param number: **pan** of sound.
   :return: ``boolean`` *result*

.. data:: getName()

   :return: ``string`` *name* of sound.

audio persistent
^^^^^^^^^^^^^^^^

.. data:: setPersistent(boolean persistent)

   :param boolean: **persistent**.

.. data:: isPersistent()

   :return: ``boolean`` *persistent*.

audio destroy
^^^^^^^^^^^^^

.. data:: destroy()

audio onEnd
^^^^^^^^^^^

.. data:: onEnd(function callback)

   :param function: **callback**.

    .. code-block:: lua

        function onEndStream(audio tAudio,string name)

        end