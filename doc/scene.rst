.. contents:: Table of Contents

.. _scene:

Scene
=====
Most games use scene mechanism. This engine is no different.

When we have a new scene, the previous scene is destroyed and the new scene will become the current scene. It means that only the current scene exists when see the game.

This is how scenes in general works.

All we need, is to overload functions in the scene script that will be called by the engine.

There are a default scene internally. If any function does not exist, the default function is called.

Now, let's see the main flow of a scene:

Main Scene’s flow
-----------------

All functions described here can be overloaded.

onInitScene
^^^^^^^^^^^

.. data:: onInitScene()

   The engine will call this method only once during the execution's script.

loop
^^^^

.. _loop:

.. data:: loop(delta)

   Called each step. Delta indicate the interval passed since the last step.

   :param number: **delta** time passed since the last step.

onEndScene
^^^^^^^^^^

.. _onEndScene:

.. data:: onEndScene()

   The engine will call this method only once during the scene' ending.
   The end of scene occurs when other scene is called or the application is shut down.

.. _onRestore:

onRestore
^^^^^^^^^

.. data:: onRestore()

   Called on restore scene.

   This usually happen when the device is lost.


Scene’s functions
-----------------

onTouchDown
^^^^^^^^^^^

.. data:: onTouchDown( number key, number x,number y)

   Called on touch down or click down on screen 2d.

   :param number: **key** mouse click or finger on touch screen (start from 1).
   :param number: **x** coordinate (left is 0).
   :param number: **y** coordinate (up is 0).

onTouchUp
^^^^^^^^^

.. data:: onTouchUp(number key, number x,number y)

   Called on touch up or click up on screen 2d.

   :param number: **key** mouse click or finger on touch screen (start from 1).
   :param number: **x** coordinate (left is 0).
   :param number: **y** coordinate (up is 0).

onTouchMove
^^^^^^^^^^^

.. data:: onTouchMove(number key,number x,number y)

   Called on touch move (mouse or finger) on screen 2d.

   :param number: **key** mouse or finger on touch screen (start from 1).
   :param number: **x** coordinate (left is 0).
   :param number: **y** coordinate (up is 0).

onTouchZoom
^^^^^^^^^^^

.. data:: onTouchZoom(number zoom)

   Called on zoom gesture or mouse scroll (values will be -1 and 1).

   :param number: **zoom** values between -1 and 1.

onKeyDown
^^^^^^^^^

.. _onKeyDown:

.. data:: onKeyDown(number key)

   Called on key down.

   :param number: **key** depends on operation system. the engine offers a function getCode and getKey translator for comparison.

onKeyUp
^^^^^^^

.. _onKeyUp:

.. data:: onKeyUp(number key)

   Called on key up.

   :param number: **key** depends on operation system. the engine offers a function getCode and getKey translator for comparison.

onInfoJoystick
^^^^^^^^^^^^^^

.. data:: onInfoJoystick(number player,number maxButtons,string deviceName,string extra)

	Called on connect joystick. depends on operation system and joystick brand.

	:param number: **player** indicate the player number.
	:param number: **maxButtons** information about how many buttons are available in this joystick.
	:param string: **deviceName** Joystick's name. information from device.
	:param string: **extra** It might have extra information about the joystick.

onKeyDownJoystick
^^^^^^^^^^^^^^^^^

.. data:: onKeyDownJoystick(number player,number key)

   Called on joystick key down.

   :param number: **player** indicate the player number.
   :param number: **key** pressed on joystick.

onKeyUpJoystick
^^^^^^^^^^^^^^^

.. data:: onKeyUpJoystick(number player,number key)

   Called on joystick key up.

   :param number: **player** indicate the player number.
   :param number: **key** pressed on joystick.

onMoveJoystick
^^^^^^^^^^^^^^

.. data:: onMoveJoystick (number player,number lx, number ly, number rx, number ry)

   Called on joystick move

   :param number: **player** indicate the player number.
   :param number: **lx** left pad on x axis.
   :param number: **ly** left pad on y axis.
   :param number: **rx** right pad on x axis.
   :param number: **ry** right pad on y axis.

   .. Attention:: Although the Joystick's functions had been tested including some joystick available on the market, there are a lot of them. Might not work properly on some devices.


onResizeWindow
^^^^^^^^^^^^^^

.. data:: onResizeWindow()

   The engine will call this method whenever the window size has changed. It is up to the developer to rescale all scenes accordingly.


   .. Note:: When 'onResizeWindow' function is not available, the engine will restart the scene automatically since the scale has changed.

.. _scene_normal_style:

Example of Scene
----------------

Next we will use the following image to play with some scene's example:

.. figure:: _static/crate.png
   :align: center
   :width: 200px
   :height: 200px
   :figclass: align-center

   crate.png

.. literalinclude:: example_modules/scene_1.lua
  :language: lua

:download:`download crate.png <_static/crate.png>`

:download:`download <example_modules/scene_1.lua>`.

.. _scene_class_style:

Example of Scene class style
----------------------------

   .. literalinclude:: example_modules/scene_2.lua
     :language: lua

   :download:`download <example_modules/scene_2.lua>`.

.. figure:: _static/sample_scene_1.gif
    :align: center
    :figclass: align-center

    Both :ref:`scene normal style <scene_normal_style>` and :ref:`scene class style <scene_class_style>` running.


.. Important::

  | Both :ref:`scene normal style <scene_normal_style>` and :ref:`scene class style <scene_class_style>` produce the same result.
  | In both case, we are not storing the local texture and occasionally will be collected (destroyed).


Example of Scene using coroutine
--------------------------------

| By default, :ref:`scene class style <scene_class_style>` has coroutine which is a way to divide the initial load in severals steps.
| Every time that you call ``coroutine.yield`` function from lua, there is an interruption and the engine does a loop of render.
| Below an example of that:

.. literalinclude:: example_modules/scene_3.lua
    :language: lua

:download:`download <example_modules/scene_3.lua>`.

.. figure:: _static/scene_using_coroutine.gif
  :align: center
  :figclass: align-center

  A :ref:`scene class style <scene_class_style>` running using coroutine.


.. figure:: _static/output_scene_using_coroutine.png
    :align: center
    :figclass: align-center

    The output in the console.


