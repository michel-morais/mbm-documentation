.. contents:: Table of Contents

.. TODO  added  USE_EDITOR_FEATURES where is needed.. Important:: The :guilabel:`USE_EDITOR_FEATURES` flag must be enabled to this method work.

.. _meshDebug:

Table meshDebug
===============

The table :ref:`meshDebug <meshDebug>` is used to build, debug and save to binary file any format supported by the engine.

The idea of this object, is to construct any object available in the engine or import an object from the well know `Wavefront <https://en.wikipedia.org/wiki/Wavefront_.obj_file>`_  for example.

The table :ref:`meshDebug <meshDebug>` is not a :ref:`renderizable <renderizable>` object, than, there is no common methods inherited.

The table :ref:`meshDebug <meshDebug>` support the following types from engine:

.. _typeMeshDebug:

     +----------------------------+---------------------------+
     |      Types                 |   Expected file extension |
     +============================+===========================+
     |      mesh                  |        ``msh``            |
     +----------------------------+---------------------------+
     |      sprite                |        ``spt``            |
     +----------------------------+---------------------------+
     |      font                  |        ``fnt``            |
     +----------------------------+---------------------------+
     |      texture               | ``png`` ``jpeg`` ``etc``  |
     +----------------------------+---------------------------+
     |      shape                 |          ``-``            |
     +----------------------------+---------------------------+
     |      particle              |          ``ptl``          |
     +----------------------------+---------------------------+
     |      tile                  |          ``tile``         |
     +----------------------------+---------------------------+

Primitive options
-----------------

| Primitive determines how consecutive vertices are organized into primitives (triangles),
| and determines the type of primitive that is used at the beginning of the graphics pipeline.
|
| Follow some explanation of the main options for primitives used in the engine.

.. _ModeDraw:

mode draw
^^^^^^^^^

    | The mode draw is the way to specific triangle primitives.
    | It is based on different interpretations of the vertex stream.
    | Next the table explaining the interpretation of each of them:

     +----------------------------+-----------------------------------------------------------------------------+
     |      Mode                  |        Explanation                                                          |
     +============================+=============================================================================+
     |      TRIANGLES             |  | Given the vertices,                                                      |
     |                            |  | vertices 1, 2, and 3 form a triangle,                                    |
     |                            |  | vertices 4, 5, and 6 form a triangle,                                    |
     |                            |  | and so on.                                                               |
     |                            |                                                                             |
     |                            |  .. figure:: _static/mesh_debug_triangle_list.png                           |
     |                            |                                                                             |
     +----------------------------+-----------------------------------------------------------------------------+
     |      TRIANGLE_STRIP        |  | Given the vertices,                                                      |
     |                            |  | Every group of 3 adjacent vertices forms a triangle.                     |
     |                            |                                                                             |
     |                            |  .. figure:: _static/mesh_debug_triangle_strip.png                          |
     |                            |      :scale: 85%                                                            |
     |                            |                                                                             |
     +----------------------------+-----------------------------------------------------------------------------+
     |      TRIANGLE_FAN          |  | Given the vertices,                                                      |
     |                            |  | The first vertex is always held fixed.                                   |
     |                            |  | From there on, every group of 2 adjacent vertices form a                 |
     |                            |  | triangle with the first.                                                 |
     |                            |  | you get a list of triangles like so: (0, 1, 2) (0, 2, 3), (0, 3, 4), etc |
     |                            |                                                                             |
     |                            |  .. figure:: _static/mesh_debug_triangle_fan.png                            |
     |                            |      :scale: 95%                                                            |
     |                            |                                                                             |
     +----------------------------+-----------------------------------------------------------------------------+
     |      LINES                 |  | Given the vertices,                                                      |
     |                            |  | vertices 1 and 2 form a line,                                            |
     |                            |  | vertices 3 and 4 form a lines                                            |
     |                            |  | and so on.                                                               |
     |                            |                                                                             |
     |                            |  .. figure:: _static/mesh_debug_line_list.png                               |
     |                            |      :scale: 35%                                                            |
     |                            |                                                                             |
     +----------------------------+-----------------------------------------------------------------------------+
     |      LINE_LOOP             |  | Given the vertices,                                                      |
     |                            |  | vertices 1 and 2 form a line,                                            |
     |                            |  | vertices 3, 4, and 5 form a more three lines                             |
     |                            |  | and the last vertex will connect to the first vertex.                    |
     |                            |                                                                             |
     |                            |  .. figure:: _static/mesh_debug_line_loop.png                               |
     |                            |      :scale: 35%                                                            |
     |                            |                                                                             |
     +----------------------------+-----------------------------------------------------------------------------+
     |      LINE_STRIP            |  | Given the vertices,                                                      |
     |                            |  | Every group of 2 adjacent vertices forms a line.                         |
     |                            |                                                                             |
     |                            |  .. figure:: _static/mesh_debug_line_strip.png                              |
     |                            |      :scale: 35%                                                            |
     |                            |                                                                             |
     +----------------------------+-----------------------------------------------------------------------------+
     |      POINTS                |  | Given the vertices,                                                      |
     |                            |  | each vertex will be a point                                              |
     |                            |                                                                             |
     |                            |  .. figure:: _static/mesh_debug_points.png                                  |
     |                            |      :scale: 35%                                                            |
     |                            |                                                                             |
     +----------------------------+-----------------------------------------------------------------------------+

.. Important:: 
   | The default mode draw for the engine is ``TRIANGLES``.
   | It is only possible to change the mode draw through :ref:`meshDebug <meshDebug>` :ref:`setModeDraw <setModeDraw>` method.

.. _cullFace:

cull face
^^^^^^^^^

    | Triangle primitives after all transformation steps have a particular facing. 
    | This is defined by the order of the three vertices that make up the triangle, as well as their apparent order on-screen.
    | Triangles can be discarded based on their apparent facing, a process known as face culling.
    |
    | Next the table the engine option:

    +----------------------------+---------------------------+
    |      Types                 |   Comment                 |
    +============================+===========================+
    |      FRONT                 |                           |
    +----------------------------+---------------------------+
    |      BACK                  | Default for the engine.   |
    +----------------------------+---------------------------+
    |      FRONT_AND_BACK        | Will draw both sides.     |
    +----------------------------+---------------------------+

.. _front_face_direction:

front face direction
^^^^^^^^^^^^^^^^^^^^

    | Triangle primitives are drawing following the direction clockwise or counter-clockwise.
    | 
    | The table shows the directions:

    +----------------------------+-----------------------------------------------------+
    |      Types                 |   Comment                                           |
    +============================+=====================================================+
    |      CW                    |   Default for the engine.                           |
    |                            |                                                     |
    |                            |  .. figure:: _static/mesh_debug_cw.png              |
    |                            |                                                     |
    +----------------------------+-----------------------------------------------------+
    |      CCW                   |                                                     |
    |                            |                                                     |
    |                            |  .. figure:: _static/mesh_debug_ccw.png             |
    |                            |                                                     |
    +----------------------------+-----------------------------------------------------+



static methods
--------------

meshDebug getInfo
^^^^^^^^^^^^^^^^^

.. data:: getInfo(string file_name)

   :param string: **file_name** to be save.
   :return: ``table`` - result.

   *Example:*
.. code-block:: lua

    local infoTable = meshDebug:getInfo('crate.msh')
    if infoTable then
        print('info','The table has the following information:')
        for k, v in pairs(infoTable) do
            if type(v) == 'table' then
                for k2, v2 in pairs(v) do
                    print(k, k2, v2)
                end
            else
                print( k, v)
            end
        end
    end


.. figure:: _static/mesh_debug_info_result.png
    :align: center
    :figclass: align-center

    Possible output.

.. Note::
   | This method does not need a instance of meshDebug. It is static.
   | The types are described at this :ref:`table <typeMeshDebug>`.


meshDebug getType
^^^^^^^^^^^^^^^^^

.. data:: getType(string file_name)

   :param string: **file_name** of mesh.
   :return: ``string`` - will be one of option from table :ref:`type of mesh <typeMeshDebug>`.

   *Example:*
.. code-block:: lua

    local type = meshDebug:getType('crate.msh')
    print('info','type of crate.msh',type)
    --possible output:
    --type of crate.msh	mesh

.. Note::
  | This method does not need a instance of meshDebug. It is static.
  | The types are described at this :ref:`table <typeMeshDebug>`.

meshDebug getExt
^^^^^^^^^^^^^^^^

.. data:: getExt(string file_name)

    Given an string it will return the extension. Will look for the last ``.`` in the string and return the string after that.

   :param string: **file_name** of mesh.
   :return: ``string`` - extension.

   *Example:*
.. code-block:: lua

    local ext = meshDebug:getType('crate.msh')
    print('expected msh:',ext)
    --output:
    --expected msh:msh

.. Note::
  | This method does not need a instance of meshDebug. It is static.


meshDebug methods
-----------------

meshDebug new
^^^^^^^^^^^^^

.. data:: new()

   Create a new instance of a :ref:`meshDebug <meshDebug>` .

   :return: :ref:`meshDebug <meshDebug>` *table*.

   *Example:*
.. code-block:: lua

    tMyMesh = meshDebug:new()

meshDebug load
^^^^^^^^^^^^^^

.. _meshDebugLoad:

.. data:: load(string file_name)

   :param string: **file_name** to be loaded.
   :return: ``boolean`` - result.


   *Example:*
.. code-block:: lua

    tMyMesh = meshDebug:new()
    if tMyMesh:load("particle.ptl") then
        print("Success ...")
    else
        print("Failed to load mesh...")
    end

.. Note::

   | The :guilabel:`load` method will search in all known path.
   | You can add a path to search by the method :ref:`addPath <addPath>`.
   | The types are described at this :ref:`table <typeMeshDebug>`.

.. data:: load(renderizable mesh)

   :param renderizable:  :ref:`mesh <renderizable>` to be loaded from memory.
   :return: ``boolean`` - result.


   *Example:*
.. code-block:: lua

    tMyMesh = meshDebug:new()
    tSprite = sprite:new()
    if tSprite:load('mike.spt') then
        if tMyMesh:load(tSprite) then
            print("Success loaded sprite from memory...")
        else
            print("Failed to load mesh...")
        end
    else
        print("Failed to load sprite...")
    end

.. Important:: The :guilabel:`USE_EDITOR_FEATURES` flag must be enabled to this method work.

.. _meshDebugSave:

meshDebug save
^^^^^^^^^^^^^^

.. data:: save(string file_name)

   :param string: **file_name** to be save.
   :return: ``boolean`` - result.

   *Example:*
.. code-block:: lua

    tMyMesh = meshDebug:new()
    if tMyMesh:load("mario.spt") then
        --do something (modify vertex etc)
        if tMyMesh:save("mario2.spt") then
            print("Success ...")
        else
            print("Failed to save file")
        end
    else
        print("Failed to load mesh...")
    end

.. Note::

   The types are described at this :ref:`table <typeMeshDebug>`.

meshDebug setType
^^^^^^^^^^^^^^^^^

.. data:: setType(string new_type)

    Modify or set the internal type of mesh.

   :param string: could be one of option from table :ref:`type of mesh <typeMeshDebug>`. 
   :return: ``boolean`` - result.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        print('my type:', tMesh:getType())
        tMesh:setType('sprite')
        print('my new type:', tMesh:getType())
    end

.. Note::
   | The types are described at this :ref:`table <typeMeshDebug>`.

meshDebug animation
^^^^^^^^^^^^^^^^^^^

.. data:: addAnim(string name, number initialFrame, number finalFrame, number timeBetweenFrame, number typeAnimation)

   Add a new animation.

   :param string: **name**  of animation (new).
   :param number: **index**  initial frame one based.
   :param number: **index**  final frame one based.
   :param number: **time**  between frames.
   :param number: **type**  animation (see :ref:`type of animation <animation>`).
   :return: ``number`` -  Index animation added one based.

   See animation :ref:`animation <animation>` for more detail.

   *Example getting an animation:*
.. code-block:: lua

    tMyMeshDebug = meshDebug:new()
    if tMyMeshDebug:load('crate.msh') then
        local animation_name   = 'default'
        local initialFrame     = 1
        local finalFrame       = 10
        local timeBetweenFrame = 0.3
        local typeAnimation    = mbm.GROWING_LOOP

        local index = tMyMeshDebug:addAnim(animation_name,initialFrame,finalFrame,timeBetweenFrame,typeAnimation)
        print('animation added number:',index)
    end

.. Note::
    | Note that this method differs from :ref:`addAnim inherited from renderizable <addAnim>` because it is not a :ref:`renderizable <renderizable>`.
    | If the initial frame or final frame is out of range the function will fail.

.. data:: getAnim(number index_animation)

    Get detail of animation. 

   :param number: **index_animation** from 1 to max number of animation.
   :return: ``string``, ``number``, ``number``, ``number``, ``number`` -  Animation name,initial frame,final frame, time between frame, type of animation.

   See animation :ref:`animation <animation>` for more detail.

   *Example getting an animation:*
.. code-block:: lua

    tMyMeshDebug = meshDebug:new()
    if tMyMeshDebug:load('crate.msh') then
        local animation,initialFrame,finalFrame, timeBetweenFrame, typeAnimation = tMyMeshDebug:getAnim(1)
        print('animation       :',animation)
        print('initialFrame    :',initialFrame)
        print('finalFrame      :',finalFrame)
        print('timeBetweenFrame:',timeBetweenFrame)
        print('typeAnimation   :',typeAnimation)
    end

.. Note::
    Note that this method differs from :ref:`getAnim inherited from renderizable <getAnimationRenderizable>` because it is not a :ref:`renderizable <renderizable>`.



.. figure:: _static/mesh_debug_get_animation.png
    :align: center
    :figclass: align-center

    Possible output.

.. Note::
    Note that this method differs from :ref:`getAnim inherited from renderizable <getAnimationRenderizable>` because it is not a :ref:`renderizable <renderizable>`.


.. data:: updateAnim(number index, string name, number initialFrame, number finalFrame, number timeBetweenFrame, number typeAnimation)

   Update an existent animation.

   :param number: **index**  animation one based.
   :param string: **name**  of animation (new).
   :param number: **index**  initial frame one based.
   :param number: **index**  final frame one based.
   :param number: **time**  between frames.
   :param number: **type**  animation (see :ref:`type of animation <animation>`).


   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        tMesh:updateAnim(1,'stand',1,5,0.33,mbm.GROWING_LOOP)
    end


.. _copyAnimationsFromMesh:


.. data:: copyAnimationsFromMesh(table renderizable)

    | Will copy each :ref:`animation <animation>` and :ref:`shader <shader>` detail from :ref:`mesh <renderizable>`  passed as parameter.
    | The mesh passed as parameter must be loaded in memory to work.

   :param renderizable: **mesh** to copy :ref:`animation <animation>` and :ref:`shader <shader>`.
   :return: ``boolean`` - result.

   *Example:*
.. code-block:: lua

    tMesh = mesh:new()
    if tMesh:load('crate.msh') then
        tMyMeshDebug = meshDebug:new()
        local animation,initialFrame,finalFrame, timeBetweenFrame, typeAnimation = tMyMeshDebug:getAnim(1)
        print('Before copy animation from crate.msh, animation       :',animation)
        print('Before copy animation from crate.msh, initialFrame    :',initialFrame)
        print('Before copy animation from crate.msh, finalFrame      :',finalFrame)
        print('Before copy animation from crate.msh, timeBetweenFrame:',timeBetweenFrame)
        print('Before copy animation from crate.msh, typeAnimation   :',typeAnimation)


        if tMyMeshDebug:copyAnimationsFromMesh(tMesh) then
            animation,initialFrame,finalFrame, timeBetweenFrame, typeAnimation = tMyMeshDebug:getAnim(1)
            print('green','Now meshDebug has the same animation details as tMesh')
            print('Before copy animation from crate.msh, animation       :',animation)
            print('After copy animation from crate.msh, initialFrame    :',initialFrame)
            print('After copy animation from crate.msh, finalFrame      :',finalFrame)
            print('After copy animation from crate.msh, timeBetweenFrame:',timeBetweenFrame)
            print('After copy animation from crate.msh, typeAnimation   :',typeAnimation)
        end
    end


.. figure:: _static/mesh_debug_copyAnimationsFromMesh.png
    :align: center
    :figclass: align-center

    Possible output.

.. _setModeDraw:

meshDebug mode draw
^^^^^^^^^^^^^^^^^^^

.. data:: setModeDraw(string mode)

    Modify the mode of draw.

   :param string: **mode** could be one of option from table :ref:`modeDraw <ModeDraw>`. 

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        print('Mode draw before:', tMesh:getModeDraw())
        tMesh:setModeDraw('TRIANGLE_FAN')
        print('Mode draw after:', tMesh:getModeDraw())
    end

.. figure:: _static/mesh_debug_mode_draw.png
    :align: center
    :figclass: align-center


.. data:: getModeDraw(string mode)

    Retrieve the mode of draw.

   :return:  ``mode`` will be one of option from table :ref:`modeDraw <ModeDraw>`. 

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        print('Mode draw before:', tMesh:getModeDraw())
        tMesh:setModeDraw('TRIANGLE_FAN')
        print('Mode draw after:', tMesh:getModeDraw())
    end

.. figure:: _static/mesh_debug_mode_draw.png
    :align: center
    :figclass: align-center

    mode draw


meshDebug Cull face
^^^^^^^^^^^^^^^^^^^

.. data:: setModeCullFace(string mode)

    Modify the mode cullFace face.

   :param string: **mode** could be one of option from table :ref:`cullFace <cullFace>`. 

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        print('Mode cull face before:', tMesh:getModeCullFace())
        tMesh:setModeCullFace('BACK')
        print('Mode cull face after:', tMesh:getModeCullFace())
    end

.. data:: getModeCullFace(string mode)

    Modify the mode cullFace face.

   :return:  ``mode`` will be one of option from table :ref:`cullFace <cullFace>`. 

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        print('Mode cull face before:', tMesh:getModeCullFace())
        tMesh:setModeCullFace('BACK')
        print('Mode cull face after:', tMesh:getModeCullFace())
    end


meshDebug front face
^^^^^^^^^^^^^^^^^^^^

.. data:: setModeFrontFace(string mode)

    Modify the mode front face.

   :param string: **mode** could be one of option from table :ref:`front face <front_face_direction>`. 

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        print('Mode front face before:', tMesh:getModeFrontFace())
        tMesh:setModeFrontFace('CW')
        print('Mode front face after:', tMesh:getModeFrontFace())
    end

.. data:: getModeFrontFace(string mode)

    Retrieve the mode front face.

   :return:  ``mode`` will be one of option from table :ref:`front face <front_face_direction>`. 

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        print('Mode front face before:', tMesh:getModeFrontFace())
        tMesh:setModeFrontFace('CW')
        print('Mode front face after:', tMesh:getModeFrontFace())
    end

.. _meshDebugGetTotalFrame:

meshDebug frame
^^^^^^^^^^^^^^^

.. data:: getTotalFrame()

    Retrieve the total frame of mesh.

   :return:  ``number`` total of frame. 

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        local total_frame = tMesh:getTotalFrame()
        print('the mesh has ' , total_frame, ' frames')
    end

.. _meshDebugGetTotalSubset:


meshDebug subset
^^^^^^^^^^^^^^^^

.. data:: getTotalSubset(number frame)

    Retrieve the total subset given a frame.

   :param number: **frame** . 
   :return: ``number`` total of subset in the indicated frame. 

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then

        local total_subset = tMesh:getTotalSubset(1)
        print('the mesh has ' , total_subset, ' subset at frame 1')
    end

.. _meshDebugGetTotalVertex:

meshDebug vertex
^^^^^^^^^^^^^^^^

.. data:: addVertex(number frame, number subset)

    Add a single vertex given frame and subset.

   :param number: **frame**
   :param number: **subset**
   :return:  ``boolean`` result of operation.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    local result = tMesh:addVertex(1,1)

.. data:: addVertex(number frame, number subset,number total_vertex)

    Add vertex given frame, subset and the total of vertex.

   :param number: **frame**
   :param number: **subset**
   :param number: **total** vertex to be added
   :return:  ``boolean`` result of operation.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    local total_vertex = 5
    local result = tMesh:addVertex(1,1,total_vertex)


.. data:: addVertex(number frame, number subset,table vertex)

    Add a vertex given frame, subset and the vertex itself in a table.

   :param number: **frame**
   :param number: **subset**
   :param table: **vertex** to be added ``{x,y,z,nx,ny,nz,u,v}``
   :return:  ``boolean`` result of operation.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    local vertex = {x = 0,y = 10,z = 1,nx = 0,ny = 0 ,nz = 1,u = 0 ,v = 1}
    local result = tMesh:addVertex(1,1,vertex)

.. data:: addVertex(number frame, number subset,table vertex_array)

    Add an array of vertex given frame, subset and the vertex array in a table.

   :param number: **frame**
   :param number: **subset**
   :param table: **vertex** array to be added ``{{x,y,z,nx,ny,nz,u,v},{x,y,z,nx,ny,nz,u,v}, ...``
   :return:  ``boolean`` result of operation.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    local vertex = {{x = 0,y = 10,z = 1,nx = 0,ny = 0 ,nz = 1,u = 0 ,v = 1}, {x = 1,y = 0,z = 1,nx = 0,ny = 0 ,nz = 1,u = 0.5 ,v = 0.5}}
    local result = tMesh:addVertex(1,1,vertex)



.. data:: getTotalVertex(number frame, number subset)

    Retrieve the total vertex given frame and subset.

   :param number: **frame**
   :param number: **subset**
   :return:  ``number`` total of vertex in the indicated frame from subset.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then

        local total_vertex = tMesh:getTotalVertex(1,1)
        print('the mesh has ' , total_vertex, ' vertex in the subset (1) at frame (1)')
    end


.. data:: getVertex(number * index_frame, number * index_subset, number * index_vertex,number * max_vertex_return)

    Get one or more vertices.

   :param number: **index_frame** from 1 to max number of frame (use :ref:`getTotalFrame <meshDebugGetTotalFrame>`) (default is 1)
   :param number: **index_subset** from 1 to max number of subset (use :ref:`getTotalSubset <meshDebugGetTotalSubset>`) (default is 1)
   :param number: **index_vertex** from 1 to max number of vertex (use :ref:`getTotalVertex <meshDebugGetTotalVertex>`) (default is 1)
   :param number: **max_vertex_return** from 1 to max number of vertex (use :ref:`getTotalVertex <meshDebugGetTotalVertex>`) (default is 1)
   :return:  single ``table`` or multiples ``table`` filled with the vertices.

   The Table will have ``x,y,z`` coordinates, ``u,v`` map to texture and ``nx,ny,nz`` representing the normal.

   *Example getting a single vertex:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local vertex = tMesh:getVertex() -- will retrieve 1 because the default value (1 for total vertex, so one single table)
        print('x: ', vertex.x)
        print('y: ', vertex.y)
        print('z: ', vertex.z)
        print('u: ', vertex.u)
        print('v: ', vertex.v)
        print('nx:', vertex.nx)
        print('ny:', vertex.ny)
        print('nz:', vertex.nz)
    end

.. figure:: _static/mesh_debug_get_vertex_1.png
    :align: center
    :figclass: align-center

    Possible output


*Example getting a specific single vertex:*

.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local index_frame = 1
        local index_subset = 1
        local index_vertex = 10 --our specific vertex

        local vertex = tMesh:getVertex(index_frame,index_subset,index_vertex)
        print('x: ', vertex.x)
        print('y: ', vertex.y)
        print('z: ', vertex.z)
        print('u: ', vertex.u)
        print('v: ', vertex.v)
        print('nx:', vertex.nx)
        print('ny:', vertex.ny)
        print('nz:', vertex.nz)
    end


.. figure:: _static/mesh_debug_get_vertex_2.png
    :align: center
    :figclass: align-center

    Possible output

*Example getting a designed amount of vertex:*

.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local index_frame = 1
        local index_subset = 1
        local index_vertex = 10 --our specific vertex
        local total_vertex = 3 -- we want 3 vertices
        --(you can get them all , just pass a number very high, like 9E9)

        local vertices = tMesh:getVertex(index_frame,index_subset,index_vertex,total_vertex)
        for i=1, #vertices do
            local vertex = vertices[1]
            print('\nVertex number:',i )
            print('x: ', vertex.x)
            print('y: ', vertex.y)
            print('z: ', vertex.z)
            print('u: ', vertex.u)
            print('v: ', vertex.v)
            print('nx:', vertex.nx)
            print('ny:', vertex.ny)
            print('nz:', vertex.nz)
        end
    end


.. figure:: _static/mesh_debug_get_vertex_3.png
    :align: center
    :figclass: align-center

    Possible output


.. data:: setVertex(number index_frame, number index_subset, number index_vertex,number max_vertex_return,table vertex)

    Set one or more vertices.

   :param number: **index_frame** from 1 to max number of frame (use :ref:`getTotalFrame <meshDebugGetTotalFrame>`)
   :param number: **index_subset** from 1 to max number of subset (use :ref:`getTotalSubset <meshDebugGetTotalSubset>`)
   :param number: **index_vertex** from 1 to max number of vertex (use :ref:`getTotalVertex <meshDebugGetTotalVertex>`)
   :param number: **max_vertex_return** from 1 to max number of vertex (use :ref:`getTotalVertex <meshDebugGetTotalVertex>`)
   :param table: **vertex** to be set. the table can be unique or array.


   The Table could have ``x,y,z`` coordinates, ``u,v`` map to texture and ``nx,ny,nz`` representing the normal.
   Values not set will not be changed.

   *Example setting a single vertex:*
.. code-block:: lua

    function printVertex(vertex)
        print('\n')
        print('x: ', vertex.x)
        print('y: ', vertex.y)
        print('z: ', vertex.z)
        print('u: ', vertex.u)
        print('v: ', vertex.v)
        print('nx:', vertex.nx)
        print('ny:', vertex.ny)
        print('nz:', vertex.nz)
    end

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local index_frame = 1
        local index_subset = 1
        local index_vertex = 10 --our specific vertex

        local vertex = tMesh:getVertex(index_frame,index_subset,index_vertex)

        printVertex(vertex)

        vertex.x = 0
        vertex.y = 1
        vertex.z = 2
        vertex.nx = 10
        vertex.ny = 11
        vertex.nz = 12
        vertex.u = nil
        vertex.v = nil
        --we will not modify u and v

        tMesh:setVertex(index_frame,index_subset,index_vertex,vertex)

        vertex = tMesh:getVertex(index_frame,index_subset,index_vertex)
        printVertex(vertex)

    end

.. figure:: _static/mesh_set_vertex_1.png
    :align: center
    :figclass: align-center

    Possible output

*Example setting two vertices:*

.. code-block:: lua

    function printVertex(vertex)
        print('\n')
        print('x: ', vertex.x)
        print('y: ', vertex.y)
        print('z: ', vertex.z)
        print('u: ', vertex.u)
        print('v: ', vertex.v)
        print('nx:', vertex.nx)
        print('ny:', vertex.ny)
        print('nz:', vertex.nz)
    end

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local index_frame = 1
        local index_subset = 1
        local index_vertex = 10 --our specific vertex

        local vertex = tMesh:getVertex(index_frame,index_subset,index_vertex)

        printVertex(vertex)

        vertex.x = 3
        vertex.y = 2
        vertex.z = 1
        vertex.nx = 1.10
        vertex.ny = 1.11
        vertex.nz = 1.12
        vertex.u = nil
        vertex.v = nil
        --we will not modify u and v
        tVertices = {[1] = vertex, [2] = vertex}--we instruct to change itself and the next one


        tMesh:setVertex(index_frame,index_subset,index_vertex,tVertices)

        vertex = tMesh:getVertex(index_frame,index_subset,index_vertex + 1)-- we will get the next vertex

        printVertex(vertex)

    end

.. figure:: _static/mesh_set_vertex_2.png
    :align: center
    :figclass: align-center

    Possible output


meshDebug index buffer
^^^^^^^^^^^^^^^^^^^^^^

.. data:: getTotalIndex(number frame, number subset)

    Retrieve the total index buffer given frame and subset.

   :param number: **frame**
   :param number: **subset**
   :return:  ``number`` total of index buffer in the indicated frame from subset.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then

        local index_buffer = tMesh:getTotalIndex(1,1)
        print('the mesh has ' , index_buffer, ' index buffer in the subset (1) at frame (1)')
    end

.. data:: isIndexBuffer()

    Verify if is indexed buffer.

   :return:  ``boolean`` result indicating if is indexed buffer.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then

        local bIndexed = tMesh:isIndexBuffer()
        print('the mesh is indexed buffer:' , tostring(bIndexed))
    end


.. data:: getIndex(number frame, number subset)

    Retrieve a index buffer given frame and subset.

   :param number: **frame**
   :param number: **subset**
   :return:  ``table`` result of operation.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local index_frame = 1
        local index_subset = 1

        local indexBuffer = tMesh:getIndex(index_frame,index_subset)
        if indexBuffer then --has index buffer
            for i = 1, #indexBuffer do
                print(indexBuffer[i])
            end
        else
            print('Object hs no index buffer!')
        end
    end

.. figure:: _static/mesh_debug_get_index_buffer.png
   :align: center
   :figclass: align-center

   Dummy index buffer

.. data:: addIndex(number frame, number subset,table index_array)

    Add index buffer given frame and subset.

   :param number: **frame**
   :param number: **subset**
   :param table: **index** array (one based)
   :return:  ``boolean`` result of operation.


*Follow an example how to add index buffer and how to save a mesh to a binary  file:*

    .. literalinclude:: example_modules/saving_mesh_to_binary_file.lua
        :language: lua
        :linenos:


   :download:`download <example_modules/saving_mesh_to_binary_file.lua>`.

.. figure:: _static/mesh_flat.gif
   :align: center
   :figclass: align-center

   Adding index buffer


meshDebug texture
^^^^^^^^^^^^^^^^^

.. data:: getTexture(number frame, number subset)

   Get the current texture name given frame and subset.

   :param number: **frame**
   :param number: **subset**
   :return: ``string`` - ``file name`` from texture.


   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local index_frame = 1
        local index_subset = 1

        local texture = tMesh:getTexture(index_frame,index_subset)
        if texture then --has texture
            print('texture:',texture)
        else
            print('Object has no texture!')
        end
    end


.. figure:: _static/mesh_debug_texture_get.png
   :align: center
   :figclass: align-center

   Get the texture name from meshDebug


.. data:: setTexture(number frame, number subset,string texture)

   Set a new texture given frame, subset and texture name.

   :param number: **frame**
   :param number: **subset**
   :param string: **texture** name or ``nil`` to unset.
   :return: ``boolean`` - ``result`` from meshDebug.


   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local index_frame = 1
        local index_subset = 1
        local texture = 'mike.png'

        print('texture name current:',tMesh:getTexture(index_frame,index_subset))
        local bSuccess = tMesh:setTexture(index_frame,index_subset,texture)
        if bSuccess then
            print('texture changed successfully:',tMesh:getTexture(index_frame,index_subset))
        else
            print('Failed to set the new texture!')
        end
    end

.. figure:: _static/mesh_debug_texture_set.png
   :align: center
   :figclass: align-center

   Setting texture to meshDebug

.. Note::
    Note that meshDebug will not check if the texture exists or not!


meshDebug stride
^^^^^^^^^^^^^^^^

.. data:: setStride(number stride, number * frame)

   Set a specific stride to specific frame or all frames.

   :param number: **stride**  *2*  ``x, y``  or *3* ``x, y, z``
   :param number: **frame** *0* for all frames or specific frame one based index.


   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local stride = 3 -- x,y,z
        local frame = 0
        tMesh:setStride(stride,frame)
    end

meshDebug normal
^^^^^^^^^^^^^^^^

.. data:: enableNormal(boolean enable)

   Enable or disable normal to be saved in the mesh

   :param boolean: **value**  ``true`` or ``false``


   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        tMesh:enableNormal(true)
    end


meshDebug UV
^^^^^^^^^^^^

.. data:: enableUv(boolean enable)

   Enable or disable uv to be saved in the mesh

   :param boolean: **value**  ``true`` or ``false``


   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        tMesh:enableUv(true)
    end

meshDebug Physics
^^^^^^^^^^^^^^^^^

.. data:: getPhysics()

   Return an array of physics.

   :return: ``table`` - Physics.


   *Example:*
.. code-block:: lua

    function printTable(space, tTbale)
        for k, v in pairs(tTbale) do
            if k == 'type' then
                -- Do nothing ... we already have printed type
            elseif type(v) == 'table' then
                if string.len(space) > 0 then
                    print(space,k,v)
                else
                    print(k,v)
                end
                printTable(space .. '\t', v)
            else
                if string.len(space) > 0 then
                    print(space, k, v)
                else
                    print(k, v)
                end
            end
        end
    end

    tMesh = meshDebug:new()   
    if tMesh:load('Mech.msh') then
        local tPhysics = tMesh:getPhysics()
        for i =1 , #tPhysics do
            local tPhysic = tPhysics[i]
            print('physic index[' .. tostring(i) .. '] type:',tPhysic.type)
            printTable('', tPhysic)
        end
    end

.. figure:: _static/mesh_debug_physics_get.png
   :align: center
   :figclass: align-center

   Possible output for get physics

.. _meshDebug_setPhysics:

.. data:: setPhysics(table tPhysics)

   Set physics from a table.

   :param table: of physics.


   The following table describe how should be the table of physics:

    +---------------+----------------+----------------------------+
    | | type /      | | sub table    | | values                   |
    | | name        | | information  | | information              |
    +===============+================+============================+
    |  ``cube``     | | ``center``   | | ``x, y, z``              |
    |               | | ``dim``      | | ``x, y, z``              |
    +---------------+----------------+----------------------------+
    |               | | ``a``        | | ``x, y, z``              |
    |  ``triangle`` | | ``b``        | | ``x, y, z``              |
    |               | | ``c``        | | ``x, y, z``              |
    +---------------+----------------+----------------------------+
    |               | | ``center``   | | ``x, y, z``              |
    |  ``sphere``   | | ``ray``      | |                          |
    +---------------+----------------+----------------------------+
    |               | | ``a``        | | ``x, y, z``              |
    |               | | ``b``        | | ``x, y, z``              |
    |               | | ``c``        | | ``x, y, z``              |
    |  ``complex``  | | ``d``        | | ``x, y, z``              |
    |               | | ``e``        | | ``x, y, z``              |
    |               | | ``f``        | | ``x, y, z``              |
    |               | | ``g``        | | ``x, y, z``              |
    |               | | ``h``        | | ``x, y, z``              |
    +---------------+----------------+----------------------------+


   *Example setting complex table physic:*

.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local tComplex = { type   = 'complex',
                        a = {x = 1, y =  1, z = 1},
                        b = {x = 2, y =  2, z = 2},
                        c = {x = 3, y =  3, z = 3},
                        d = {x = 4, y =  4, z = 4},
                        e = {x = 5, y =  5, z = 5},
                        f = {x = 6, y =  6, z = 6},
                        g = {x = 7, y =  7, z = 7},
                        h = {x = 8, y =  8, z = 8}}

        tMesh:setPhysics(tComplex)

        local tPhysics = tMesh:getPhysics()
        local tPhysic = tPhysics[1]
        print(tPhysic.type)
        print('a',tPhysic.a.x,tPhysic.a.y,tPhysic.a.z)
        print('b',tPhysic.b.x,tPhysic.b.y,tPhysic.b.z)
        print('c',tPhysic.c.x,tPhysic.c.y,tPhysic.c.z)
        print('d',tPhysic.d.x,tPhysic.d.y,tPhysic.d.z)
        print('e',tPhysic.e.x,tPhysic.e.y,tPhysic.e.z)
        print('f',tPhysic.f.x,tPhysic.f.y,tPhysic.f.z)
        print('g',tPhysic.g.x,tPhysic.g.y,tPhysic.g.z)
        print('h',tPhysic.h.x,tPhysic.h.y,tPhysic.h.z)
    end

.. figure:: _static/mesh_debug_physics_set_single.png
   :align: center
   :figclass: align-center

   Output for complex table physic

*Example setting multiple table physics:*

.. code-block:: lua

    function createTableCubePhysics()
        local tCube = { type   = 'cube',
                        center = {x = 0.1, y = 0.2, z = 0.3},
                        half   = {x = 5.0, y = 6.0, z = 7.0}}
        return tCube
    end

    function createTableTrianglePhysics()
        local tTriangle = { type   = 'triangle',
                        a = {x = -1, y = -1, z = 0.1},
                        b = {x = 1,  y = 1,  z = 0.1},
                        c = {x = 1,  y = -1, z = 0.1}}
        return tTriangle
    end

    function createTableSpherePhysics()
        local tSphere = { type   = 'sphere',
                        center = {x = -0.3, y = -0.1, z = 0.1},
                        ray = 5.56}
        return tSphere
    end

    function createTableComplexPhysics()
        local tComplex = { type   = 'complex',
                        a = {x = 1, y =  1, z = 1},
                        b = {x = 2, y =  2, z = 2},
                        c = {x = 3, y =  3, z = 3},
                        d = {x = 4, y =  4, z = 4},
                        e = {x = 5, y =  5, z = 5},
                        f = {x = 6, y =  6, z = 6},
                        g = {x = 7, y =  7, z = 7},
                        h = {x = 8, y =  8, z = 8}}
        return tComplex
    end


    function printPhysics(tMesh)
        local tPhysics = tMesh:getPhysics()
        print('Total physics:',#tPhysics)
        for i=1, #tPhysics do
            local tPhysic = tPhysics[i]
            print('\n')
            print('type:',tPhysic.type)
            if tPhysic.type == 'cube' then
                print('center',tPhysic.center.x,tPhysic.center.y,tPhysic.center.z)
                print('half',tPhysic.half.x,tPhysic.half.y,tPhysic.half.z)
            elseif tPhysic.type == 'triangle' then
                print('a',tPhysic.a.x,tPhysic.a.y,tPhysic.a.z)
                print('b',tPhysic.b.x,tPhysic.b.y,tPhysic.b.z)
                print('c',tPhysic.c.x,tPhysic.c.y,tPhysic.c.z)
            elseif tPhysic.type == 'sphere' then
                print('center',tPhysic.center.x,tPhysic.center.y,tPhysic.center.z)
                print('ray',tPhysic.ray)
            elseif tPhysic.type == 'complex' then
                print('a',tPhysic.a.x,tPhysic.a.y,tPhysic.a.z)
                print('b',tPhysic.b.x,tPhysic.b.y,tPhysic.b.z)
                print('c',tPhysic.c.x,tPhysic.c.y,tPhysic.c.z)
                print('d',tPhysic.d.x,tPhysic.d.y,tPhysic.d.z)
                print('e',tPhysic.e.x,tPhysic.e.y,tPhysic.e.z)
                print('f',tPhysic.f.x,tPhysic.f.y,tPhysic.f.z)
                print('g',tPhysic.g.x,tPhysic.g.y,tPhysic.g.z)
                print('h',tPhysic.h.x,tPhysic.h.y,tPhysic.h.z)
            else
                print('ERR','type physics unknown')
            end
        end
    end

    tMesh = meshDebug:new()
    if tMesh:load('Mech.msh') then
        local tCube     = createTableCubePhysics()
        local tTriangle = createTableTrianglePhysics()
        local tSphere   = createTableSpherePhysics()
        local tComplex  = createTableComplexPhysics()

        local tPhysics = { [1] = tSphere, [2] = tCube, [3] = tComplex, [4] = tTriangle }

        tMesh:setPhysics(tPhysics)
        printPhysics(tMesh)
    end


.. figure:: _static/mesh_debug_physics_set_multiple.png
   :align: center
   :figclass: align-center

   Output for multiples physic


meshDebug centralize
^^^^^^^^^^^^^^^^^^^^

.. data:: centralize(number * frame, number * subset)

    Centralize a specific frame/subset or all of them (do not supply the arguments)

   :param number: **frame**  from 1 to total of frames.
   :param number: **subset**  from 1 to total of subset.

   *Example:*
.. code-block:: lua

    tMesh = meshDebug:new()
    if tMesh:load('crate.msh') then
        tMesh:centralize() -- centralize all of them
    end
