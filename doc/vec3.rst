.. contents:: Table of Contents

Table vec3
==========
.. _vecthree:

Table :ref:`vec3 <vecthree>` is a vector 3d (x,y,z) class for the most known operations using vector.

vec3 methods
------------

vec3 new
^^^^^^^^

.. data:: new()

   Create a new instance of :ref:`vec3 <vecthree>` with no argument.

   :return: :ref:`vec3 <vecthree>` *table*.

   *Example:*
.. code-block:: lua

   local v = vec3:new()
   print(v.x,v.y,v.z) -- 0,0,0



.. data:: new(number x, number * y, number * z)

    Create a new instance of :ref:`vec3 <vecthree>` passing three arguments.

   :param number: **x** value.
   :param number: **y** value (optional).
   :param number: **z** value (optional).
   :return: :ref:`vec3 <vecthree>` *table*.

   *Example:*
.. code-block:: lua

   local v = vec3:new(15,66,88)
   print(v.x,v.y,v.z) -- 15,66,88

   v = vec3:new(15)
   print(v.x,v.y,v.z) -- 15,0,0

.. data:: new(vec3 other)

    Create a new instance of :ref:`vec3 <vecthree>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to copy values (x, y and z).
   :return: :ref:`vec3 <vecthree>` *table*.

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(1,2,3)
   print(v1.x,v1.y,v1.z) -- 1,2,3

   local v2 = vec3:new(v1)
   print(v2.x,v2.y,v2.z) -- 1,2,3


.. data:: new(renderizable render)

    Create a new instance of :ref:`vec3 <vecthree>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to copy values from position.
   :return: :ref:`vec3 <vecthree>` *table*.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('3D')
   tTexture:setPos(88,99,101)

   local v2 = vec3:new(tTexture)
   print(v2.x,v2.y,v2.z) -- 88,99,101

vec3 get
^^^^^^^^

.. _vecthreeget:
.. data:: get()

    Retrieve the values x, y and z from :ref:`vec3 <vecthree>`.

   :return: :literal:`number` *x*, :literal:`number` *y*, :literal:`number` *z*

   *Example:*
.. code-block:: lua

   local v = vec3:new(415,669,222)
   local x,y,z = v:get()
   print(x,y) -- 415,669,222

vec3 set
^^^^^^^^

.. data:: set(number x, number y, number z)

    Set new values to :ref:`vec3 <vecthree>` passing three number arguments (x, y and z).

   :param number: **x** value.
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua

   local v = vec3:new(15,66,33)
   v:set(88,74,55)
   print(v:get()) -- 88,74,55

   v:set(15) -- Note only x change
   print(v:get()) -- 15,74,55

.. data:: set(vec3 other)

    Set new values to :ref:`vec3 <vecthree>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to set values.

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(15,66,99)
   local v2 = vec3:new()
   print(v1) -- x:15 y:66 z:99
   print(v2) -- x:0 y:0 z:0

   v2:set(v1)
   print(v2) -- x:15 y:66 z:99


.. data:: set(renderizable render)

    Set new values to :ref:`vec3 <vecthree>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to set values from position.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('3D')
   tTexture:setPos(12,15,75)

   local v2 = vec3:new()
   v2:set(tTexture)
   print(v2) --x:12 y:15 z:75

vec3 add
^^^^^^^^

.. data:: add(number x, number y, number z)

    Add values to :ref:`vec3 <vecthree>` passing three number arguments (x, y and z).

   :param number: **x** value.
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua

   local v = vec3:new(15,66,33)
   v:add(88,74,2)
   print(v:get()) -- 103,140, 35

   v:add(15) -- Note.: 103 + 15, 140 + 15 and 35 + 15
   print(v:get()) -- 118,155,50


.. data:: add(vec3 other)

    Add values to :ref:`vec3 <vecthree>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to add values.

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(15,66,12)
   local v2 = vec3:new(1,2,3)
   print(v1) -- x:15 y:66 z:12
   print(v2) -- x:1 y:2 z:3

   v2:add(v1)
   print(v2) -- x:16 y:68 z:15


.. data:: add(renderizable render)

    Add values to :ref:`vec3 <vecthree>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to add values from position.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('3D')
   tTexture:setPos(12,15,22)

   local v2 = vec3:new(5,5,5)
   v2:add(tTexture)
   print(v2) --x:17 y:20 z:27


vec3 sub
^^^^^^^^

.. data:: sub(number x, number y, number z)

    Subtract values in the :ref:`vec3 <vecthree>` passing three number arguments (x, y and z).

   :param number: **x** value.
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua

   local v = vec3:new(150,660,1)
   v:sub(88,74,1)
   print(v:get()) -- 62 586 0

   v:sub(15) --note.: 62 - 15, 586 - 15, 0 - 15
   print(v:get()) -- 47 571 -15

.. data:: sub(vec3 other)

    Subtract values in the :ref:`vec3 <vecthree>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to subtract values.

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(15,66,99)
   local v2 = vec3:new(1,2,3)
   print(v1) -- x:15 y:66 z:99
   print(v2) -- x:1 y:2 z:3

   v2:sub(v1)
   print(v2) -- x:-14 y:-64 z:-96


.. data:: sub(renderizable render)

    Subtract values in the :ref:`vec3 <vecthree>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to subtract values from position.

   *Example:*
.. code-block:: lua

   local spt =  sprite:new('3D')
   spt:setPos(12,15,10)

   local v2 = vec3:new(5,5,5)
   v2:sub(spt)
   print(v2) --   x:-7 y:-10 z:-5


vec3 mul
^^^^^^^^

.. data:: mul(number x, number y, number z)

    Multiply (scalar) values to :ref:`vec3 <vecthree>` passing three number arguments (x, y and z).

   :param number: **x** value.
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua

   local v = vec3:new(2,3,1.5)
   v:mul(5,2,3)
   print(v:get()) -- 10 6 4.5

   v:mul(2) -- Note.: 10 * 2, 6 * 2, 4.5 * 2
   print(v:get()) -- 20 12 9

.. data:: mul(vec3 other)

    Multiply (scalar) values to :ref:`vec3 <vecthree>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to multiply values.

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(2,3,1)
   local v2 = vec3:new(3,4,2)
   print(v1) -- x:2 y:3 z:1
   print(v2) -- x:3 y:4 z:2

   v2:mul(v1)
   print(v2) -- 6 12 2


.. data:: mul(renderizable render)

    Multiply (scalar) values by :ref:`vec3 <vecthree>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to multiply values by position.

   *Example:*
.. code-block:: lua

   local spt =  sprite:new('3d')
   spt:setPos(2.2,3.3,4.4)

   local v2 = vec3:new(3,4,2)
   v2:mul(spt)
   print(v2) --   x:6.6 y:13.2 z:8.8

vec3 div
^^^^^^^^

.. Note:: Any number divided by zero is an error and the engine set the result to 0.

.. data:: div(number x,number y)

    Divide values by :ref:`vec3 <vecthree>` passing three number arguments (x, y and z).

   :param number: **x** value.
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*

.. code-block:: lua

   local v = vec3:new(2,3,4)
   v:div(2,1.5,0.5)
   print(v:get()) -- 1 2 8

   v:div(2) -- Note.: 1 / 2, 2 / 2 and 8 / 2
   print(v:get()) -- 0.5    1    4

   v:div(0) -- Note.: 0.5 / 0, 1 / 0 and 4 / 0 not throw an exception and the result is set to 0
   print(v) -- x:0 y:0 z:0

.. data:: div(vec3 other)

    Divide values by :ref:`vec3 <vecthree>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to divide values.

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(20,30,40)
   local v2 = vec3:new(2,4,5)
   print(v1) -- x:20 y:30 z:40
   print(v2) -- x:2 y:4 z:5

   v1:div(v2)
   print(v1) -- x:10 y:7.5 z:8

   v2.y = 0
   v1:div(v2)
   print(v1) -- x:5 y:0 z:1.6

   v1:set(10,20,30)
   v2:set(0,10,20)
   v1:div(v2)-- Note.: 10 / 0 not throw an exception however the result is set to 0
   print(v1) -- x:0 y:2 z:1.5


.. data:: div(renderizable render)

    Divide values by :ref:`vec3 <vecthree>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to divide values by position.

   *Example:*
.. code-block:: lua

   local spt =  sprite:new('3d')
   spt:setPos(2,50,75)

   local v2 = vec3:new(200,300,400)
   v2:div(spt)
   print(v2) --   x:100 y:6 z:5.333333


.. _vecthreelength:

vec3 length
^^^^^^^^^^^
 The formula to :ref:`length <vecthreelength>` is:

 :math:`\sqrt{(x * x) + (y * y) + (z * z)}`

.. data:: length()

   Get the length from :ref:`vec3 <vecthree>`. Same as :ref:`len <vecthreelen>` attribute.

   :return: :literal:`number` *length* of vector.

   *Example:*
.. code-block:: lua

   local v = vec3:new(100,300,500)
   print(v:length()) -- 591.608


.. _vecthreemove:

vec3 move
^^^^^^^^^
.. _vec2move:
.. data:: move(number x,number * y, number * z)

   Move x, y and z units frames by seconds. This method consider the FPS.

   :param number: **x** position.
   :param number: **y** position (optional).
   :param number: **z** position (optional).

   *Example:*
.. code-block:: lua

   local v = vec3:new(5,55,33)
   v:move(100,50,40)

   --this is equivalent
   function loop(delta)
        v.x = v.x + (delta * 100)
        v.y = v.y + (delta * 50)
        v.z = v.z + (delta * 40)
   end


.. _vecthreedot:

vec3 dot
^^^^^^^^
 |  A dot product is an algebraic operation that takes two equal-length sequences of numbers and returns a single number.
 |  Learn more at `wiki dot product <https://en.wikipedia.org/wiki/Dot_product>`__  .
 |  The formula to :ref:`vec3 dot <vecthreedot>` is:

 :math:`(x * x) + (y * y) + (z * z)`

.. data:: dot(vec3 b)

   Calculate a dot product value to :ref:`vec3 <vecthree>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **parameter**.
   :return: :literal:`number` *dot* calculated.

   *Example:*
.. code-block:: lua

    a = vec3:new(3,4,5)          --x = 3      y = 4    z = 5
    b = vec3:new(10,25,15)       --x = 10     y = 25   z = 15
    local dot_result = a:dot(b)  --3 * 10 + 4 * 25  + 5 * 15 = 205

    print(dot_result)            -- 205


.. data:: dot(vec3 a, vec3 b)

    Calculate a dot product value to :ref:`vec3 <vecthree>` between a and b :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **a** a parameter.
   :param vec3: **b** b parameter.
   :return: :literal:`number` *dot* calculated.

   *Example:*
.. code-block:: lua

    a = vec3:new(3,4,5)              --x = 3      y = 4    z = 5
    b = vec3:new(10,25,15)           --x = 10     y = 25   z = 15
    c = vec3:new(1,2,3)              --x = 1      y = 2    z = 3
    local dot_result = c:dot(a,b)    --3 * 10 + 4 * 25  + 5 * 15 = 205

    print(dot_result)                -- 205

.. _vecthreelerp:

vec3 lerp
^^^^^^^^^
 |  Linear interpolation is a method of curve fitting using linear polynomials to construct new data points within the range of a discrete set of known data points.
 |  Learn more at `wiki linear interpolation <https://en.wikipedia.org/wiki/Linear_interpolation>`__  .
 |  The formula to :ref:`vec3 lerp <vecthreelerp>` is:
 |
 | :math:`v = (1 - s) * (v1) + s * (v2)`


.. data:: lerp(vec3 a, vec3 b, number s)

    Calculate a linear interpolation between two :ref:`vec3 <vecthree>`. Store the result on itself.

   :param vec3:  **a** a parameter.
   :param vec3:  **b** b parameter.
   :param number: **s** parameter.

   *Example:*
.. code-block:: lua

    local a = vec3:new(20,30,40)
    local b = vec3:new(10,15,20)
    local c = vec3:new()       --x = 0     y = 0    z = 0
    c:lerp(a,b,0.5)            --Keep the result on 'c' vector
    print(c)                   --x:15 y:22.5 z:30

.. _vecthreenormalize:

vec3 normalize
^^^^^^^^^^^^^^

 |  A normal vector is a vector that is perpendicular to a given object.
 |  Learn more at `wiki Normal (geometry) <https://en.wikipedia.org/wiki/Normal_(geometry)#Calculating_a_surface_normal>`__  and `wiki Unit vector (geometry) <https://en.wikipedia.org/wiki/Unit_vector>`__.
 |  The formula to :ref:`vec3 normal <vecthreenormalize>` is:
 |
 | :math:`\Hat{U} = \frac{U}{|U|}`
 |
 |  where :literal:`|u|` is the length of :literal:`u`.


.. data:: normalize()

    Normalize a :ref:`vec3 <vecthree>`. Store the result on itself.


   *Example:*
.. code-block:: lua

    local a = vec3:new(500,100,200)
    a:normalize()
    print(a)   -- x:0.9128709 y:0.1825742 z:0.3651484


.. data:: normalize(vec3 out)

    Normalize a :ref:`vec3 <vecthree>` and store the result in the :ref:`vec3 <vecthree>` out.

   :param vec3:  **out** as parameter.


   *Example:*
.. code-block:: lua

    local a = vec3:new(500,100,200)
    local b = vec3:new()
    a:normalize(b)
    print(a)   -- x:500 y:100 z:200
    print(b)   -- x:0.9128709 y:0.1825742 z:0.3651484

.. _vecthreecross:

vec3 cross
^^^^^^^^^^

 A cross vector between :literal:`a` and :literal:`b` is  the cross product :literal:`a` x :literal:`b`
 vector that is perpendicular to both and thus normal to the plane containing them.
 Learn more at `wiki Cross product <https://en.wikipedia.org/wiki/Cross_product>`__.

 |  The formula to :ref:`vec3 cross <vecthreecross>` is:
 |
 | :math:`a \mathsf{x} b = |a||b|\sin\theta n`

 Where :literal:`θ` is the angle between :literal:`a` and :literal:`b` in the plane containing them (hence, it is between **0°** and **180°**), :literal:`|a|` and :literal:`|b|` are the magnitudes
 of vectors :literal:`a` and :literal:`b`, and :literal:`n` is a unit vector perpendicular to the plane containing :literal:`a` and :literal:`b` in the direction given by the *right-hand* rule.
 If the vectors :literal:`a` and :literal:`b` are parallel (i.e., the angle :literal:`θ` between them is either **0°** or **180°**),
 by the above formula, the cross product of :literal:`a` and :literal:`b` is a zero vector.


.. data:: cross(vec3 a, vec3 b)

   :param vec3:  **a** as parameter.
   :param vec3:  **b** as parameter.

   *Example:*
.. code-block:: lua

    local a = vec3:new(1,1,0)
    local b = vec3:new(0,1,1)
    local c = vec3:new()
    c:cross(a,b)
    print(c)   -- x:1 y:-1 z:1



vec3 attributes
---------------

.. data:: x, y, z [read / write]

   Access the position :literal:`x` , :literal:`y` and :literal:`z` member from :ref:`vec3 <vecthree>`.

   *Example:*
.. code-block:: lua

   local v = vec3:new()
   v.x = 100
   v.y = 200
   v.z = 300

   print(v.x,v.y,v.z) -- 100 200 300


.. _vecthreelen:

.. data:: len [ read only ]

   | Call :literal:`len` method as member from :ref:`vec3 <vecthree>` for read only.
   | Same as :ref:`length <vecthreelength>`

   *Example:*
.. code-block:: lua

   local v = vec3:new()
   v.x = 100
   v.y = 200
   v.z = 300

   print(v.len) -- 374.1657


User data acknowledgment
------------------------

   This table uses the first index to store the userdata :guilabel:`C++ class` internally, and
   is not allowed for :ref:`vec3 <vecthree>` to write / read to index.


.. code-block:: lua
   :linenos:
   :caption: **Do not do this:**
   :emphasize-lines: 4,6

   v = vec3:new()
   v:set(10,10,10)

   print(v[1]) -- read, okay will print userdata: 0x44958588a6a8 for example

   v[1] = 5 -- Error, not allowed for this table. program will crash!