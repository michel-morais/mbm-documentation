.. contents:: Table of Contents

Mbm Engine
==========
**MBM** is a given name to a graphics engine which provides basic and essential resources to develop games or graphics applications 2D or 3D.

For now, is possible to develop games for :guilabel:`Windows` , :guilabel:`Linux` , and :guilabel:`Android` .

The intention is to provide the minimum necessary for game development or graphics application using minimal dependencies.

The engine is written in `C++11 <https://en.wikipedia.org/wiki/C%2B%2B11>`__ language using `OpenGL-ES <https://www.khronos.org/opengles>`__ on its core and `LUA <https://www.lua.org>`__ as a scripting language.

.. note:: Although there is an effort to migrate the whole project to `C++11 <https://en.wikipedia.org/wiki/C%2B%2B11>`__ still there is a lot of mixed `C <https://en.wikipedia.org/wiki/C_(programming_language>`__ code and maybe is not the best project to be as an example!


Another Game Engine!
--------------------

.. figure:: _static/emoji.png
   :align: left

Yes I know!

This is a personal project that I have been working for a long time (~ 2004).

It started as a way to understand :guilabel:`C++`, :guilabel:`DirectX`, :guilabel:`2D`, :guilabel:`3D`, :guilabel:`geometry` etc  (at the time). How games in general works.

Then, I have learned more about game programming and, in the beginning, it was used the concept of :guilabel:`framework` a lot, which later, became a :guilabel:`game engine` embeding :guilabel:`LUA` script.

Of course, I had (and still have) to do a lot of improvement/refactoring and also make **new features** as well! (You might find some comments or variable names in **portuguese BR** since at the time I used to program in my on language ¯\\_(ツ)\_/¯) 

But life is the greatest teacher and I have decided to go ahead and make **new features**, **improvements** or **refactoring** according to the demands.

It means that when some work in as a new game requires some **feature** or **improvements**  it is implemented.

So, if you want to use it, go ahead and occasionally **new features** or **improvements** will come (¬‿¬).

Which version should I get?
---------------------------

I occasionally tag versions however it is generally safe and recommended to sync to the master.

The library is fairly stable for :guilabel:`2D` applications and issues tend to be fixed as fast as reported/faced.


Looking for binary?
-------------------

Here the latest binaries for :ref:`Windows <binary_for_windows>`.

Here the latest binaries for :ref:`Android <binary_for_android>`.

For :ref:`Linux <build_for_linux>` you must compile by yourself.

.. _lib_versions:

Main Libraries And Status
-------------------------

+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| Library                                                                |  Version   | Windows      | Linux         | Android      | Required    | Status      |
+========================================================================+============+==============+===============+==============+=============+=============+
| `OpenGL ES <https://www.khronos.org/opengles>`__                       |    2.0     | YES          | YES           | YES          | YES         | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `ANGLE <https://github.com/google/angle>`__                            |    2.0     | YES          | NO            | NO           | Windows     | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `LUA <https://www.lua.org/manual/5.2>`__                               |    5.4.1   | YES          | YES           | YES          | NO          | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `LodePNG <https://lodev.org/lodepng/>`__                               |  20151024  | YES          | YES           | YES          | YES         | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `Box 2D physics <http://box2d.org>`__                                  |   2.3.1    | YES          | YES           | YES          | NO          | Stable      |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| | `Liquidfun <https://github.com/google/liquidfun>`__                  | | 1.1.0    | YES          | YES           | YES          | NO          | Stable      |
| | `Box 2D physics <http://box2d.org>`__                                | | 2.3.0    |              |               |              |             |             |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `Bullet 3D physics <http://bulletphysics.org/wordpress>`__             |   2.84     | YES          | YES           | YES          | NO          | in Progress |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `LuaSQLite 3 <http://lua.sqlite.org/index.cgi/index>`__                |   0.9.5    | YES          | YES           | YES          | NO          | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `SQLite 3 <https://www.sqlite.org/index.html>`__                       |   3.24     | YES          | YES           | YES          | NO          | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `AESCrypt <https://www.aescrypt.com>`__                                |    3.0.9   | YES          | YES           | YES          | NO          | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `Audiere <http://audiere.sourceforge.net>`__                           |  1.9.4     | YES          | NO            | NO           | Windows     | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `PortAudio <http://www.portaudio.com/>`__                              |  19        | NO           | YES           | NO           | Linux       | in Progress |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `PlusWindows <https://bitbucket.org/Michel-braz-morais/pluswindows>`__ |1.0         | YES          | NO            | NO           | Windows     | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `STB True Type <https://github.com/nothings/stb>`__                    |  1.11      | YES          | YES           | YES          | YES         | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `MiniZ <https://github.com/richgel999/miniz>`__                        |  1.16      | YES          | YES           | YES          | YES         | Complete    |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `ImGui <https://github.com/ocornut/imgui>`__                           |  1.75      | YES          | YES           | NO           | NO          | Stable      |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+
| `Audio`                                                                |    -       | ``Audiere``  | ``PortAudio`` | ``Native``   | NO          | in Progress |
+------------------------------------------------------------------------+------------+--------------+---------------+--------------+-------------+-------------+


:OpenGL ES: OpenGL ES is a royalty-free, cross-platform API for rendering advanced 2D and 3D graphics on embedded and mobile systems - including consoles, phones, appliances and vehicles. It consists of a well-defined subset of desktop OpenGL suitable for low-power devices, and provides a flexible and powerful interface between software and graphics acceleration hardware.

:Angle: Almost Native Graphics Layer Engine. The goal of ANGLE is to allow users of multiple operating systems to seamlessly run OpenGL ES content by translating
        OpenGL ES API calls to one of the hardware-supported APIs available for that platform.

:LUA: LUA is a powerful, efficient, lightweight, embeddable scripting language. It supports procedural programming, object-oriented programming,
      functional programming, data-driven programming, and data description. Although the main feature of this engine is to have LUA script as main way to develop,
      is not mandatory. You can, if you want, develop in pure C++.

:LodePNG: LodePNG is a PNG image decoder and encoder, all in one, no dependency or linkage to zlib or libpng required.
          It's made for C (ISO C90), and has a C++ wrapper with a more convenient interface on top.

:Box2D: Box2D is an open source C++ engine for simulating rigid bodies in 2D. it is available as :guilabel:`plugin`. The flag to use it in :guilabel:`Android` is ``USE_BOX2D``.

:Liquidfun: Liquidfun is a 2D rigid-body and fluid simulation ``C++`` library for games based upon `Box2D <https://github.com/google/liquidfun>`__ . This version forces to drawback Box2D to 2.3.0. The Liquidfun is under the :guilabel:`feature/liquid_fun` branch.

:Bullet 3D: Bullet 3D is an open source C++ engine for simulating rigid bodies in 3D. It is available as external plugin/module however it is not ready yet :/.

:LuaSQLite 3: Available as :guilabel:`plugin`,  LuaSQLite 3 is a thin wrapper around the public domain :guilabel:`SQLite3` database engine. The flag to use it in :guilabel:`Android` is ``USE_LSQLITE3``.

:SQLite 3: `SQLite <https://www.sqlite.org/index.html>`__  is a C-language library that implements a small, fast, self-contained, high-reliability, full-featured, SQL database engine. It is part of the :guilabel:`lsqlite3`.

:AES Crypt: AES Crypt is a file encryption API that uses standard Advanced Encryption Standard (AES) to easily and securely encrypt files. Although is not mandatory, it is implicit added to the engine.

:Audiere: Audiere is a audio API. It can play Ogg Vorbis, MP3, FLAC, uncompressed WAV, AIFF, MOD, S3M, XM, and IT files. It is required only on :guilabel:`Windows` for now.

:PortAudio: `PortAudio <http://www.portaudio.com/>`__  is a free, cross-platform, open-source, audio I/O library. For now ``PortAudio`` only handle `wav sound format <https://en.wikipedia.org/wiki/WAV>`__.

:PlusWindows: PlusWindows is a simple library for creating Owner-Drawn components on :guilabel:`Windows` . It is required only on :guilabel:`Windows`.

:STB True Type: Do parse, decode, and rasterize characters from true type fonts.

:MiniZ: Miniz is a lossless, high performance data compression library that implements the zlib (RFC 1950) and Deflate (RFC 1951) compressed data format specification standards.

:ImGui: An immediate graphical user interface for C ++ with minimal dependencies. It is mainly used for editor. it is available as :guilabel:`plugin`. The flag to use it in :guilabel:`Android` is ``USE_IMGUI``.

:Audio: Unfortunately, this is still time consuming and not every platform is easy to handle with a common built-in music library. The main focus of the engine is *graphics*, so, for now, we will keep those libraries. ``PortAudio`` is a potential cross-platform API to be used however the codecs might demand some time to incorporate, so, for now, it is available over ``Linux`` only.


Games made with mbm
-------------------

Here some games made using the engine:


Tower defense
^^^^^^^^^^^^^

.. image:: _static/tower_defense_preview.gif
    :align: center

.. image:: _static/google-play-badge.png
    :width: 210px
    :align: center
    :target: https://play.google.com/store/apps/details?id=com.mini.mbm.towerdefense

.. image:: _static/badge-itch.png
    :width: 210px
    :align: center
    :target: https://mbm.itch.io/tower-defense-monster


Space ship combat
^^^^^^^^^^^^^^^^^

.. image:: _static/space_ship_combat.png
    :align: center

.. image:: _static/google-play-badge.png
    :width: 210px
    :align: center
    :target: https://play.google.com/store/apps/details?id=com.mini.mbm.spaceshipcombat

   
:download:`download for Windows <example_modules/SpaceShipCombatInstaller.zip>`



Puzzle from photos
^^^^^^^^^^^^^^^^^^

.. image:: _static/puzzle_from_photos.png
    :align: center

.. image:: _static/google-play-badge.png
   :width: 210px
   :align: center
   :target: https://play.google.com/store/apps/details?id=com.mini.mbm.puzzlefromphotos2



*Let us know if you have some game built using the engine :)*


Quick start guide
-----------------

.. _quick_start_guide:

Here a quickly description how to build the engine selecting the environment.

For now, the engine is available for :guilabel:`Windows` :guilabel:`Linux` and :guilabel:`Android`. 

More platforms are coming soon!


Getting the source code
^^^^^^^^^^^^^^^^^^^^^^^

Here the needed steps:


:Get git: Install `git <https://git-scm.com/downloads>`__  and clone the project:

.. prompt:: bash $

    git clone https://bitbucket.org/michel-morais/mini-mbm.git

:Download instead: You also can download the project:

    https://bitbucket.org/michel-morais/mini-mbm/downloads/


Build for Windows
^^^^^^^^^^^^^^^^^

:Install: :guilabel:`Visual Studio 2019` or above.

    *You might need C++ tools for Visual Studio to be able to compile.*

:Find the solution: Go to the folder :guilabel:`mini-mbm/platform-msvs/`
:Open: :guilabel:`mini-mbm.sln` solution:

    .. figure:: _static/visual_studio_1.png
        :align: center
        :figclass: align-center

        Building project on Visual Studio

:Build: Find the button ``Build Solution`` on :guilabel:`Visual Studio` and build it:

    .. figure:: _static/visual_studio_2.png
        :align: center
        :figclass: align-center

        Building project on Visual Studio

In the end, you should have all project inside the solution built.

Now, let's create an example...

:Main lua: let's create a text file called :guilabel:`main.lua` in the :guilabel:`Windows`':guilabel:`Desktop`

    Rename it for ``main.lua``

    Copy and paste this code:

.. code-block:: lua

    mbm.setColor(1,1,1) --white background

    tTexture = texture:new('2dw') --texture mesh

    tTexture:load('HB_smile.png') -- load our texture

.. figure:: _static/visual_studio_3.png
        :align: center
        :figclass: align-center

        Building project on Visual Studio / Create a empty file and rename it to ``main.lua``


Next let's use the following image:

.. figure:: example_modules/HB_smile.png
    :align: center
    :figclass: align-center

    Creating a new project on Visual Studio / :guilabel:`HB_smile.png`

Download the :guilabel:`HB_smile.png` to :guilabel:`Desktop` folder:

:download:`download HB_smile <example_modules/HB_smile.png>`.

Set the debug properties on :guilabel:`Visual Studio` to your just created scene :guilabel:`main.lua` :

.. prompt:: bash

    --scene "C:\Users\home\Desktop\main.lua" -w 800 -h 600


.. Note:: Of course your path would be different then this.


.. figure:: _static/visual_studio_4.png
        :align: center
        :figclass: align-center

        Building project on Visual Studio / debug properties on Visual Studio


Finally, when you run you should be able to see the first scene being loaded:

.. figure:: _static/visual_studio_5.png
        :align: center
        :figclass: align-center

        Building project on Visual Studio / First scene

.. _binary_for_windows:

Binary for Windows
^^^^^^^^^^^^^^^^^^

You can get the binaries for :guilabel:`Windows`  here:

:download:`download mini-mbm-windows-3.3.b.zip <example_modules/mini-mbm-windows-3.3.b.zip>`.

What's in this folder?

.. figure:: _static/content_of_folder_windows.png
        :align: center
        :figclass: align-center

        Content of :guilabel:`mini-mbm-windows-3.3.b.zip`  for :guilabel:`Windows`

.. _build_for_linux:

Build for Linux
^^^^^^^^^^^^^^^

:Tools: As ``root`` install  :guilabel:`cmake` :guilabel:`clang` :guilabel:`g++`  :guilabel:`gcc` :guilabel:`build-essential` :guilabel:`libgles2-mesa-dev` :guilabel:`libreadline-dev` :guilabel:`portaudio19-dev`.

.. prompt:: bash $

    apt-get cmake
    apt-get clang
    apt-get g++
    apt-get gcc
    apt-get install build-essential
    apt-get install libgles2-mesa-dev
    apt-get install libreadline-dev
    apt-get install portaudio19-dev

:Or: As ``sudo`` install :guilabel:`cmake` :guilabel:`clang` :guilabel:`g++`  :guilabel:`gcc` :guilabel:`build-essential` :guilabel:`libgles2-mesa-dev` :guilabel:`libreadline-dev` :guilabel:`portaudio19-dev`.

.. prompt:: bash $

    sudo apt-get cmake
    sudo apt-get clang
    sudo apt-get g++
    sudo apt-get gcc
    sudo apt-get install build-essential
    sudo apt-get install libgles2-mesa-dev
    sudo apt-get install libreadline-dev
    sudo apt-get install portaudio19-dev

:Create a folder: Go to the project folder and create a folder ``build``.

.. prompt:: bash mini-mbm/$

    mkdir build
    cd build/

:Cmake: To build your project you must to select a platform and which feature are you going to include:

    In this case the platform is :guilabel:`Linux` and we want to include everything (modules and third party libs):

.. prompt:: bash mini-mbm/build$

    cmake .. -DPLAT=Linux -DUSE_ALL=1 -DAUDIO=portaudio -DCMAKE_BUILD_TYPE=Release

:Make: Now you just need to compile typing ``make``.


.. prompt:: bash mini-mbm/build$

    make
    # or
    make -j8 # 8 threads to be faster

In the end, it will show you where the folder is, what is the size and what was built:


.. figure:: _static/build_linux.png
    :align: center
    :figclass: align-center

    Building for :guilabel:`Linux`


Audiere for Linux
^^^^^^^^^^^^^^^^^


.. Note:: Audiere for linux is only available under `audiere_linux <https://bitbucket.org/michel-morais/mini-mbm/branch/audiere_linux>`__  branch. The default sound API for Linux is `PortAudio <http://www.portaudio.com/>`__.

:Compile Audiere: From project folder, install osspd, go to ``third-party/audiere-1.9.4``,  configure and compile.

.. prompt:: bash mini-mbm/$

    sudo apt-get install osspd
    cd third-party/audiere-1.9.4
    chmod 777 configure
    autoreconf -f -i
    ./configure
    make

:Alternative Audiere: As described here (https://github.com/poikilos/Audiere/releases), you can install it from this fork:

.. prompt:: bash $

    cd /tmp
    wget https://github.com/expertmm/Audiere/releases/download/1.10.1/audiere-1.10.1.tar.gz
    tar -xzf audiere-1.10.1.tar.gz
    cd audiere-1.10.1
    autoreconf -f -i
    ./configure
    make
    sudo make install


Later you must follow the process of ::ref:`build_for_linux` but skip ``portaudio19-dev`` installation. Include the option ``-DAUDIO=audiere`` from cmake command line.

Build for Android
^^^^^^^^^^^^^^^^^

    For :guilabel:`Android` we are using :guilabel:`Linux` environment.

:Tools: As ``root`` install :guilabel:`cmake` :guilabel:`clang` :guilabel:`g++`  :guilabel:`gcc` :guilabel:`build-essential`.

.. prompt:: bash $

    apt-get cmake
    apt-get clang
    apt-get g++
    apt-get gcc
    apt-get install build-essential

:Or: As ``sudo`` install :guilabel:`cmake` :guilabel:`clang` :guilabel:`g++`  :guilabel:`gcc` :guilabel:`build-essential`.

.. prompt:: bash $

    sudo apt-get cmake
    sudo apt-get clang
    sudo apt-get g++
    sudo apt-get gcc
    sudo apt-get install build-essential


:NDK: For :guilabel:`Android` is necessary ``NDK``. 

    The engine is using the `Android ndk r26c <https://developer.android.com/ndk/downloads#stable-downloads>`__ version.

    Download, extract and export the variable ``NDK_ROOT`` to that folder:

:MINSDKVERSION: For :guilabel:`Android` is to define the minumum version ``SDK``. 

    The engine is using the :guilabel:`Android 8.0 (API level 24) (Nougat)`.

    You can re-define the ``SDK`` with ``-DANDROID_NATIVE_API_LEVEL=24`` 

.. prompt:: bash $

    export NDK_ROOT=your-path/android-ndk-r26c

.. Tip:: You can add ``export NDK_ROOT=your-path/android-ndk-r26c`` in your ``.bashrc`` if you want.

.. Note:: You should not have problem using a major version then ``r26c``. If you have, please let me know.

:Create a folder to build: Go to the project folder and create a folder ``build``

.. prompt:: bash mini-mbm/$

    mkdir build
    cd build/

:Cmake: To build your project you must to select a platform and which feature are you going to include:

    In this case the platform is :guilabel:`Android` and we want to include only ``LUA`` (modules and third party libs):
    You also need to specify the architecture. **Cmake** will set to default if not specified.

.. Important:: 
    Since :guilabel:`Android 7.0`, the system prevents apps from dynamically linking against non-NDK libraries. 
    
    https://developer.android.com/about/versions/nougat/android-7.0-changes.html#ndk

    For this reason, you have to specify which libraries are you going to use together (:guilabel:`box2d`, :guilabel:`ImGui`, :guilabel:`lsqlite3` etc  ).

    We do this through the command ``USE`` when invoking :guilabel:`Cmake`: ``-USE_LSQLITE3=1``, ``-DUSE_IMGUI=1``, ``-DUSE_BOX2D=1``, ``-DAUDIO=jni``, etc...

    Note that we are in :guilabel:`Android` using native audio with the flag ``-DAUDIO=jni``.

.. prompt:: bash mini-mbm/build$

    cmake .. -DPLAT=Android -DANDROID_ABI=arm64-v8a -DANDROID_NATIVE_API_LEVEL=24 -DUSE_LUA=1 -DAUDIO=jni -DCMAKE_BUILD_TYPE=Release

:Make: Now you just need to compile typing ``make``:


.. prompt:: bash mini-mbm/build$

    make
    # or 
    make -j8 # 8 threads to be faster

In the end, it will show you where the folder is, what is the size and what was built:


.. figure:: _static/build_android.png
    :align: center
    :figclass: align-center

    Building for :guilabel:`Android`

.. Note:: 
    | The needed libraries are :guilabel:`libcore_mbm.so`, :guilabel:`libmini-mbm.so`, :guilabel:`liblua-5.2.2.so` and :guilabel:`libc++_shared.so`.
    | 
    | The libraries :guilabel:`ImGui.so`, :guilabel:`box2d.so`, :guilabel:`lsqlite3.so` and others *plugins* are only necessary if will be used in your project.
    | In the figure above, we have not added any plug-in options, so it is not generated any of :guilabel:`ImGui.so`, :guilabel:`box2d.so` or :guilabel:`lsqlite3.so`.

For :guilabel:`Android`  there are more steps to do...

:Install Android Studio: You need to install :guilabel:`Android Studio`.

        Go to https://developer.android.com/studio download and install :guilabel:`Android Studio`.
        Version used: :guilabel:`Iguana`.

.. Note:: You might need to install the latest SDK. At this point the minimum version is Android 13 (Tiramisu). See https://apilevels.com/ for more information

:Create a project: Now open :guilabel:`Android Studio` and create a new project:

    .. figure:: _static/android_studio_1.png
        :align: center
        :figclass: align-center

        Creating a new project on Android Studio

:Select: **Add No Activity**  option:

    .. figure:: _static/android_studio_2.png
        :align: center
        :figclass: align-center

        Creating a new project on Android Studio / Add No Activity

:Give a name for your project: Let's call it :guilabel:`My First Game`.
:Set the language to Java: The project has some dependencies on :guilabel:`JNI` and you must use :guilabel:`Java` as interface.

    .. figure:: _static/android_studio_3.png
        :align: center
        :figclass: align-center

        Creating a new project on Android Studio / Configure the project

.. Note::
    Important to leave the ``minimum SDK`` for as many devices as possible, but use the ``target SDK`` as recommended by Google play here https://apilevels.com/.

:Folder com/mini/mbm: You must to copy the interfaces to the right folder:

    Go to ``mini-mbm/platform-android`` folder and copy the folder ``com/mini/mbm`` and its sub-folder to ``your-project-android/app/src/main/java/com/mini/mbm``.

.. prompt:: bash mini-mbm/platform-android$

    cp -r com ~/AndroidStudioProjects/MyFirstGame/app/src/main/java/com

The folder on :guilabel:`Android Studio` should be like this:

    .. figure:: _static/android_studio_4.png
        :align: center
        :figclass: align-center

        Creating a new project on Android Studio / copy ``com/mini/mbm`` folder


:Then: After :guilabel:`Android Studio` update the indices, you will be able to add a new class:

    Add a class which will extend of the main class of the engine. Let's call it :guilabel:`Main`.

    .. figure:: _static/android_studio_5.png
        :align: center
        :figclass: align-center

        Creating a new project on Android Studio / Add a class "Main.java"


:Main: Create a class :guilabel:`Main.java` which extend from :guilabel:`com.mini.mbm.MainJniEngine`:

    .. figure:: _static/android_studio_6.png
        :align: center
        :figclass: align-center

        Creating a new project on Android Studio / Class "Main.java" extend from "MainJniEngine"

:Implement: Use :guilabel:`Android Studio` option to implement abstract methods:

    .. figure:: _static/android_studio_7.png
        :align: center
        :figclass: align-center

        Creating a new project on Android Studio / implement abstract methods

*Your Main class should be like this:*

.. code-block:: java

    package com.example.myfirstgame;

    import android.content.Intent;
    import android.graphics.Point;
    import android.os.Bundle;
    import com.mini.mbm.MainJniEngine;

    public final class Main extends MainJniEngine {
        
        @Override
        public int getMaxSimultaneousStreams() {
            return 0;
        }

        @Override
        public Point getExpectedSizeOfWindow() {
            return null;
        }

        @Override
        public String OnDoCommands(String key, String param) {
            return null;
        }

        @Override
        public void OnCreate(Bundle icicle) {

        }

        @Override
        public void OnResume() {

        }

        @Override
        public void OnStart() {

        }

        @Override
        public void OnPause() {

        }

        @Override
        public void OnStop() {

        }

        @Override
        public void OnDestroy() {

        }

        @Override
        public void OnActivityResult(int requestCode, int resultCode, Intent data) {

        }

        @Override
        public void OnPostCreate(Bundle savedInstanceState) {

        }
    }


:Libs: Now you need to copy the needed libraries to the right folder.

.. Note::
    The minimun :guilabel:`ANDROID_API` is defined as :guilabel:`ANDROID_NATIVE_API_LEVEL = 24` in the CMakeLists.txt. 
    This mean :guilabel:`Android 8.0 (API level 24) (Nougat)`


Here a table showing the right folders depend on :guilabel:`ABI`:

+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| ABI                         |  Folder                                                      |         Comment                                                                                |
+=============================+==============================================================+================================================================================================+
| ``arm64-v8a``               |  ``yourProject/app/src/main/jniLibs/arm64-v8a/``             |   (principal 64) ARMv8 AArch64 instruction set - since NDK r10  (mandatory)                    |
+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| ``armeabi-v7a``             |  ``yourProject/app/src/main/jniLibs/armeabi-v7a/``           |   (principal)    ARMv7 based devices with hardware FPU instructions (VFPv3_D16)                |
+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| ``armeabi-v7a with NEON``   |  ``yourProject/app/src/main/jniLibs/armeabi-v7a/``           |   Same as armeabi-v7a, but sets NEON as floating-point unit                                    |
+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| ``armeabi-v7a with VFPV3``  |  ``yourProject/app/src/main/jniLibs/armeabi-v7a/``           |   same as armeabi-v7a, but sets VFPv3_D32 as floating-point unit                               |
+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| ``x86``                     |  ``yourProject/app/src/main/jniLibs/x86/``                   |   IA-32 instruction set                                                                        |
+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| ``mips``                    |  ``yourProject/app/src/main/jniLibs/mips/``                  |   MIPS32 instruction set                                                                       |
+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| ``mips64``                  |  ``yourProject/app/src/main/jniLibs/mips64/``                |   MIPS64 instruction set (r6) - since NDK r10                                                  |
+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+
| ``x86_64``                  |  ``yourProject/app/src/main/jniLibs/x86_64/``                |   Intel64 instruction set (r1) - since NDK r10                                                 |
+-----------------------------+--------------------------------------------------------------+------------------------------------------------------------------------------------------------+

.. Note::
    The :guilabel:`ABI` option can be set when you invoke :guilabel:`cmake` setting the argument ``ANDROID_ABI``

    Examples:

    .. prompt:: bash mini-mbm/build$

        cmake .. -DPLAT=Android -DANDROID_ABI=arm64-v8a   -DUSE_LUA=1  -DCMAKE_BUILD_TYPE=Release
        cmake .. -DPLAT=Android -DANDROID_ABI=armeabi-v7a -DUSE_LUA=1  -DCMAKE_BUILD_TYPE=Release
        cmake .. -DPLAT=Android -DANDROID_ABI=x86         -DUSE_LUA=1  -DCMAKE_BUILD_TYPE=Release

.. Important::
    Whenever you change the :guilabel:`ABI`, clean the whole folder.

    On build folder do:

    .. prompt:: bash mini-mbm/build$

        make clean
        rm -rf *

    Then do the ``cmake`` command selecting the :guilabel:`ABI` again.


Now, let's create the ``jniLibs`` folder:

.. prompt:: bash mini-mbm/platform-android$

    mkdir ~/AndroidStudioProjects/MyFirstGame/app/src/main/jniLibs


Then for this example, we will copy the folder :guilabel:`arm64-v8a` within its libs inside to the ``jniLibs`` folder. We remove the libraries that we do not use.

.. prompt:: bash mini-mbm/$

    cp -r bin/release/arm64-v8a/ ~/AndroidStudioProjects/MyFirstGame/app/src/main/jniLibs


:guilabel:`Android Studio`  will look like this:

.. figure:: _static/android_studio_8.png
    :align: center
    :figclass: align-center

    Creating a new project on Android Studio / ``jniLibs`` libraries.


:Android Manifest: Now you need to update the ``AndroidManifest.xml``:

    Here the default ``AndroidManifest.xml``:

.. code-block:: xml

    <?xml version="1.0" encoding="utf-8"?>
    <!-- example AndroidManifest.xml!-->
    <!-- your package (your_package) android must be replaced!-->

    <manifest xmlns:android="http://schemas.android.com/apk/res/android"
        package="com.mini.mbm.your_package"
        android:configChanges="orientation|keyboardHidden|screenSize"
        android:glEsVersion="0x00020000"
        android:installLocation="preferExternal"
        xmlns:tools="http://schemas.android.com/tools">

        <!-- permission if you need uncomment -->
        <!-- <uses-permission android:name="android.permission.GET_ACCOUNTS" /> -->
        <!-- needed permission for mini-mbm-->

        <!-- https://developers.google.com/android/reference/com/google/android/gms/ads/identifier/AdvertisingIdClient.Info#public-methods -->
        <!-- Action Items If you use an advertising ID, you must declare the AD_ID Permission when your app targets Android 13 or above. Apps that don’t declare the permission will get a string of zeros.-->
        <!--Case 1: The app doesn't contain any Ads:-->
        <uses-permission android:name="com.google.android.gms.permission.AD_ID" tools:node="remove"/>
        <!-- Case 2: The app contains Ads:-->
        <!--<uses-permission android:name="com.google.android.gms.permission.AD_ID"/>-->
        <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
        <uses-permission android:name="android.permission.WRITE_OWNER_DATA" />
        <uses-permission android:name="android.permission.READ_OWNER_DATA" />
        <uses-permission android:name="android.permission.READ_MEDIA_AUDIO" />
        <uses-permission android:name="android.permission.READ_MEDIA_IMAGES" />
        <uses-permission android:name="android.permission.WAKE_LOCK" />
        <uses-permission android:name="android.permission.VIBRATE" />
        <!-- We do need opengl-es 2.0 support-->
        <uses-feature android:glEsVersion="0x00020000" />

        <!-- Important LargeHeap and hardwareAccelerated!-->
        <application
            android:hardwareAccelerated="true"
            android:largeHeap="true"
            android:allowBackup="true"
            android:dataExtractionRules="@xml/data_extraction_rules"
            android:fullBackupContent="@xml/backup_rules"
            android:icon="@mipmap/ic_launcher"
            android:label="@string/app_name"
            android:roundIcon="@mipmap/ic_launcher_round"
            android:supportsRtl="true"
            android:theme="@style/Theme.your_package"
            tools:targetApi="34"> <!-- Change the target API here if needed -->
            <!-- Mode singleTask and horizontal adjust (you can change it) -->
            <activity
                android:name="com.mini.mbm.your_package.Main"
                android:configChanges="orientation|keyboardHidden|screenSize"
                android:label="@string/app_name"
                android:launchMode="singleTask"
                android:orientation="horizontal"
                android:screenOrientation="landscape"
                android:exported="true">
                <intent-filter>
                    <action android:name="android.intent.action.MAIN" />
                    <category android:name="android.intent.category.LAUNCHER" />
                </intent-filter>

                <meta-data
                    android:name="android.app.lib_name"
                    android:value="your_package" />
            </activity>
        </application>

    </manifest>


:Update Android Manifest: You need to update according to project:

.. code-block:: xml

    <manifest xmlns:android="http://schemas.android.com/apk/res/android"
	package="com.mini.mbm.your_package"
    >

    <manifest xmlns:android="http://schemas.android.com/apk/res/android"
	package="com.example.myfirstgame"
    >


.. code-block:: xml

    <activity 
    android:name="com.mini.mbm.your_package.YourMainClassExtendedFrom_MainJniEngine"
    >

    <activity 
    android:name="com.example.myfirstgame.Main"
    >


:Assets: For your scripts, textures, binary files, etc, you must to add them in the folder :guilabel:`assets`.

    Let's create the ``assets`` folder in the right place:

.. prompt:: bash mini-mbm/platform-android$

    mkdir ~/AndroidStudioProjects/MyFirstGame/app/src/main/assets

Let's create a sub-folder to organize some textures:

.. prompt:: bash mini-mbm/platform-android$

    mkdir ~/AndroidStudioProjects/MyFirstGame/app/src/main/assets/textures


:Main lua: Next let's create a file called :guilabel:`main.lua` inside the :guilabel:`assets` folder.

.. code-block:: lua

    mbm.addPath('textures') --sub folder to search images

    tTexture = texture:new('2dw') --texture mesh

    tTexture:load('HB_smile.png') -- load our texture


Let's copy the image :guilabel:`HB_smile.png` to :guilabel:`assets/textures` folder:


.. figure:: example_modules/HB_smile.png
    :align: center
    :figclass: align-center

    Creating a new project on Android Studio / :guilabel:`HB_smile.png` into :guilabel:`assets/textures` folder

:download:`download HB_smile <example_modules/HB_smile.png>`.

The final structure should be like this:

.. figure:: _static/android_studio_10.png
    :align: center
    :figclass: align-center

    Creating a new project on Android Studio / assets structure


.. Tip::
    Every time that you update the folder :guilabel:`assets` you **MUST** update the versionName in build.gradle. The :guilabel:`getMyVersionAPP` read it to update the asset:

    .. figure:: _static/android_studio_11.png

      Update the return of the method :guilabel:`getMyVersionAPP`.

    This is the way the engine organize to update from asset folders.


.. Tip::
    Always use LUA files encoded to UTF8.
    Here a script to help you using ``iconv``:

    .. prompt:: bash $

        find . -type f -iname *.lua -exec sh -c 'iconv -f $(file -bi "$1" |sed -e "s/.*[ ]charset=//") -t utf-8 -o converted "$1" && mv converted "$1"' -- {} \;

    This avoid some error on interpret ``LUA`` files.


:Run: At this point, you should be able to run the project.

    The :guilabel:`Log cat`  in the :guilabel:`Android Studio` shall logs some useful information.

.. figure:: _static/android_studio_9.png
    :align: center
    :figclass: align-center

    Creating a new project on Android Studio / Running / Log cat

.. _binary_for_android:

Binary for Android
^^^^^^^^^^^^^^^^^^

Even though you have some doubt? Here the project using **Android Studio** version **3.5.3** :

:download:`Example full project here <example_modules/MyFirstGame.7z>`


Or just would like to take a look on the :guilabel:`apk` generated?

:download:`download example apk for Android <example_modules/example.apk>`

Or the latest libraries to get start at :guilabel:`Android`?

:download:`binary libs for Android version 3.2.a <example_modules/mini-mbm-3.2.a-android-basic.7z>`

It contains the core of engine and :guilabel:`LUA` script support to start to develop on :guilabel:`Android`:

.. figure:: _static/libs_android-1.png
    :align: center
    :figclass: align-center

.. figure:: _static/libs_android-2.png
    :align: center
    :figclass: align-center

.. Note::
    | The needed libraries are :guilabel:`libcore_mbm.so`, :guilabel:`libmini-mbm.so`, :guilabel:`liblua-5.2.2.so` and :guilabel:`libc++_shared.so`. 
    | The libraries like :guilabel:`ImGui.so`, :guilabel:`box2d.so`, :guilabel:`lsqlite3.so` and others are *plugins* and they are only necessary if will be used in your project.
    | If you wish to add any of libraries (plugins) it is necessary to compile with theirs respective ``flags``.


.. Important:: 
    Since :guilabel:`Android 7.0`, the system prevents apps from dynamically linking against non-NDK libraries. 
    
    https://developer.android.com/about/versions/nougat/android-7.0-changes.html#ndk

    For this reason, you have to specify which libraries are you going to use together (:guilabel:`box2d`, :guilabel:`ImGui`, :guilabel:`sqlite`, etc  ).

    We do this through the flag ``USE`` when invoking :guilabel:`Cmake`: ``-USE_LSQLITE3=1``, ``-DUSE_IMGUI=1``, ``-DUSE_BOX2D=1`` etc...

On :guilabel:`Android` it is possible to list the files in the cache folder through command line using :guilabel:`adb` from folder ``~/Android/Sdk/platform-tools``:

.. prompt:: bash $
    
    ./adb exec-out run-as com.mini.mbm.yourcompany ls -R /data/data/com.mini.mbm.yourcompany


More information at https://developer.android.com/studio/command-line/adb

.. Warning::

    known **issue**. 
    
    The current implementation does not load *sound stream* from cache folder in :guilabel:`Android`. This is a **TODO**.

    You can leave the *sound stream* in asset folder (**NOT** inside of your database) then it will works.

Command line usage
^^^^^^^^^^^^^^^^^^

Command line options:

.. code-block:: php

    mini-mbm
    Version:3.1.b
    Usage: mini-mbm [options]... [file_name.lua] or [--scene file_name.lua]...
        --help              display this help
    -w, --width             set window's width
    -h, --height            set window's height
    -ew,--expectedwidth     set expected window's width
    -eh,--expectedheight    set expected window's height
        --stretch           stretch to axis ('x', 'y', or 'xy') default is 'y'
        --nosplash          do not display logo
    -x, --posx              set x window's position
    -y, --posy              set y window's position
    -s, --scene             set scene to load (e.g.: main.lua)
    -n, --name              set window's name
    -a, --addpath           add path to search file at run time
    --noborder              new window without border

        note: to add a global variable use '='. example:
                                'myVarNumber=5.0' 'stringVar=someString'
        the variable will be stored as number when is number.
        otherwise the variable will be stored as string.
        use getGlobal function to get variable at lua script (see doc for more information)

        use space to separate arguments:
        usage example:
                mini-mbm --scene main.lua -w 800 -h 600
                mini-mbm -s main.lua -w 800 -h 600 'myVarNumber=5.0' 'stringVar=someString' --nosplash
