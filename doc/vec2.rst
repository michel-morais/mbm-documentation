.. contents:: Table of Contents

Table vec2
==========
.. _vectwo:

Table :ref:`vec2 <vectwo>` is a vector 2d (x,y) class for the most known operations using vector.

vec2 methods
------------

vec2 new
^^^^^^^^

.. data:: new()

   Create a new instance of :ref:`vec2 <vectwo>` with no argument.

   :return: :ref:`vec2 <vectwo>` *table*.

   *Example:*
.. code-block:: lua

   local v = vec2:new()
   print(v.x,v.y) -- 0,0


.. data:: new(number x,number * y)

    Create a new instance of :ref:`vec2 <vectwo>` passing two arguments.

   :param number: **x** value.
   :param number: **y** value (optional).
   :return: :ref:`vec2 <vectwo>` *table*.

   *Example:*
.. code-block:: lua

   local v = vec2:new(15,66)
   print(v.x,v.y) -- 15,66

   v = vec2:new(15)
   print(v.x,v.y) -- 15,0

.. data:: new(vec2 other)

    Create a new instance of :ref:`vec2 <vectwo>` passing other :ref:`vec2 <vectwo>` as parameter.

   :param vec2: **other** to copy values.
   :return: :ref:`vec2 <vectwo>` *table*.

   *Example:*
.. code-block:: lua

   local v1 = vec2:new(15,66)
   print(v1.x,v1.y) -- 15,66

   local v2 = vec2:new(v1)
   print(v2.x,v2.y) -- 15,66


.. data:: new(vec3 other)

    Create a new instance of :ref:`vec2 <vectwo>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to copy values (x and y).
   :return: :ref:`vec2 <vectwo>` *table*.

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(1,2,3)
   print(v1.x,v1.y,v1.z) -- 1,2,3

   local v2 = vec2:new(v1)
   print(v2.x,v2.y) -- 1,2


.. data:: new(renderizable render)

    Create a new instance of :ref:`vec2 <vectwo>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to copy values from position.
   :return: :ref:`vec2 <vectwo>` *table*.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('2dw')
   tTexture:setPos(88,99)

   local v2 = vec2:new(tTexture)
   print(v2.x,v2.y) -- 88,99

vec2 get
^^^^^^^^

.. _vectwoget:
.. data:: get()

    Retrieve the values x and y from :ref:`vec2 <vectwo>`.

   :return: :literal:`number` *x*, :literal:`number` *y*

   *Example:*
.. code-block:: lua

   local v2 = vec2:new(415,669)
   local x,y = v2:get()
   print(x,y) -- 415,669

vec2 set
^^^^^^^^

.. data:: set(number x,number y)

    Set new values to :ref:`vec2 <vectwo>` passing two number arguments (x and y).

   :param number: **x** value.
   :param number: **y** value (optional).

   *Example:*
.. code-block:: lua

   local v = vec2:new(15,66)
   v:set(88,74)
   print(v:get()) -- 88,74

   v:set(15) -- Note only x change
   print(v:get()) -- 15,74

.. data:: set(vec2 other)

    Set new values to :ref:`vec2 <vectwo>` passing other :ref:`vec2 <vectwo>` as parameter.

   :param vec2: **other** to set values.

   *Example:*
.. code-block:: lua

   local v1 = vec2:new(15,66)
   local v2 = vec2:new()
   print(v1) -- x:15 y:66
   print(v2) -- x:0 y:0

   v2:set(v1)
   print(v2) -- x:15 y:66


.. data:: set(vec3 other)

    Set new values to :ref:`vec2 <vectwo>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to set values (x and y).

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(44,55,33)
   local v2 = vec2:new()
   print(v1:get()) -- 44,55,33
   print(v2:get()) -- 0,0

   v2:set(v1)
   print(v2) -- x:44 y:55


.. data:: set(renderizable render)

    Set new values to :ref:`vec2 <vectwo>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to set values from position.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('2dw')
   tTexture:setPos(12,15)

   local v2 = vec2:new()
   v2:set(tTexture)
   print(v2) --x:12 y:15

vec2 add
^^^^^^^^

.. data:: add(number x,number y)

    Add values to :ref:`vec2 <vectwo>` passing two number arguments (x and y).

   :param number: **x** value.
   :param number: **y** value (optional).

   *Example:*
.. code-block:: lua

   local v = vec2:new(15,66)
   v:add(88,74)
   print(v:get()) -- 103,140

   v:add(15) -- Note.: 103 + 15 and 140 + 15
   print(v:get()) -- 118,155


.. data:: add(vec2 other)

    Add values to :ref:`vec2 <vectwo>` passing other :ref:`vec2 <vectwo>` as parameter.

   :param vec2: **other** to add values.

   *Example:*
.. code-block:: lua

   local v1 = vec2:new(15,66)
   local v2 = vec2:new(1,2)
   print(v1) -- x:15 y:66
   print(v2) -- x:1 y:2

   v2:add(v1)
   print(v2) -- x:16 y:68


.. data:: add(vec3 other)

    Add values to :ref:`vec2 <vectwo>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to add values (x and y).

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(44,55,33)
   local v2 = vec2:new(5,8)
   print(v1:get()) -- 44,55,33
   print(v2:get()) -- 5,8

   v2:add(v1)
   print(v2) -- x:49 y:63


.. data:: add(renderizable render)

    Add values to :ref:`vec2 <vectwo>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to add values from position.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('2dw')
   tTexture:setPos(12,15)

   local v2 = vec2:new(5,5)
   v2:add(tTexture)
   print(v2) --x:17 y:20


vec2 sub
^^^^^^^^

.. data:: sub(number x,number y)

    Subtract values in the :ref:`vec2 <vectwo>` passing two number arguments (x and y).

   :param number: **x** value.
   :param number: **y** value (optional).

   *Example:*
.. code-block:: lua

   local v = vec2:new(150,660)
   v:sub(88,74)
   print(v:get()) -- 62 586

   v:sub(15) --note.: 62 - 15, 586 - 15
   print(v:get()) -- 47 571

.. data:: sub(vec2 other)

    Subtract values in the :ref:`vec2 <vectwo>` passing other :ref:`vec2 <vectwo>` as parameter.

   :param vec2: **other** to subtract values.

   *Example:*
.. code-block:: lua

   local v1 = vec2:new(15,66)
   local v2 = vec2:new(1,2)
   print(v1) -- x:15 y:66
   print(v2) -- x:1 y:2

   v2:sub(v1)
   print(v2) -- x:-14 y:-64


.. data:: sub(vec3 other)

    Subtract values in the :ref:`vec2 <vectwo>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to subtract values (x and y).

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(44,55,33)
   local v2 = vec2:new(5,8)
   print(v1:get()) -- 44,55,33
   print(v2:get()) -- 5,8

   v2:sub(v1)
   print(v2) --  x:-39 y:-47


.. data:: sub(renderizable render)

    Subtract values in the :ref:`vec2 <vectwo>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to subtract values from position.

   *Example:*
.. code-block:: lua

   local spt =  sprite:new('2dw')
   spt:setPos(12,15)

   local v2 = vec2:new(5,5)
   v2:sub(spt)
   print(v2) --   x:-7 y:-10


vec2 mul
^^^^^^^^

.. data:: mul(number x,number y)

    Multiply (scalar) values to :ref:`vec2 <vectwo>` passing two number arguments (x and y).

   :param number: **x** value.
   :param number: **y** value (optional).

   *Example:*
.. code-block:: lua

   local v = vec2:new(2,3)
   v:mul(5,2)
   print(v:get()) -- 10 6

   v:mul(2) -- Note.: 10 * 2 and 6 * 2
   print(v:get()) -- 20 12

.. data:: mul(vec2 other)

    Multiply (scalar) values to :ref:`vec2 <vectwo>` passing other :ref:`vec2 <vectwo>` as parameter.

   :param vec2: **other** to multiply values.

   *Example:*
.. code-block:: lua

   local v1 = vec2:new(2,3)
   local v2 = vec2:new(3,4)
   print(v1) -- x:2 y:3
   print(v2) -- x:3 y:4

   v2:mul(v1)
   print(v2) -- 6 12


.. data:: mul(vec3 other)

    Multiply (scalar) values by :ref:`vec2 <vectwo>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to multiply values (x and y).

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(2,3,4)
   local v2 = vec2:new(2,1.5)
   print(v1:get()) -- 2,3,4
   print(v2:get()) -- 2,1.5

   v2:mul(v1)
   print(v2) --  4, 4.5


.. data:: mul(renderizable render)

    Multiply (scalar) values by :ref:`vec2 <vectwo>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to multiply values by position.

   *Example:*
.. code-block:: lua

   local spt =  sprite:new('2dw')
   spt:setPos(2.2,3.3)

   local v2 = vec2:new(3,4)
   v2:mul(spt)
   print(v2) --   x:6.6 y:13.2

vec2 div
^^^^^^^^

.. Note:: Any number divided by zero is an error however the engine set the result to 0.

.. data:: div(number x,number y)

    Divide values by :ref:`vec2 <vectwo>` passing two number arguments (x and y).

   :param number: **x** value.
   :param number: **y** value (optional).

   *Example:*

.. code-block:: lua

   local v = vec2:new(2,3)
   v:div(2,1.5)
   print(v:get()) -- 1 2

   v:div(2) -- Note.: 1 / 2 and 2 / 2
   print(v:get()) -- 0.5    1

   v:div(0) -- Note.: 0.5 / 0 and 1 / 0 not throw an exception and the result is set to 0
   print(v) -- x:0 y:0

.. data:: div(vec2 other)

    Divide values by :ref:`vec2 <vectwo>` passing other :ref:`vec2 <vectwo>` as parameter.

   :param vec2: **other** to divide values.

   *Example:*
.. code-block:: lua

   local v1 = vec2:new(20,30)
   local v2 = vec2:new(2,4)
   print(v1) -- x:20 y:30
   print(v2) -- x:2 y:4

   v1:div(v2)
   print(v1) -- x:10 y:7.5

   v2.y = 0
   v1:div(v2)
   print(v1) -- x:5 y:0

   v1:set(10,20)
   v2:set(0,10)
   v1:div(v2)-- Note.: 10 / 0 not throw an exception and the result is set to 0
   print(v1) -- x:0 y:2


.. data:: div(vec3 other)

    Divide values by :ref:`vec2 <vectwo>` passing other :ref:`vec3 <vecthree>` as parameter.

   :param vec3: **other** to divide values by (x and y).

   *Example:*
.. code-block:: lua

   local v1 = vec3:new(2,3,4)
   local v2 = vec2:new(2,1.5)
   print(v1:get()) -- 2,3,4
   print(v2:get()) -- 2,1.5

   v2:div(v1)
   print(v2) --  x:1 y:0.5


.. data:: div(renderizable render)

    Divide values by :ref:`vec2 <vectwo>` passing a :ref:`renderizable <renderizable>` as parameter.

   :param renderizable: **render** to divide values by position.

   *Example:*
.. code-block:: lua

   local spt =  sprite:new('2dw')
   spt:setPos(2,50)

   local v2 = vec2:new(200,300)
   v2:div(spt)
   print(v2) --   x:100 y:6


.. _vectwolength:

vec2 length
^^^^^^^^^^^
 The formula to :ref:`length <vectwolength>` is:

 :math:`\sqrt{(x * x) + (y * y)}`

.. data:: length()

   Get the length from :ref:`vec2 <vectwo>`. Same as :ref:`len <vectwolen>` attribute.

   :return: ``number`` - *length* of vector.

   *Example:*
.. code-block:: lua

   local v = vec2:new(100,300)
   print(v:length()) -- 316.2278


vec2 azimuth
^^^^^^^^^^^^
 
.. data:: azimuth()

   Get the azimuth from :ref:`vec2 <vectwo>`.

   :return: ``number`` - *azimuth* of vector (radian).

   *Example:*
.. code-block:: lua

   local v = vec2:new(1,0)
   print(math.deg(v:azimuth())) -- 90

.. data:: azimuth(vec2 v2)

   Get the azimuth between two :ref:`vec2 <vectwo>`.

   :return: ``number`` - *azimuth* between two vectors (radian).

   *Example:*
.. code-block:: lua

   local v1 = vec2:new(1,0)
   local v2 = vec2:new(0,1)
   print(math.deg(v1:azimuth(v2))) -- 135

.. _vectwomove:

vec2 move
^^^^^^^^^
.. _vec2move:
.. data:: move(number x,number * y)

   Move x and y units frames by seconds. This method consider the FPS.

   :param number: **x** position.
   :param number: **y** position  (optional).

   *Example:*
.. code-block:: lua

   local v = vec2:new(5,55)
   v:move(100,50)

   --this is equivalent
   function loop(delta)
        v.x = v.x + (delta * 100)
        v.y = v.y + (delta * 50)
   end


.. _vectwodot:

vec2 dot
^^^^^^^^
 |  A dot product is an algebraic operation that takes two equal-length sequences of numbers and returns a single number.
 |  Learn more at `wiki dot product <https://en.wikipedia.org/wiki/Dot_product>`__  .
 |  The formula to :ref:`vec2 dot <vectwodot>` is:

 :math:`(x * x) + (y * y)`

.. data:: dot(vec2 b)

   Calculate a dot product value to :ref:`vec2 <vectwo>` passing other :ref:`vec2 <vectwo>` as parameter.

   :param vec2: **parameter**.
   :return: :literal:`number` *dot* calculated.

   *Example:*
.. code-block:: lua

    a = vec2:new(3,4)            --x = 3      y = 4
    b = vec2:new(10,25)          --x = 10     y = 25
    local dot_result = a:dot(b)  --3 * 10 + 4 * 25 = 130

    print(dot_result)            -- 130


.. data:: dot(vec2 a, vec2 b)

    Calculate a dot product value to :ref:`vec2 <vectwo>` between a and b :ref:`vec2 <vectwo>` as parameter.

   :param vec2: **a** a parameter.
   :param vec2: **b** b parameter.
   :return: :literal:`number` *dot* calculated.

   *Example:*
.. code-block:: lua

    a = vec2:new(3,4)              --x = 3      y = 4
    b = vec2:new(10,25)            --x = 10     y = 25
    c = vec2:new(1,2)              --x = 1      y = 2
    local dot_result = c:dot(a,b)  --3 * 10 + 4 * 25 = 130

    print(dot_result)              -- 130

.. _vectwolerp:

vec2 lerp
^^^^^^^^^
 |  Linear interpolation is a method of curve fitting using linear polynomials to construct new data points within the range of a discrete set of known data points.
 |  Learn more at `wiki linear interpolation <https://en.wikipedia.org/wiki/Linear_interpolation>`__  .
 |  The formula to :ref:`vec2 lerp <vectwolerp>` is:
 |
 | :math:`v = (1 - s) * (v1) + s * (v2)`


.. data:: lerp(vec2 a, vec2 b, number s)

    Calculate a linear interpolation between two :ref:`vec2 <vectwo>`. Store the result on itself.

   :param vec2:  **a** a parameter.
   :param vec2:  **b** b parameter.
   :param number: **s** parameter.

   *Example:*
.. code-block:: lua

    local a = vec2:new(20,30)
    local b = vec2:new(10,15)
    local c = vec2:new()       --x = 0     y = 0
    c:lerp(a,b,0.5)            --Keep the result on 'c' vector
    print(c)                   --x:15 y:22.5

.. _vectwonormalize:

vec2 normalize
^^^^^^^^^^^^^^

 |  A normal vector is a vector that is perpendicular to a given object.
 |  Learn more at `wiki Normal (geometry) <https://en.wikipedia.org/wiki/Normal_(geometry)#Calculating_a_surface_normal>`__  and `wiki Unit vector (geometry) <https://en.wikipedia.org/wiki/Unit_vector>`__.
 |  The formula to :ref:`vec2 normal <vectwonormalize>` is:
 |
 | :math:`\Hat{U} = \frac{U}{|U|}`
 |
 |  where :literal:`|u|` is the length of :literal:`u`.


.. data:: normalize()

    Normalize a :ref:`vec2 <vectwo>`. Store the result on itself.


   *Example:*
.. code-block:: lua

    local a = vec2:new(500,100)
    a:normalize()
    print(a)   -- x:0.9805807 y:0.1961161


.. data:: normalize(vec2 out)

    Normalize a :ref:`vec2 <vectwo>` and store the result in the :ref:`vec2 <vectwo>` out.

   :param vec2:  **out** as parameter.


   *Example:*
.. code-block:: lua

    local a = vec2:new(500,100)
    local b = vec2:new()
    a:normalize(b)
    print(a)   -- x:500 y:100
    print(b)   -- x:0.9805807 y:0.1961161


vec2 attributes
---------------

.. data:: x, y [read / write]

   Access the position :literal:`x` and :literal:`y` member from :ref:`vec2 <vectwo>`.

   *Example:*
.. code-block:: lua

   local v = vec2:new()
   v.x = 100
   v.y = 200

   print(v) -- x:100 y:200


.. _vectwolen:

.. data:: len [ read only ]

   | Call :literal:`len` method as member from :ref:`vec2 <vectwo>` for read only.
   | Same as :ref:`length <vectwolength>`

   *Example:*
.. code-block:: lua

   local v = vec2:new()
   v.x = 100
   v.y = 200

   print(v.len) -- 223.6068


User data acknowledgment
------------------------

   This table uses the first index to store the userdata :guilabel:`C++ class` internally, and
   is not allowed for :ref:`vec2 <vectwo>` to write / read to index.


.. code-block:: lua
   :linenos:
   :caption: **Do not do this:**
   :emphasize-lines: 4,6

   v = vec2:new()
   v:set(10,10)

   print(v[1]) -- read, okay will print userdata: 0x44958588a6a8 for example

   v[1] = 5 -- Error, not allowed for this table. program will crash!
