.. contents:: Table of Contents

.. _timer:

Table timer
===========

The table :ref:`timer <timer>` calls a function (callback) in a specific interval of time.


timer callback signature
------------------------

The following signature must be implemented to be used in a timer callback:

.. code-block:: lua

    function onTimeOut(self)
        -- self is the timer table
    end

timer methods
-------------

timer new
^^^^^^^^^

.. data:: new(function callback_timer, number time)

   Create a new instance of a :ref:`timer <timer>` passing the function callback and the interval of time to be called.

   :param string: **callback_timer** to the real function in lua.
   :param number: **interval** in seconds to call the callback.
   :return: :ref:`timer <timer>` *table*.

   *Example:*
.. code-block:: lua

    --important the function be defined before register the callback
    function on2Seconds(self)
      print('2 seconds has been passed...')
    end

    tTimer = timer:new(on2Seconds,2)

*Also the function might be anonymous:*

.. code-block:: lua

    tTimer = timer:new(function (self)
                print('2 seconds has been passed...')
            end, 2 )


.. data:: new(string callback_timer, number time)

   Create a new instance of a :ref:`timer <timer>` passing the function callback as string and the interval of time to be called.

   :param string: **callback_timer** string to real function in lua.
   :param number: **interval** in seconds to call the callback.
   :return: :ref:`timer <timer>` *table*.

   *Example:*
.. code-block:: lua

    --important the function be defined before register the callback
    function on2Seconds(self)
      print('2 seconds has been passed...')
    end

    tTimer = timer:new('on2Seconds',2)


.. _startTimer:

timer start
^^^^^^^^^^^

.. data:: start()

   Start the :ref:`timer <timer>`.

   If the timer was stopped using :ref:`stop <stopTimer>` function, the timer will begin from zero.

   If the timer was paused using :ref:`pause <pauseTimer>` function, the timer will take account the time passed before.

   *Example:*
.. code-block:: lua

    tTimer = timer:new(function (self) print('2 seconds has been passed...') end, 2 )
    tTimer:start()

.. _stopTimer:

.. data:: stop()

   Start the :ref:`timer <timer>`.

   Stop the timer callback and the internal timer.

   *Example:*
.. code-block:: lua

    tTimer = timer:new(function (self) print('I will be never called...') end, 2 )
    tTimer:stop()

.. _pauseTimer:

.. data:: pause()

   Pause the :ref:`timer <timer>`.

   Pause the timer callback. This method does not zero the internal timer. Next time the timer :ref:`start <startTimer>`, it will take account the time elapsed.

   *Example:*
.. code-block:: lua

    tTimer = timer:new(function (self) print('2 seconds has been passed..') end, 2 )
    tTimer:pause()


.. data:: restart()

   Restart the :ref:`timer <timer>`.

   If it was paused or stopped it will start again.

   The internal timer will be set to zero.

   *Example:*
.. code-block:: lua

    tTimer = timer:new(function (self) print('2 seconds has been passed..') end, 2 )
    tTimer:restart()


.. data:: isRunning()

   Check if the timer is running or not.

   :return: ``boolean`` *running*  -  ``true`` is is running, ``false`` if is not.

   If it was paused or stopped it will return ``false``.


   *Example:*
.. code-block:: lua

    tTimer = timer:new(function (self) print('2 seconds has been passed..') end, 2 )
    print('Timer is running:', tTimer:isRunning())


.. data:: set(number interval)

   Change the interval of timer callback

   :param number: **internal** of callback timer.

   *Example:*
.. code-block:: lua

    tTimer = timer:new(function (self) print('3 seconds has been passed..') end, 2 )
    tTimer:set(3)

.. data:: get()

   Retrieve the interval of the timer callback

   :return: ``number`` *interval*  - the interval of timer callback.


   *Example:*
.. code-block:: lua

    tTimer = timer:new(function (self) print('2 seconds has been passed..') end, 2 )
    print('Timer to callback:', tTimer:get())

.. data:: elapsed()

   Retrieve the time elapsed since the last callback.

   :return: ``number`` *elapsed* - the elapsed timer.


   *Example:*
.. code-block:: lua

    tTimer = timer:new(
        function (self)
            print('Timer elapsed:', tTimer:elapsed()) --always 0
        end, 2 )

    function loop(delta)
        print('Timer elapsed:', tTimer:elapsed()) -- here is possible to the timer elapsed
    end

.. data:: times()

   Retrieve how much times the callback has been called since the last :ref:`start <startTimer>`.

   :return: ``number`` *times*  - how much times the callback has been called.


   *Example:*
.. code-block:: lua

    tTimer = timer:new(
        function (self)
            print('Time number :', tTimer:times())
        end, 2 )

.. data:: setCallBack(string callback_function_name)

   Change the callback of timer passing the callback as string.

   :param string: **name** of new callback timer.

   *Example:*
.. code-block:: lua


    tTimer = timer:new(function (self) print('2 seconds has been passed..') end, 2 )

    function onTimer(self)
        print('New callback in action')
    end

    tTimer:setCallBack('onTimer')


.. data:: setCallBack(function callback_function)

   Change the callback of timer passing as argument.

   :param function: **callback** of new callback timer.

   *Example:*
.. code-block:: lua


    tTimer = timer:new(function (self) print('2 seconds has been passed..') end, 2 )

    function onTimer(self)
        print('New callback in action')
    end

    tTimer:setCallBack(onTimer)

    -- or as anonymous

    tTimer:setCallBack(
        function(self)
            print('New anonymous callback in action')
        end)


.. data:: destroy()

   Destroy a timer.

   *Example:*
.. code-block:: lua


    tTimer = timer:new(function (self) print('I will never be called') end, 2 )

    tTimer:destroy()
    tTimer = nil


timer attributes
----------------


   There is no attribute for :ref:`timer <timer>` table. So it is possible to add any tipe of attribute like ``number``, ``boolean``, ``string``, ``table`` or ``function``.

   *Example:*

.. code-block:: lua

   tTimer = timer:new(function (self) print('2 seconds has been passed..') end, 2 )
   tTimer.x = 100
   tTimer.y = 200

   print(tTimer.x,tTimer.y) -- 100 200


User data acknowledgment
------------------------

   This table uses the first index to store the userdata :guilabel:`C++ class` internally, however,
   for :ref:`timer <timer>`, is allowed to write / read to / from index.

.. Error::

   It is important to understand that as all tables used in this engine, this table uses the first index to store the userdata
   pointing to the :guilabel:`C++ class` internally.

   There is no mechanism to prevent to access the first index from :ref:`timer <timer>`. If you overload the userdata, probably the program will crash.


.. code-block:: lua
   :linenos:
   :caption: **Do not do this:**
   :emphasize-lines: 4,6

   tTimer = timer:new(function (self) print('2 seconds has been passed..') end, 2 )
   tTimer.x = 0

   print(tTimer[1]) -- read, okay will print userdata: 0x44958588a6a8 for example

   tTimer[1] = 5 -- Error, not allowed for this table. program will crash!