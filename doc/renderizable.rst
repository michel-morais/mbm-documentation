.. contents:: Table of Contents

.. _renderizable:

Table renderizable
==================

Table renderizable is a base class for all object that are 'renderizable' in the engine.

All table which inherit from renderizable will have these commons methods and attributes in this section.


Renderizable methods
--------------------

Renderizable setPos
^^^^^^^^^^^^^^^^^^^

.. data:: setPos(number x, number * y, number * z)

   Set new values to :ref:`renderizable <renderizable>`'s :ref:`position <vecthree>` passing three number arguments (x, y and z).

   :param  number: **x** value.
   :param  number: **y** value (optional).
   :param  number: **z** value (optional).

   *Example:*
.. code-block:: lua

   tTexture = texture:new('2dw')
   tTexture:setPos(200,500,-1)

.. data:: setPos(vec3 position)

   Set new values to :ref:`renderizable <renderizable>`'s :ref:`position <vecthree>` passing a :ref:`vec3 <vecthree>`.

   :param vec3: **position**.

   *Example:*
.. code-block:: lua

   tSprite = sprite:new('3d')
   v = vec3:new(200,500,500)
   tSprite:setPos(v) -- z is set to 500 because tSprite is 3d otherwise would not be set.

.. Note::

  | If the renderizable is 2d then z position is not set. You have to force if you want.
  | The reason behind this is because the method accepts another objects that can be renderizable as a parameter and, most of the time, in the 2D game, z would be wrong.
  | Example:

  .. code-block:: lua

      tSprite = sprite:new('3d')
      tSprite.z = 5
      -- or
      v = vec3:new(200,500,500)
      tSprite:setPos(v.x,v.y,v.z)
      print(tostring(tSprite:getPos())) -- x:200 y:500 z:500



.. data:: setPos(renderizable obj)

   Set new values to :ref:`renderizable <renderizable>`'s :ref:`position <vecthree>` passing other :ref:`renderizable <renderizable>`.

   :param renderizable: **render** object.

   *Example:*
.. code-block:: lua

   tSprite = sprite:new('2ds',0,0,-2)
   tTexture = texture:new('3d')
   tTexture:setPos(200,500,-1)
   tSprite:setPos(tTexture) --set position tSprite same as tTexture. z continues -2 because tSprite is 2d
   print(tostring(tSprite:getPos())) -- x:200 y:500 z:-2

.. Note::

  | If the renderizable is 2d then z position is not set. You have to force if you want.
  | The reason behind this is because the method accepts another objects that can be renderizable as a parameter and, most of the time, in the 2D game, z would be wrong.
  | Example:

  .. code-block:: lua

     tSprite = sprite:new('3d')
     tSprite.z = 5
     -- or
     v = vec3:new(200,500,600)
     tSprite:setPos(v.x,v.y,v.z)
     print(tostring(tSprite:getPos())) -- x:200 y:500 z:600

.. _get_position_renderizable:

Renderizable getPos
^^^^^^^^^^^^^^^^^^^

.. data:: getPos()

   Get a :ref:`vec3 position <vecthree>` from :ref:`renderizable <renderizable>`.

   :return: :literal:`vec3` - renderizable's position.

   *Example:*
.. code-block:: lua

   tTexture = texture:new('3d')
   local position = tTexture:getPos()
   position:set(220,100,1)

Renderizable setScale
^^^^^^^^^^^^^^^^^^^^^

.. data:: setScale(number sx, number * sy, number * sz)

   Set new values to :ref:`renderizable <renderizable>`'s :ref:`scale <vecthree>` passing three number arguments (sx, sy and sz).

   :param number: **sx** value.
   :param number: **sy** value (optional).
   :param number: **sz** value (optional).

   *Example:*
.. code-block:: lua

   tMesh = mesh:new('3d')
   tMesh:setScale(2,2,2)

.. data:: setScale(vec3 scale)

   Set new values to :ref:`renderizable <renderizable>`'s :ref:`scale <vecthree>` passing a :ref:`vec3 <vecthree>`.

   :param number: **vec3** scale.

   *Example:*
.. code-block:: lua

   tSprite = sprite:new('3d')
   v = vec3:new(1.5,1.5,1.5)
   tSprite:setScale(v)

Renderizable getScale
^^^^^^^^^^^^^^^^^^^^^

.. data:: getScale()

   Get a :ref:`vec3 scale <vecthree>` from :ref:`renderizable <renderizable>`.

   :return: :literal:`vec3` - renderizable's scale.

   *Example:*
.. code-block:: lua

   tTexture = texture:new('3d')
   local scale_texture = tTexture:getScale()
   scale_texture:set(3,3,1)


.. TODO: implement setAngle properly

Renderizable setAngle
^^^^^^^^^^^^^^^^^^^^^

.. data:: setAngle(number x, number * y, number * z)

   Set new values to :ref:`renderizable <renderizable>`'s :ref:`angle <vecthree>` passing three number arguments (x, y and z).

   :param number: **x** value.
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua

   tTexture = texture:new('2dw')
   tTexture:setAngle(0,0,math.rad(180))--180 degree on z axis

.. _TODO: Implemet set angle to vec3

.. data:: setAngle(renderizable render)

    Set new values to :ref:`renderizable <renderizable>`'s :ref:`angle <vecthree>` passing a :ref:`renderizable <renderizable>`.

   :param renderizable: **render** to set values from angle.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('3D')
   tTexture:setAngle(math.rad(90),0,math.rad(120))

   tSprite = sprite:new('3d')
   tSprite:setAngle(tTexture)

.. Note::

   | If the renderizable is 2d then only az angle is set. You have to force the other angles if desired.
   | The reason behind this is because the method accepts another objects that can be renderizable as a parameter and, most of the time, in the 2D game, the z angle would be the only one desired.
   | Example:

   .. code-block:: lua

      local tSprite = sprite:new('2DW')
      local tTexture =  texture:new('3D')

      tTexture:setAngle(6,7,8)
      tSprite.ax = 6
      -- or
      tSprite:setAngle(tTexture.ax,tTexture.ay,tTexture.az)
      print(tostring(tSprite:getAngle()))-- x:6 y:7 z:8

      tTexture:setAngle(1,2,3)
      tSprite:setAngle(tTexture) --will set only az because tSprite is 2d
      print(tostring(tSprite:getAngle()))-- x:6 y:7 z:3



Renderizable getAngle
^^^^^^^^^^^^^^^^^^^^^

.. data:: getAngle()

   Get a :ref:`vec3 angle <vecthree>` from :ref:`renderizable <renderizable>`.

   :return: :literal:`vec3` - renderizable's angle.

   *Example:*
.. code-block:: lua

   tTexture = texture:new('3d')
   local angle_texture = tTexture:getAngle()
   angle_texture:set(math.rad(90),math.rad(120),math.rad(180))


Renderizable move
^^^^^^^^^^^^^^^^^

.. data:: move(number x,number * y, number * z)

   Move x, y and z units frames by seconds. This method consider the FPS.

   :param number: **x** position.
   :param number: **y** position (optional).
   :param number: **z** position (optional).

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('3D')
   tTexture:move(10,20,30)

   --this is equivalent
   function loop(delta)
        tTexture.x = tTexture.x + (delta * 10)
        tTexture.y = tTexture.y + (delta * 20)
        tTexture.z = tTexture.z + (delta * 30)
   end



Renderizable isOver
^^^^^^^^^^^^^^^^^^^

.. data:: isOver(number x,number y, number * z)

   Check if the point (x,y) or (x,y,z) for 3D is over the bounding box of :ref:`renderizable <renderizable>`.

   :param number: **x** position.
   :param number: **y** position.
   :param number: **z** position (optional).
   :return: ``boolean`` - *result* if point is over bounding box of renderizable.

   *Example:*
.. code-block:: lua

   local tShape =  shape:new('2DW')
   tShape:create('rectangle',100,100)
   tShape:setPos(10,20)

   function onTouchDown(key,x,y)

      if tShape:isOver(x,y) then
         print('Is over my shape.',x,y)
      end
   end

.. Attention::
   Is expected x,y in 2d screen coordinates.


Renderizable collide
^^^^^^^^^^^^^^^^^^^^

.. data:: collide(renderizable other, boolean * useAABB)

   Check if two :ref:`renderizable <renderizable>` collide.

   :param renderizable: **render** to check the collision.
   :param boolean: **useAABB** consider the object rotation (optional).
   :return: ``boolean`` - *result* if the renderizable is colliding.

   *Example:*
.. code-block:: lua

   local mytex_a =  texture:new('2DW')
   mytex_a:setPos(10,20)
   mytex_a:load('#ff000000')
   mytex_a:setSize(100,100)

   local mytex_b =  texture:new('2DW')
   mytex_b:setPos(15,25)
   mytex_b:load('#ffff0000')
   mytex_b:setSize(100,100)

  if mytex_a:collide(mytex_b) then
     print('Collision (without AABB): true')
  else
     print('Collision (without AABB): false')
  end

  mytex_a.az = math.rad(90) --rotate the texture
  mytex_b.az = math.rad(-45) --rotate the texture

  if mytex_a:collide(mytex_b,true) then
     print('Collision (using AABB): true')
  else
     print('Collision (using AABB): false')
  end

.. data:: collide(number x, number y,number * z)

   Check if the point (x,y) or (x,y,z) for 3D is over the bounding box of :ref:`renderizable <renderizable>`.

   :param number: **x** in 2ds coordinate.
   :param number: **y** in 2ds coordinate.
   :param number: **z** in 2ds coordinate (for 3d objects).
   :return: ``boolean`` - *result* if the renderizable is colliding.

   *Example:*
.. code-block:: lua

   local mytex_a =  texture:new('2DW')
   mytex_a:setPos(10,20)
   mytex_a:load('#ff000000')
   mytex_a:setSize(100,100)

   function onTouchDown(key,x,y)
      if mytex_a:collide(x,x) then
         print('it collides ')
      end
   end


Renderizable rotate
^^^^^^^^^^^^^^^^^^^

.. data:: rotate(string angle, number radian)

   Rotate a :ref:`renderizable <renderizable>` considering the FPS making it smooth.

   :param string: **angle** :literal:`x`, :literal:`y` or :literal:`z`.
   :param number: **radian** to be rotated per seconds.

   *Example:*
.. code-block:: lua

   local mytex_a =  texture:new('2DW')
   mytex_a:setPos(10,20)
   mytex_a:load('#ff000000')
   mytex_a:setSize(100,100)

   function loop(delta)
      mytex_a:rotate('z',math.rad(360)) -- one complete turn per second
   end


Renderizable getSize
^^^^^^^^^^^^^^^^^^^^

.. data:: getSize(boolean * consider_scale )

   Get the size of :ref:`renderizable <renderizable>` **NOT** considering the rotation.
   It considers the scale.

   :param boolean: **consider_scale** default is ``true`` if not supplied.
   :return: ``number`` *width*, ``number`` *height*, ``number`` *depth* * - *dimension* of :ref:`renderizable <renderizable>`.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('2DW')
   tTexture:load('#ff000000')
   tTexture:setSize(100,100)
   local width,height = tTexture:getSize()
   print(width,height)

   local tMesh =  mesh:new('3D')
   tMesh:load('some_file')
   local width,height,depth = tTexture:getSize()
   print(width,height,depth)

.. Note::

   Only if the :ref:`renderizable <renderizable>` is ``3D`` there will be a depth value.


Renderizable getAABB
^^^^^^^^^^^^^^^^^^^^

.. data:: getAABB(boolean * recalculate)

   Get the size of :ref:`renderizable <renderizable>` considering the rotation.
   It considers the scale.

   :param boolean: **recalculate** is only need after resize the scale.
   :return: ``number`` width, ``number`` height, ``number`` depth* - *dimension* of renderizable.

   *Example:*
.. code-block:: lua

   local tTexture =  texture:new('2DW')
   tTexture:load('#ff000000')
   tTexture:setSize(100,100)
   local width,height = tTexture:getSize()
   print(width,height)

   local tMesh =  mesh:new('3D')
   tMesh:load('some_file')
   local width,height,depth = tTexture:getSize()
   print(width,height,depth)

.. Note::

   Only if the :ref:`renderizable <renderizable>` is ``3D`` there will be a depth value.

.. _isOnScreen:

Renderizable isOnScreen
^^^^^^^^^^^^^^^^^^^^^^^


.. data:: isOnScreen()

   Check if the :ref:`renderizable <renderizable>` is on screen.

   :return: ``boolean`` - *true* if the renderizable is on screen.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW',500,800)
   tSprite:load('tSprite.spt')

   function loop(delta)
      if tSprite:isOnScreen() then
         print('Sprite on screen: True')
      else
         print('Sprite on screen: False')
      end
   end


.. Attention::
   If you set the property ``visible`` as ``true`` and check the :ref:`isOnScreen <isOnScreen>` method next (same step), the result always will be ``true`` even if the object is not on screen.
   This happens because it has to wait for at least one loop to the engine update the objects that are on screen.


.. _isOnScreenNotWorking:

| *Here a example of a false positive logic as explained before:*
| *Note the* :sup:`highlighted` *lines bellow demonstrating the logic* :strong:`error`.

.. code-block:: lua
   :linenos:
   :emphasize-lines: 6,11

   local tSprite =  sprite:new('2DW',500,800)
   tSprite:load('tSprite.spt')

   function loop(delta)
      tSprite.x = -99999999 -- far far away from screen (isOnScreen should return false)
      tSprite.visible = true --force to be on screen

      -- tSprite:isOnScreen() is always true because the
      -- tSprite.visible it was set to true in the same step.
      -- next loop should be false (if not set again the visible property)
      if tSprite:isOnScreen() then
         print('Sprite on screen: True')
      else
         print('Sprite on screen: False')
      end
   end


Renderizable isLoaded
^^^^^^^^^^^^^^^^^^^^^

.. data:: isLoaded()

   Check if the :ref:`renderizable <renderizable>` is loaded.

   :return: ``boolean`` - *true* if the renderizable is loaded.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW',500,800)
   tSprite:load('tSprite.spt')

   if tSprite:isLoaded() then
      print('Sprite is loaded')
   else
      print('Sprite is not loaded')
   end

.. :TODO: check if the name  has the extension

.. _onEndFx:

Renderizable onEndFx
^^^^^^^^^^^^^^^^^^^^

   Set a :ref:`renderizable <renderizable>`'s :ref:`shader effect <shader>` callback.

   The callback must have the following signature:

.. code-block:: lua

   function callBack(renderizable self,string shader_fileName)

.. Note::

   Some shader effect may never end because it is flagged as ``loop``.

   See animation table :ref:`animation table <animation>`.

.. data:: onEndFx(string)

   Set a :ref:`renderizable <renderizable>`'s :ref:`shader effect <shader>` callback passing a string name from a function defined somewhere.

   :param string: function name callback.

   *Example:*
.. code-block:: lua

   --example callback
   function fFxCallBack(self,shader_fileName)
      if shader_fileName == 'bloom.ps' then
         self:setAnim('destroy')
      end
   end

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:onEndFx('fFxCallBack')
   tSprite:setAnim('fall')


.. data:: onEndFx(function)

   Set a :ref:`renderizable <renderizable>`'s  :ref:`shader effect <shader>` callback passing a function.

   :param function: callback.

   *Example:*
.. code-block:: lua

   --example callback
   function fFxCallBack(self,shader_fileName)
      if shader_fileName == 'bloom.ps' then
         self:setAnim('destroy')
      end
   end

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:onEndFx(fFxCallBack)--named function


   local otherSprite =  sprite:new('2DW')
   otherSprite:load('tSprite.spt')
   otherSprite:onEndFx(
      function (self,shader_fileName)      --anonymous function
         if shader_fileName == 'bloom.ps' then
            self:setAnim('destroy')
         end
      end
   )


Renderizable forceEndAnimFx
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: forceEndAnimFx(boolean bEndAnim,boolean bEndFx)

   Force to end an :ref:`animation <animation>` and / or :ref:`shader effect <shader>` from a :ref:`renderizable <renderizable>`.

   :param boolean: **end** animation.
   :param boolean: **end** effect shader.

   *Example:*
.. code-block:: lua

   --example callback
   function fFxCallBack(self,shader_fileName)
      if shader_fileName == 'bloom.ps' then
         self:setAnim('destroy')
      end
   end

   --example callback
   function myCallBackSprite(self,nameAnimation)
      if nameAnimation == 'fall' then
         self:setAnim('destroy')
      end
   end

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:onEndAnim(myCallBackSprite)--named function
   tSprite:onEndFx(fFxCallBack)--named function
   tSprite:forceEndAnimFx(true,true) --force and both (animation and fx) and do a callback to functions.


Renderizable getTotalFrame
^^^^^^^^^^^^^^^^^^^^^^^^^^


.. data:: getTotalFrame()

   Retrieve the total frame from a :ref:`renderizable <renderizable>` independent of animation.

   :return: ``number`` - *total* frame from renderizable.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   local iTotal = tSprite:getTotalFrame()


.. _destroy:

Renderizable destroy
^^^^^^^^^^^^^^^^^^^^


.. data:: destroy()

   Force to destroy an object :ref:`renderizable <renderizable>`.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:destroy() -- can not be used anymore
   tSprite = nil     -- so set to nil be be collected ASAP

.. Note::

   When an object destroyed it does not means that will be collected immediately. To force that you can call ``collectgarbage`` from LUA API.


.. _blend_state:

Blending
--------

  Blend state or Blending is the stage of OpenGL rendering pipeline that
  takes the fragment color outputs from the Fragment Shader and combines
  them with the colors in the color buffers
  that these outputs map to.

  Blending parameters can allow the source and destination colors for each output to be combined in various ways.

  The color ``S`` is the source color; the color ``D`` is the destination color; the color ``O`` is the output color that is written to the buffer.

  The ``S``, ``D``, and so forth represent all of the components of that color.

  ``Srgb`` represents only the ``RGB`` components of the source color. Da represents the alpha component of the destination color.

  You can learn more at `wiki Blending <https://www.khronos.org/opengl/wiki/Blending>`__.

   Blend operation can also be accessed by :ref:`shader blend state <shader_blend_operation>` methods.

.. _blend_operation:

  +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
  |    Function       |  Explanation                                                                                                                    |
  +===================+=================================================================================================================================+
  | ADD               | | The source and destination colors are added to each other. ``O = sS + dD``.                                                   |
  |                   | | The ``s`` and ``d`` are blending parameters that are multiplied into each of ``S`` and ``D`` before the addition.             |
  +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
  | SUBTRACT          | | Subtracts the destination from the source. ``O = sS - dD``.                                                                   |
  |                   | | The source and dest are multiplied by blending parameters.                                                                    |
  +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
  | REVERSE_SUBTRACT  | | Subtracts the source from the destination. ``O = dD - sS``.                                                                   |
  |                   | | The source and dest are multiplied by blending parameters.                                                                    |
  +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
  | MIN               | | The output color is the component-wise minimum value of the source and dest colors.                                           |
  |                   | | So performing ``GL_MIN`` in the ``RGB`` equation means that ``Or = min(Sr, Dr), Og = min(Sg, Dg)``, and so forth.             |
  |                   | | The parameters s and d are ignored for this equation.                                                                         |
  +-------------------+---------------------------------------------------------------------------------------------------------------------------------+
  | MAX               | | The output color is the component-wise maximum value of the source and dest colors.                                           |
  |                   | | The parameters ``s`` and ``d`` are ignored for this equation.                                                                 |
  +-------------------+---------------------------------------------------------------------------------------------------------------------------------+

  Following the constant table for blend state used by this engine:

  +---------------------------------------------+
  |             Blend State                     |
  +---------------------+-----------------------+
  |    Constant Name    |  String Name          |
  +=====================+=======================+
  |   DISABLE           |     disable           |
  +---------------------+-----------------------+
  |   ZERO              |     zero              |
  +---------------------+-----------------------+
  |   ONE               |     one               |
  +---------------------+-----------------------+
  |   SRC_COLOR         |     src_color         |
  +---------------------+-----------------------+
  |   INV_SRC_COLOR     |     inv_src_color     |
  +---------------------+-----------------------+
  |   SRC_ALPHA         |     src_alpha         |
  +---------------------+-----------------------+
  |   INV_SRC_ALPHA     |     inv_src_alpha     |
  +---------------------+-----------------------+
  |   DEST_ALPHA        |     dest_alpha        |
  +---------------------+-----------------------+
  |   INV_DEST_ALPHA    |     inv_dest_alpha    |
  +---------------------+-----------------------+
  |   DEST_COLOR        |     dest_color        |
  +---------------------+-----------------------+
  |   INV_DEST_COLOR    |     inv_dest_color    |
  +---------------------+-----------------------+


Following the constant table for blend operation used by this engine:

  +---------------------------------------------+
  |             Blend Operation                 |
  +---------------------+-----------------------+
  |    Constant Name    |  String Name          |
  +=====================+=======================+
  |   ADD               |     ADD               |
  +---------------------+-----------------------+
  |   SUBTRACT          |     SUBTRACT          |
  +---------------------+-----------------------+
  |   REVERSE_SUBTRACT  |     REVERSE_SUBTRACT  |
  +---------------------+-----------------------+
  |   MIN               |     MIN               |
  +---------------------+-----------------------+
  |   MAX               |     MAX               |
  +---------------------+-----------------------+



Blending setBlend
^^^^^^^^^^^^^^^^^

.. data:: setBlend(string name)

   Set the :ref:`blend state <blend_state>` to a :ref:`renderizable <renderizable>`.

   :param string: **name** regard the :ref:`blend state <blend_state>`.

   *Example:*
.. code-block:: lua

   local tShape =  shape:new('2DW')
   tShape:create('circle')
   tShape:setBlend('inv_src_color') -- same as mbm.INV_SRC_COLOR
   local nameBlend, constantBlend = tShape:getBlend()
   print(nameBlend) -- inv_src_color
   if constantBlend == mbm.INV_SRC_COLOR then
      print('expected constant: INV_SRC_COLOR')
   end

.. figure:: _static/blend_constant_set.png
   :align: center
   :figclass: align-center

   setting blend  by string name



.. data:: setBlend(number constant)

   Set the :ref:`blend state <blend_state>` to a :ref:`renderizable <renderizable>`.

   :param number: **index** regard the :ref:`blend state <blend_state>`.

   *Example:*
.. code-block:: lua

   local tShape =  shape:new('2DW')
   tShape:create('circle')
   tShape:setBlend(mbm.DEST_ALPHA) -- same as 'dest_alpha'
   local nameBlend, constantBlend = tShape:getBlend()
   print(nameBlend) -- dest_alpha
   if constantBlend == mbm.DEST_ALPHA then
      print('expected constant: DEST_ALPHA')
   end

.. figure:: _static/blend_constant_get.png
   :align: center
   :figclass: align-center

   setting blend  by string name


Blending getBlend
^^^^^^^^^^^^^^^^^

.. data:: getBlend()

   Get the :ref:`blend state <blend_state>` from a :ref:`renderizable <renderizable>`.

   :return: ``string`` *name*, ``number`` index - *blend state* of renderizable.

   *Example:*

.. code-block:: lua

   local tShape =  shape:new('2DW')
   tShape:create('circle')
   local nameBlend, constantBlend = tShape:getBlend()
   print(nameBlend) -- DISABLE
   if constantBlend == mbm.DISABLE then
      print('expected constant: DISABLE')
   end

.. figure:: _static/blend_constant_get_2.png
   :align: center
   :figclass: align-center

   getting blend

Blend Operation
^^^^^^^^^^^^^^^

Blend operation is part of :ref:`shader <shader>`.

See :ref:`shader blend operation <shader_blend_operation>`.

.. _animation:

Animation
---------

   | All inheritable table from :ref:`renderizable <renderizable>` might have one or more animation.
   | An animation is basically a change of frames regularly.
   | An can be added through the method :ref:`addAnim <addAnim>`.
   | Each animation has its own :ref:`shader <shader>`.
   |
   | An animation changes the frame regularly according to this table:

  +------------------------+--------------------------------------------------------+------------------+
  |  | Animation           |  | Explanation                                         | | callback end   |
  |  | Name                |  |                                                     | | of animation ? |
  +========================+========================================================+==================+
  | ``PAUSED``             | | The animation is paused                              |    ``false``     |
  +------------------------+--------------------------------------------------------+------------------+
  | ``GROWING``            | | Increment the frames till the final frame.           |    ``true``      |
  |                        | | Stop animation in the final frame.                   |                  |
  +------------------------+--------------------------------------------------------+------------------+
  | ``GROWING_LOOP``       | | Increment the frames till the final frame.           |    ``false``     |
  |                        | | repeat when reach the final frame.                   |                  |
  +------------------------+--------------------------------------------------------+------------------+
  | ``DECREASING``         | | Decrement the frames till the initial frame.         |    ``true``      |
  |                        | | Stop animation in the initial frame.                 |                  |
  +------------------------+--------------------------------------------------------+------------------+
  | ``DECREASING_LOOP``    | | Decrement the frames till the initial frame.         |    ``false``     |
  |                        | | repeat when reach the initial frame.                 |                  |
  +------------------------+--------------------------------------------------------+------------------+
  | ``RECURSIVE``          | | Increment the frames till the final frame.           |    ``true``      |
  |                        | | Decrement the frames till the initial frame.         |                  |
  |                        | | Stop animation in the initial frame.                 |                  |
  +------------------------+--------------------------------------------------------+------------------+
  | ``RECURSIVE_LOOP``     | | Increment the frames till the final frame.           |    ``true``      |
  |                        | | Decrement the frames till the initial frame.         |                  |
  |                        | | repeat when reach the initial frame.                 |                  |
  +------------------------+--------------------------------------------------------+------------------+

  | All member are constant and can be accessible through ``mbm`` table.

*Example:*

.. code-block:: lua

   tShape = shape:new('2dw')
   tShape:create('Circle')

   --Added an animation type GROWING
   tShape:addAnim('rolling', mbm.GROWING)


   local tShader = tShape:getShader()

   if tShader:load(nil,'scale.vs',1,0,2.0,0) then
      --Added an animation type GROWING_LOOP to shader
      tShader:setVStype(mbm.GROWING_LOOP)
   end

   local nameAnim, indexAnim = tShape:getAnim()
   print(nameAnim, indexAnim) -- 'rolling', 2


Animation setAnim
^^^^^^^^^^^^^^^^^

.. data:: setAnim(string name)

   Set the :ref:`animation <animation>` to a :ref:`renderizable <renderizable>`.

   :param string: **name** of animation.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:setAnim('walk')


.. data:: setAnim(number index)

   Set the :ref:`animation <animation>` to a :ref:`renderizable <renderizable>`.

   :param number: **index** of animation (one based index).

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:setAnim(1) --first animation


.. _getAnimationRenderizable:

Animation getAnim
^^^^^^^^^^^^^^^^^

.. data:: getAnim(number * index_anim)

   Get the name and index (1 based) of the :ref:`animation <animation>` from a :ref:`renderizable <renderizable>`.

   :param number: **index** of animation (one based index). If not supplied get the current.
   :return: ``string`` *name*, ``number`` *index* (1 based) - *animation* of renderizable.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   local nameAnim, indexAnim = tSprite:getAnim()
   print(nameAnim, indexAnim) -- 'walk 0' for current animation 1


Animation setTypeAnim
^^^^^^^^^^^^^^^^^^^^^

.. data:: setTypeAnim(number type_animation)

   Set a new type to :ref:`animation <animation>`.

   :param number: **type** of animation.
   :return: ``number`` *old_type*  - type of animation.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   local iOldTypeAnimation = tSprite:setTypeAnim(mbm.PAUSED) -- pause the current animation
   tSprite:setTypeAnim(iOldTypeAnimation) -- set animation type defined previously

Animation restartAnim
^^^^^^^^^^^^^^^^^^^^^

   Restart an :ref:`renderizable <renderizable>`'s :ref:`animation <animation>`.


 *Example:*

.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:restartAnim()


.. _onEndAnim:

Animation onEndAnim
^^^^^^^^^^^^^^^^^^^

   Set a :ref:`renderizable <renderizable>`'s :ref:`animation <animation>` callback.

   The callback must have the following signature:

.. code-block:: lua

   function callBack(renderizable self,string name_animation)

.. Note::

   Some animation may never end because it is flagged as ``loop``.

   See animation table :ref:`animation table <animation>`.

.. data:: onEndAnim(string)

   Set a :ref:`renderizable <renderizable>`'s :ref:`animation <animation>` callback passing a string name from a function defined somewhere.

   :param string: function name callback.

   *Example:*
.. code-block:: lua

   --example
   function myCallBackSprite(self,nameAnimation)
      if nameAnimation == 'fall' then
         self:setAnim('destroy')
      end
   end

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:onEndAnim('myCallBackSprite')
   tSprite:setAnim('fall')

   local otherSprite =  sprite:new('2DW')
   otherSprite:load('tSprite.spt')
   otherSprite:onEndAnim('myCallBackSprite')
   otherSprite:setAnim('fall')

.. data:: onEndAnim(function)

   Set a :ref:`renderizable <renderizable>`'s :ref:`animation <animation>` callback passing a function.

   :param function: callback.

   *Example:*
.. code-block:: lua

   --example
   function myCallBackSprite(self,nameAnimation)
      if nameAnimation == 'fall' then
         self:setAnim('destroy')
      end
   end

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:onEndAnim(myCallBackSprite)--named function
   tSprite:setAnim('fall')

   local otherSprite =  sprite:new('2DW')
   otherSprite:load('tSprite.spt')
   otherSprite:onEndAnim(
      function (self,nameAnimation)      --anonymous function
         if nameAnimation == 'fall' then
            self:setAnim('destroy')
         end
      end
   )
   otherSprite:setAnim('fall')


Animation isEndedAnim
^^^^^^^^^^^^^^^^^^^^^

.. data:: isEndedAnim()

   Check if the :ref:`animation <animation>` from a :ref:`renderizable <renderizable>` has ended the animation.

   :return: ``boolean`` - *true* if animation has ended.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')

   function loop(delta)
      if tSprite:isEndedAnim() then
         print('Animation has ended')
         tSprite:restartAnim()
      end
   end

Animation getIndexFrame
^^^^^^^^^^^^^^^^^^^^^^^

.. data:: getIndexFrame()

   Return which index frame is currently to the :ref:`renderizable <renderizable>` (1 based).

   :return: ``number`` - *index* of frame from current :ref:`renderizable <renderizable>`.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')

   function loop(delta)
      if tSprite:getIndexFrame() == 2 then
         print('reach index 2 from animation')
         tSprite:restartAnim()
      end
   end



.. _setTexture:

Animation setTexture
^^^^^^^^^^^^^^^^^^^^


.. data:: setTexture(string file_name_texture, boolean* alpha, number* stage)

   Set a new texture for a :ref:`renderizable <renderizable>`. It considers the current animation.

   :param string:  **file name texture** to be set to current animation.
   :param boolean: **alpha** apply alpha if texture not loaded before. Default is ``true``.
   :param number:  **stage** to apply. (``1`` or ``2``). Default is ``1``.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:setTexture('mario.png',true,1)


.. Note::

   - Stage 1 means apply the texture for the current animation
       - Each animation hold the texture for stage 1.
   - Stage 2 means apply the texture for the current shader.
       - If the current shader does not use the texture stage 2, nothing happens.

.. data:: setTexture(number red, number green, number blue, number * alpha)

   Set a new solid texture for a :ref:`renderizable <renderizable>`. It considers the current animation.

   :param number:  **red** color (0-1).
   :param number:  **green** color (0-1).
   :param number:  **blue** color (0-1).
   :param number:  **alpha** color (0-1) (optional, default 1).

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:setTexture(1,0,0) --Apply a solid (red color) to the sprite


.. data:: setColor(number red, number green, number blue, number * alpha)

   Set a new solid texture for a :ref:`renderizable <renderizable>`. It considers the current animation.

   :param number:  **red** color (0-1).
   :param number:  **green** color (0-1).
   :param number:  **blue** color (0-1).
   :param number:  **alpha** color (0-1) (optional, default 1).

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   tSprite:setColor(1,0,0) --Apply a solid (red color) to the sprite (same as setTexture)


Animation getTotalAnim
^^^^^^^^^^^^^^^^^^^^^^


.. data:: getTotalAnim()

   Retrieve the total animation from a :ref:`renderizable <renderizable>`.

   :return: ``number`` - *total* animation for the renderizable.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   local iTotal = tSprite:getTotalAnim()


.. _addAnim:

Animation addAnim
^^^^^^^^^^^^^^^^^


.. data:: addAnim(string* name, number* type, number* start_frame, number* final_Frame, number * interval)

   Create a new animation to a :ref:`renderizable <renderizable>` based on :ref:`animation table <animation>`.

   :param string:  **name** of animation.
   :param number:  **type** of animation (see :ref:`animation table index type <animation>`).
   :param number:  **start frame** of animation.
   :param number:  **final frame** of animation.
   :param number:  **interval** between frames of animation.
   :return: ``string`` *name*, ``number`` *index* (1 based) - *animation* of renderizable created.


   *Example:*

.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('sprite.spt')
   tSprite:addAnim('walk',mbm.GROWING_LOOP,1,5,0.2)


.. TODO: check if getShader change when change animation

.. _getShader:

Animation getShader
^^^^^^^^^^^^^^^^^^^


.. data:: getShader()

   Retrieve the :ref:`shader <shader>` used by :ref:`renderizable <renderizable>` for the current animation.

   :return: ``shader`` - from :ref:`renderizable <renderizable>`.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   local shader = tSprite:getShader()


Physics
-------

It is possible to retrieve the physics information applied to :ref:`renderizable <renderizable>` however it is **not** possible to change in the mesh. You only can change the info retrieved.


.. data:: getPhysics()

   :return: ``table`` - array with each physics applied to this :ref:`renderizable <renderizable>`.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite:load('tSprite.spt')
   local tInfoPhysics = tSprite:getPhysics()
   for i=1, #tInfoPhysics do
      print(tInfoPhysics[i].type)
      if tInfoPhysics[i].type == 'cube' then
         print('center x:',tInfoPhysics[i].x)
         print('center y:',tInfoPhysics[i].y)
         print('center z:',tInfoPhysics[i].z)
         print('width:',   tInfoPhysics[i].width)
         print('height:',  tInfoPhysics[i].height)
         print('depth:',   tInfoPhysics[i].depth)
      elseif tInfoPhysics[i].type == 'sphere' then
         print('center x:',tInfoPhysics[i].x)
         print('center y:',tInfoPhysics[i].y)
         print('center z:',tInfoPhysics[i].z)
         print('ray:',     tInfoPhysics[i].ray)
      elseif tInfoPhysics[i].type == 'triangle' then
         print('a x:',tInfoPhysics[i].a.x)
         print('a y:',tInfoPhysics[i].a.y)
         print('a z:',tInfoPhysics[i].a.z)
         print('b x:',tInfoPhysics[i].b.x)
         print('b y:',tInfoPhysics[i].b.y)
         print('b z:',tInfoPhysics[i].b.z)
         print('c x:',tInfoPhysics[i].c.x)
         print('c y:',tInfoPhysics[i].c.y)
         print('c z:',tInfoPhysics[i].c.z)
      elseif tInfoPhysics[i].type == 'complex' then
         print('a x:',tInfoPhysics[i].a.x)
         print('a y:',tInfoPhysics[i].a.y)
         print('a z:',tInfoPhysics[i].a.z)
         print('b x:',tInfoPhysics[i].b.x)
         print('b y:',tInfoPhysics[i].b.y)
         print('b z:',tInfoPhysics[i].b.z)
         print('c x:',tInfoPhysics[i].c.x)
         print('c y:',tInfoPhysics[i].c.y)
         print('c z:',tInfoPhysics[i].c.z)
         print('d x:',tInfoPhysics[i].d.x)
         print('d y:',tInfoPhysics[i].d.y)
         print('d z:',tInfoPhysics[i].d.z)
         print('e x:',tInfoPhysics[i].e.x)
         print('e y:',tInfoPhysics[i].e.y)
         print('e z:',tInfoPhysics[i].e.z)
         print('f x:',tInfoPhysics[i].f.x)
         print('f y:',tInfoPhysics[i].f.y)
         print('f z:',tInfoPhysics[i].f.z)
         print('g x:',tInfoPhysics[i].g.x)
         print('g y:',tInfoPhysics[i].g.y)
         print('g z:',tInfoPhysics[i].g.z)
         print('h x:',tInfoPhysics[i].h.x)
         print('h y:',tInfoPhysics[i].h.y)
         print('h z:',tInfoPhysics[i].h.z)
      end
   end


Renderizable attributes
-----------------------

.. data:: x, y, z [read / write]

   Access the position :literal:`x`, :literal:`y` and :literal:`z` member from :ref:`renderizable <renderizable>`.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite.x = 100
   tSprite.y = 200
   tSprite.z = -1

   print(tSprite.x,tSprite.y,tSprite.z) -- 100 200 -1


.. data:: sx, sy, sz [read / write]

   Access the scale :literal:`sx`, :literal:`sy` and :literal:`sz` member from :ref:`renderizable <renderizable>`.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite.sx = 0.8
   tSprite.sy = 0.8
   tSprite.sz = 1

   print(tSprite.sx,tSprite.sy,tSprite.sz) -- 0.8 0.8 1


.. data:: ax, ay, az [read / write]

   Access the angle :literal:`ax`, :literal:`ay` and :literal:`az` member from :ref:`renderizable <renderizable>`.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite.ax = math.rad(180)
   tSprite.ay = math.rad(90)
   tSprite.az = math.rad(45)

   print(math.deg(tSprite.ax),math.deg(tSprite.ay),math.deg(tSprite.az)) -- 180 90 45

.. Note::

   The angle is radian.


.. data:: visible [read / write]

   Enable or disable the :ref:`renderizable <renderizable>` to be rendered.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite.visible = false --turn invisible the object


.. Attention::
   If you set the property ``visible`` as ``true`` and check the :ref:`isOnScreen <isOnScreen>` method next (same step), the result always will be ``true`` even if the object is not on screen.
   This happens because it has to wait for at least one loop to the engine update the objects that are on screen.

   More information at :ref:`isOnScreen <isOnScreenNotWorking>` explanation.


.. data:: alwaysRender [read / write]

   Force always render the :ref:`renderizable <renderizable>` even if is not at the screen.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')
   tSprite.alwaysRender = true -- always will be renderized


.. data:: 'variable' [read / write]

   Create a variable read/write to :ref:`renderizable <renderizable>`. The type allowed is :literal:`number`, :literal:`string`, :literal:`boolean`, :literal:`table` and :literal:`function`.

   *Example:*
.. code-block:: lua

   local tSprite =  sprite:new('2DW')

   function myPrint(self,message)
      print('message:', message or '')
   end

   tSprite.myFloat = 3.151413
   tSprite.myInt = 9
   tSprite.myString = 'Hi there!'
   tSprite.myTable = {msg = 'Hi there!'}
   tSprite.myFunction = myPrint
   print('Accessing the variables myFloat:',tSprite.myFloat)
   print('Accessing the variables myInt:',tSprite.myInt)
   print('Accessing the variables myString:',tSprite.myString)
   print('Accessing the variables myTable:',tSprite.myTable.msg)
   tSprite:myPrint('Hello!')


User data acknowledgment
------------------------

   This table uses the first index to store the userdata :guilabel:`C++ class` internally.
   So, if you want to store some values to the table you must use from the second element as exampled bellow:

.. code-block:: lua
   :linenos:
   :caption: *You can do this:*
   :emphasize-lines: 4,6

   tTexture = texture:new('2ds')
   tTexture:setPos(12,5)

   tTexture[2] = 'some value var'

   print(tTexture[2]) -- 'some value var'


.. Error::

   It is important to understand that as all tables used in this engine, this table uses the first index to store the userdata
   pointing to the :guilabel:`C++ class` internally.

   There is no mechanism to prevent to access the first index from :ref:`renderizable <renderizable>`. If you overload the userdata, probably the program will crash.

.. code-block:: lua
   :linenos:
   :caption: **Do not do this:**
   :emphasize-lines: 4,6

   tTexture = texture:new('2dw')
   tTexture:setPos(100,300,-1)

   print(tTexture[1]) -- read, okay will print userdata: 0x55957888a6a8 for example

   tTexture[1] = 2 -- Error, this will do your program crash!
