.. contents:: Table of Contents



Table camera
============
.. _camera:

Table global
instanced by engine to access both camera 2D and 3D to the application.
A camera have a scale to make easy to work on device.

|  Use :ref:`getCamera <getCamera>` method to get a camera ``2DW`` supplying ``2D`` string as parameter.
|  Use :ref:`getCamera <camera_threed>` method to get a camera ``3D`` supplying ``3D`` string as parameter.

Camera 2D methods
-----------------

camera 2d setPos
^^^^^^^^^^^^^^^^
.. data:: setPos(number x,number y)

   Set position using x and y (2D world) position.
   This method consider camera's :ref:`scale <scale_camera>`.

   :param number: **x** position.
   :param number: **y** position.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   camera2D:setPos(0,0)


.. data:: setPos(table renderizable)

   Set position using a :ref:`renderizable <renderizable>` object position.
   This method consider camera's :ref:`scale <scale_camera>`.

   :param table: **renderizable** object.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   tSprite = sprite:new('2d')
   tSprite:setPos(500,300)
   camera2D:setPos(tSprite)

.. data:: setPos(vec2 vector)

   Set position using a :ref:`vec2 <vectwo>` position.
   This method consider camera's :ref:`scale <scale_camera>`.

   :param table: **vec2** vector.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   tVec2 = vec2:new(100,20)
   camera2D:setPos(tVec2)


camera 2d getRealPos
^^^^^^^^^^^^^^^^^^^^

.. data:: getRealPos()

   Return a :ref:`vec2 <vectwo>` which point to the real position (without scale) to the camera 2D.

   :return: :literal:`vec2` - *position* to the camera 2D.

   *Example:*
.. code-block:: lua

    camera2D = mbm.getCamera('2d')
    local posCamera2d = camera2D:getRealPos()
    --change the position directly
    posCamera2d.x = 0
    posCamera2d.y = 10


camera 2d move
^^^^^^^^^^^^^^

.. _cameratwodmove:

.. data:: move(number x,number * y)

   Move x and y units frames by seconds. This method consider the FPS.

   :param number: **x** position.
   :param number: **y** position.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   camera2D:move(100,50)

   --this is equivalent
   function loop(delta)
        camera2d.x = camera2d.x + (delta * 100)
        camera2d.y = camera2d.y + (delta * 50)
   end


.. data:: move(vec2 position)

   Move x and y units frames by seconds using a :ref:`vec2 <vectwo>`. This method consider the FPS.

   :param number: **vec2** position.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   vec = vec2:new(100,50)
   camera2D:move(vec)

   --this is equivalent
   function loop(delta)
        camera2d.x = camera2d.x + (delta * vec.x)
        camera2d.y = camera2d.y + (delta * vec.y)
   end


camera 2d scaleToScreen
^^^^^^^^^^^^^^^^^^^^^^^

.. _scale_camera:
.. data:: scaleToScreen(number width, number height,*string axis)

   Modify the camera 2D scale passing the expected :literal:`screen width` and :literal:`screen height`.
   The axis instruct to consider the :literal:`x` axis, :literal:`y` axis or both :literal:`xy` axis.
   The :literal:`xy` axis means that we want to stretch the screen to the client size.
   If not feed the :literal:`axis` the engine will decide the better axis based on screen size of client.
   The method to get the scaled size screen is :ref:`getSizeScreen <getSizeScreen>`.

   :param number: **width** screen expected.
   :param number: **height** screen expected.
   :param string: **axis** to apply the scale.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   --cause a stretch on screen
   camera2d:scaleToScreen(1024,768,'xy')

   --our application always will fit to the screen on y axis
   camera2d:scaleToScreen(800,600,'y')

   --our application always will fit to the screen on x axis
   camera2d:scaleToScreen(1920,1080,'x')


.. Note::
  Scale to screen allows the application has the same 'look' for different screen size.

  - Let's take a look at the table bellow understand better.

   - The camera 2D is set to resolution (width x height) (scaleToScreen) method.
   - The client width and client height is get by the method getSizeScreen (after set the expected width and height).
   - The scaled width and scaled height is the size adapted to the client.
   - The scale sx and scale sy is camera's attribute.

    +---------+-----------+-------+----------------+-----------------+-------------+----------------+-------------------+-------------------+
    | width   |  height   | axis  | client width   |  client height  |scaled width | scaled height  |  camera scale 'sx'|  camera scale 'sy'|
    +=========+===========+=======+================+=================+=============+================+===================+===================+
    |         |           |   xy  |                |                 |   800       |    600         |      1.0          |       1.0         |
    |         |           +-------+                |                 +-------------+----------------+-------------------+-------------------+
    |         |           |   x   |                |                 |   800       |    600         |      1.0          |       1.0         |
    |         |           +-------+                |                 +-------------+----------------+-------------------+-------------------+
    | 800     |  600      |   y   | 800            |  600            |   800       |    600         |      1.0          |       1.0         |
    +---------+-----------+-------+----------------+-----------------+-------------+----------------+-------------------+-------------------+
    |         |           |   xy  |                |                 |   1024      |    768         |      0.78125      |       0.78125     |
    |         |           +-------+                |                 +-------------+----------------+-------------------+-------------------+
    |         |           |   x   |                |                 |   1024      |    768         |      0.78125      |       0.78125     |
    |         |           +-------+                |                 +-------------+----------------+-------------------+-------------------+
    | 1024    |  768      |   y   | 800            |  600            |   1024      |    768         |      0.78125      |       0.78125     |
    +---------+-----------+-------+----------------+-----------------+-------------+----------------+-------------------+-------------------+
    |         |           |   xy  |                |                 |   1920      |    1080        |      0.5333334    |       0.7111111   |
    |         |           +-------+                |                 +-------------+----------------+-------------------+-------------------+
    |         |           |   x   |                |                 |   1920      |    1440        |      0.5333334    |       0.5333334   |
    |         |           +-------+                |                 +-------------+----------------+-------------------+-------------------+
    | 1920    |  1080     |   y   | 1024           |  768            |   1440      |    1080        |      0.7111111    |       0.7111111   |
    +---------+-----------+-------+----------------+-----------------+-------------+----------------+-------------------+-------------------+



Camera 2D attributes
--------------------

.. data:: x, y [read / write]

   Access the position :literal:`x` and :literal:`y` member from :ref:`camera <camera>`.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   camera2D.x = 500
   camera2D.y = 250
   print('X:',camera2D.x,'Y:',camera2D.y)


.. data:: sx, sy [read / write]

   Access the scale :literal:`sx` and :literal:`sy` member from :ref:`camera <camera>`.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   camera2D.sx = 1.5
   camera2D.sy = 1.0
   print('Scale X:',camera2D.sx,'Scale Y:',camera2D.sy)

.. data:: 'variable' [read / write]

   Create a variable to read/write to :ref:`camera <camera>`.
   The type allowed is :literal:`number`, :literal:`string`, :literal:`boolean`, and :literal:`table` and :literal:`function`.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   camera2D.myFloat = 1.5
   camera2D.myInt = 5
   camera2D.myString = 'Hello there!'
   print('Accessing the variables myFloat:',camera2D.myFloat)
   print('Accessing the variables myInt:',camera2D.myInt)
   print('Accessing the variables myString:',camera2D.myString)

.. Tip::
  The variable is stored internally and belongs to the camera.
  So if we create a variable to the camera and load a new scene, then the variables will NOT be available because it is other scene.
  If you want to keep variable between scenes use :ref:`setGlobal <setGlobal>` and :ref:`getGlobal <getGlobal>` for that.


.. Error::

   It is important to understand that as all tables used in this engine, this table uses the first index from table to store the userdata
   pointing to the :guilabel:`C++ class` internally.

   There is no mechanism to prevent to access the first index from :ref:`camera <camera>`. If you overload the userdata probably the program will crash.

.. code-block:: lua
   :linenos:
   :caption: **Do not do this:**
   :emphasize-lines: 4,6

   camera2D = mbm.getCamera('2d')
   camera2D:setPos(100,20)

   print(camera2D[1]) -- read, okay will print userdata: 0x32958758b6a7 for example

   camera2D[1] = 2 -- Error, this will do your program crash!


.. _camera_threed:

Camera 3D methods
-----------------

Use :ref:`getCamera <getCamera>` method to get a camera 3D.

camera 3d setPos
^^^^^^^^^^^^^^^^
.. data:: setPos(number x,number y, number z)

   Set position using x, y and z (3D world) position.

   :param number: **x** position.
   :param number: **y** position.
   :param number: **z** position.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   camera3D:setPos(0,0,0) --origin


.. data:: setPos(table renderizable)

   Set position using a :ref:`renderizable <renderizable>` object position.

   :param table: **renderizable** object.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   tMesh = mesh:new('3d')
   tMesh:setPos(500,300,400)
   camera3D:setPos(tMesh)

.. data:: setPos(vec3 vector)

   Set position using a :ref:`vec3 <vecthree>` position.

   :param table: **vec3** vector.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   tVec3 = vec3:new(30,20,50)
   camera3D:setPos(tVec3)

camera 3d setFocus
^^^^^^^^^^^^^^^^^^
.. data:: setFocus(number x,number y, number z)

   Set focus using x, y and z.

   :param number: **x** focus.
   :param number: **y** focus.
   :param number: **z** focus.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   camera3D:setFocus(0,0,0) --look at the origin


.. data:: setFocus(table renderizable)

   Set focus using a :ref:`renderizable <renderizable>` object position.

   :param table: **renderizable** object.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   tMesh = mesh:new('3d')
   tMesh:setFocus(50,120,20)
   camera3D:setFocus(tMesh)

.. data:: setFocus(vec3 vector)

   Set focus using a :ref:`vec3 <vecthree>` position.

   :param table: **vec3** vector.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   tVec3 = vec3:new(5,15,0)
   camera3D:setFocus(tVec3)


camera 3d setUp
^^^^^^^^^^^^^^^

.. data:: setUp(number x,number y, number z)

   Set 'up camera' using x, y and z.

   :param number: **x** 'up camera'.
   :param number: **y** 'up camera'.
   :param number: **z** 'up camera'.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   camera3D:setUp(0,1,0) --default
   camera3D:setUp(0,-1,0) --upside down


.. data:: setUp(vec3 vector)

   Set 'up camera' using a :ref:`vec3 <vecthree>` position.

   :param table: **vec3** vector.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   tVecNormal = vec3:new(0,1,0)
   tVecUpsideDown = vec3:new(0,-1,0)
   camera3D:setUp(tVecNormal) --default
   camera3D:setUp(tVecUpsideDown) --upside down

camera 3d getPos
^^^^^^^^^^^^^^^^

.. data:: getPos()

    Get the camera's position.

   :return: :literal:`vec3` - camera's  position .

   *Example:*
.. code-block:: lua

    camera3D = mbm.getCamera('3d')
    local posCamera = camera3D:getPos()
    --move camera position to origin
    posCamera.x = 0
    posCamera.y = 0
    posCamera.z = 0

camera 3d getFocus
^^^^^^^^^^^^^^^^^^

.. data:: getFocus()

    Get the camera's focus vector.

   :return: :literal:`vec3` - camera's  focus vector .

   *Example:*
.. code-block:: lua

    camera3D = mbm.getCamera('3d')
    local focus = camera3D:getFocus()
    --Look at the origin
    focus.x = 0
    focus.y = 0
    focus.z = 0

camera 3d move
^^^^^^^^^^^^^^

.. data:: move(number x,number * y, number * z)

   Move x, y and z units frames by seconds. This method consider the FPS.

   :param number: **x** position.
   :param number: **y** position.
   :param number: **z** position.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   camera3D:move(50,60,70)

   --this is equivalent
   function loop(delta)
        camera3d.x = camera3d.x + (delta * 50)
        camera3d.y = camera3d.y + (delta * 60)
        camera3d.z = camera3d.z + (delta * 70)
   end

.. data:: move(string direction, number units)

   - Move to a specific direction. This method consider the FPS.

    + :literal:`R` move to the right direction.
    + :literal:`L` move to the left direction.
    + :literal:`U` move to up direction.
    + :literal:`D` move to down direction.
    + :literal:`B` move to back direction.
    + :literal:`F` move to forward direction.

   :param string: *direction* to move. Available: :literal:`R`, :literal:`L`, :literal:`U`, :literal:`D`, :literal:`B` and :literal:`F`.
   :param number: *units* position.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   --move to the right 100 FPS
   camera3D:move('R',100)



.. data:: getNormal(string direction)

   - Retrieve the normal from specific direction based on current camera 3d position.

    + :literal:`R` get the the right direction.
    + :literal:`L` get the the left direction.
    + :literal:`U` get the up direction.
    + :literal:`D` get the down direction.
    + :literal:`B` get the back direction.
    + :literal:`F` get the forward direction.

   :param string: *direction* Available: :literal:`R`, :literal:`L`, :literal:`U`, :literal:`D`, :literal:`B` and :literal:`F`.
   :return: :literal:`vec3` - camera's direction normal vector.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   local tNormalRight = camera3D:getNormal('R')
   print(tNormalRight.x)

camera 3d setFar
^^^^^^^^^^^^^^^^

.. data:: setFar(number far)

    Set the :literal:`zFar` for 3D projection.

   :param number: **far** value. Default is 1000.

   *Example:*

.. code-block:: lua

    camera3D = mbm.getCamera('3d')
    camera3D:setFar(500)

camera 3d setNear
^^^^^^^^^^^^^^^^^

.. data:: setNear(number near)

    Set the :literal:`zNear` for 3D projection.

   :param number: **near** value. Default is 0.1.

   *Example:*

.. code-block:: lua

    camera3D = mbm.getCamera('3d')
    camera3D:setNear(2)


camera 3d setAngleOfView
^^^^^^^^^^^^^^^^^^^^^^^^

.. TODO: some examples

.. data:: setAngleOfView(number degree)

    Set a angle of view for 3D projection.

   :param number: **degree** value. Default is 110.

   *Example:*

.. code-block:: lua

    camera3D = mbm.getCamera('3d')
    camera3D:setAngleOfView(65)

Camera 3D attributes
--------------------

.. data:: x, y, z [read / write]

   Access the position :literal:`x`, :literal:`y` and :literal:`z` member from :ref:`camera <camera>`.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   camera3D.x = 100
   camera3D.y = 200
   camera3D.z = 300
   print('X:',camera3D.x,'Y:',camera3D.y,'Z:',camera3D.z)


.. data:: fx, fy, fz [read / write]

   Access the focus :literal:`fx`, :literal:`fy` and :literal:`fz` member from :ref:`camera <camera>`.

   *Example:*
.. code-block:: lua

   camera3D = mbm.getCamera('3d')
   camera3D.fx = 10
   camera3D.fy = 15
   camera3D.fz = 0
   print('Looking at X:',camera3D.fx,'Y:',camera3D.fy,'Z:',camera3D.fz)

.. data:: 'variable' [read / write]

   Create a variable read/write to :ref:`camera <camera>`. The type allowed is :literal:`number`, :literal:`string`, :literal:`boolean`, :literal:`table` and :literal:`function`.

   *Example:*
.. code-block:: lua

   camera2D = mbm.getCamera('2d')
   camera2D.myFloat = 2.5
   camera2D.myInt = 8
   camera2D.myString = 'Hi there!'
   camera2D.bValue = true
   camera2D.tUser = {name = 'Mario' , address = 'my street'}
   camera2D.fSayHello = function (self)
       print('Hello ', self.tUser.name)
   end

   print('Accessing the variables myFloat:',camera2D.myFloat)
   print('Accessing the variables myInt:',camera2D.myInt)
   print('Accessing the variables myString:',camera2D.myString)
   print('Accessing the variables bValue:',camera2D.bValue)
   print('Accessing the table tUser:',camera2D.tUser.name,camera2D.tUser.address )

   camera2D:fSayHello() -- invoke the function passing self as first argument

.. Note::
  The variable is stored internally and belongs to the camera from current scene.
  The variable is lost when loaded a new scene.

User data acknowledgment
------------------------

   This table uses the first index to store the userdata :guilabel:`C++ class` internally.
   So, if you want to store some values to the table you must use from the second element as exampled bellow:

.. code-block:: lua
   :linenos:
   :caption: *You can do this:*
   :emphasize-lines: 4,6

   camera3D = mbm.getCamera('3d')
   camera3D:setPos(10,10,10)

   camera3D[2] = 'my var'

   print(camera3D[2]) -- 'my var'


.. Error::

   It is important to understand that as all tables used in this engine, this table uses the first index to store the userdata
   pointing to the :guilabel:`C++ class` internally.

   There is no mechanism to prevent to access the first index from :ref:`camera <camera>`. If you overload the userdata, probably the program will crash.

.. code-block:: lua
   :linenos:
   :caption: **Do not do this:**
   :emphasize-lines: 4,6

   camera2D = mbm.getCamera('2d')
   camera2D:setPos(10,10)

   print(camera2D[1]) -- read, okay will print userdata: 0x44958588a6a8 for example

   camera2D[1] = 2 -- Error, this will do your program crash!


