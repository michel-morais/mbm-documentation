.. contents:: Table of Contents

.. _mesh:

Table mesh
============

A table :ref:`mesh <mesh>` inherit from :ref:`renderizable <renderizable>` and implement all methods as usual..
Each animation on mesh has an initial frame and final frame which represent its cycle.
Also each animation has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.

mesh methods
------------

mesh new
^^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`mesh <mesh>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`mesh <mesh>` *table*.

   *Example:*
.. code-block:: lua

   tMesh = mesh:new('3d') --note that mesh inherits from renderizable


mesh load
^^^^^^^^^

.. data:: load(string file_name)

   | Load a :ref:`mesh <mesh>` from a binary file (expected ``.msh`` extension).


   :param string: **file name** from binary file.

   *Example:*
.. code-block:: lua

   tMesh = mesh:new('3d')
   if tMesh:load('crate.msh') then
      print('Successfully loaded mesh:','crate.msh')
   else
      print('Failed to loaded mesh:','crate.msh')
   end

.. Note::

   | The :guilabel:`load` method will search in all known path.
   | You can add a path to search by the method :ref:`addPath <addPath>`.


If you want to create a binary file :ref:`next <generate_mesh>` an example how to do that.

.. _generate_mesh:

Creating a mesh programmatically
--------------------------------

   It is possible to save any mesh with help of :ref:`meshDebug <meshDebug>`. For that it is necessary to prepare the :ref:`meshDebug <meshDebug>` based on our :ref:`renderizable <renderizable>`.

   * First, we have to create coordinates of vertex buffer (indexed buffer based).
   * We also need to create index buffer (1 index based) for the vertex.
   * Then we create a :ref:`meshDebug <meshDebug>` which will store all the information.
   * The UV and normal coordinates are not mandatory. For this example we will not fill it.
   * Next we create a frame informing the stride and add a new subset. 
   * One frame can have one or more subsets. Each subset has its own texture.
   * Next we add the vertex buffer and index buffer.
   * The vertex buffer can have normal buffer (optional) and texture coordinates (optional).
   * Next we set the ``mesh`` type (others types are ``sprite``, ``particle``, ``font``, ...).
   * Finally we just need to specify the file name to save through the function :ref:`save <meshDebugSave>`.

   For the next examples we will use the following texture:

.. figure:: _static/crate.png
   :align: center
   :width: 200px
   :height: 200px
   :figclass: align-center

   crate.png

Mesh to a binary  file
^^^^^^^^^^^^^^^^^^^^^^

*Follow an example how to save a mesh to a binary  file:*

   .. literalinclude:: example_modules/saving_mesh_to_binary_file.lua
     :language: lua
     :linenos:


   :download:`download <example_modules/saving_mesh_to_binary_file.lua>`.

.. figure:: _static/mesh_flat.gif
   :align: center
   :figclass: align-center

   Mesh flat simple (two triangles)


Create a cube and fill the texture coordinates
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For the next example we will create a cube and fill it with our texture coordinates.

   .. literalinclude:: example_modules/saving_mesh_to_binary_file_example_2.lua
     :language: lua

   :download:`download <example_modules/saving_mesh_to_binary_file_example_2.lua>`.

.. figure:: _static/mesh_cube.gif
   :align: center
   :figclass: align-center

   Mesh cube 3D filled with texture coordinates


Importing mesh
--------------

You can find how to Import mesh and also some examples in the :ref:`modules <modules>` topic.

There you will find some tips and ready to use modules.

.. TODO: show how setModeDraw works for meshDebug
