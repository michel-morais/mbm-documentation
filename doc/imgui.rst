.. contents:: Table of Contents


.. _ImGui:

Module ImGui
=============

This :guilabel:`Plugin` is a wrapper over `Dear ImGui <https://github.com/ocornut/imgui>`__ and has some modification to be able to use it in **Lua** as external module/plugin for this engine.

It is widely used to build editor such as :guilabel:`sprite maker` :guilabel:`shader editor` , etc...

.. Important:: `Dear ImGui <https://github.com/ocornut/imgui>`__ is immediate GUI! so it is based on loop. Then, basically, you have to control what should be render inside the main loop of the engine otherwise will **not** be render.


.. figure:: _static/sprite_maker.png
   :align: center
   :figclass: align-center

   Sprite maker running.

Begin / End methods
-------------------

Begin / End
^^^^^^^^^^^

    **Window**

    - Begin() = push window to the stack and start appending to it.
    - End() = pop window from the stack.

        * You may append multiple times to the same window during the same frame.
        * Passing ``boolean`` closeable as ``true`` shows a window-closing widget in the upper-right corner of the window,
          which clicking will result the second boolean ``closed_clicked`` as ``true``.

    - Begin() return ``false`` to indicate the window is collapsed or fully clipped.

        * You may early out and omit submitting anything to the window checking this flag.
        * Always call a matching End() for each Begin() call, regardless of its return value!

    .. Important::
        Due to legacy reason, this is inconsistent with most other functions such as BeginPopup/EndPopup, etc. where the EndXXX call should only be called if the corresponding BeginXXX function returned true.
        Begin() and BeginChild() are the only odd ones out. Will be fixed in a future update!

        * Note that the bottom of window stack always contains a window called "Debug".




.. data:: Begin(title, closeable, flags)


    :param string: **title**
    :param boolean: **closeable** might be closed?
    :param number: **flags**
    :return: ``boolean``, ``boolean`` - *is_opened*, *closed_clicked*

.. data:: End()

    End a Window

*Example:*

.. code-block:: lua
    :emphasize-lines: 8, 13

    tImGui         =  require "ImGui"

    function loop(delta)
        local title     = 'Hello ImGui Plugin'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)
        print(is_opened, closed_clicked)
        if is_opened then
            tImGui.Text('Some nice content to show!')
        end
        tImGui.End()
    end


.. figure:: _static/imgui_begin.png
   :align: center
   :figclass: align-center



BeginPopupModal
^^^^^^^^^^^^^^^

.. data:: BeginPopupModal(title, closeable, flags)

     Modal dialog (regular window with title bar, block interactions behind the modal window, can't close the modal window by clicking outside)

    :param string: **title**
    :param boolean: **closeable** might be closed?
    :param number: **flags**
    :return: ``boolean``, ``boolean`` - *is_opened*, *closed_clicked*

.. data:: EndPopup()

     Only call EndPopup() if BeginPopupXXX() returns true!

*Example:*

.. code-block:: lua
    :emphasize-lines: 8, 22

    tImGui         =  require "ImGui"
    open_modal     = true
    function loop(delta)
        if open_modal then
            local flags       = tImGui.Flags('ImGuiWindowFlags_AlwaysAutoResize')
            local title_popup = 'Remove all images ?'
            tImGui.OpenPopup(title_popup);
            local is_opened, closed_clicked = tImGui.BeginPopupModal(title_popup, false, flags)
            if is_opened then
                tImGui.Text('Are vou sure to remove all images?')
                tImGui.Separator();
                if tImGui.Button("OK", {x=120, y= 0}) then
                    tImGui.CloseCurrentPopup()
                    open_modal = false
                end
                tImGui.SetItemDefaultFocus();
                tImGui.SameLine();
                if tImGui.Button("Cancel", {x=120, y= 0}) then
                    tImGui.CloseCurrentPopup()
                    open_modal = false
                end
                tImGui.EndPopup()
            end
        end
    end

.. figure:: _static/imgui_BeginPopupModal.png
   :align: center
   :figclass: align-center

BeginChild
^^^^^^^^^^

    **Child Windows**

    - Use child windows to begin into a self-contained independent scrolling/clipping regions within a host window. Child windows can embed their own child.

    - For each independent axis of 'size': ==0.0f: use remaining host window size / >0.0f: fixed size / <0.0f: use remaining window size minus abs(size) / Each axis can use a different mode, e.g. ImVec2(0,400).

    - BeginChild() returns false to indicate the window is collapsed or fully clipped, so you may early out and omit submitting anything to the window.

    - Always call a matching EndChild() for each BeginChild() call, regardless of its return value [as with Begin: this is due to legacy reason and inconsistent with most BeginXXX functions apart from the regular Begin() which behaves like BeginChild().]


.. data:: BeginChild(str_id, size, border, flags)


    :param string: **str_id** (might be ``nil``)
    :param table: **size**  default: ``{ x = 0, y = 0}``
    :param boolean: **border**
    :param number: **flags**
    :return: ``boolean`` - *result*

.. data:: EndChild()

    End Child.

*Example:*

.. code-block:: lua
    :emphasize-lines: 16, 18

    tImGui          =  require "ImGui"

    function loop(delta)
        local title     = 'Main window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)
        if is_opened then
            tImGui.Text('Other nice content to show!')

            local str_id  =  '01'
            local size    =  {x = 200, y = 50}
            local border  =  true

            tImGui.BeginChild(str_id, size, border, flags)
            tImGui.Text('I am a child')
            tImGui.EndChild()
        end
        tImGui.End()
    end


.. figure:: _static/imgui_child.png
   :align: center
   :figclass: align-center


.. _imgui_begin_menu:

BeginMenu
^^^^^^^^^

.. data:: BeginMenu(label, enabled)

     Create a sub-menu entry. only call EndMenu() if this returns true!

    :param string: **label**
    :param boolean: **enabled**
    :return: ``boolean`` - *result*

.. data:: EndMenu()

     Only call EndMenu() if BeginMenu() returns true!


*Example:*

.. code-block:: lua
    :emphasize-lines: 15,17,19,21

    tImGui          =  require "ImGui"
    local title     = 'Main window'
    local closeable =  true
    local flags     =  0

    function loop(delta)
        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)
        if is_opened then
            tImGui.Text('Other nice content to show!')

            local label    = 'menu 1'
            local label2   = 'menu 2'
            local enabled  =  true

            if tImGui.BeginMenu(label, enabled) then
                tImGui.Text('Option menu 1')
                if tImGui.BeginMenu(label2, enabled) then
                    tImGui.Text('Option menu 2')
                    tImGui.EndMenu()
                end
                tImGui.EndMenu()
            end
        end
        tImGui.End()
    end

.. figure:: _static/imgui_menu.png
   :align: center
   :figclass: align-center


BeginPopupContextVoid
^^^^^^^^^^^^^^^^^^^^^

.. data:: BeginPopupContextVoid(str_id, mouse_button)

     Helper to open and begin popup when clicked in void (where there are no ImGui windows).

    :param string: **str_id** (might be ``nil``)
    :param number: **mouse_button** (0=left, 1=right, 2=middle)
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua
    :emphasize-lines: 5

    tImGui              =  require "ImGui"
    local str_id        =  'id_1'
    local mouse_button  =  0
    function loop(delta)
        if tImGui.BeginPopupContextVoid(str_id, mouse_button) then
            if tImGui.Selectable("Some option") then
                print('You have chosen this option')
            end
            tImGui.EndPopup()
        end
    end

BeginPopupContextWindow
^^^^^^^^^^^^^^^^^^^^^^^

.. data:: BeginPopupContextWindow(str_id, mouse_button, also_over_items)

     Helper to open and begin popup when clicked on current window.

    :param string: **str_id** (might be ``nil``)
    :param number: **mouse_button** (0=left, 1=right, 2=middle)
    :param boolean: **also_over_items**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua
    :emphasize-lines: 6

    tImGui                 =  require "ImGui"
    local str_id           =  '01'
    local mouse_button     =  1
    local also_over_items  =  true
    function loop(delta)
        if tImGui.BeginPopupContextWindow(str_id, mouse_button, also_over_items) then
            if tImGui.Selectable("Some option") then
                print('You have chosen this option')
            end
            tImGui.EndPopup()
        end
    end

BeginPopupContextItem
^^^^^^^^^^^^^^^^^^^^^

.. data:: BeginPopupContextItem(str_id, mouse_button)

     Helper to open and begin popup when clicked on last item. if you can pass a ``nil`` str_id only if the previous item had an id. 
     If you want to use that on a non-interactive item such as Text() you need to pass in an explicit ID here. read comments in .cpp!

    :param string: **str_id** (might be ``nil``)
    :param number: **mouse_button** (0=left, 1=right, 2=middle)
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua
    :emphasize-lines: 5

    tImGui              =  require "ImGui"
    local str_id        =  '01'
    local mouse_button  =  1
    function loop(delta)
        if tImGui.BeginPopupContextItem(str_id, mouse_button) then
            if tImGui.Selectable("Some option") then
                print('You have chosen this option')
            end
            tImGui.EndPopup()
        end
    end

BeginPopup
^^^^^^^^^^

.. data:: BeginPopup(str_id, flags)

     Return true if the popup is open, and you can start outputting to it. only call EndPopup() if BeginPopup() returns true!

    :param string: **str_id** (might be ``nil``)
    :param number: **flags**
    :return: ``boolean`` - *result*

.. data:: EndPopup()

     Only call EndPopup() if BeginPopupXXX() returns true!


*Example:*

.. code-block:: lua
    :emphasize-lines: 5,6

    tImGui        =  require "ImGui"
    function loop(delta)
        local str_id  =  '01'
        local flags   =  0
        if tImGui.BeginPopup(str_id, flags) then
            tImGui.EndPopup()
        end
    end

.. _imgui_main_menu:

BeginMainMenuBar
^^^^^^^^^^^^^^^^

.. data:: BeginMainMenuBar()

     Create and append to a full screen menu-bar.

    :return: ``boolean`` - *result*

.. data:: EndMainMenuBar()

     Only call EndMainMenuBar() if BeginMainMenuBar() returns true!

*Example:*

.. code-block:: lua
    :emphasize-lines: 7, 25

    tImGui        =  require "ImGui"
    bAutoSave     = false
    bOtherMenuBar = false

    function loop(delta)
        -- Main Menu Bar
        if (tImGui.BeginMainMenuBar()) then

            if tImGui.BeginMenu("File") then

                local pressed,checked = tImGui.MenuItem("Auto Save ", nil, bAutoSave)
                if pressed then
                    bAutoSave = checked
                end
                tImGui.EndMenu();
            end
            if tImGui.BeginMenu("Examples") then

                local pressed,checked = tImGui.MenuItem("Other menu bar", nil, bOtherMenuBar)
                if pressed then
                    bOtherMenuBar = checked
                end
                tImGui.EndMenu()
            end
            tImGui.EndMainMenuBar()
        end
    end

.. figure:: _static/imgui_BeginMainMenuBar.png
   :align: center
   :figclass: align-center

BeginTooltip
^^^^^^^^^^^^

.. data:: BeginTooltip()

     Begin/append a tooltip window. to create full-featured tooltip (with any kind of items).

.. data:: EndTooltip()

    End Tooltip

*Example:*

.. code-block:: lua
    :emphasize-lines: 3,5

    tImGui  =  require "ImGui"
    function loop(delta)
        tImGui.BeginTooltip()
        tImGui.Text('This area is free!')
        tImGui.EndTooltip()
    end

.. figure:: _static/imgui_begin_tool_tip.png
   :align: center
   :figclass: align-center

BeginTabBar
^^^^^^^^^^^

.. data:: BeginTabBar(str_id, flags)

     Create and append into a TabBar

    :param string: **str_id** (might be ``nil``)
    :param number: **flags**
    :return: ``boolean`` - *result*

.. data:: EndTabBar()

     Only call EndTabBar() if BeginTabBar() returns true!


*Example:*

.. code-block:: lua
    :emphasize-lines: 4,13

    tImGui        =  require "ImGui"
    function loop(delta)
        local flags   =  tImGui.Flags('ImGuiTabBarFlags_Reorderable')
        if tImGui.BeginTabBar('##TabBar_id', flags) then
            if tImGui.BeginTabItem("Tab 1") then
                tImGui.Text('Hello')
                tImGui.EndTabItem()
            end
            if tImGui.BeginTabItem("Tab 2") then
                tImGui.Text('Tab')
                tImGui.EndTabItem()
            end
            tImGui.EndTabBar()
        end
    end

BeginDragDropSource
^^^^^^^^^^^^^^^^^^^

.. data:: BeginDragDropSource(flags)

     Call when the current item is active. If this return true, you can call SetDragDropPayload() + EndDragDropSource()

    :param number: **flags**
    :return: ``boolean`` - *result*

.. data:: EndDragDropSource()

     Only call EndDragDropSource() if BeginDragDropSource() returns true!

*Example:*

.. code-block:: lua
    :emphasize-lines: 4,5

    tImGui       =  require "ImGui"
    function loop(delta)
        local flags  =  0
        if tImGui.BeginDragDropSource(flags) then
            tImGui.EndDragDropSource()
        end
    end

BeginGroup
^^^^^^^^^^

.. data:: BeginGroup()

     Lock horizontal starting position

.. data:: EndGroup()

     Unlock horizontal starting position + capture the whole group bounding box into one "item" (so you can use IsItemHovered() or layout primitives such as SameLine() on whole group, etc.)

*Example:*

.. code-block:: lua
    :emphasize-lines: 3,4

    tImGui  =  require "ImGui"
    function loop(delta)
        tImGui.BeginGroup()
        tImGui.EndGroup()
    end

BeginChildFrame
^^^^^^^^^^^^^^^

.. data:: BeginChildFrame(id, size, flags)

     Helper to create a child window / scrolling region that looks like a normal widget frame

    :param number: **id**
    :param table: **size** default is ``{x=0, y=0}``
    :param number: **flags**
    :return: ``boolean`` - *result*

.. data:: EndChildFrame()

     Always call EndChildFrame() regardless of BeginChildFrame() return values (which indicates a collapsed/clipped window)

*Example:*

.. code-block:: lua
    :emphasize-lines: 6,7

    tImGui       =  require "ImGui"
    function loop(delta)
        local id     = 1
        local size   = {x = 200, y = 200}
        local flags  =  0
        tImGui.BeginChildFrame(id, size, flags)
        tImGui.EndChildFrame()
    end

BeginTabItem
^^^^^^^^^^^^

.. data:: BeginTabItem(label, p_open, flags)

     Create a Tab. Returns true if the Tab is selected.

    :param string: **label**
    :param boolean: **p_open** (might be ``nil``)
    :param number: **flags** (``ImGuiTabItemFlags_SetSelected``, ``ImGuiTabItemFlags_UnsavedDocument``, etc...)
    :return: ``boolean`` - *result*

.. data:: EndTabItem()

     Only call EndTabItem() if BeginTabItem() returns true!

*Example:*

.. code-block:: lua
    :emphasize-lines: 5,7,9,11

    tImGui        =  require "ImGui"
    function loop(delta)
        local flags   =  0
        if tImGui.BeginTabBar('##TabBar_id', flags) then
            if tImGui.BeginTabItem("Tab 1") then
                tImGui.Text('Hello')
                tImGui.EndTabItem()
            end
            if tImGui.BeginTabItem("Tab 2") then
                tImGui.Text('Tab')
                tImGui.EndTabItem()
            end
            tImGui.EndTabBar()
        end
    end


BeginDragDropTarget
^^^^^^^^^^^^^^^^^^^

.. data:: BeginDragDropTarget()

     Call after submitting an item that may receive a payload. If this returns true, you can call AcceptDragDropPayload() + EndDragDropTarget()

    :return: ``boolean`` - *result*

.. data:: EndDragDropTarget()

     Only call EndDragDropTarget() if BeginDragDropTarget() returns true!


.. data:: AcceptDragDropPayload(type, flags)

     Accept contents of a given type. If ImGuiDragDropFlags_AcceptBeforeDelivery is set you can peek into the payload before the mouse button is released.

    :param string: **type**
    :param number: **flags**
    :return: ``table`` - *ImGuiPayload*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.BeginDragDropTarget()

BeginMenuBar
^^^^^^^^^^^^

.. data:: BeginMenuBar()

     Append to menu-bar of current window (requires :guilabel:`ImGuiWindowFlags_MenuBar`  flag set on parent window).

    :return: ``boolean`` - *result*

.. data:: EndMenuBar()

     Only call EndMenuBar() if BeginMenuBar() returns true!

*Example:*

.. code-block:: lua
    :emphasize-lines: 12, 30

    tImGui  =  require "ImGui"

    bMain_file_checked = false
    bOtherMenuBar      = false

    function loop(delta)
        local flags     =  1024--ImGuiWindowFlags_MenuBar
        local closeable =  true
        local is_opened, closed_clicked = tImGui.Begin('Demo menu bar', closeable, flags)
        if is_opened then
            -- Menu Bar
            if tImGui.BeginMenuBar() then

                if tImGui.BeginMenu("Menu") then

                    local pressed,checked = tImGui.MenuItem("Main file ", nil, bMain_file_checked)
                    if pressed then
                        bMain_file_checked = checked
                    end
                    tImGui.EndMenu();
                end
                if tImGui.BeginMenu("Examples") then

                    local pressed,checked = tImGui.MenuItem("Other menu bar", nil, bOtherMenuBar)
                    if pressed then
                        bOtherMenuBar = checked
                    end
                    tImGui.EndMenu()
                end
                tImGui.EndMenuBar()
            end
        end
        tImGui.End()
    end

.. figure:: _static/imgui_BeginMenuBar.png
   :align: center
   :figclass: align-center

Calc methods
------------

CalcItemWidth
^^^^^^^^^^^^^

.. data:: CalcItemWidth()

     Width of item given pushed settings and current cursor position. NOT necessarily the width of last item unlike most 'Item' functions.

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.CalcItemWidth()


CalcTextSize
^^^^^^^^^^^^

.. data:: CalcTextSize(text, text_end, hide_text_after_double_hash, wrap_width)


    :param string: **text**
    :param string: **text_end**
    :param boolean: **hide_text_after_double_hash**
    :param number: **wrap_width**
    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui                             =  require "ImGui"
    local text                         =  'some text'
    local text_end                     =  'some text'
    local hide_text_after_double_hash  =  false
    local wrap_width                   =  -1.0

    tImGui.CalcTextSize(text, text_end, hide_text_after_double_hash, wrap_width)


.. data:: CalcListClipping(items_count, items_height, out_items_display_start, out_items_display_end)

     Calculate coarse clipping for large list of evenly sized items. Prefer using the ImGuiListClipper higher-level helper if you can.

    :param number: **items_count**
    :param number: **items_height**
    :return: ``number`` **items_display_start**, ``number`` **items_display_end**

*Example:*

.. code-block:: lua

    tImGui                     =  require "ImGui"
    local items_count          = 2
    local items_height         = 10
    local items_display_start  = 0
    local items_display_end    = 0

    items_display_start, items_display_end = tImGui.CalcListClipping(items_count, items_height)


Capture methods
---------------

CaptureKeyboardFromApp
^^^^^^^^^^^^^^^^^^^^^^

.. data:: CaptureKeyboardFromApp(want_capture_keyboard_value)

     Attention: misleading name! manually override io.WantCaptureKeyboard flag next frame (said flag is entirely left for your application to handle). e.g. force capture keyboard when your widget is being hovered. This is equivalent to setting "io.WantCaptureKeyboard = want_capture_keyboard_value"; after the next NewFrame() call.

    :param boolean: **want_capture_keyboard_value**

*Example:*

.. code-block:: lua

    tImGui                             =  require "ImGui"
    local want_capture_keyboard_value  =  true

    tImGui.CaptureKeyboardFromApp(want_capture_keyboard_value)


CaptureMouseFromApp
^^^^^^^^^^^^^^^^^^^

.. data:: CaptureMouseFromApp(want_capture_mouse_value)

     Attention: misleading name! manually override io.WantCaptureMouse flag next frame (said flag is entirely left for your application to handle). This is equivalent to setting "io.WantCaptureMouse = want_capture_mouse_value;" after the next NewFrame() call.

    :param boolean: **want_capture_mouse_value**

*Example:*

.. code-block:: lua

    tImGui                          =  require "ImGui"
    local want_capture_mouse_value  =  true

    tImGui.CaptureMouseFromApp(want_capture_mouse_value)


Color methods
-------------


ColorConvertHSVtoRGB
^^^^^^^^^^^^^^^^^^^^

.. data:: ColorConvertHSVtoRGB(h, s, v, out_r, out_g, out_b)


    :param number: **h**
    :param number: **s**
    :param number: **v**
    :return: ``number`` - *red*, ``number`` - *green*, ``number`` - *blue*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local h      = 0.1
    local s      = 0.2
    local v      = 0.3

    local r,g,b = tImGui.ColorConvertHSVtoRGB(h, s, v)


ColorConvertFloat4ToU32
^^^^^^^^^^^^^^^^^^^^^^^

.. data:: ColorConvertFloat4ToU32(in)


    :param table: **in** ``{x=0,y=0,z=0,w=0}`` or ``{r=0,g=0,b=0,a=0}``
    :return: ``number`` - *ImU32*

*Example:*

.. code-block:: lua

    tImGui    =  require "ImGui"
    local in  = {x=0.66,y=0.5,z=1,z=0.1}

    local ret = tImGui.ColorConvertFloat4ToU32(in)


ColorConvertU32ToFloat4
^^^^^^^^^^^^^^^^^^^^^^^

.. data:: ColorConvertU32ToFloat4(in)


    :param number: **integer**
    :return: ``table`` - *ImVec4* ``{x,y,z,w}``

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local in    = 255

    local tVec4 = tImGui.ColorConvertU32ToFloat4(in)


ColorConvertRGBtoHSV
^^^^^^^^^^^^^^^^^^^^

.. data:: ColorConvertRGBtoHSV(r, g, b, out_h, out_s, out_v)


    :param number: **r**
    :param number: **g**
    :param number: **b**
    :return: ``number`` - *h*, ``number`` - *s*, ``number`` - *v*


*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local r      = 1.0
    local g      = 0.5
    local b      = 0.1
    local h,s,v  = tImGui.ColorConvertRGBtoHSV(r, g, b)

Columns
^^^^^^^

.. data:: Columns(count, id, border)


    :param number: **count**
    :param string: **id**
    :param boolean: **border**

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local count   =  1
    local id      =  nil
    local border  =  true

    tImGui.Columns(count, id, border)


CreateContext
-------------

    .. Note:: You do not need to create a context for this module. It is automatically created by the engine.


DestroyContext
--------------

    .. Note:: You do not need to destroy the context for this module. It is automatically destroyed by the engine.


Flag / Enum
-----------

Flags
^^^^^

    It is possible to list flag or make flags combining as ``string``.

    Enum is considered as Flag.

.. data:: Flags(string flag_name_1,string flag_name_2,string flag_name_3, ...)

    This method accept variable argument of string as input and make it as unique flag out.

    :param string: **flag name** - it must be identical to the real name. 
    :return: ``number`` - *flag* combined ``OR`` operation.

*Example:*

.. code-block:: lua

    tImGui                             =  require "ImGui"
    local ImGuiWindowFlags_MenuBar     = tImGui.Flags('ImGuiWindowFlags_MenuBar')
    local ImGuiWindowFlags_NoCollapse  = tImGui.Flags('ImGuiWindowFlags_NoCollapse')
    local flag_combined                = tImGui.Flags('ImGuiWindowFlags_MenuBar','ImGuiWindowFlags_NoCollapse')
    print('ImGuiWindowFlags_MenuBar',ImGuiWindowFlags_MenuBar)
    print('ImGuiWindowFlags_NoCollapse',ImGuiWindowFlags_NoCollapse)
    print('flag_combined',flag_combined)

.. figure:: _static/ImGui_flags.png
   :align: center
   :figclass: align-center

.. data:: Flags(table flags)

    This method accept a table with array of string as input and make it as unique flag out.

    :param table: **flags** - it must contains array of flags identical to the real name.
    :return: ``number`` - *flag* combined ``OR`` operation.    

*Example:*

.. code-block:: lua

    tImGui                             =  require "ImGui"
    local tFlags = {'ImGuiWindowFlags_MenuBar','ImGuiWindowFlags_NoCollapse',}
    local flag_combined                = tImGui.Flags(tFlags)
    print('flag_combined',flag_combined) -- flag_combined	1056


.. data:: FlagList(string flag_name_1,string flag_name_2,string flag_name_3, ...)

    This method accept a var argument of string as input to list all flags that match as string.

    :param string: **flag name** - it must be partial case sensitive to the real name. 
    :return: ``table`` - *flags* ``{key = flag, value = number}``

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local flags           = tImGui.FlagList('Window')
    for k,v in pairs(flags) do
        local l = k:len()
        print(k .. string.rep(' ',50 - l)  .. tostring(v))
    end

.. figure:: _static/ImGui_flags_list.png
   :align: center
   :figclass: align-center

.. data:: FlagList(table flags)

    This method accept a table of string as input to list all flags that match as string.

    :param table: **flag table** - it must have strings partial case sensitive to the real name.
    :return: ``table`` - *flags* ``{key = flag, value = number}``

*Example:*

.. code-block:: lua

    tImGui                 = require "ImGui"
    local flags            = tImGui.FlagList({'Window','InputText'})
    for k,v in pairs(flags) do
        local l = k:len()
        print(k .. string.rep(' ',50 - l)  .. tostring(v))
    end

All Flags / Enum Listed
^^^^^^^^^^^^^^^^^^^^^^^

    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_All                             |     15                             |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_Bot                             |     12                             |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_BotLeft                         |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_BotRight                        |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_Left                            |     5                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_None                            |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_Right                           |     10                             |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_Top                             |     3                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_TopLeft                         |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawCornerFlags_TopRight                        |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawListFlags_AllowVtxOffset                    |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawListFlags_AntiAliasedFill                   |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawListFlags_AntiAliasedLines                  |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImDrawListFlags_None                              |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiBackendFlags_HasGamepad                      |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiBackendFlags_HasMouseCursors                 |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiBackendFlags_HasSetMousePos                  |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiBackendFlags_None                            |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiBackendFlags_RendererHasVtxOffset            |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_Border                                   |     5                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_BorderShadow                             |     6                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_Button                                   |     21                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ButtonActive                             |     23                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ButtonHovered                            |     22                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_CheckMark                                |     18                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ChildBg                                  |     3                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_DragDropTarget                           |     43                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_FrameBg                                  |     7                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_FrameBgActive                            |     9                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_FrameBgHovered                           |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_Header                                   |     24                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_HeaderActive                             |     26                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_HeaderHovered                            |     25                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_MenuBarBg                                |     13                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ModalWindowDimBg                         |     47                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_NavHighlight                             |     44                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_NavWindowingDimBg                        |     46                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_NavWindowingHighlight                    |     45                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_PlotHistogram                            |     40                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_PlotHistogramHovered                     |     41                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_PlotLines                                |     38                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_PlotLinesHovered                         |     39                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_PopupBg                                  |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ResizeGrip                               |     30                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ResizeGripActive                         |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ResizeGripHovered                        |     31                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ScrollbarBg                              |     14                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ScrollbarGrab                            |     15                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ScrollbarGrabActive                      |     17                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_ScrollbarGrabHovered                     |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_Separator                                |     27                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_SeparatorActive                          |     29                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_SeparatorHovered                         |     28                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_SliderGrab                               |     19                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_SliderGrabActive                         |     20                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_Tab                                      |     33                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TabActive                                |     35                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TabHovered                               |     34                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TabUnfocused                             |     36                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TabUnfocusedActive                       |     37                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_Text                                     |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TextDisabled                             |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TextSelectedBg                           |     42                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TitleBg                                  |     10                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TitleBgActive                            |     11                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_TitleBgCollapsed                         |     12                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCol_WindowBg                                 |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_AlphaBar                      |     65536                          |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_AlphaPreview                  |     131072                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_AlphaPreviewHalf              |     262144                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_DisplayHSV                    |     2097152                        |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_DisplayHex                    |     4194304                        |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_DisplayRGB                    |     1048576                        |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_Float                         | 1.677722e+07                       |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_HDR                           |     524288                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_InputHSV                      | 2.684355e+08                       |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_InputRGB                      | 1.342177e+08                       |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoAlpha                       |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoDragDrop                    |     512                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoInputs                      |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoLabel                       |     128                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoOptions                     |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoPicker                      |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoSidePreview                 |     256                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoSmallPreview                |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_NoTooltip                     |     64                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_None                          |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_PickerHueBar                  | 3.355443e+07                       |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_PickerHueWheel                | 6.710886e+07                       |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags_Uint8                         |     8388608                        |
    +---------------------------------------------------+------------------------------------+
    | ImGuiColorEditFlags__OptionsDefault               | 1.772093e+08                       |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_HeightLarge                       |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_HeightLargest                     |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_HeightMask\_                      |     30                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_HeightRegular                     |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_HeightSmall                       |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_NoArrowButton                     |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_NoPreview                         |     64                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_None                              |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiComboFlags_PopupAlignLeft                    |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCond_Always                                  |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCond_Appearing                               |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCond_FirstUseEver                            |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiCond_Once                                    |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_IsSRGB                           |     1048576                        |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_IsTouchScreen                    |     2097152                        |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_NavEnableGamepad                 |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_NavEnableKeyboard                |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_NavEnableSetMousePos             |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_NavNoCaptureKeyboard             |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_NoMouse                          |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_NoMouseCursorChange              |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiConfigFlags_None                             |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDir_Down                                     |     3                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDir_Left                                     |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDir_None                                     |     -1                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDir_Right                                    |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDir_Up                                       |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_AcceptBeforeDelivery           |     1024                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_AcceptNoDrawDefaultRect        |     2048                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_AcceptNoPreviewTooltip         |     4096                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_AcceptPeekOnly                 |     3072                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_None                           |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_SourceAllowNullID              |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_SourceAutoExpirePayload        |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_SourceExtern                   |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_SourceNoDisableHover           |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_SourceNoHoldToOpenOthers       |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiDragDropFlags_SourceNoPreviewTooltip         |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiFocusedFlags_AnyWindow                       |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiFocusedFlags_ChildWindows                    |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiFocusedFlags_None                            |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiFocusedFlags_RootAndChildWindows             |     3                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiFocusedFlags_RootWindow                      |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_AllowWhenBlockedByActiveItem    |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_AllowWhenBlockedByPopup         |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_AllowWhenDisabled               |     128                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_AllowWhenOverlapped             |     64                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_AnyWindow                       |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_ChildWindows                    |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_None                            |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_RectOnly                        |     104                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_RootAndChildWindows             |     3                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiHoveredFlags_RootWindow                      |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_AllowTabInput                 |     1024                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_AlwaysInsertMode              |     8192                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_AutoSelectAll                 |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CallbackAlways                |     256                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CallbackCharFilter            |     512                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CallbackCompletion            |     64                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CallbackHistory               |     128                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CallbackResize                |     262144                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CharsDecimal                  |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CharsHexadecimal              |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CharsNoBlank                  |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CharsScientific               |     131072                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CharsUppercase                |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_CtrlEnterForNewLine           |     2048                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_EnterReturnsTrue              |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_NoHorizontalScroll            |     4096                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_NoUndoRedo                    |     65536                          |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_None                          |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_Password                      |     32768                          |
    +---------------------------------------------------+------------------------------------+
    | ImGuiInputTextFlags_ReadOnly                      |     16384                          |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseButton_COUNT                            |     5                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseButton_Left                             |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseButton_Middle                           |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseButton_Right                            |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_Arrow                            |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_Hand                             |     7                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_None                             |     -1                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_NotAllowed                       |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_ResizeAll                        |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_ResizeEW                         |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_ResizeNESW                       |     5                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_ResizeNS                         |     3                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_ResizeNWSE                       |     6                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiMouseCursor_TextInput                        |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_Activate                            |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_Cancel                              |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_DpadDown                            |     7                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_DpadLeft                            |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_DpadRight                           |     5                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_DpadUp                              |     6                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_FocusNext                           |     13                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_FocusPrev                           |     12                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_Input                               |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_LStickDown                          |     11                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_LStickLeft                          |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_LStickRight                         |     9                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_LStickUp                            |     10                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_Menu                                |     3                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_TweakFast                           |     15                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiNavInput_TweakSlow                           |     14                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiSelectableFlags_AllowDoubleClick             |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiSelectableFlags_AllowItemOverlap             |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiSelectableFlags_Disabled                     |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiSelectableFlags_DontClosePopups              |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiSelectableFlags_None                         |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiSelectableFlags_SpanAllColumns               |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_Alpha                               |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_ButtonTextAlign                     |     21                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_ChildBorderSize                     |     7                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_ChildRounding                       |     6                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_FrameBorderSize                     |     12                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_FramePadding                        |     10                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_FrameRounding                       |     11                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_GrabMinSize                         |     18                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_GrabRounding                        |     19                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_IndentSpacing                       |     15                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_ItemInnerSpacing                    |     14                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_ItemSpacing                         |     13                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_PopupBorderSize                     |     9                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_PopupRounding                       |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_ScrollbarRounding                   |     17                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_ScrollbarSize                       |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_SelectableTextAlign                 |     22                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_TabRounding                         |     20                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_WindowBorderSize                    |     3                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_WindowMinSize                       |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_WindowPadding                       |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_WindowRounding                      |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiStyleVar_WindowTitleAlign                    |     5                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_AutoSelectNewTabs                |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_FittingPolicyDefault\_           |     64                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_FittingPolicyMask\_              |     192                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_FittingPolicyResizeDown          |     64                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_FittingPolicyScroll              |     128                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_NoCloseWithMiddleMouseButton     |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_NoTabListScrollingButtons        |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_NoTooltip                        |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_None                             |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_Reorderable                      |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabBarFlags_TabListPopupButton               |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabItemFlags_NoCloseWithMiddleMouseButton    |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabItemFlags_NoPushId                        |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabItemFlags_None                            |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabItemFlags_SetSelected                     |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTabItemFlags_UnsavedDocument                 |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_AllowItemOverlap               |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_Bullet                         |     512                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_DefaultOpen                    |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_FramePadding                   |     1024                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_Framed                         |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_Leaf                           |     256                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_NavLeftJumpsBackHere           |     8192                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_NoAutoOpenOnLog                |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_NoTreePushOnOpen               |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_None                           |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_OpenOnArrow                    |     128                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_OpenOnDoubleClick              |     64                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_Selected                       |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_SpanAvailWidth                 |     2048                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiTreeNodeFlags_SpanFullWidth                  |     4096                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_AlwaysAutoResize                 |     64                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_AlwaysHorizontalScrollbar        |     32768                          |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_AlwaysUseWindowPadding           |     65536                          |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_AlwaysVerticalScrollbar          |     16384                          |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_HorizontalScrollbar              |     2048                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_MenuBar                          |     1024                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoBackground                     |     128                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoBringToFrontOnFocus            |     8192                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoCollapse                       |     32                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoDecoration                     |     43                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoFocusOnAppearing               |     4096                           |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoInputs                         |     786944                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoMouseInputs                    |     512                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoMove                           |     4                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoNav                            |     786432                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoNavFocus                       |     524288                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoNavInputs                      |     262144                         |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoResize                         |     2                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoSavedSettings                  |     256                            |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoScrollWithMouse                |     16                             |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoScrollbar                      |     8                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_NoTitleBar                       |     1                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_None                             |     0                              |
    +---------------------------------------------------+------------------------------------+
    | ImGuiWindowFlags_UnsavedDocument                  |     1048576                        |
    +---------------------------------------------------+------------------------------------+


ImGuiKey
^^^^^^^^
    +---------------------------------------------------+
    | ImGuiKey_A                                        |
    +---------------------------------------------------+
    | ImGuiKey_Backspace                                |
    +---------------------------------------------------+
    | ImGuiKey_C                                        |
    +---------------------------------------------------+
    | ImGuiKey_Delete                                   |
    +---------------------------------------------------+
    | ImGuiKey_DownArrow                                |
    +---------------------------------------------------+
    | ImGuiKey_End                                      |
    +---------------------------------------------------+
    | ImGuiKey_Enter                                    |
    +---------------------------------------------------+
    | ImGuiKey_Escape                                   |
    +---------------------------------------------------+
    | ImGuiKey_Home                                     |
    +---------------------------------------------------+
    | ImGuiKey_Insert                                   |
    +---------------------------------------------------+
    | ImGuiKey_KeyPadEnter                              |
    +---------------------------------------------------+
    | ImGuiKey_LeftArrow                                |
    +---------------------------------------------------+
    | ImGuiKey_PageDown                                 |
    +---------------------------------------------------+
    | ImGuiKey_PageUp                                   |
    +---------------------------------------------------+
    | ImGuiKey_RightArrow                               |
    +---------------------------------------------------+
    | ImGuiKey_Space                                    |
    +---------------------------------------------------+
    | ImGuiKey_Tab                                      |
    +---------------------------------------------------+
    | ImGuiKey_UpArrow                                  |
    +---------------------------------------------------+
    | ImGuiKey_V                                        |
    +---------------------------------------------------+
    | ImGuiKey_X                                        |
    +---------------------------------------------------+
    | ImGuiKey_Y                                        |
    +---------------------------------------------------+
    | ImGuiKey_Z                                        |
    +---------------------------------------------------+


.. Note:: ImGuiKey is dependent on operation system. Use ``GetKeyIndex`` method to retrieve the value.

General methods
---------------


Dummy
^^^^^

.. data:: Dummy(size)

     Add a dummy item of given size. unlike InvisibleButton(), Dummy() won't take the mouse click or be navigable into.

    :param table: **size** ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local size  = {x=100,y=100}
    tImGui.Dummy(size)


Indent
^^^^^^

.. data:: Indent(indent_w)

     Move content position toward the right, by style.IndentSpacing or indent_w if != 0

    :param number: **indent_w**

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local indent_w  =  0.0
    tImGui.Indent(indent_w)

InvisibleButton
^^^^^^^^^^^^^^^

.. data:: InvisibleButton(str_id, size)

     Button behavior without the visuals, frequently useful to build custom behaviors using the public api (along with IsItemActive, IsItemHovered, etc.)

    :param string: **str_id**
    :param table: **size** ``{x,y}``
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local str_id  =  '01'
    local size    = {x=100,y=100}
    tImGui.InvisibleButton(str_id, size)


NextColumn
^^^^^^^^^^

.. data:: NextColumn()

     Next column, defaults to current row or next row if the current row is finished

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.NextColumn()


ResetMouseDragDelta
^^^^^^^^^^^^^^^^^^^

.. data:: ResetMouseDragDelta(button)

    :param number: **button**

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local button  =  0
    tImGui.ResetMouseDragDelta(button)

SameLine
^^^^^^^^

.. data:: SameLine(offset_from_start_x, spacing)

     Call between widgets or groups to layout them horizontally. X position given in window coordinates.

    :param number: **offset_from_start_x**
    :param number: **spacing**

*Example:*

.. code-block:: lua

    tImGui                     =  require "ImGui"
    local offset_from_start_x  = 0.0
    local spacing              = -1.0
    tImGui.SameLine(offset_from_start_x, spacing)

Separator
^^^^^^^^^

.. data:: Separator()

     Separator, generally horizontal. inside a menu bar or in horizontal layout mode, this becomes a vertical separator.

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.Separator()


Spacing
^^^^^^^

.. data:: Spacing()

     Add vertical spacing.

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.Spacing()

Unindent
^^^^^^^^

.. data:: Unindent(indent_w)

     Move content position back to the left, by style.IndentSpacing or indent_w if != 0

    :param number: **indent_w**

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local indent_w  =  0.0
    tImGui.Unindent(indent_w)

Get methods
-----------

GetClipboardText
^^^^^^^^^^^^^^^^


.. data:: GetClipboardText()

    :return: ``string`` - *text*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    local text = tImGui.GetClipboardText()
    print(text)


.. Note:: TODO: On :guilabel:`Linux` is used just the internal clipboard text.

GetScrollY
^^^^^^^^^^

.. data:: GetScrollY()

     Get scrolling amount [0..GetScrollMaxY()]

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.GetScrollY()


GetMainMenuBarHeight
^^^^^^^^^^^^^^^^^^^^

.. data:: GetMainMenuBarHeight()

    :return: ``number`` - *height*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local height  = tImGui.GetMainMenuBarHeight()


GetMouseCursor
^^^^^^^^^^^^^^

.. data:: GetMouseCursor()

     Get cursor type, reset in ImGui:NewFrame(), this is updated during the frame. Make sense be the last ImGui function called since everything was rendered.

    :return: ``string`` - One of the followings: ``ImGuiMouseCursor_None``, ``ImGuiMouseCursor_Arrow``, ``ImGuiMouseCursor_TextInput``, ``ImGuiMouseCursor_ResizeAll``, ``ImGuiMouseCursor_ResizeNS``, ``ImGuiMouseCursor_ResizeEW``, ``ImGuiMouseCursor_ResizeNESW``, ``ImGuiMouseCursor_ResizeNWSE``, ``ImGuiMouseCursor_Hand``, ``ImGuiMouseCursor_NotAllowed``.

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local sCursor = tImGui.GetMouseCursor()

GetWindowContentRegionMin
^^^^^^^^^^^^^^^^^^^^^^^^^


.. data:: GetWindowContentRegionMin()

     Content boundaries min (roughly (0,0)-Scroll), in window coordinates

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImVec2 = tImGui.GetWindowContentRegionMin()
    print(ImVec2.x,ImVec2.y)

GetWindowContentRegionMax
^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: GetWindowContentRegionMax()

     Content boundaries max (roughly (0,0)+Size-Scroll) where Size can be override with SetNextWindowContentSize(), in window coordinates

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    local tMax = tImGui.GetWindowContentRegionMax()
    print(tMax.x,tMax.y)

GetContentRegionAvail
^^^^^^^^^^^^^^^^^^^^^

.. data:: GetContentRegionAvail()

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImVec2 = tImGui.GetContentRegionAvail()
    print(ImVec2.x,ImVec2.y)

GetID
^^^^^

    Calculate unique ID (hash of whole ID stack + given parameter). e.g. if you want to query into ImGuiStorage yourself

.. data:: GetID(ptr_id)

    :param string: **ptr_id**
    :return: ``number`` - *ImGuiID*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local ptr_id  = 'id_0'
    local id      = tImGui.GetID(ptr_id)


GetContentRegionMax
^^^^^^^^^^^^^^^^^^^

.. data:: GetContentRegionMax()

     Current content boundaries (typically window boundaries including scrolling, or current column boundaries), in windows coordinates

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImVec2 = tImGui.GetContentRegionMax()
    print(ImVec2.x,ImVec2.y)

GetMouseDragDelta
^^^^^^^^^^^^^^^^^

.. data:: GetMouseDragDelta(button, lock_threshold)

     Return the delta from the initial clicking position while the mouse button is pressed or was just released. This is locked and return 0.0f until the mouse moves past a distance threshold at least once (if lock_threshold < -1.0, uses io.MouseDraggingThreshold)

    :param number: **button**
    :param number: **lock_threshold**
    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local button          =  0
    local lock_threshold  =  -1.0
    tImGui.GetMouseDragDelta(button, lock_threshold)

GetFrameHeight
^^^^^^^^^^^^^^

.. data:: GetFrameHeight()

     ~FontSize + style.FramePadding.y  *  2

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui             =  require "ImGui"
    local frame_height = tImGui.GetFrameHeight()
    print(frame_height)


GetFrameHeightWithSpacing
^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: GetFrameHeightWithSpacing()

     ~FontSize + style.FramePadding.y  *  2 + style.ItemSpacing.y (distance in pixels between 2 consecutive lines of framed widgets)

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    local s = tImGui.GetFrameHeightWithSpacing()
    print(s)

GetMousePosOnOpeningCurrentPopup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



.. data:: GetMousePosOnOpeningCurrentPopup()

     Retrieve mouse position at the time of opening popup we have BeginPopup() into (helper to avoid user backing that value themselves)

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImVec2 = tImGui.GetMousePosOnOpeningCurrentPopup()
    print(ImVec2.x,ImVec2.y)

GetMousePos
^^^^^^^^^^^

.. data:: GetMousePos()

     Shortcut to ImGui:GetIO().MousePos provided by user, to be consistent with other calls

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImVec2 = tImGui.GetMousePos()
    print(ImVec2.x,ImVec2.y)

GetStyleColorName
^^^^^^^^^^^^^^^^^

.. data:: GetStyleColorName(idx)

     Get a string corresponding to the enum value (for display, saving, etc.).

    :param string: **idx**
    :return: ``string`` - *style*

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    local idx  = 'id 09'
    local style = tImGui.GetStyleColorName(idx)
    print(style)

GetKeyIndex
^^^^^^^^^^^

.. data:: GetKeyIndex(imgui_key)

     Map ImGuiKey *  values into user's key index. == io.KeyMap[key] (0 to :guilabel:`ImGuiKey_COUNT` -1  (22))

    :param string: **imgui_key** * any of ``ImGuiKey_XXX``
    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui               = require "ImGui"
    local imgui_key      = 'ImGuiKey_DownArrow'
    local key_down_arrow = tImGui.GetKeyIndex(imgui_key)
    if tImGui.IsKeyDown(key_down_arrow) then
        print('Pressed key arrow down')
    end

.. data:: GetKeyIndex(imgui_key)

     Map ImGuiKey *  values into user's key index. == io.KeyMap[key] (0 to :guilabel:`ImGuiKey_COUNT` -1  (22))

    :param number: **imgui_key** * Dependent on SO. prefer to use ``ImGuiKey_XXX``
    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    local imgui_key  = 13 -- ImGuiKey_Enter if Windows
    local enter      = tImGui.GetKeyIndex(imgui_key)
    print(enter)

GetKeyPressedAmount
^^^^^^^^^^^^^^^^^^^


.. data:: GetKeyPressedAmount(ImGuiKey, repeat_delay, rate)

     Uses provided repeat rate/delay. return a count, most often 0 or 1 but might be >1 if RepeatRate is small enough that DeltaTime > RepeatRate

    :param string: **ImGuiKey** or raw index as ``number``
    :param number: **repeat_delay**
    :param number: **rate**
    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local ImGuiKey      = 'ImGuiKey_Space'
    local repeat_delay  = 0.2
    local rate          = 1

    tImGui.GetKeyPressedAmount(ImGuiKey, repeat_delay, rate)

GetScrollMaxY
^^^^^^^^^^^^^

.. data:: GetScrollMaxY()

     Get maximum scrolling amount ~~ ContentSize.Y - WindowSize.Y

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.GetScrollMaxY()

GetScrollMaxX
^^^^^^^^^^^^^

.. data:: GetScrollMaxX()

     Get maximum scrolling amount ~~ ContentSize.X - WindowSize.X

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.GetScrollMaxX()


GetScrollX
^^^^^^^^^^

.. data:: GetScrollX()

     Get scrolling amount [0..GetScrollMaxX()]

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    local ret = tImGui.GetScrollX()
    print(ret)

GetIO
^^^^^

    .. Note:: It is not exported to lua. It is used internally by the engine.

GetTextLineHeight
^^^^^^^^^^^^^^^^^

.. data:: GetTextLineHeight()

     ~FontSize

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.GetTextLineHeight()

GetWindowContentRegionWidth
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: GetWindowContentRegionWidth()

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.GetWindowContentRegionWidth()

GetWindowHeight
^^^^^^^^^^^^^^^

.. data:: GetWindowHeight()

     Get current window height (shortcut for GetWindowSize().y)

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local h = tImGui.GetWindowHeight()

GetTextLineHeightWithSpacing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: GetTextLineHeightWithSpacing()

     ~FontSize + style.ItemSpacing.y (distance in pixels between 2 consecutive lines of text)

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.GetTextLineHeightWithSpacing()

GetWindowSize
^^^^^^^^^^^^^

.. data:: GetWindowSize()

     Get current window size

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    local size = tImGui.GetWindowSize()
    print(size.x,size.y)


GetColorU32
^^^^^^^^^^^

.. data:: GetColorU32(color)

     Retrieve given color with style alpha applied

    :param number: **color**
    :param number: **alpha_mul**
    :return: ``number`` - *ImU32*

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local color     = 1
    local alpha_mul = 1.0
    tImGui.GetColorU32(color,alpha_mul)


GetItemRectMin
^^^^^^^^^^^^^^

.. data:: GetItemRectMin()

     Get upper-left bounding rectangle of the last item (screen space)

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    ImVec2  = tImGui.GetItemRectMin()

GetItemRectSize
^^^^^^^^^^^^^^^



.. data:: GetItemRectSize()

     Get size of last item

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    ImVec2  =  tImGui.GetItemRectSize()

GetItemRectMax
^^^^^^^^^^^^^^

.. data:: GetItemRectMax()

     Get lower-right bounding rectangle of the last item (screen space)

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    ImVec2  =  tImGui.GetItemRectMax()

GetFrameCount
^^^^^^^^^^^^^

.. data:: GetFrameCount()

     Get global ImGui frame count. incremented by 1 every frame.

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local fps = tImGui.GetFrameCount()

GetTime
^^^^^^^

.. data:: GetTime()

     Get global ImGui time. incremented by io.DeltaTime every frame.

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.GetTime()

GetColorU32
^^^^^^^^^^^

.. data:: GetColorU32(idx, alpha_mul)

     Retrieve given style color with style alpha applied and optional extra alpha multiplier

    :param number: **idx**
    :param number: **alpha_mul**
    :return: ``number`` - *ImU32*

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    local idx        =  0
    local alpha_mul  =  1.0

    tImGui.GetColorU32(idx, alpha_mul)

GetFontSize
^^^^^^^^^^^

.. data:: GetFontSize()

     Get current font size (= height in pixels) of current font with current scale applied

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local s = tImGui.GetFontSize()
    print(s)

GetColumnOffset
^^^^^^^^^^^^^^^

.. data:: GetColumnOffset(column_index)

     Get position of column line (in pixels, from the left side of the contents region). pass -1 to use current column, otherwise 0..GetColumnsCount() inclusive. column 0 is typically 0.0f

    :param number: **column_index**
    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local column_index  =  -1

    local ret = tImGui.GetColumnOffset(column_index)

GetDragDropPayload
^^^^^^^^^^^^^^^^^^

.. data:: GetDragDropPayload()

     Peek directly into the current payload from anywhere. may return ``nil``. use ImGuiPayload:IsDataType() to test for the payload type.

    :return: ``table`` - *ImGuiPayload* (might be ``nil``)

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImGuiPayload = tImGui.GetDragDropPayload()

GetColumnsCount
^^^^^^^^^^^^^^^

.. data:: GetColumnsCount()

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local s = tImGui.GetColumnsCount()
    print(s)

GetFontTexUvWhitePixel
^^^^^^^^^^^^^^^^^^^^^^

.. data:: GetFontTexUvWhitePixel()

     Get UV coordinate for a while pixel, useful to draw custom shapes via the ImDrawList API

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImVec2 = tImGui.GetFontTexUvWhitePixel()
    print(ImVec2.x,ImVec2.y)

GetColumnWidth
^^^^^^^^^^^^^^

.. data:: GetColumnWidth(column_index)

     Get column width (in pixels). pass -1 to use current column. Useful to calculate the width till the end when there is more than one component in the same column.

    :param number: **column_index**
    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local column_index  =  -1
    local width = tImGui.GetColumnWidth(column_index)
    print(width)

GetVersion
^^^^^^^^^^

.. data:: GetVersion()

     Get the compiled version string e.g. "1.23" (essentially the compiled value for IMGUI_VERSION)

    :return: ``string`` - *version*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local version = tImGui.GetVersion()
    print(version) --1.75 WIP

GetColumnIndex
^^^^^^^^^^^^^^

.. data:: GetColumnIndex()

     Get current column index

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local index = tImGui.GetColumnIndex()
    print(index)

GetFont
^^^^^^^

.. data:: GetFont()

     Get current font

    :return: ``table`` - *ImFont*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.GetFont()

GetStyleColorVec4
^^^^^^^^^^^^^^^^^

.. data:: GetStyleColorVec4(idx)

     Retrieve style color as stored in ImGuiStyle structure. use to feed back into PushStyleColor(), otherwise use GetColorU32() to get style color with style alpha baked in.

    :param number: **idx** (Start from 0)
    :return: ``table`` - *rgb* ``{r,g,b,a}``

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    local idx  = 0
    local color = tImGui.GetStyleColorVec4(0)
    print('color',color.r,color.g,color.b,color.a)

GetCursorScreenPos
^^^^^^^^^^^^^^^^^^

.. data:: GetCursorScreenPos()

     Cursor position in absolute screen coordinates [0..io.DisplaySize] (useful to work with ImDrawList API)

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local ImVec2  = tImGui.GetCursorScreenPos()
    print(ImVec2.x,ImVec2.y)


GetCursorPos
^^^^^^^^^^^^

.. data:: GetCursorPos()

     Cursor position in window coordinates (relative to window position)

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImVec2  = tImGui.GetCursorPos()
    print(ImVec2.x,ImVec2.y)

GetWindowPos
^^^^^^^^^^^^

.. data:: GetWindowPos()

     Get current window position in screen space (useful if you want to do your own drawing via the DrawList API)

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local ImVec2  = tImGui.GetWindowPos()
    print(ImVec2.x,ImVec2.y)


GetCursorPosX
^^^^^^^^^^^^^

.. data:: GetCursorPosX()

      (some functions are using window-relative coordinates, such as: GetCursorPos, GetCursorStartPos, GetContentRegionMax, GetWindowContentRegion *  etc.

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local x  = tImGui.GetCursorPosX()
    print(x)

GetCursorPosY
^^^^^^^^^^^^^

.. data:: GetCursorPosY()

      other functions such as GetCursorScreenPos or everything in ImDrawList:

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local y = tImGui.GetCursorPosY()

GetContentRegionAvailWidth
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: GetContentRegionAvailWidth()

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local width  = tImGui.GetContentRegionAvailWidth()

GetCursorStartPos
^^^^^^^^^^^^^^^^^

.. data:: GetCursorStartPos()

     Initial cursor position in window coordinates

    :return: ``table`` - *ImVec2* ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local ImVec2  = tImGui.GetCursorStartPos()

GetWindowWidth
^^^^^^^^^^^^^^

.. data:: GetWindowWidth()

     Get current window width (shortcut for GetWindowSize().x)

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  = require "ImGui"
    local w = tImGui.GetWindowWidth()


GetZoom
^^^^^^^

.. data:: GetZoom()

     Get current *zoom* or *scrolling wheel* (zoom might means literally zoom gesture on mobile).

    :return: ``number`` - *value* (``-1``, ``0`` or ``1``).

*Example:*

.. code-block:: lua

    tImGui     = require "ImGui"
    local zoom = tImGui.GetZoom()


ImDrawList
----------

:guilabel:`ImDrawList` is an internal class in the :guilabel:`ImGui` library responsible to draw native primitive commands.
 
:guilabel:`ImDrawList` is never accessed from :guilabel:`Lua` side. All methods are available through the :guilabel:`ImGui` module.

Use :guilabel:`SetImDrawListToBackground` and :guilabel:`SetImDrawListToForeground` to any subsequent command from :guilabel:`ImDrawList`. This enable/disable get ImDrawList from background or foreground.

SetImDrawListToBackground
^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: SetImDrawListToBackground(enable)

    :param boolean: **enable**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    tImGui.SetImDrawListToBackground(true)
    --any subsequent draw using ImDrawList will be added to background list.

SetImDrawListToForeground
^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: SetImDrawListToForeground(enable)

    :param boolean: **enable**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    tImGui.SetImDrawListToForeground(true)
    --any subsequent draw using ImDrawList will be added to foreground list.

AddImageQuad
^^^^^^^^^^^^


.. data:: AddImageQuad(texture_file_name, p1, p2, p3, p4, color, uv1, uv2, uv3, uv4)


    :param string: **texture file name** or ``number`` ( **id** of texture )
    :param table: **p1** ``{x,y}``
    :param table: **p2** ``{x,y}``
    :param table: **p3** ``{x,y}``
    :param table: **p4** ``{x,y}``
    :param table: **color** ``{r,g,b,a}`` default is ``{r=1,g=1,g=1,a=1}``
    :param table: **uv1** ``{x,y}`` default is ``{x=0,y=0}``
    :param table: **uv2** ``{x,y}`` default is ``{x=1,y=0}``
    :param table: **uv3** ``{x,y}`` default is ``{x=1,y=1}``
    :param table: **uv4** ``{x,y}`` default is ``{x=0,y=1}``

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"

    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then
            tImGui.Text('Some nice content to show!')
            local texture_file_name  = 'HB_smile.png'
            local p1                 = {x=0,y=0}
            local p2                 = {x=0,y=100}
            local p3                 = {x=100,y=100}
            local p4                 = {x=100,y=0}
            local color              = {r=1,g=1,b=1,a=1}
            local uv1                = {x=0,y=0}
            local uv2                = {x=0,y=1}
            local uv3                = {x=1,y=1}
            local uv4                = {x=1,y=0}
            tImGui.AddImageQuad(texture_file_name, p1, p2, p3, p4, color, uv1, uv2, uv3, uv4)
        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddImageQuad.gif
   :align: center
   :figclass: align-center

:download:`download HB_smile <example_modules/HB_smile.png>`.

AddText
^^^^^^^

.. data:: AddText(pos, color, text_begin, text_end)


    :param table: **pos** ``{x,y}``
    :param table: **color** ``{r,g,b,a}``
    :param string: **text_begin**
    :param string: **text_end** ``might be omitted``

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then
            tImGui.Text('Some nice content to show!')
            local ImVec2  = tImGui.GetWindowPos()

            local pos         = {x=100 + ImVec2.x,y=100 + ImVec2.y}
            local color       = {r=1,g=1,b=1,a=1}
            local text_begin  = '123456789 \n 123456789 \n123456789 \n123456789 \n123456789 \n'

            tImGui.AddText(pos, color, text_begin)
        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddText.png
   :align: center
   :figclass: align-center

AddPolyline
^^^^^^^^^^^

.. data:: AddPolyline(points, color, closed, thickness)

    :param table: **points** ``{{x,y},{x,y},{x,y},...}``
    :param table: **color** ``{r,g,b,a}``
    :param boolean: **closed**
    :param number: **thickness**

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    function loop(delta)
    
        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then
            tImGui.Text('Some nice content to show!')

            local winPos      = tImGui.GetWindowPos()
            local points      = {{x=0,y=0},{x=100,y=100},{x=100,y=200},{x=0,y=200}}
            local color       = {r=1,g=1,b=1,a=1}
            local closed      = false
            local thickness   = 1

            for i=1, #points do
                points[i].x = points[i].x + winPos.x
                points[i].y = points[i].y + winPos.y
            end

            tImGui.AddPolyline(points, color, closed, thickness)
        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddPolyline.png
   :align: center
   :figclass: align-center

AddNgonFilled
^^^^^^^^^^^^^

.. data:: AddNgonFilled(center, radius, color, num_segments)


    :param table: **center** ``{x,y}``
    :param number: **radius**
    :param table: **color** ``{r,g,b,a}``
    :param number: **num_segments**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then
            tImGui.Text('Some nice content to show!')

            local winPos      = tImGui.GetWindowPos()

            local center        = {x=winPos.x + 100,y=winPos.y + 100}
            local radius        = 15
            local color         = {r=1,g=1,b=1,a=1}
            local num_segments  = 15

            tImGui.AddNgonFilled(center, radius, color, num_segments)
        end
        tImGui.End()
    end

    tImGui.AddNgonFilled(center, radius, color, num_segments)

.. figure:: _static/imgui_AddNgonFilled.png
   :align: center
   :figclass: align-center

AddNgon
^^^^^^^

.. data:: AddNgon(center, radius, color, num_segments, thickness)

    :param table: **center** ``{x,y}``
    :param number: **radius**
    :param table: **color** ``{r,g,b,a}``
    :param number: **num_segments**
    :param number: **thickness**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then
            tImGui.Text('Some nice content to show!')

            local winPos      = tImGui.GetWindowPos()

            local center        = {x=winPos.x + 100,y=winPos.y + 100}
            local radius        = 15
            local color         = {r=1,g=0,b=1,a=1}
            local num_segments  = 5
            local thickness     =  3.0

            tImGui.AddNgon(center, radius, color, num_segments, thickness)
        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddNgon.png
   :align: center
   :figclass: align-center

AddCircle
^^^^^^^^^

.. data:: AddCircle(center, radius, color, num_segments, thickness)

    :param table: **center** ``{x,y}``
    :param number: **radius**
    :param table: **color** ``{r,g,b,a}``
    :param number: **num_segments**
    :param number: **thickness**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then
            tImGui.Text('Some nice content to show!')

            local winPos      = tImGui.GetWindowPos()

            local center        = {x=winPos.x + 50,y=winPos.y + 50}
            local radius        = 15
            local color         = {r=1,g=0,b=0,a=0.8}
            local num_segments  = 12
            local thickness     = 1.0

            tImGui.AddCircle(center, radius, color, num_segments, thickness)
        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddCircle.png
   :align: center
   :figclass: align-center

AddConvexPolyFilled
^^^^^^^^^^^^^^^^^^^

.. data:: AddConvexPolyFilled(points, color)

     Note: Anti-aliased filling requires points to be in clockwise order.

    :param table: **points** ``{{x,y},{x,y},{x,y},...}``
    :param table: **color** ``{r,g,b,a}``

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            local winPos      = tImGui.GetWindowPos()

            local points      = {{x=10,y=10},{x=200,y=10},{x=200,y=100},{x=30,y=90}}
            local color       = {r=0,g=0.2,b=1,a=0.5}

            for i=1, #points do
                points[i].x = points[i].x + winPos.x
                points[i].y = points[i].y + winPos.y
            end

            tImGui.AddConvexPolyFilled(points, color)

            tImGui.Text('Some nice content to show!')
        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddConvexPolyFilled.png
   :align: center
   :figclass: align-center

AddImage
^^^^^^^^

.. data:: AddImage(texture_file_name, p_min, p_max, color, uv_min, uv_max)


    :param string: **texture file name** or ``number`` ( **id** of texture )
    :param table: **p_min** ``{x,y}``
    :param table: **p_max** ``{x,y}``
    :param table: **color** ``{r,g,b,a}`` default is ``{r=1,g=1,b=1,a=1}``
    :param table: **uv_min** ``{x,y}`` default is ``{x=0,y=0}``
    :param table: **uv_max** ``{x,y}`` default is ``{x=1,y=1}``

*Example:*

.. code-block:: lua

    tImGui                   =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            local winPos      = tImGui.GetWindowPos()

            local texture_file_name  =  'HB_smile.png'
            local color              =  {r=1,g=1,b=1,a=1}
            local p_min              =  winPos
            local p_max              =  {x=winPos.x + 100,y= winPos.y + 100}
            local uv_min             =  {x=0,y=0}
            local uv_max             =  {x=1,y=1}

            tImGui.AddImage(texture_file_name, p_min, p_max, color, uv_min,uv_max )

            tImGui.Text('Some nice content to show!')
        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddImage.png
   :align: center
   :figclass: align-center

:download:`download HB_smile <example_modules/HB_smile.png>`.

AddBezierCurve
^^^^^^^^^^^^^^

.. data:: AddBezierCurve(p1, p2, p3, p4, color, thickness, num_segments)


    :param table: **p1** ``{x,y}``
    :param table: **p2** ``{x,y}``
    :param table: **p3** ``{x,y}``
    :param table: **p4** ``{x,y}``
    :param table: **color** ``{r,g,b,a}``
    :param number: **thickness**
    :param number: **num_segments**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos        = tImGui.GetWindowPos()

            local p1            = winPos
            local p2            = {x=winPos.x + 50,y=winPos.y + 50}
            local p3            = {x=winPos.x + 150,y=winPos.y + 150}
            local p4            = {x=winPos.x + 200,y=winPos.y + 50}
            local color         = {r = 1,g = 1,b = 0,a = 1}
            local thickness     = 2
            local num_segments  = 30

            tImGui.AddBezierCurve(p1, p2, p3, p4, color, thickness, num_segments)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddBezierCurve.png
   :align: center
   :figclass: align-center

AddDrawCmd
^^^^^^^^^^

.. data:: AddDrawCmd()

     This is useful if you need to forcefully create a new draw call (to allow for dependent rendering / blending). Otherwise primitives are merged into the same draw-call as much as possible

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.AddDrawCmd()

AddImageRounded
^^^^^^^^^^^^^^^

.. data:: AddImageRounded(texture_file_name, p_min, p_max, uv_min, uv_max, color, rounding, rounding_corners)


    :param string: **texture file name** or ``number`` ( **id** of texture )
    :param table: **p_min** ``{x,y}``
    :param table: **p_max** ``{x,y}``
    :param table: **color** ``{r,g,b,a}``
    :param table: **uv_min** ``{x,y}`` default is ``{x=0,y=0}``
    :param table: **uv_max** ``{x,y}`` default is ``{x=1,y=1}``
    :param number: **rounding** default is ``0.0``
    :param number: **rounding_corners** default is ``ImDrawCornerFlags_All``

*Example:*

.. code-block:: lua

    tImGui                  =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos      = tImGui.GetWindowPos()

            local texture_file_name = 'crate.png'
            local p_min             = {x= 100,y= 100}
            local p_max             = {x= 200,y= 200}
            local color             = {r=1,g=1,b=1,a=1}
            local uv_min            = {x=0,y=0}
            local uv_max            = {x=1,y=1}
            local rounding          = 20.0
            local rounding_corners  =  tImGui.Flags('ImDrawCornerFlags_All')

            tImGui.AddImageRounded(texture_file_name, p_min, p_max, color, uv_min, uv_max, rounding, rounding_corners)

        end
        tImGui.End()
    end

.. figure:: _static/crate.png
   :align: center
   :width: 200
   :height: 200
   :figclass: align-center

   Texture original

.. figure:: _static/imgui_AddImageRounded.png
   :align: center
   :figclass: align-center

   AddImageRounded

:download:`download crate <_static/crate.png>`.

AddTriangleFilled
^^^^^^^^^^^^^^^^^

.. data:: AddTriangleFilled(p1, p2, p3, color)

    :param table: **p1** ``{x,y}``
    :param table: **p2** ``{x,y}``
    :param table: **p3** ``{x,y}``
    :param table: **color** ``{r,g,b,a}``

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    function loop(delta)
        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local p1     = {x=10,y=100}
            local p2     = {x=50,y=200}
            local p3     = {x=100,y=100}
            local color  = {r=1,g=1,b=0,a=1}

            tImGui.AddTriangleFilled(p1, p2, p3, color)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddTriangleFilled.png
   :align: center
   :figclass: align-center

AddCircleFilled
^^^^^^^^^^^^^^^

.. data:: AddCircleFilled(center, radius, color, num_segments)

    :param table: **center** ``{x,y}``
    :param number: **radius**
    :param table: **color** ``{r,g,b,a}``
    :param number: **num_segments**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos        = tImGui.GetWindowPos()
            local center        = {x=winPos.x + 50,y=winPos.y + 50}
            local radius        = 15
            local color         = {r=0,g=0,b=1,a=1}
            local num_segments  =  12

            tImGui.AddCircleFilled(center, radius, color, num_segments)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddCircleFilled.png
   :align: center
   :figclass: align-center

AddLine
^^^^^^^

.. data:: AddLine(p1, p2, color, thickness)

    :param table: **p1** ``{x,y}``
    :param table: **p2** ``{x,y}``
    :param number: **color** ``{r,g,b,a}``
    :param number: **thickness**

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos     = tImGui.GetWindowPos()
            local p1         = winPos
            local p2         = {x = winPos.x + 50, y = winPos.y + 50}
            local color      = {r=1, g=1, b=0, a=1}
            local thickness  =  1.0

            tImGui.AddLine(p1, p2, color, thickness)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddLine.png
   :align: center
   :figclass: align-center

AddTriangle
^^^^^^^^^^^

.. data:: AddTriangle(p1, p2, p3, color, thickness)

    :param table: **p1** ``{x,y}``
    :param table: **p2** ``{x,y}``
    :param table: **p3** ``{x,y}``
    :param table: **color** ``{r,g,b,a}``
    :param number: **thickness**

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos     = tImGui.GetWindowPos()
            local p1     = {x=100 + winPos.x,y=100 + winPos.y}
            local p2     = {x=150 + winPos.x,y=200 + winPos.y}
            local p3     = {x=200 + winPos.x,y=100 + winPos.y}
            local color  = {r=1,g=1,b=1,a=1}
            local thickness  =  10.0

            tImGui.AddTriangle(p1, p2, p3, color, thickness)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddTriangle.png
   :align: center
   :figclass: align-center

AddRect
^^^^^^^

.. data:: AddRect(p_min, p_max, color, rounding, rounding_corners, thickness)

     A: upper-left, b: lower-right (== upper-left + size), rounding_corners_flags: 4 bits corresponding to which corner to round

    :param table: **p_min** ``{x,y}``
    :param table: **p_max** ``{x,y}``
    :param table: **color**  ``{r,g,b,a}``
    :param number: **rounding**
    :param table: **rounding_corners**
    :param number: **thickness**

*Example:*

.. code-block:: lua

    tImGui                  =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local p_min             = {x=100,y=100}
            local p_max             = {x=200,y=200}
            local color             = {r=1,g=1,b=1,a=1}
            local rounding          =  10.0
            local rounding_corners  =  tImGui.Flags('ImDrawCornerFlags_All')
            local thickness         =  5.0

            tImGui.AddRect(p_min, p_max, color, rounding, rounding_corners, thickness)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddRect.png
   :align: center
   :figclass: align-center

AddRectFilledMultiColor
^^^^^^^^^^^^^^^^^^^^^^^

.. data:: AddRectFilledMultiColor(p_min, p_max, color_upr_left, color_upr_right, color_bot_right, color_bot_left)

    :param table: **p_min** ``{x,y}``
    :param table: **p_max** ``{x,y}``
    :param table: **color_upr_left**   ``{r,g,b,a}``
    :param table: **color_upr_right**  ``{r,g,b,a}``
    :param table: **color_bot_right**  ``{r,g,b,a}``
    :param table: **color_bot_left**   ``{r,g,b,a}``

*Example:*

.. code-block:: lua

    tImGui               =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos         = tImGui.GetWindowPos()
            local p_min          = winPos
            local p_max          = {x = winPos.x +  100, y = winPos.y + 90}
            local color_upr_left   = {r = 1, g = 0, b = 0, a = 1}
            local color_upr_right  = {r = 0, g = 1, b = 0, a = 1}
            local color_bot_right  = {r = 0, g = 0, b = 1, a = 1}
            local color_bot_left   = {r = 1, g = 1, b = 0, a = 1}

            tImGui.AddRectFilledMultiColor(p_min, p_max, color_upr_left, color_upr_right, color_bot_right, color_bot_left)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddRectFilledMultiColor.png
   :align: center
   :figclass: align-center

AddRectFilled
^^^^^^^^^^^^^

.. data:: AddRectFilled(p_min, p_max, color, rounding, rounding_corners)

     A: upper-left, b: lower-right (== upper-left + size)

    :param table: **p_min** ``{x,y}``
    :param table: **p_max** ``{x,y}``
    :param table: **color** ``{r,g,b,a}``
    :param number: **rounding**
    :param number: **rounding_corners**

*Example:*

.. code-block:: lua

    tImGui                  =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos         = tImGui.GetWindowPos()
            local p_min             = {x=winPos.x + 50,y=winPos.y + 50}
            local p_max             = {x=winPos.x + 150,y=winPos.y + 100}
            local color             = {r=0,g=0,b=0.5,a=1}
            local rounding          =  20.0
            local rounding_corners  =  tImGui.Flags('ImDrawCornerFlags_All')

            tImGui.AddRectFilled(p_min, p_max, color, rounding, rounding_corners)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddRectFilled.png
   :align: center
   :figclass: align-center

AddQuadFilled
^^^^^^^^^^^^^

.. data:: AddQuadFilled(p1, p2, p3, p4, color)

    :param table: **p1** ``{x,y}``
    :param table: **p2** ``{x,y}``
    :param table: **p3** ``{x,y}``
    :param table: **p4** ``{x,y}``
    :param table: **color** ``{r,g,b,a}``

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos = tImGui.GetWindowPos()
            local p1     = {x= winPos.x + 100 ,y = winPos.y + 100}
            local p2     = {x= winPos.x + 200 ,y = winPos.y + 100}
            local p3     = {x= winPos.x + 200 ,y = winPos.y + 200}
            local p4     = {x= winPos.x + 100 ,y = winPos.y + 150}
            local color  = {r=1,g=0,b=0,a=1}

            tImGui.AddQuadFilled(p1, p2, p3, p4, color)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddQuadFilled.png
   :align: center
   :figclass: align-center

AddQuad
^^^^^^^

.. data:: AddQuad(p1, p2, p3, p4, color, thickness)

    :param table: **p1** ``{x,y}``
    :param table: **p2** ``{x,y}``
    :param table: **p3** ``{x,y}``
    :param table: **p4** ``{x,y}``
    :param table: **color** ``{r,g,b,a}``
    :param number: **thickness**

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    function loop(delta)

        local title     = 'Some window'
        local closeable =  true
        local flags     =  0

        local is_opened, closed_clicked = tImGui.Begin(title, closeable, flags)

        if is_opened then

            tImGui.Text('Some nice content to show!')

            local winPos     = tImGui.GetWindowPos()
            local p1         = {x= winPos.x + 100 ,y = winPos.y + 100}
            local p2         = {x= winPos.x + 200 ,y = winPos.y + 100}
            local p3         = {x= winPos.x + 200 ,y = winPos.y + 200}
            local p4         = {x= winPos.x + 100 ,y = winPos.y + 150}
            local color      = {r=1,g=1,b=1,a=1}
            local thickness  =  10.0

            tImGui.AddQuad(p1, p2, p3, p4, color, thickness)

        end
        tImGui.End()
    end

.. figure:: _static/imgui_AddQuad.png
   :align: center
   :figclass: align-center

ImDrawList PopTextureID
^^^^^^^^^^^^^^^^^^^^^^^

.. data:: PopTextureID()

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.PopTextureID()

ImDrawList PushTextureID
^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: PushTextureID(texture_filename)

    :param string: **texture_filename**

*Example:*

.. code-block:: lua

    tImGui                  =  require "ImGui"
    local texture_filename  = 'mario.png'

    tImGui.PushTextureID(texture_filename)


NB: all primitives needs to be reserved via PrimReserve() beforehand!

Is methods
----------


IsAnyItemHovered
^^^^^^^^^^^^^^^^


.. data:: IsAnyItemHovered()

     Is any item hovered?

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    local hovered = tImGui.IsAnyItemHovered()
    print(hovered)

IsAnyMouseDown
^^^^^^^^^^^^^^

.. data:: IsAnyMouseDown()

     Is any mouse button held?

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsAnyMouseDown()
    print(result)

IsAnyWindowHovered
^^^^^^^^^^^^^^^^^^

.. data:: IsAnyWindowHovered()

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsAnyWindowHovered()
    print(result)

IsAnyWindowFocused
^^^^^^^^^^^^^^^^^^

.. data:: IsAnyWindowFocused()

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsAnyWindowFocused()
    print(result)

IsAnyItemFocused
^^^^^^^^^^^^^^^^

.. data:: IsAnyItemFocused()

     Is any item focused?

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsAnyItemFocused()
    print(result)

IsAnyItemActive
^^^^^^^^^^^^^^^



.. data:: IsAnyItemActive()

     Is any item active?

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsAnyItemActive()
    print(result)


IsItemDeactivatedAfterEdit
^^^^^^^^^^^^^^^^^^^^^^^^^^



.. data:: IsItemDeactivatedAfterEdit()

     Was the last item just made inactive and made a value change when it was active? (e.g. Slider/Drag moved). Useful for Undo/Redo patterns with widgets that requires continuous editing. Note that you may get false positives (some widgets such as Combo()/ListBox()/Selectable() will return true even when clicking an already selected item).

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemDeactivatedAfterEdit()
    print(result)

IsItemEdited
^^^^^^^^^^^^

.. data:: IsItemEdited()

     Did the last item modify its underlying value this frame? or was pressed? This is generally the same as the "bool" return value of many widgets.

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemEdited()
    print(result)

IsItemToggledOpen
^^^^^^^^^^^^^^^^^

.. data:: IsItemToggledOpen()

     Was the last item open state toggled? set by TreeNode().

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemToggledOpen()
    print(result)

IsItemDeactivatedAfterChange
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: IsItemDeactivatedAfterChange()


    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemDeactivatedAfterChange()
    print(result)

IsItemDeactivated
^^^^^^^^^^^^^^^^^

.. data:: IsItemDeactivated()

     Was the last item just made inactive (item was previously active). Useful for Undo/Redo patterns with widgets that requires continuous editing.

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemDeactivated()
    print(result)

IsItemActive
^^^^^^^^^^^^

.. data:: IsItemActive()

     Is the last item active? (e.g. button being held, text field being edited. This will continuously return true while holding mouse button on an item. Items that don't interact will always return false)

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemActive()
    print(result)

IsItemHovered
^^^^^^^^^^^^^

.. data:: IsItemHovered(flags)

     Is the last item hovered? (and usable, aka not blocked by a popup, etc.). See ImGuiHoveredFlags for more options.

    :param number: **flags**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local flags  =  0
    local result = tImGui.IsItemHovered(flags)
    print(result)


IsItemActivated
^^^^^^^^^^^^^^^

.. data:: IsItemActivated()

     Was the last item just made active (item was previously inactive).

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemActivated()
    print(result)

IsItemVisible
^^^^^^^^^^^^^

.. data:: IsItemVisible()

     Is the last item visible? (items may be out of sight because of clipping/scrolling)

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemVisible()
    print(result)

IsItemFocused
^^^^^^^^^^^^^

.. data:: IsItemFocused()

     Is the last item focused for keyboard/gamepad navigation?

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsItemFocused()
    print(result)

IsItemClicked
^^^^^^^^^^^^^

.. data:: IsItemClicked(mouse_button)

     Is the last item clicked? (e.g. button/node just clicked on) == IsMouseClicked(mouse_button) && IsItemHovered()

    :param number: **mouse_button** (0=left, 1=right, 2=middle)
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local mouse_button  =  0
    local result = tImGui.IsItemClicked(mouse_button)
    print(result)

IsKeyReleased
^^^^^^^^^^^^^

.. data:: IsKeyReleased(ImGuiKey)

     Was key released (went from Down to !Down)?

    :param string: **ImGuiKey** or raw index as ``number``
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local ImGuiKey        = 'ImGuiKey_Enter'
    local result          = tImGui.IsKeyReleased(ImGuiKey)
    print(result)

IsKeyDown
^^^^^^^^^

.. data:: IsKeyDown(ImGuiKey)

     Is key being held. == io.KeysDown[key_index].

    :param string: **ImGuiKey** or raw index as ``number``
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local ImGuiKey        = 'ImGuiKey_A'
    local result          = tImGui.IsKeyDown(ImGuiKey)
    print(result)

IsKeyPressed
^^^^^^^^^^^^

.. data:: IsKeyPressed(ImGuiKey, is_repeated)

     Was key pressed (went from !Down to Down)? if is_repeated = ``true``, uses io.KeyRepeatDelay / KeyRepeatRate

    :param string: **ImGuiKey** or raw index as ``number``
    :param boolean: **is_repeated**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local ImGuiKey        = 'ImGuiKey_Space'
    local is_repeated     =  true
    local result          = tImGui.IsKeyPressed(ImGuiKey,is_repeated)
    print(result)

IsMouseDown
^^^^^^^^^^^

.. data:: IsMouseDown(button)

     Is mouse button held? (0=left, 1=right, 2=middle)

    :param number: **button**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local button          = 1
    local result          = tImGui.IsMouseDown(button)
    print(result)

IsMouseDoubleClicked
^^^^^^^^^^^^^^^^^^^^

.. data:: IsMouseDoubleClicked(button)

     Did mouse button double-clicked? a double-click returns false in IsMouseClicked(). uses io.MouseDoubleClickTime.

    :param number: **button**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local button  = 0
    local result  = tImGui.IsMouseDoubleClicked(button)
    print(result)

IsMouseClicked
^^^^^^^^^^^^^^

.. data:: IsMouseClicked(button, is_repeated)

     Did mouse button clicked? (went from !Down to Down) (0=left, 1=right, 2=middle)

    :param number: **button**
    :param boolean: **is_repeated**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui             =  require "ImGui"
    local button       = 0
    local is_repeated  = false
    local result       = tImGui.IsMouseClicked(button,is_repeated)
    print(result)

IsMouseHoveringRect
^^^^^^^^^^^^^^^^^^^

.. data:: IsMouseHoveringRect(r_min, r_max, clip)

     Is mouse hovering given bounding rect (in screen space). clipped by current clipping settings, but disregarding of other consideration of focus/window ordering/popup-block.

    :param table: **r_min** ``{x,y}``
    :param table: **r_max** ``{x,y}``
    :param boolean: **clip**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local r_min  = {x = 0 , y = 0}
    local r_max  = {x = 100 , y = 100}
    local clip   = true
    local result = tImGui.IsMouseHoveringRect(r_min, r_max, clip)
    print(result)

IsMouseDragging
^^^^^^^^^^^^^^^

.. data:: IsMouseDragging(button, lock_threshold)

     Is mouse dragging? (if lock_threshold < -1.0, uses io.MouseDraggingThreshold)

    :param number: **button**
    :param number: **lock_threshold**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local button          =  0
    local lock_threshold  = -1.0
    local result          = tImGui.IsMouseDragging(button, lock_threshold)
    print(result)

IsMouseReleased
^^^^^^^^^^^^^^^

.. data:: IsMouseReleased(button)

     Did mouse button released? (went from Down to !Down)

    :param number: **button**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local button  = 0
    local result  = tImGui.IsMouseReleased(button)
    print(result)

IsMousePosValid
^^^^^^^^^^^^^^^

.. data:: IsMousePosValid(mouse_pos)

     By convention we use (-FLT_MAX,-FLT_MAX) to denote that there is no mouse available

    :param table: **mouse_pos** ``{x,y}``
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    local mouse_pos  = {x=100,y=100}
    local result     = tImGui.IsMousePosValid(mouse_pos)
    print(result)


IsPopupOpen
^^^^^^^^^^^

.. data:: IsPopupOpen(str_id, * flag)

     Return true if the popup is open at the current begin-ed level of the popup stack.

    :param string: **str_id** (might be ``nil``)
    :param param: **flag** (default is 0)
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local str_id  =  '01'
    tImGui.IsPopupOpen(str_id)

.. data:: IsPopupOpen(id)

     Return true if the popup is open at the current begin-ed level of the popup stack.

    :param number: **id**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local id      =  1
    tImGui.IsPopupOpen(id)

IsRectVisible
^^^^^^^^^^^^^

.. data:: IsRectVisible(rect_min, rect_max)

     Test if rectangle (in screen space) is visible / not clipped. to perform coarse clipping on user's side.

    :param table: **rect_min** ``{x,y}``
    :param table: **rect_max** ``{x,y}``
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local rect_min  = {x = 0,y = 0}
    local rect_max  = {x = 100,y = 100}
    local result    = tImGui.IsRectVisible(rect_min, rect_max)
    print(result)

IsScrollVisible
^^^^^^^^^^^^^^^

.. data:: IsScrollVisible()

     Return the state of the scroll (visible / not visible)

    :return: ``boolean`` - *x*, ``boolean`` - *y*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local x,y    = tImGui.IsScrollVisible()
    print('Scroll visible in the axis x?',x)
    print('Scroll visible in the axis y?',y)


IsWindowFocused
^^^^^^^^^^^^^^^

.. data:: IsWindowFocused(flags)

     Is current window focused? or its root/child, depending on flags. see flags for options.

    :param number: **flags**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local flags  = 0
    local result = tImGui.IsWindowFocused(flags)
    print(result)

IsWindowHovered
^^^^^^^^^^^^^^^

.. data:: IsWindowHovered(flags)

     Is current window hovered (and typically: not blocked by a popup/modal)? see flags for options. NB: If you are trying to check whether your mouse should be dispatched to ImGui or to your app, you should use the 'io.WantCaptureMouse' boolean for that! Please read the FAQ!

    :param number: **flags**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local flags  = 0
    local result = tImGui.IsWindowHovered(flags)
    print(result)

IsWindowCollapsed
^^^^^^^^^^^^^^^^^

.. data:: IsWindowCollapsed()

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsWindowCollapsed()
    print(result)

IsWindowAppearing
^^^^^^^^^^^^^^^^^

.. data:: IsWindowAppearing()

    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local result = tImGui.IsWindowAppearing()
    print(result)


Save / Load methods
-------------------


SaveIniSettingsToMemory
^^^^^^^^^^^^^^^^^^^^^^^

.. data:: SaveIniSettingsToMemory()

     Return a zero-terminated string with the .ini data which you can save by your own mean. call when io.WantSaveIniSettings is set, then save data by your own mean and clear io.WantSaveIniSettings.

    :return: ``string`` - *setting as string*

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local settings      =  tImGui.SaveIniSettingsToMemory()

SaveIniSettingsToDisk
^^^^^^^^^^^^^^^^^^^^^

.. data:: SaveIniSettingsToDisk(ini_filename)

     This is automatically called (if io.IniFilename is not empty) a few seconds after any modification that should be reflected in the .ini file (and also by DestroyContext).

    :param string: **ini_filename**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local ini_filename  = 'file_name.ini'
    tImGui.SaveIniSettingsToDisk(ini_filename)

LoadIniSettingsFromDisk
^^^^^^^^^^^^^^^^^^^^^^^

    TODO: This method is available however is not implemented properly (it should be called after CreateContext() and before the first call to NewFrame())

.. data:: LoadIniSettingsFromDisk(ini_filename)

     Call after CreateContext() and before the first call to NewFrame(). NewFrame() automatically calls LoadIniSettingsFromDisk(io.IniFilename).

    :param string: **ini_filename**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local ini_filename  = 'file_name.ini'

    tImGui.LoadIniSettingsFromDisk(ini_filename)

LoadIniSettingsFromMemory
^^^^^^^^^^^^^^^^^^^^^^^^^

    TODO: This method is available however is not implemented properly (it should be called after CreateContext() and before the first call to NewFrame())

.. data:: LoadIniSettingsFromMemory(ini_data)

     Call after CreateContext() and before the first call to NewFrame() to provide .ini data from your own data source.

    :param string: **ini_data**

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local ini_data  = '[Window][Debug##Default] \n Pos=229,80\n Size=407,363 \n Collapsed=0'

    tImGui.LoadIniSettingsFromMemory(ini_data)


Log methods
-----------


LogText
^^^^^^^


.. data:: LogText(text)

     Pass text data straight to log (without being displayed)

    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui             =  require "ImGui"
    local text         = 'some log ..'

    tImGui.LogText(text)

LogFinish
^^^^^^^^^

.. data:: LogFinish()

     Stop logging (close file, etc.)

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.LogFinish()

LogButtons
^^^^^^^^^^

.. data:: LogButtons()

     Helper to display buttons for logging to tty/file/clipboard

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.LogButtons()

LogToTTY
^^^^^^^^

.. data:: LogToTTY(auto_open_depth)

     Start logging to tty (stdout)

    :param number: **auto_open_depth**

*Example:*

.. code-block:: lua

    tImGui                 =  require "ImGui"
    local auto_open_depth  =  -1
    tImGui.LogToTTY(auto_open_depth)

LogToClipboard
^^^^^^^^^^^^^^

.. data:: LogToClipboard(auto_open_depth)

     Start logging to OS clipboard

    :param number: **auto_open_depth**

*Example:*

.. code-block:: lua

    tImGui                 =  require "ImGui"
    local auto_open_depth  =  -1
    tImGui.LogToClipboard(auto_open_depth)

LogToFile
^^^^^^^^^

.. data:: LogToFile(auto_open_depth, filename)

     Start logging to file

    :param number: **auto_open_depth**
    :param string: **filename**

*Example:*

.. code-block:: lua

    tImGui                 =  require "ImGui"
    local auto_open_depth  =  -1
    local filename         =  'file.log'

    tImGui.LogToFile(auto_open_depth, filename)


NewFrame / EndFrame
-------------------

The engine calls NewFrame() / EndFrame() automatically. You do not to need to worry if using this module with :guilabel:`mbm` engine.


Open / Close methods
--------------------

OpenPopupOnItemClick
^^^^^^^^^^^^^^^^^^^^

.. data:: OpenPopupOnItemClick(str_id, mouse_button)

     Helper to open popup when clicked on last item (note: actually triggers on the mouse _released_ event to be consistent with popup behaviors). return true when just opened.

    :param string: **str_id** (might be ``nil``)
    :param number: **mouse_button** (0=left, 1=right, 2=middle)
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local str_id        =  nil
    local mouse_button  =  0
    tImGui.OpenPopupOnItemClick(str_id, mouse_button)

OpenPopup
^^^^^^^^^

    Popups, Modals

    The properties of popups windows are:

    - They block normal mouse hovering detection outside them. (*) - Unless modal, they can be closed by clicking anywhere outside them, or by pressing ESCAPE.

    - Their visibility state (~bool) is held internally by imgui instead of being held by the programmer as we are used to with regular Begin() calls.

    User can manipulate the visibility state by calling OpenPopup().

    - We default to use the right mouse (ImGuiMouseButton_Right=1) for the Popup Context functions.

    .. Note:: 
        You can use IsItemHovered(ImGuiHoveredFlags_AllowWhenBlockedByPopup) to bypass it and detect hovering even when normally blocked by a popup.
        Those three properties are connected. The library needs to hold their visibility state because it can close popups at any time.


.. data:: OpenPopup(str_id)

     Call to mark popup as open (don't call every frame!). popups are closed when user click outside, or if CloseCurrentPopup() is called within a BeginPopup()/EndPopup() block. By default, Selectable()/MenuItem() are calling CloseCurrentPopup(). Popup identifiers are relative to the current ID-stack (so OpenPopup and BeginPopup needs to be at the same level).

    :param string: **str_id**

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local str_id  =  '01'
    tImGui.OpenPopup(str_id)


CloseCurrentPopup
^^^^^^^^^^^^^^^^^

.. data:: CloseCurrentPopup()

     Close the popup we have begin-ed into. clicking on a MenuItem or Selectable automatically close the current popup.

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.CloseCurrentPopup()

Pop methods
-----------


PopAllowKeyboardFocus
^^^^^^^^^^^^^^^^^^^^^

.. data:: PopAllowKeyboardFocus()


*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.PopAllowKeyboardFocus()


PopItemWidth
^^^^^^^^^^^^

.. data:: PopItemWidth()


*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.PopItemWidth()

PopButtonRepeat
^^^^^^^^^^^^^^^

.. data:: PopButtonRepeat()


*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.PopButtonRepeat()

PopTextWrapPos
^^^^^^^^^^^^^^

.. data:: PopTextWrapPos()


*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.PopTextWrapPos()

PopFont
^^^^^^^

.. data:: PopFont()

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.PopFont()

PopClipRect
^^^^^^^^^^^

.. data:: PopClipRect()


*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.PopClipRect()

PopID
^^^^^

.. data:: PopID()

     Pop from the ID stack.

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.PopID()


Push methods
------------

PushFont
^^^^^^^^

    TODO: implement / check

.. data:: PushFont(font)

     Use ``nil`` as a shortcut to push default font

    :param table: **font**

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local font  = {}
    tImGui.PushFont(font)


PushAllowKeyboardFocus
^^^^^^^^^^^^^^^^^^^^^^

.. data:: PushAllowKeyboardFocus(allow_keyboard_focus)

     Allow focusing using TAB/Shift-TAB, enabled by default but you can disable it for certain widgets

    :param boolean: **allow_keyboard_focus**

*Example:*

.. code-block:: lua

    tImGui                      =  require "ImGui"
    local allow_keyboard_focus  = {}
    tImGui.PushAllowKeyboardFocus(allow_keyboard_focus)


PushID
^^^^^^
    **ID stack/scopes**

    - Read the FAQ for more details about how ID are handled in dear imgui. If you are creating widgets in a loop you most likely want to push a unique identifier (e.g. object pointer, loop index) to uniquely differentiate them.

    - The resulting ID are hashes of the entire stack.

    - You can also use the "Label##foobar" syntax within widget label to distinguish them from each others.

    - In this header file we use the "label"/"name" terminology to denote a string that will be displayed and used as an ID, whereas "str_id" denote a string that is only used as an ID and not normally displayed.


.. data:: PushID(str_id_begin, str_id_end)

     Push string into the ID stack (will hash string).

    :param string: **str_id_begin**
    :param string: **str_id_end**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local str_id_begin  = 'id 0'
    local str_id_end    = '9'
    tImGui.PushID(str_id_begin, str_id_end)

PushID
^^^^^^

.. data:: PushID(str_id)

     Push string into the ID stack (will hash string).

    :param string: **str_id**

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local str_id  =  '01'
    tImGui.PushID(str_id)

PushID
^^^^^^

.. data:: PushID(value)

     Push number into the ID stack (will hash int).

    :param number: **value**

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local value   =  1
    tImGui.PushID(value)

PushButtonRepeat
^^^^^^^^^^^^^^^^

.. data:: PushButtonRepeat(repeated)

     In 'repeat' mode, Button * () functions return repeated true in a typematic manner (using io.KeyRepeatDelay/io.KeyRepeatRate setting). Note that you can call IsItemActive() after any Button() to tell if the button is held in the current frame.

    :param boolean: **repeated**

*Example:*

.. code-block:: lua

    tImGui          = require "ImGui"
    local repeated  = false
    tImGui.PushButtonRepeat(repeated)

PushTextWrapPos
^^^^^^^^^^^^^^^

.. data:: PushTextWrapPos(wrap_local_pos_x)

     Word-wrapping for Text * () commands. < 0.0f: no wrapping; 0.0f: wrap to end of window (or column); > 0.0f: wrap at 'wrap_pos_x' position in window local space

    :param number: **wrap_local_pos_x**

*Example:*

.. code-block:: lua

    tImGui                  =  require "ImGui"
    local wrap_local_pos_x  =  0.0
    tImGui.PushTextWrapPos(wrap_local_pos_x)

PushClipRect
^^^^^^^^^^^^

.. data:: PushClipRect(clip_rect_min, clip_rect_max, intersect_with_current_clip_rect)

    :param table: **clip_rect_min** ``{x,y}``
    :param table: **clip_rect_max** ``{x,y}``
    :param boolean: **intersect_with_current_clip_rect**

*Example:*

.. code-block:: lua

    tImGui                                  =  require "ImGui"
    local clip_rect_min                     = {x=0,y=0}
    local clip_rect_max                     = {x=0,y=0}
    local intersect_with_current_clip_rect  = false
    tImGui.PushClipRect(clip_rect_min, clip_rect_max, intersect_with_current_clip_rect)

PushItemWidth
^^^^^^^^^^^^^

.. data:: PushItemWidth(item_width)

     Set width of items for common large "item+label" widgets. >0.0f: width in pixels, <0.0f align xx pixels to the right of window (so -1.0 always align width to the right side). 0.0f = default to ~2/3 of windows width,

    :param number: **item_width**

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    local item_width  = 50
    tImGui.PushItemWidth(item_width)


Render
------

    .. Note:: You do not need to call render for this module. It is automatically called by the engine.


Set methods
-----------


SetKeyboardFocusHere
^^^^^^^^^^^^^^^^^^^^

.. data:: SetKeyboardFocusHere(offset)

     Focus keyboard on the next widget. Use positive 'offset' to access sub components of a multiple component widget. Use -1 to access previous widget.

    :param number: **offset**

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local offset  =  0

    tImGui.SetKeyboardFocusHere(offset)

SetMouseCursor
^^^^^^^^^^^^^^

.. data:: SetMouseCursor(string cursor_type)

     Set desired cursor type

    :param struing: **cursor_type** valids are: ``ImGuiMouseCursor_None``, ``ImGuiMouseCursor_Arrow``, ``ImGuiMouseCursor_TextInput``, ``ImGuiMouseCursor_ResizeAll``, ``ImGuiMouseCursor_ResizeNS``, ``ImGuiMouseCursor_ResizeEW``, ``ImGuiMouseCursor_ResizeNESW``, ``ImGuiMouseCursor_ResizeNWSE``, ``ImGuiMouseCursor_Hand``, ``ImGuiMouseCursor_NotAllowed``.

*Example:*

.. code-block:: lua

    tImGui             =  require "ImGui"
    local cursor_type  = 'ImGuiMouseCursor_Hand'
    tImGui.SetMouseCursor(cursor_type)

SetCurrentContext
^^^^^^^^^^^^^^^^^

    .. Note:: You do not need to set the context for this module. It is automatically set by the engine.

SetClipboardText
^^^^^^^^^^^^^^^^

.. data:: SetClipboardText(text)

    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local text  = 'your data'
    tImGui.SetClipboardText(text)

.. Note:: TODO: On :guilabel:`Linux` is used just the internal clipboard text.

SetNextTreeNodeOpen
^^^^^^^^^^^^^^^^^^^

.. data:: SetNextTreeNodeOpen(open, cond)

    :param boolean: **open**
    :param number: **condition**

*Example:*

.. code-block:: lua

    tImGui           = require "ImGui"
    local open       = true
    local condition  =  0
    tImGui.SetNextTreeNodeOpen(open, condition)

SetItemDefaultFocus
^^^^^^^^^^^^^^^^^^^

.. data:: SetItemDefaultFocus()

     Make last item the default focused item of a window.

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.SetItemDefaultFocus()

SetScrollHere
^^^^^^^^^^^^^

.. data:: SetScrollHere(center_ratio)

    :param number: **center_ratio**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local center_ratio  = 0.5
    tImGui.SetScrollHere(center_ratio)

SetItemAllowOverlap
^^^^^^^^^^^^^^^^^^^

.. data:: SetItemAllowOverlap()

     Allow last item to be overlapped by a subsequent item. sometimes useful with invisible buttons, selectables, etc. to catch unused area.

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.SetItemAllowOverlap()

SetDragDropPayload
^^^^^^^^^^^^^^^^^^

.. data:: SetDragDropPayload(str_type, string_data, cond)

     Type is a user defined string of maximum 32 characters. Strings starting with '_' are reserved for dear ImGui internal types. Data is copied and held by ImGui.

    :param string: **str_type**
    :param string: **data**
    :param number: **cond**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua
    :emphasize-lines: 5

    tImGui         =  require "ImGui"
    local str_type = 'my type 01'
    local data     = 'some data'
    local cond     =  0
    local ret      =  tImGui.SetDragDropPayload(str_type, data, cond)

SetWindowFocus
^^^^^^^^^^^^^^

.. data:: SetWindowFocus(name)

     Set named window to be focused / top-most. use ``nil`` to remove focus.

    :param string: **name**

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local name  = nil

    tImGui.SetWindowFocus(name)

SetTabItemClosed
^^^^^^^^^^^^^^^^

.. data:: SetTabItemClosed(tab_or_docked_window_label)

     Notify TabBar or Docking system of a closed tab/window ahead (useful to reduce visual flicker on reorderable tab bars). For tab-bar: call after BeginTabBar() and before Tab submissions. Otherwise call with a window name.

    :param string: **tab_or_docked_window_label**

*Example:*

.. code-block:: lua

    tImGui                            =  require "ImGui"
    local tab_or_docked_window_label  = 'name'
    tImGui.SetTabItemClosed(tab_or_docked_window_label)

SetWindowFontScale
^^^^^^^^^^^^^^^^^^

.. data:: SetWindowFontScale(scale)

     Set font scale. Adjust IO.FontGlobalScale if you want to scale all windows. This is an old API! For correct scaling, prefer to reload font + rebuild ImFontAtlas + call style.ScaleAllSizes().

    :param number: **scale**

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local scale  = 2.0
    tImGui.SetWindowFontScale(scale)

SetWindowCollapsed
^^^^^^^^^^^^^^^^^^

.. data:: SetWindowCollapsed(name,collapsed, cond)

     (not recommended) set current window collapsed state. prefer using SetNextWindowCollapsed().

    :param string: **name** (might be ``nil`` for the current window)
    :param boolean: **collapsed**
    :param number: **cond**

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    local name       = nil
    local collapsed  = true
    local cond       =  0

    tImGui.SetWindowCollapsed(collapsed, cond)

SetWindowSize
^^^^^^^^^^^^^

.. data:: SetWindowSize(name, size, cond)

     (not recommended) set current window size - call within Begin()/End(). set to ImVec2(0,0) to force an auto-fit. prefer using SetNextWindowSize(), as this may incur tearing and minor side-effects.

    :param string: **name** (might be ``nil`` for the current window)
    :param table: **size** ``{x,y}``
    :param number: **cond**

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local name  = nil
    local size  = {x=200,y=200}
    local cond  =  0
    tImGui.SetWindowSize(size, cond)


SetCursorPosY
^^^^^^^^^^^^^

.. data:: SetCursorPosY(local_y)

    :param number: **local_y**

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    local local_y  = 100

    tImGui.SetCursorPosY(local_y)

SetCursorScreenPos
^^^^^^^^^^^^^^^^^^

.. data:: SetCursorScreenPos(pos)

     Cursor position in absolute screen coordinates [0..io.DisplaySize]

    :param table: **pos**

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    local pos  = {x=0,y=0}
    tImGui.SetCursorScreenPos(pos)

SetCursorPosX
^^^^^^^^^^^^^

.. data:: SetCursorPosX(local_x)

      GetWindowPos() + GetCursorPos() == GetCursorScreenPos() etc.)

    :param number: **local_x**

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    local local_x  = 0

    tImGui.SetCursorPosX(local_x)

SetWindowPos
^^^^^^^^^^^^

.. data:: SetWindowPos(name, pos, cond)

     Set named window position.

    :param string: **name** might be ``nil`` for the current window.
    :param table: **pos** ``{x,y}``
    :param number: **cond** see ``ImGuiCond_``

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local name  = nil
    local pos   = {x=0,y=0}
    local cond  =  tImGui.Flags('ImGuiCond_Always')
    tImGui.SetWindowPos(name, pos, cond)

SetColorEditOptions
^^^^^^^^^^^^^^^^^^^

.. data:: SetColorEditOptions(flags)

     Initialize current options (generally on application startup) if you want to select a default format, picker type, etc. User will be able to change many settings, unless you pass the _NoOptions flag to your calls.

    :param number: **flags**

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local flags  = 0
    tImGui.SetColorEditOptions(flags)

SetNextWindowSize
^^^^^^^^^^^^^^^^^

.. data:: SetNextWindowSize(size, cond)

     Set next window size. set axis to 0.0f to force an auto-fit on this axis. call before Begin()

    :param table: **size** ``{x,y}``
    :param number: **cond**

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local size  = {x=200,y=200}
    local cond  =  0
    tImGui.SetNextWindowSize(size, cond)

SetNextWindowPos
^^^^^^^^^^^^^^^^

.. data:: SetNextWindowPos(pos, cond, pivot)

     Set next window position. call before Begin(). use pivot=(0.5f,0.5f) to center on given point, etc.

    :param table: **pos** ``{x,y}``
    :param number: **cond**
    :param table: **pivot** ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local pos    = {x = 0,y = 100}
    local cond   =  0
    local pivot  =  {x=0.5,y=0.5}
    tImGui.SetNextWindowPos(pos, cond, pivot)

SetNextWindowBgAlpha
^^^^^^^^^^^^^^^^^^^^

.. data:: SetNextWindowBgAlpha(alpha)

     Set next window background color alpha. helper to easily modify ImGuiCol_WindowBg/ChildBg/PopupBg. you may also use ImGuiWindowFlags_NoBackground.

    :param number: **alpha**

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local alpha  = 0.5
    tImGui.SetNextWindowBgAlpha(alpha)

SetNextWindowSizeConstraints
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: SetNextWindowSizeConstraints(size_min, size_max)

     Set next window size limits. use -1,-1 on either X/Y axis to preserve the current size. Sizes will be rounded down.

    :param table: **size_min** ``{x,y}``
    :param table: **size_max** ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui                          =  require "ImGui"
    local size_min                  = {x = 100, y = 200}
    local size_max                  = {x = 150, y = 250}
    tImGui.SetNextWindowSizeConstraints(size_min, size_max)


SetNextWindowCollapsed
^^^^^^^^^^^^^^^^^^^^^^

.. data:: SetNextWindowCollapsed(collapsed, cond)

     Set next window collapsed state. call before Begin()

    :param boolean: **collapsed**
    :param number: **cond**

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    local collapsed  = true
    local cond       =  0

    tImGui.SetNextWindowCollapsed(collapsed, cond)

SetNextWindowContentSize
^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: SetNextWindowContentSize(size)

     Set next window content size (~ scrollable client area, which enforce the range of scrollbars). Not including window decorations (title bar, menu bar, etc.) nor WindowPadding. set an axis to 0.0f to leave it automatic. call before Begin()

    :param table: **size** ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local size  = {x=100,y=100}

    tImGui.SetNextWindowContentSize(size)

SetNextWindowFocus
^^^^^^^^^^^^^^^^^^

.. data:: SetNextWindowFocus()

     Set next window to be focused / top-most. call before Begin()

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.SetNextWindowFocus()

SetCursorPos
^^^^^^^^^^^^

.. data:: SetCursorPos(local_pos)

      Using the absolute coordinate system.

    :param table: **local_pos**

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    local local_pos  = {x=0,y=0}
    tImGui.SetCursorPos(local_pos)

SetScrollHereX
^^^^^^^^^^^^^^

.. data:: SetScrollHereX(center_x_ratio)

     Adjust scrolling amount to make current cursor position visible. center_x_ratio=0.0: left, 0.5: center, 1.0: right. When using to make a "default/current item" visible, consider using SetItemDefaultFocus() instead.

    :param number: **center_x_ratio**

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local center_x_ratio  =  0.5
    tImGui.SetScrollHereX(center_x_ratio)

SetScrollY
^^^^^^^^^^

.. data:: SetScrollY(scroll_y)

     Set scrolling amount [0..GetScrollMaxY()]

    :param number: **scroll_y**

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local scroll_y  = 1
    tImGui.SetScrollY(scroll_y)

SetColumnOffset
^^^^^^^^^^^^^^^

.. data:: SetColumnOffset(column_index, offset_x)

     Set position of column line (in pixels, from the left side of the contents region). pass -1 to use current column

    :param number: **column_index**
    :param number: **offset_x**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local column_index  = 1
    local offset_x      = 0

    tImGui.SetColumnOffset(column_index, offset_x)

SetScrollX
^^^^^^^^^^

.. data:: SetScrollX(scroll_x)

     Set scrolling amount [0..GetScrollMaxX()]

    :param number: **scroll_x**

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local scroll_x  = 1
    tImGui.SetScrollX(scroll_x)

SetColumnWidth
^^^^^^^^^^^^^^

.. data:: SetColumnWidth(column_index, width)

     Set column width (in pixels). pass -1 to use current column

    :param number: **column_index**
    :param number: **width**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local column_index  = 0
    local width         = 100
    tImGui.SetColumnWidth(column_index, width)

SetScrollHereY
^^^^^^^^^^^^^^

.. data:: SetScrollHereY(center_y_ratio)

     Adjust scrolling amount to make current cursor position visible. center_y_ratio=0.0: top, 0.5: center, 1.0: bottom. When using to make a "default/current item" visible, consider using SetItemDefaultFocus() instead.

    :param number: **center_y_ratio**

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local center_y_ratio  =  0.5
    tImGui.SetScrollHereY(center_y_ratio)

SetNextItemWidth
^^^^^^^^^^^^^^^^

.. data:: SetNextItemWidth(item_width)

     Set width of the _next_ common large "item+label" widget. >0.0f: width in pixels, <0.0f align xx pixels to the right of window (so -1.0 always align width to the right side)

    :param number: **item_width**

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    local item_width  = 100
    tImGui.SetNextItemWidth(item_width)

SetTooltip
^^^^^^^^^^

.. data:: SetTooltip(text)

     Set a text-only tooltip, typically use with ImGui:IsItemHovered(). override any previous call to SetTooltip().

    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui             =  require "ImGui"
    local text         = 'tool tip'
    tImGui.SetTooltip(text)

SetScrollFromPosY
^^^^^^^^^^^^^^^^^

.. data:: SetScrollFromPosY(local_y, center_y_ratio)

     Adjust scrolling amount to make given position visible. Generally GetCursorStartPos() + offset to compute a valid position.

    :param number: **local_y**
    :param number: **center_y_ratio**

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local local_y         = 1
    local center_y_ratio  =  0.5
    tImGui.SetScrollFromPosY(local_y, center_y_ratio)

SetScrollFromPosX
^^^^^^^^^^^^^^^^^

.. data:: SetScrollFromPosX(local_x, center_x_ratio)

     Adjust scrolling amount to make given position visible. Generally GetCursorStartPos() + offset to compute a valid position.

    :param number: **local_x**
    :param number: **center_x_ratio**

*Example:*

.. code-block:: lua

    tImGui                =  require "ImGui"
    local local_x         =  0
    local center_x_ratio  =  0.5
    tImGui.SetScrollFromPosX(local_x, center_x_ratio)


Show methods
------------

ShowUserGuide
^^^^^^^^^^^^^

.. data:: ShowUserGuide()

     Add basic help/info block (not a window): how to manipulate ImGui as a end-user (mouse/keyboard controls).

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.ShowUserGuide()

ShowFontSelector
^^^^^^^^^^^^^^^^

.. data:: ShowFontSelector(label)

     Add font selector block (not a window), essentially a combo listing the loaded fonts.

    :param string: **label**

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    function loop(delta)
        tImGui.ShowFontSelector('Font')
    end

.. figure:: _static/imgui_ShowFontSelector.png
   :align: center
   :figclass: align-center

ShowStyleSelector
^^^^^^^^^^^^^^^^^

.. data:: ShowStyleSelector(label)

     Add style selector block (not a window), essentially a combo listing the default styles.

    :param string: **label**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local label  = 'label'
    function loop(delta)
        tImGui.ShowStyleSelector('Selector')
    end

.. figure:: _static/imgui_ShowStyleSelector.png
   :align: center
   :figclass: align-center

Style methods
-------------


PushStyleColor
^^^^^^^^^^^^^^

.. data:: PushStyleColor(styleColorEnum , color)

    :param string: **styleColorEnum** (see GuiStyle) or ``number`` (0 to ``ImGuiCol_COUNT``-1)
    :param table: **color** *rgb* ``{r,g,b,a}`` or ``{x,y,z,w}``

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local idx    = tImGui.Flags('ImGuiCol_ButtonHovered')
    local color  = {r=1,g=0,b=0.3,a=1}
    tImGui.PushStyleColor(idx, color)

.. data:: PushStyleColor(styleColorEnum, color)

    :param string: **styleColorEnum** (see GuiStyle) or ``number`` (0 to ``ImGuiCol_COUNT``-1)
    :param number: **color**

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local color  = 0xff0000ff
    tImGui.PushStyleColor('ImGuiCol_ButtonHovered', color)

PopStyleColor
^^^^^^^^^^^^^

.. data:: PopStyleColor(count)

    :param number: **count**

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local count  =  1
    tImGui.PopStyleColor(count)

PushStyleVar
^^^^^^^^^^^^

.. data:: PushStyleVar(ImGuiStyleVar, value)

    :param string: **ImGuiStyleVar**
    :param table: **value**  ``{x,y}`` or ``number``

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local ImGuiStyleVar = 'ImGuiStyleVar_WindowMinSize'
    local value         = {x = 400, y = 400}
    tImGui.PushStyleVar(ImGuiStyleVar, value)

.. data:: PushStyleVar(idx, value)

    :param number: **idx** (0 to ``ImGuiStyleVar_COUNT``)
    :param table: **value** ``{x,y}`` or ``number``

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local idx   = tImGui.Flags('ImGuiStyleVar_Alpha')
    local value = 0.5
    tImGui.PushStyleVar(idx, value)

PopStyleVar
^^^^^^^^^^^

.. data:: PopStyleVar(count)

    :param number: **count**

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    local count  =  1
    tImGui.PopStyleVar(count)

.. _ImGuiStyleVarFlagTable:

ImGuiStyleVar flag table
^^^^^^^^^^^^^^^^^^^^^^^^

    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_Alpha                | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_WindowPadding        | ``{x,y}``                     |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_WindowRounding       | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_WindowBorderSize     | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_WindowMinSize        | ``{x,y}``                     |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_WindowTitleAlign     | ``{x,y}``                     |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_ChildRounding        | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_ChildBorderSize      | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_PopupRounding        | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_PopupBorderSize      | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_FramePadding         | ``{x,y}``                     |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_FrameRounding        | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_FrameBorderSize      | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_ItemSpacing          | ``{x,y}``                     |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_ItemInnerSpacing     | ``{x,y}``                     |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_IndentSpacing        | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_ScrollbarSize        | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_ScrollbarRounding    | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_GrabMinSize          | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_GrabRounding         | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_TabRounding          | ``number``                    |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_ButtonTextAlign      | ``{x,y}``                     |
    +------------------------------------+-------------------------------+
    | ImGuiStyleVar_SelectableTextAlign  | ``{x,y}``                     |
    +------------------------------------+-------------------------------+

StyleColorsLight
^^^^^^^^^^^^^^^^

.. data:: StyleColorsLight(ImGuiStyle)

     Best used with borders and a custom, thicker font

    :param table: **ImGuiStyle** (might be ``nil``)

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    local ImGuiStyle  =  nil
    tImGui.StyleColorsLight(dst)

StyleColorsDark
^^^^^^^^^^^^^^^

.. data:: StyleColorsDark(ImGuiStyle)

     New, recommended style (default)

    :param table: **ImGuiStyle** (might be ``nil``)

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    local ImGuiStyle  =  nil
    tImGui.StyleColorsDark(ImGuiStyle)

StyleColorsClassic
^^^^^^^^^^^^^^^^^^

.. data:: StyleColorsClassic(ImGuiStyle)

     Classic ImGui style

    :param table: **ImGuiStyle** (might be ``nil``)

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    local ImGuiStyle  =  nil
    tImGui.StyleColorsClassic(ImGuiStyle)

GetStyle
^^^^^^^^

.. data:: GetStyle(string * attribute)

     Access the Style structure (colors, sizes). Always use PushStyleColor(), PushStyleVar() to modify style mid-frame.

    If not supplied *attribute* then will return the whole structure.

    :return: ``table`` - *ImGuiStyle* or an ``attribute``


*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    local WindowPadding = tImGui.GetStyle('WindowPadding')
    print(WindowPadding.x,WindowPadding.y)
    local ImGuiCol_Button = tImGui.GetStyle('ImGuiCol_Button')
    print(ImGuiCol_Button.r,ImGuiCol_Button.g,ImGuiCol_Button.b,ImGuiCol_Button.a)


ImGuiStyle Table
^^^^^^^^^^^^^^^^

    .. Note:: Use :ref:`ImGuiStyleVar Flag Table <ImGuiStyleVarFlagTable>` + ``PushStyleColor()``, ``PushStyleVar()`` to modify.

 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 |  ImGuiStyle  attribute         |    Type                   |                                                                                                                                                                                                                   |
 +================================+===========================+===================================================================================================================================================================================================================+
 | Alpha                          |      ``number``           |  Global alpha applies to everything in <br /> Dear ImGui.                                                                                                                                                         |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | WindowPadding                  |      ``{x,y}``            |  Padding within a window.                                                                                                                                                                                         |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | WindowRounding                 |      ``number``           |  Radius of window corners rounding. Set to 0.0f to have rectangular windows.                                                                                                                                      |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | WindowBorderSize               |      ``number``           |  Thickness of border around windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).                                                                                   |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | WindowMinSize                  |      ``{x,y}``            |  Minimum window size. This is a global setting. If you want to constraint individual windows, use SetNextWindowSizeConstraints().                                                                                 |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | WindowTitleAlign               |      ``{x,y}``            |  Alignment for title bar text. Defaults to (0.0f,0.5f) for left-aligned,vertically centered.                                                                                                                      |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | WindowMenuButtonPosition       |      ``number``           |  Side of the collapsing/docking button in the title bar (None/Left/Right). Defaults to ImGuiDir_Left.                                                                                                             +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ChildRounding                  |      ``number``           |  Radius of child window corners rounding. Set to 0.0f to have rectangular windows.                                                                                                                                |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ChildBorderSize                |      ``number``           |  Thickness of border around child windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).                                                                             |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | PopupRounding                  |      ``number``           |  Radius of popup window corners rounding. (Note that tooltip windows use WindowRounding)                                                                                                                          |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | PopupBorderSize                |      ``number``           |  Thickness of border around popup/tooltip windows. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).                                                                     |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | FramePadding                   |      ``{x,y}``            |  Padding within a framed rectangle (used by most widgets).                                                                                                                                                        +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | FrameRounding                  |      ``number``           |  Radius of frame corners rounding. Set to 0.0f to have rectangular frame (used by most widgets).                                                                                                                  |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | FrameBorderSize                |      ``number``           |  Thickness of border around frames. Generally set to 0.0f or 1.0f. (Other values are not well tested and more CPU/GPU costly).                                                                                    |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ItemSpacing                    |      ``{x,y}``            |  Horizontal and vertical spacing between widgets/lines.                                                                                                                                                           +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ItemInnerSpacing               |      ``{x,y}``            |  Horizontal and vertical spacing between within elements of a composed widget (e.g. a slider and its label).                                                                                                      |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | TouchExtraPadding              |      ``{x,y}``            |  Expand reactive bounding box for touch-based system where touch position is not accurate enough. Unfortunately we don't sort widgets so priority on overlap will always be given to the first widget.            |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | IndentSpacing                  |      ``number``           |  Horizontal indentation when e.g. entering a tree node. Generally == (FontSize + FramePadding.x*2).                                                                                                               +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ColumnsMinSpacing              |      ``number``           |  Minimum horizontal spacing between two columns. Preferably > (FramePadding.x + 1).                                                                                                                               |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ScrollbarSize                  |      ``number``           |  Width of the vertical scrollbar, Height of the horizontal scrollbar.                                                                                                                                             +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ScrollbarRounding              |      ``number``           |  Radius of grab corners for scrollbar.                                                                                                                                                                            +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | GrabMinSize                    |      ``number``           |  Minimum width/height of a grab box for slider/scrollbar.                                                                                                                                                         |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | GrabRounding                   |      ``number``           |  Radius of grabs corners rounding. Set to 0.0f to have rectangular slider grabs.                                                                                                                                  |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | TabRounding                    |      ``number``           |  Radius of upper corners of a tab. Set to 0.0f to have rectangular tabs.                                                                                                                                          |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | TabBorderSize                  |      ``number``           |  Thickness of border around tabs.                                                                                                                                                                                 |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ColorButtonPosition            |      ``number``           |  Side of the color button in the ColorEdit4 widget (left/right). Defaults to ImGuiDir_Right.                                                                                                                      |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | ButtonTextAlign                |      ``{x,y}``            |  Alignment of button text when button is larger than text. Defaults to (0.5f, 0.5f) (centered).                                                                                                                   |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | SelectableTextAlign            |      ``{x,y}``            |  Alignment of selectable text when selectable is larger than text. Defaults to (0.0f, 0.0f) (top-left aligned).                                                                                                   +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | DisplayWindowPadding           |      ``{x,y}``            |  Window position are clamped to be visible within the display area by at least this amount. Only applies to regular windows.                                                                                      |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | DisplaySafeAreaPadding         |      ``{x,y}``            |  If you cannot see the edges of your screen (e.g. on a TV) increase the safe area padding. Apply to popups/tooltips as well regular windows. NB: Prefer configuring your TV sets correctly!                       |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | MouseCursorScale               |      ``number``           |  Scale software rendered mouse cursor (when io.MouseDrawCursor is enabled). May be removed later.                                                                                                                 +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | AntiAliasedLines               |      ``boolean``          |  Enable anti-aliasing on lines/borders. Disable if you are really tight on CPU/GPU.                                                                                                                               |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | AntiAliasedFill                |      ``boolean``          |  Enable anti-aliasing on filled shapes (rounded rectangles, circles, etc.)                                                                                                                                        +
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | CurveTessellationTol           |      ``number``           |  Tessellation tolerance when using PathBezierCurveTo() without a specific number of segments. Decrease for highly tessellated curves (higher quality, more polygons), increase to reduce quality.                 |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 | Colors                         | ``{{r,g,b,a},...}``       |   Colors [40] from ImGuiCol_Text to ImGuiCol_ModalWindowDimBg, see ImGuiCol Table.                                                                                                                                |
 +--------------------------------+---------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
 
ImGuiCol Table
^^^^^^^^^^^^^^

 +--------------------------------+---------------------------+
 |  ImGuiCol_XXX  attributes      |    Type                   |
 +================================+===========================+
 | ImGuiCol_Text                  | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TextDisabled          | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_WindowBg              | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ChildBg               | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_PopupBg               | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_Border                | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_BorderShadow          | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_FrameBg               | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_FrameBgHovered        | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_FrameBgActive         | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TitleBg               | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TitleBgActive         | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TitleBgCollapsed      | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_MenuBarBg             | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ScrollbarBg           | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ScrollbarGrab         | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ScrollbarGrabHovered  | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ScrollbarGrabActive   | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_CheckMark             | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_SliderGrab            | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_SliderGrabActive      | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_Button                | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ButtonHovered         | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ButtonActive          | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_Header                | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_HeaderHovered         | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_HeaderActive          | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_Separator             | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_SeparatorHovered      | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_SeparatorActive       | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ResizeGrip            | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ResizeGripHovered     | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ResizeGripActive      | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_Tab                   | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TabHovered            | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TabActive             | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TabUnfocused          | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TabUnfocusedActive    | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_PlotLines             | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_PlotLinesHovered      | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_PlotHistogram         | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_PlotHistogramHovered  | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_TextSelectedBg        | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_DragDropTarget        | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_NavHighlight          | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_NavWindowingHighlight | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_NavWindowingDimBg     | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+
 | ImGuiCol_ModalWindowDimBg      | ``{r,g,b,a}``             |
 +--------------------------------+---------------------------+

.. Tip:: Use *attribute* to get only what you need!

Text methods
------------

AlignTextToFramePadding
^^^^^^^^^^^^^^^^^^^^^^^

.. data:: AlignTextToFramePadding()

     Vertically align upcoming text baseline to FramePadding.y so that it will align properly to regularly framed items (call if you have text on a line before a framed item)

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.AlignTextToFramePadding()

TextColored
^^^^^^^^^^^

.. data:: TextColored(color, text)

     Shortcut for PushStyleColor(ImGuiCol_Text, color); Text(text); PopStyleColor();

    :param table: **color** *rgb* ``{r,g,b,a}`` or ``{x,y,z,w}``
    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"
    local color         = {r=1,g=0,b=0,a=1}
    local text          = 'Some text'
    tImGui.TextColored(color, text)

Text
^^^^

.. data:: Text(text)

     Formatted text

    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    local text        = 'Some text'
    tImGui.Text(text)

TextDisabled
^^^^^^^^^^^^

.. data:: TextDisabled(text)

     Shortcut for PushStyleColor(ImGuiCol_Text, style.Colors[ImGuiCol_TextDisabled]); Text(text); PopStyleColor();

    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    local text        = 'Text disabled'
    tImGui.TextDisabled(text)

TextWrapped
^^^^^^^^^^^

.. data:: TextWrapped(text)

     Shortcut for PushTextWrapPos(0.0f); Text(fmt, ...); PopTextWrapPos();. Note that this won't work on an auto-resizing window if there's no other widgets to extend the window width, yoy may need to set a size using SetNextWindowSize().

    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    local text  = 'Some text'
    tImGui.TextWrapped(text)


Widgets
-------

    Button, Image, Checkbox, RadioButton, ProgressBar, Bullet, etc.

ArrowButton
^^^^^^^^^^^

.. data:: ArrowButton(str_id, dir)

     Square button with an arrow shape

    :param string: **str_id**
    :param number: **dir**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    function loop(delta)
        local str_id  =  '01'
        local dir     =  0
        tImGui.ArrowButton(str_id, dir)
    end


.. figure:: _static/imgui_ArrowButton.png
   :align: center
   :figclass: align-center

BeginCombo
^^^^^^^^^^

    - The BeginCombo()/EndCombo() api allows you to manage your contents and selection state however you want it, by creating e.g. Selectable() items.

    - The old Combo() api are helpers over BeginCombo()/EndCombo() which are kept available for convenience purpose.


.. data:: BeginCombo(label, preview_value, flags)


    :param string: **label**
    :param string: **preview_value**
    :param number: **flags**
    :return: ``boolean`` - *result*


.. data:: EndCombo()

     Only call EndCombo() if BeginCombo() returns true!


*Example:*

.. code-block:: lua
    :emphasize-lines: 7,17

    tImGui               =  require "ImGui"
    items = { "AAAA", "BBBB", "CCCC", "DDDD", "EEEE", "FFFF", "GGGG", "HHHH", "IIII", "JJJJ", "KKKK", "LLLLLLL", "MMMM", "OOOOOOO" }
    item_current = items[1] -- global

    function loop(delta)
        local flags = 0
        if tImGui.BeginCombo("combo 1", item_current, flags) then -- The second parameter is the label previewed before opening the combo.
            for n=1, #items do
                local is_selected = (item_current == items[n])
                if (tImGui.Selectable(items[n], is_selected)) then
                    item_current = items[n]
                end
                if (is_selected) then
                    tImGui.SetItemDefaultFocus()   -- Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
                end
            end
            tImGui.EndCombo()
        end
    end

.. figure:: _static/BeginCombo.png
   :align: center
   :figclass: align-center

Bullet
^^^^^^

.. data:: BulletText(str)

     Shortcut for Bullet()+Text()

    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"
    function loop(delta)
        local str         = 'text'
        tImGui.BulletText(str)
    end

.. figure:: _static/imgui_BulletText.png
   :align: center
   :figclass: align-center

.. data:: Bullet()

     Draw a small circle and keep the cursor on the same line. advance cursor x position by GetTreeNodeToLabelSpacing(), same distance that TreeNode() uses

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.Bullet()

Button
^^^^^^

.. data:: Button(label, size)

     Button

    :param string: **label**
    :param table: **size**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"

    function loop(delta)
        local label  = 'Start'
        local size   =  {x=0,y=0}
        if tImGui.Button(label, size) then
            print('Clicked')
        end
    end

.. figure:: _static/imgui_Button.png
   :align: center
   :figclass: align-center

Checkbox
^^^^^^^^

.. data:: Checkbox(label, value)


    :param string: **label**
    :param boolean: **value**
    :return: ``boolean`` - *checked*

*Example:*

.. code-block:: lua

    tImGui =  require "ImGui"
    value  = false

    function loop(delta)
        local label  = 'Check me'
        value = tImGui.Checkbox(label,value)
    end

.. figure:: _static/imgui_Checkbox.png
   :align: center
   :figclass: align-center


.. data:: CheckboxFlags(label, flags, flags_value)


    :param string: **label**
    :param number: **flags**
    :param number: **flags_value**
    :return: ``boolean`` - *result*, ``number`` *flags*

*Example:*

.. code-block:: lua

    tImGui             =  require "ImGui"
    value              = false
    local flags        = 0

    function loop(delta)
        local label        = 'Check me'
        local flags_value  = 1

        ret, flags = tImGui.CheckboxFlags(label, flags, flags_value)
        flags_value = flags
    end

Combo
^^^^^

.. data:: Combo(label, current_item, out_text), data, items_count, popup_max_height_in_items)

    :param string: **label** might be ``nil``
    :param number: **current_item** ``one`` based!
    :param table: **array of items** ``{'option 1', 'option 2', 'option 3',... }``
    :param number: **height_in_items**
    :return: ``boolean`` - *result*, ``number`` - *current_item*, ``string`` - *item*

*Example:*

.. code-block:: lua

    tImGui =  require "ImGui"
    items  = { "AAAA", "BBBB", "CCCC", "DDDD", "EEEE", "FFFF", "GGGG", "HHHH", "IIII", "JJJJ", "KKKK", "LLLLLLL", "MMMM", "OOOOOOO", 1,2,3,false,true,{} }
    current_item_selected = 1

    function loop(delta)

        local label            = 'Options'
        local current_item     = current_item_selected
        local height_in_items  =  -1

        local ret, current_item, item_as_string = tImGui.Combo(label, current_item, items, height_in_items)
        if ret then
            current_item_selected = current_item --number of item selected
        end
    end


.. figure:: _static/imgui_combo.png
   :align: center
   :figclass: align-center

Color
^^^^^

ColorPicker
"""""""""""

.. data:: ColorPicker3(label, color, flags)


    :param string: **label**
    :param table: **color** ``{r,g,b}``
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` ``{r,g,b}``

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    color      = {r=1,g=0,b=0}
 
    function loop(delta)
        local label      = 'Pick a color'
        local flags      =  0

        local ret, tRgb = tImGui.ColorPicker3(label, color, flags)
        if ret then
            color = tRgb
        end
    end

.. figure:: _static/imgui_ColorPicker3.png
   :align: center
   :figclass: align-center

.. data:: ColorPicker4(label, color, flags, ref_col)


    :param string: **label**
    :param table: **color** ``{r,g,b,a}``
    :param number: **flags**
    :param number: **ref_col** ``{r,g,b,a}``
    :return: ``boolean`` - *result*, ``table`` ``{r,g,b,a}``

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    color      = {r=1,g=0,b=0,a=1}
    ref_color  = {r=0,g=1,b=1,a=1}

    function loop(delta)
        local label      = 'Pick a color'
        local flags      =  0

        local ret, tRgba = tImGui.ColorPicker4(label, color, flags, ref_color)
        if ret then
            color = tRgba
        end
    end

.. figure:: _static/imgui_ColorPicker4.png
   :align: center
   :figclass: align-center


ColorButton
"""""""""""

.. data:: ColorButton(desc_id, color, flags, size)

     Display a colored square/button, hover for details, return true when pressed.

    :param string: **desc_id**
    :param table: **color** ``{r,g,b,a}``
    :param number: **flags**
    :param table: **size** ``{x,y}``
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"

    function loop(delta)
        local desc_id  = 'id 01'
        local color    = {r=1,g=0,b=1,a=1}
        local flags    = 0
        local size     = {x=0,y=0}

        local clicked = tImGui.ColorButton(desc_id, color, flags, size)
    end


ColorEdit3
""""""""""

.. data:: ColorEdit3(label, color, flags)


    :param string: **label**
    :param table: **color**  ``{r,g,b}``
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` - ``{r,g,b}``

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    color      = {r=1,g=0,b=0}
 
    function loop(delta)

        local label    = 'select your color'
        local flags    = tImGui.Flags('ImGuiColorEditFlags_HDR','ImGuiColorEditFlags_NoLabel')
        -- ImGuiColorEditFlags_HDR -> Currently all this does is to lift the 0..1 limits on dragging widgets

        local clicked, tRgb = tImGui.ColorEdit3(label, color, flags)
        if clicked then
            color = tRgb
        end
    end


.. figure:: _static/imgui_ColorEdit3.png
   :align: center
   :figclass: align-center


ColorEdit4
""""""""""

.. data:: ColorEdit4(label, color, flags)


    :param string: **label**
    :param number: **color** ``{r,g,b,a}``
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` - ``{r,g,b,a}``

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    color      = {r=1,g=0,b=0,a=1}
 
    function loop(delta)

        local label    = 'select your color'
        local flags    = tImGui.Flags('ImGuiColorEditFlags_NoLabel')

        local clicked, tRgba = tImGui.ColorEdit4(label, color, flags)
        if clicked then
            color = tRgba
        end
    end


Drag
^^^^

DragInt
"""""""

.. data:: DragInt(label, value, v_speed, v_min, v_max, format)

     If v_min >= v_max we have no bound

    :param string: **label**
    :param number: **value**
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    value         = 0
 
    function loop(delta)
        local label    = 'drag int'
        local v_speed  =  1.0
        local v_min    =  0
        local v_max    =  0
        local format   =  "%d seconds"

        local dragged, iValue = tImGui.DragInt(label,value, v_speed, v_min, v_max, format)
        if dragged then
            value = iValue
        end
    end

.. figure:: _static/imgui_DragInt.png
   :align: center
   :figclass: align-center

.. data:: DragInt2(label, values, v_speed, v_min, v_max, format)


    :param string: **label**
    :param table: **int values** ``{v1,v2}`` 
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``table`` - ``{v1,v2}``

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    local values   = {5,255}
 
    function loop(delta)
        local label    = 'drag int2'
        local v_speed  =  1.0
        local v_min    =  0
        local v_max    =  0
        local format   =  "%d"

        local dragged, tValues = tImGui.DragInt2(label,values, v_speed, v_min, v_max, format)
        if dragged then
            values = tValues
        end
    end

.. data:: DragInt3(label, values, v_speed, v_min, v_max, format)


    :param string: **label**
    :param table: **int values** ``{v1,v2,v3}`` 
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``table`` - ``{v1,v2,v3}``

*Example:*

.. code-block:: lua

    tImGui   =  require "ImGui"
    values   = {5,255,34}
 
    function loop(delta)
        local label    = 'drag int3'
        local v_speed  =  1.0
        local v_min    =  0
        local v_max    =  0
        local format   =  "%d"

        local dragged, tValues = tImGui.DragInt3(label,values, v_speed, v_min, v_max, format)
        if dragged then
            values = tValues
        end
    end

.. data:: DragInt4(label, values, v_speed, v_min, v_max, format)


    :param string: **label**
    :param table: **int values** ``{v1,v2,v3,v4}`` 
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``table`` - ``{v1,v2,v3,v4}``

*Example:*

.. code-block:: lua

    tImGui   =  require "ImGui"
    values   = {5,255,34,99}
 
    function loop(delta)
        local label    = 'drag int4'
        local v_speed  =  1.0
        local v_min    =  0
        local v_max    =  0
        local format   =  "%d"

        local dragged, tValues = tImGui.DragInt4(label,values, v_speed, v_min, v_max, format)
        if dragged then
            values = tValues
        end
    end

.. figure:: _static/imgui_DragInt4.png
   :align: center
   :figclass: align-center

DragIntRange2
"""""""""""""

.. data:: DragIntRange2(label, v_current_min, v_current_max, v_speed, v_min, v_max, format, format_max)


    :param string: **label**
    :param number: **v_current_min**
    :param number: **v_current_max**
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format_min**
    :param string: **format_max**
    :return: ``boolean`` - *result*, ``number`` - dragged_min, ``number`` - dragged_max

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    v_current_min  = 0
    v_current_max  = 255

    function loop(delta)
        local label          = 'Drag Int Range 2'
        local v_speed        =  1.0
        local v_min          =  0
        local v_max          =  255
        local format_min     =  "%d MIN"
        local format_max     =  "%d MAX"

        local dragged, dragged_min, dragged_max = tImGui.DragIntRange2(label, v_current_min, v_current_max, v_speed, v_min, v_max, format_min, format_max)
        if dragged then
            v_current_min = dragged_min
            v_current_max = dragged_max
        end
    end

.. figure:: _static/imgui_DragIntRange2.png
   :align: center
   :figclass: align-center



DragFloat
"""""""""

.. data:: DragFloat(label, value, v_speed, v_min, v_max, format, power)

     If v_min >= v_max we have no bound

    :param string: **label**
    :param number: **value**
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    value         = 0

    function loop(delta)
        local label    = 'drag float'
        local v_speed  =  1.0
        local v_min    =  0
        local v_max    =  1000
        local format   =  "%.4f"
        local power    = 1

        local dragged, fValue = tImGui.DragFloat(label,value, v_speed, v_min, v_max, format, power)
        if dragged then
            value = fValue
        end
    end

.. figure:: _static/imgui_DragFloat.png
   :align: center
   :figclass: align-center


.. data:: DragFloat2(label, values, v_speed, v_min, v_max, format, power)


    :param string: **label**
    :param table: **values** ``{v1,v2}``
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``table`` - ``{v1,v2}``

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    tValues        =  {0.0, 1000.0}

    function loop(delta)
        local label    = 'drag float 2'
        local v_speed  =  0.5
        local v_min    =  0
        local v_max    =  1000
        local format   =  "%.2f"
        local power    = 1

        local dragged, fValue = tImGui.DragFloat2(label,tValues, v_speed, v_min, v_max, format, power)
        if dragged then
            tValues = fValue
        end
    end

.. figure:: _static/imgui_DragFloat2.png
   :align: center
   :figclass: align-center


.. data:: DragFloat3(label, values, v_speed, v_min, v_max, format, power)


    :param string: **label**
    :param table: **values** ``{v1,v2,v3}``
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``table`` - ``{v1,v2,v3}``

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    tValues        =  {0.0, 1000.0, 88.8}

    function loop(delta)
        local label    = 'drag float 3'
        local v_speed  =  0.5
        local v_min    =  0
        local v_max    =  1000
        local format   =  "%.2f"
        local power    = 1

        local dragged, fValue = tImGui.DragFloat3(label,tValues, v_speed, v_min, v_max, format, power)
        if dragged then
            tValues = fValue
        end
    end

.. data:: DragFloat4(label, values, v_speed, v_min, v_max, format, power)


    :param string: **label**
    :param table: **values** ``{v1,v2,v3,v4}``
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``table`` - ``{v1,v2,v3,v4}``

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    tValues        =  {0.0, 1000.0, 88.8, 66.65}

    function loop(delta)
        local label    = 'drag float 4'
        local v_speed  =  0.5
        local v_min    =  0
        local v_max    =  1000
        local format   =  "%.2f"
        local power    = 1

        local dragged, fValue = tImGui.DragFloat3(label,tValues, v_speed, v_min, v_max, format, power)
        if dragged then
            tValues = fValue
        end
    end


DragFloatRange2
"""""""""""""""

.. data:: DragFloatRange2(label, v_current_min, v_current_max, v_speed, v_min, v_max, format_min, format_max, power)


    :param string: **label**
    :param number: **v_current_min**
    :param number: **v_current_max**
    :param number: **v_speed**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format_min**
    :param string: **format_max**
    :param number: **power**
    :return: ``boolean`` - *result*, ``number`` - dragged_min, ``number`` - dragged_max

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    v_current_min  = 0.0
    v_current_max  = 1000.0

    function loop(delta)
        local label          = 'Drag float Range 2'
        local v_speed        =  0.5
        local v_min          =  0
        local v_max          =  1000.0
        local format_min     =  "%.1f MIN"
        local format_max     =  "%.1f MAX"
        local power          = 1.0

        local dragged, dragged_min, dragged_max = tImGui.DragFloatRange2(label, v_current_min, v_current_max, v_speed, v_min, v_max, format_min, format_max)
        if dragged then
            v_current_min = dragged_min
            v_current_max = dragged_max
        end
    end

.. figure:: _static/imgui_DragFloatRange2.png
   :align: center
   :figclass: align-center

HelpMarker
^^^^^^^^^^

.. data:: HelpMarker(text,string * mark)

    Helper to display a little (?) mark which shows a tooltip when hovered.

    :param string: **text**
    :param string: **mark** (default is ``(?)``)

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"

    function loop(delta)
        local text = 'Those flags are set by the back-ends (imgui_impl_xxx files) bla bla bla...'
        tImGui.HelpMarker(text)
    end

.. figure:: _static/imgui_HelpMarker.png
   :align: center
   :figclass: align-center

Image
^^^^^

.. data:: Image(texture_file_name, size, uv0, uv1, bg_col, line_color)

    :param string: **texture file name** or ``number`` ( **id** of texture )
    :param table: **size**         ``{x,y}`` (pass ``nil`` to get the default size)
    :param table: **uv0**          ``{x,y}`` (default size is 0,0)
    :param table: **uv1**          ``{x,y}`` (default size is 1,1)
    :param table: **bg_col**       ``{x,y,z,w}`` or ``{r,g,b,a}`` (default size is 1,1,1,1)
    :param table: **line_color**   ``{x,y,z,w}`` or ``{r,g,b,a}`` (default size is 0,0,0,1)

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"

    function loop(delta)
        local texture_name  = 'HB_smile.png'
        local size          = {x=50,y=50}
        local uv0           = {x=0,y=0}
        local uv1           = {x=1,y=1}
        local bg_col        = {r=1,g=1,b=1,a=1}
        local line_color    = {r=1,g=1,b=1,a=1} -- white color

        tImGui.Image(texture_name, size,uv0,uv1,bg_col,line_color)

    end

.. figure:: _static/imgui_Image.png
   :align: center
   :figclass: align-center

:download:`download HB_smile <example_modules/HB_smile.png>`.


ImageQuad
^^^^^^^^^

.. data:: ImageQuad(texture_file_name, size, uv0, uv1, bg_col, line_color)

    :param string: **texture file name** or ``number`` ( **id** of texture )
    :param table: **size**         ``{x,y}`` (pass ``nil`` to get the default size)
    :param table: **uv0**          ``{x,y}``
    :param table: **uv1**          ``{x,y}``
    :param table: **uv2**          ``{x,y}``
    :param table: **uv3**          ``{x,y}``
    :param table: **bg_col**       ``{x,y,z,w}`` or ``{r,g,b,a}`` (default size is 1,1,1,1)
    :param table: **line_color**   ``{x,y,z,w}`` or ``{r,g,b,a}`` (default size is 0,0,0,1)

*Example:*

.. code-block:: lua

    tImGui              =  require "ImGui"

    function loop(delta)
        local texture_name  = 'HB_smile.png'
        local size          = {x=50,y=50}
        local uv1           = {x = 0, y = 0}
        local uv2           = {x = 1, y = 0}
        local uv3           = {x = 1, y = 1}
        local uv4           = {x = 0, y = 1}
        local bg_col        = {r=1,g=1,b=1,a=1}
        local line_color    = {r=1,g=1,b=1,a=1} -- white color

        tImGui.ImageQuad(texture_name, size,uv1, uv2, uv3, uv4, bg_col, line_color)

    end

.. figure:: _static/imgui_Image.png
   :align: center
   :figclass: align-center

:download:`download HB_smile <example_modules/HB_smile.png>`.

ImageButton
^^^^^^^^^^^

.. data:: ImageButton(texture_file_name, size, uv0, uv1, bg_col, line_color)

    :param string: **texture file name** or ``number`` ( **id** of texture )
    :param table: **size**         ``{x,y}`` (pass ``nil`` to get the default size)
    :param table: **uv0**          ``{x,y}`` (default size is 0,0)
    :param table: **uv1**          ``{x,y}`` (default size is 1,1)
    :param number: **frame_padding**  <0 use default frame padding settings. 0 for no padding.
    :param table: **bg_col**       ``{x,y,z,w}`` or ``{r,g,b,a}`` (default size is 1,1,1,1)
    :param table: **tint_color**   ``{x,y,z,w}`` or ``{r,g,b,a}`` (default size is 1,1,1,1)

*Example:*

.. code-block:: lua

    tImGui                  =  require "ImGui"

    function loop(delta)
        local texture       = 'HB_smile.png'
        local size          = {x=100,y=100}
        local uv0           = {x=0,y=0}
        local uv1           = {x=1,y=1}
        local frame_padding = -1
        local bg_col        = {r=0,g=0,b=0,a=0}
        local tint_color    = {r=1,g=1,b=1,a=1}

        if tImGui.ImageButton(texture, size,uv0,uv1,frame_padding,bg_col,tint_color) then
            print('Button image pressed')
        end
    end

.. figure:: _static/imgui_ImageButton.png
   :align: center
   :figclass: align-center

:download:`download HB_smile <example_modules/HB_smile.png>`.

Input
^^^^^


InputInt
""""""""

.. data:: InputInt(label, value, step, step_fast, flags)


    :param string: **label**
    :param number: **value**
    :param number: **step**
    :param number: **step_fast**
    :param number: **flags**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    value      = 0

    function loop(delta)

        local label      = 'input int'
        local step       =  1
        local step_fast  =  100
        local flags      =  0

        local result, iValue = tImGui.InputInt(label, value, step, step_fast, flags)
        if result then
            value = iValue
        end
    end

.. figure:: _static/imgui_InputInt.png
   :align: center
   :figclass: align-center

InputInt2
"""""""""

.. data:: InputInt2(label, values, flags)

    :param string: **label**
    :param table: **values** ``{v1,v2}``
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    values      = {10,5}
    function loop(delta)

        local label      = 'InputInt2'
        local flags      =  0

        local result, iValues = tImGui.InputInt2(label, values, flags)
        if result then
            values = iValues
        end
    end

.. figure:: _static/imgui_InputInt2.png
   :align: center
   :figclass: align-center

InputInt3
"""""""""

.. data:: InputInt3(label, values, flags)

    :param string: **label**
    :param table: **values** ``{v1,v2,v3}``
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    values      = {10,5,25}

    function loop(delta)

        local label      = 'InputInt3'
        local flags      =  0

        local result, iValues = tImGui.InputInt3(label, values, flags)
        if result then
            values = iValues
        end
    end

.. figure:: _static/imgui_InputInt3.png
   :align: center
   :figclass: align-center

InputInt4
"""""""""

.. data:: InputInt4(label, values, flags)

    :param string: **label**
    :param table: **values** ``{v1,v2,v3,v4}``
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    values      = {10,-5,25,-99}

    function loop(delta)

        local label      = 'InputInt4'
        local flags      =  0

        local result, iValues = tImGui.InputInt4(label, values, flags)
        if result then
            values = iValues
        end
    end

.. figure:: _static/imgui_InputInt4.png
   :align: center
   :figclass: align-center

InputFloat
""""""""""

.. data:: InputFloat(label, value, step, step_fast, format, flags)

    :param string: **label**
    :param number: **value**
    :param number: **step**
    :param number: **step_fast**
    :param string: **format**
    :param number: **flags**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    value      = 50.0

    function loop(delta)

        local label      = 'input float'
        local step       =  1.0
        local step_fast  =  100.0
        local format     = "%.2f"
        local flags      =  0

        local result, fValue = tImGui.InputFloat(label, value, step, step_fast, format, flags)
        if result then
            value = fValue
        end
    end

.. figure:: _static/imgui_InputFloat.png
   :align: center
   :figclass: align-center


InputFloat2
"""""""""""

.. data:: InputFloat2(label, values, format, flags)

    :param string: **label**
    :param table: **values** ``{v1,v2}``
    :param string: **format**
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    values      = {10,5}
    function loop(delta)

        local label      = 'InputFloat2'
        local format     = '%.2f'
        local flags      =  0

        local result, fValues = tImGui.InputFloat2(label, values, format, flags)
        if result then
            values = fValues
        end
    end

.. figure:: _static/imgui_InputFloat2.png
   :align: center
   :figclass: align-center

InputFloat3
"""""""""""

.. data:: InputFloat3(label, values, format, flags)

    :param string: **label**
    :param table: **values** ``{v1,v2,v3}``
    :param string: **format**
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    values      = {10,5,88}
    function loop(delta)

        local label      = 'InputFloat3'
        local format     = '%.2f'
        local flags      =  0

        local result, fValues = tImGui.InputFloat3(label, values, format, flags)
        if result then
            values = fValues
        end
    end

.. figure:: _static/imgui_InputFloat3.png
   :align: center
   :figclass: align-center


InputFloat4
"""""""""""

.. data:: InputFloat4(label, values, format, flags)

    :param string: **label**
    :param table: **values** ``{v1,v2,v3,v4}``
    :param string: **format**
    :param number: **flags**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui      =  require "ImGui"
    values      = {10,5,-78.5,99.95}
    function loop(delta)

        local label      = 'InputFloat4'
        local format     = '%.2f'
        local flags      =  0

        local result, fValues = tImGui.InputFloat4(label, values, format, flags)
        if result then
            values = fValues
        end
    end

.. figure:: _static/imgui_InputFloat4.png
   :align: center
   :figclass: align-center


InputDouble
"""""""""""

.. data:: InputDouble(label, value, step, step_fast, format, flags)

    :param string: **label**
    :param number: **value**
    :param number: **step**
    :param number: **step_fast**
    :param string: **format**
    :param number: **flags**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    value      = math.pi

    function loop(delta)

        local label      = 'input double'
        local step       =  0.0001
        local step_fast  =  1.0
        local format     = "%.9f"
        local flags      =  0

        local result, fValue = tImGui.InputDouble(label, value, step, step_fast, format, flags)
        if result then
            value = fValue
        end
    end

.. figure:: _static/imgui_InputDouble.png
   :align: center
   :figclass: align-center

InputText
"""""""""

.. data:: InputText(label, text, flags)

    :param string: **label**
    :param string: **text**
    :param number: **flags**
    :return: ``boolean`` - *modified*, ``string`` - new_text

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    text       = 'text'
    function loop(delta)

        local label      = 'Label'
        local flags      = 0

        local modified , sNewText = tImGui.InputText(label,text,flags)
        if modified then
            text = sNewText
        end

    end

.. figure:: _static/imgui_inputText.png
   :align: center
   :figclass: align-center


InputTextWithHint
"""""""""""""""""

.. data:: InputTextWithHint(label, hint, text, flags)


    :param string: **label**
    :param string: **hint**
    :param string: **text**
    :param number: **flags**
    :return: ``boolean`` - *modified*, ``string`` - new_text

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    text       = 'text'
    function loop(delta)

        local label      = 'Label'
        local hint       = '<name>'
        local flags      = 0

        local modified , sNewText = tImGui.InputTextWithHint(label,text,hint,flags)
        if modified then
            text = sNewText
        end

    end

.. figure:: _static/imgui_inputTextHint.png
   :align: center
   :figclass: align-center


InputTextMultiline
""""""""""""""""""

.. data:: InputTextMultiline(label, text, size, flags)


    :param string: **label**
    :param string: **text**
    :param table:  **size** ``{x,y}``
    :param number: **flags**
    :return: ``boolean`` - *modified*, ``string`` - new_text

*Example:*

.. code-block:: lua

    tImGui     =  require "ImGui"
    text       = 'line 1\nline 2\nline 3\n...'
    function loop(delta)

        local label      = 'Label'
        local size       = {x=100,y=100}
        local flags      = 0

        local modified , sNewText = tImGui.InputTextMultiline(label,text,size,flags)
        if modified then
            text = sNewText
        end

    end

.. figure:: _static/imgui_InputTextMultiline.png
   :align: center
   :figclass: align-center

LabelText
^^^^^^^^^

.. data:: LabelText(label, text)

     Display text+label aligned the same way as value+label widgets

    :param string: **label**
    :param string: **text**

*Example:*

.. code-block:: lua

    tImGui            =  require "ImGui"

    function loop(delta)
        local label       = 'Label'
        local text        = 'text'
        tImGui.LabelText(label, text)
    end

.. figure:: _static/imgui_LabelText.png
   :align: center
   :figclass: align-center

ListBox
^^^^^^^

.. data:: ListBox(label, current_item, items, height_in_items)

    :param string: **label**
    :param number: **current_item**
    :param table: **array of items** ``{'item 1', 'item 2', 'item 3',... }``
    :param number: **height_in_items**
    :return: ``boolean`` - *result*, ``number`` - *current_item*, ``string`` - *item*

*Example:*

.. code-block:: lua

    tImGui           =  require "ImGui"
    current_item     = 1

    function loop(delta)

        local label              = "listbox\n(single select)"
        local items              = {1,2,3,true,false,"Apple", "Banana", "Cherry", "Kiwi", "Mango", "Orange", "Pineapple", "Strawberry", "Watermelon"}
        local height_in_items    =  -1
        local ret, iItem, item   = tImGui.ListBox(label, current_item,items, height_in_items)
        if ret then
            print(item)
            current_item = iItem
        end

    end

.. figure:: _static/imgui_ListBox.png
   :align: center
   :figclass: align-center

ListBoxHeader
"""""""""""""

    In principle this function should be called BeginListBox().

.. data:: ListBoxHeader(label, size)

     Use if you want to re-implement ListBox() will custom data or interactions. if the function return true, you can output elements then call ListBoxFooter() afterwards.

    :param string: **label**
    :param table: **size** ``{x,y}``
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local label     = 'label'
    local size_arg  =  {x=200,y=70}

    local ret = tImGui.ListBoxHeader(label, size_arg)

.. data:: ListBoxHeader(label, items_count,height_in_items)

     Use if you want to re-implement ListBox() will custom data or interactions. if the function return true, you can output elements then call ListBoxFooter() afterwards.

    :param string: **label**
    :param number: **items_count**
    :param number: **height_in_items** (default: ``-1``)
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui                 =  require "ImGui"
    local label            = 'label'
    local items_count      =  20
    local height_in_items  =  -1

    local ret = tImGui.ListBoxHeader(label, items_count,height_in_items)

ListBoxFooter
"""""""""""""

.. data:: ListBoxFooter()

     Terminate the scrolling region. only call ListBoxFooter() if ListBoxHeader() returned true!

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.ListBoxFooter()

Menu
^^^^

Menu
""""

In the section :ref:`Begin Menu <imgui_begin_menu>`.

.. figure:: _static/imgui_menu.png
   :align: center
   :figclass: align-center


MainMenuBar
"""""""""""

In the section :ref:`Begin Main Menu Bar <imgui_main_menu>`

.. figure:: _static/imgui_BeginMainMenuBar.png
   :align: center
   :figclass: align-center


MenuItem
""""""""

.. data:: MenuItem(label, shortcut, selected, enabled)

     Return true when clicked and ``boolean`` checked. shortcuts are displayed for convenience but not processed by ImGui at the moment

    :param string: **label**
    :param string: **shortcut** (Might be ``nil``)
    :param boolean: **selected**
    :param boolean: **enabled**
    :return: ``boolean`` - *clicked*, ``boolean`` - *checked*

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    selected        =  true
 
    function loop(delta)

        local label     =  'menu label'
        local shortcut  =  'F6'
        local enabled   =  true

        local clicked,checked = tImGui.MenuItem(label, shortcut, selected, enabled)
        if clicked then
            selected = checked
        end
    end


.. figure:: _static/imgui_MenuItem.png
   :align: center
   :figclass: align-center

NewLine
^^^^^^^

.. data:: NewLine()

     Undo a SameLine() or force a new line when in an horizontal-layout context.

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.NewLine()


PlotLines
^^^^^^^^^

.. data:: PlotLines(label, values, values_offset, overlay_text, scale_min, scale_max, graph_size)

    :param string: **label**
    :param number: **values** ``{v1,v2,v3,v4,...}``
    :param number: **values_offset**
    :param string: **overlay_text**
    :param number: **scale_min**
    :param number: **scale_max**
    :param number: **graph_size** ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui               =  require "ImGui"

    function loop(delta)
        local label          = "Lines"
        local values         = { 0.6, 0.1, 1.0, 0.5, 0.92, 0.1, 0.2 }
        local values_offset  =  0
        local overlay_text   =  'Result' -- might be nil
        local scale_min      =  0.1
        local scale_max      =  1.0
        local graph_size     =  {x=200,y=70}

        tImGui.PlotLines(label, values, values_offset, overlay_text, scale_min, scale_max, graph_size)
    end

.. figure:: _static/imgui_PlotLines.png
   :align: center
   :figclass: align-center

PlotHistogram
^^^^^^^^^^^^^

.. data:: PlotHistogram(label, values, values_offset, overlay_text, scale_min, scale_max, graph_size)

    :param string: **label**
    :param number: **values** ``{v1,v2,v3,v4,...}``
    :param number: **values_offset**
    :param string: **overlay_text**
    :param number: **scale_min**
    :param number: **scale_max**
    :param number: **graph_size** ``{x,y}``

*Example:*

.. code-block:: lua

    tImGui               =  require "ImGui"

    function loop(delta)
        local label          = ""
        local values         = { 0.6, 0.1, 1.0, 0.5, 0.92, 0.1, 0.2 }
        local values_offset  =  0
        local overlay_text   =  'Fancy Histogram' -- might be nil
        local scale_min      =  0.1
        local scale_max      =  1.0
        local graph_size     =  {x=300,y=150}

        tImGui.PlotHistogram(label, values, values_offset, overlay_text, scale_min, scale_max, graph_size)
    end

.. figure:: _static/imgui_PlotHistogram.png
   :align: center
   :figclass: align-center

ProgressBar
^^^^^^^^^^^

.. data:: ProgressBar(progress, size_arg, overlay)

    :param number: **progress**
    :param table: **size_arg**
    :param string: **overlay**

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local estimated = 2
    local current   = 0

    function loop(delta)

        current = current + delta
        if current >= estimated then
            current = 0
        end

        local progress = current / estimated
        local size_arg  =  {x = -1, y = 0}
        local overlay   =  string.format('Loading %d',progress * 100)
        tImGui.ProgressBar(progress, size_arg, overlay)
    end

.. figure:: _static/imgui_ProgressBar.gif
   :align: center
   :figclass: align-center

RadioButton
^^^^^^^^^^^

.. data:: RadioButton(label, active)

    :param string: **label**
    :param number: **index_activated** Radio-button which is activated.
    :param number: **my_index** .
    :return: ``number`` - *index of activated radio-button*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    index_activated = 2

    function loop(delta)
        index_activated = tImGui.RadioButton('Option 1', index_activated, 1)
        index_activated = tImGui.RadioButton('Option 2', index_activated, 2)
        index_activated = tImGui.RadioButton('Option 3', index_activated, 3)
    end

.. figure:: _static/imgui_RadioButton.png
   :align: center
   :figclass: align-center

Selectable
^^^^^^^^^^

.. data:: Selectable(label, selected, flags, size)

     "bool selected" carry the selection state (read-only). Selectable() is clicked is returns true so you can modify your selection state. size.x==0.0: use remaining width, size.x>0.0: specify width. size.y==0.0: use label height, size.y>0.0: specify height

    :param string: **label**
    :param boolean: **selected**
    :param number: **flags**
    :param table: **size**
    :return: ``boolean`` - *result*, ``boolean`` *selected*

*Example:*

.. code-block:: lua

    tImGui          =  require "ImGui"
    local label     = 'label'
    local selected  =  false
    local flags     =  0
    local size      =  {x=0,y=0}
    local result, selected = tImGui.Selectable(label, selected, flags, size)


SmallButton
^^^^^^^^^^^

.. data:: SmallButton(label)

     Button with FramePadding=(0,0) to easily embed within text

    :param string: **label**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"
    function loop(delta)
        local label  = 'Start'
        tImGui.SmallButton(label)
    end

.. figure:: _static/imgui_SmallButton.png
   :align: center
   :figclass: align-center

Slider
^^^^^^

SliderInt
"""""""""

.. data:: SliderInt(label, value, v_min, v_max, format)

    :param string: **label**
    :param number: **value**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    value   = 0

    function loop(delta)
        local label   = 'SliderInt'
        local v_min   = 0
        local v_max   = 100
        local format  = "%d"
        local result, iValue = tImGui.SliderInt(label, value, v_min, v_max, format)
        if result then
            value = iValue
        end
    end

.. figure:: _static/imgui_SliderInt.png
   :align: center
   :figclass: align-center

SliderInt2
""""""""""

.. data:: SliderInt2(label, values, v_min, v_max, format)

    :param string: **label**
    :param table: **values** ``{v1,v2}``
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    tValue        = {0,0}

    function loop(delta)
        local label   = 'SliderInt2'
        local v_min   = 0
        local v_max   = 100
        local format  = "%d"
        local result, iValues = tImGui.SliderInt2(label, tValue, v_min, v_max, format)
        if result then
            tValue = iValues
        end
    end

.. figure:: _static/imgui_SliderInt2.png
   :align: center
   :figclass: align-center


SliderInt3
""""""""""

.. data:: SliderInt3(label, values, v_min, v_max, format)

    :param string: **label**
    :param table: **values** ``{v1,v2,v3}``
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    tValue        = {0,0,0}

    function loop(delta)
        local label   = 'SliderInt3'
        local v_min   = 0
        local v_max   = 100
        local format  = "%d"
        local result, iValues = tImGui.SliderInt3(label, tValue, v_min, v_max, format)
        if result then
            tValue = iValues
        end
    end

.. figure:: _static/imgui_SliderInt3.png
   :align: center
   :figclass: align-center

SliderInt4
""""""""""

.. data:: SliderInt4(label, values, v_min, v_max, format)

    :param string: **label**
    :param table: **values** ``{v1,v2,v3,v4}``
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    tValue        = {0,0,0,0}

    function loop(delta)
        local label   = 'SliderInt4'
        local v_min   = 0
        local v_max   = 100
        local format  = "%d"
        local result, iValues = tImGui.SliderInt4(label, tValue, v_min, v_max, format)
        if result then
            tValue = iValues
        end
    end

.. figure:: _static/imgui_SliderInt4.png
   :align: center
   :figclass: align-center

SliderFloat
"""""""""""

.. data:: SliderFloat(label, value, v_min, v_max, format, power)

     Adjust format to decorate the value with a prefix or a suffix for in-slider labels or unit display. Use power!=1.0 for power curve sliders

    :param string: **label**
    :param number: **value**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    value         = 0

    function loop(delta)
        local label   = 'SliderFloat'
        local v_min   = 0
        local v_max   = 100
        local format  = "%.3f"
        local power   = 1.0
        local result, fValue = tImGui.SliderFloat(label, value, v_min, v_max, format,power)
        if result then
            value = fValue
        end
    end

.. figure:: _static/imgui_SliderFloat.png
   :align: center
   :figclass: align-center

SliderFloat2
""""""""""""

.. data:: SliderFloat2(label, values, v_min, v_max, format, power)

    :param string: **label**
    :param table: **values** ``{v1,v2}``
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    tValue = {0.0, 0.0}

    function loop(delta)
        local label   = 'SliderFloat2'
        local v_min   = 0
        local v_max   = 100
        local format  = "%.3f"
        local power   = 1.0
        local result, fValues = tImGui.SliderFloat2(label, tValue, v_min, v_max, format, power)
        if result then
            tValue = fValues
        end
    end

.. figure:: _static/imgui_SliderFloat2.png
   :align: center
   :figclass: align-center

SliderFloat3
""""""""""""

.. data:: SliderFloat3(label, values, v_min, v_max, format, power)

    :param string: **label**
    :param table: **values** ``{v1,v2,v3}``
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    tValue = {0.0, 0.0, 0.0}

    function loop(delta)
        local label   = 'SliderFloat3'
        local v_min   = 0
        local v_max   = 100
        local format  = "%.3f"
        local power   = 1.0
        local result, fValues = tImGui.SliderFloat3(label, tValue, v_min, v_max, format, power)
        if result then
            tValue = fValues
        end
    end

.. figure:: _static/imgui_SliderFloat3.png
   :align: center
   :figclass: align-center

SliderFloat4
""""""""""""

.. data:: SliderFloat4(label, values, v_min, v_max, format, power)

    :param string: **label**
    :param table: **values** ``{v1,v2,v3,v4}``
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``table`` *values*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    tValue = {0.0, 0.0, 0.0, 0.0}

    function loop(delta)
        local label   = 'SliderFloat4'
        local v_min   = 0
        local v_max   = 100
        local format  = "%.1f"
        local power   = 1.0
        local result, fValues = tImGui.SliderFloat4(label, tValue, v_min, v_max, format, power)
        if result then
            tValue = fValues
        end
    end

.. figure:: _static/imgui_SliderFloat4.png
   :align: center
   :figclass: align-center


SliderAngle
"""""""""""

    Display angle in degree from radian value. The returned value is in radian.

.. data:: SliderAngle(label, value_in_radian, v_degrees_min, v_degrees_max, format)

    :param string: **label**
    :param number: **value_in_radian**
    :param number: **v_degrees_min**
    :param number: **v_degrees_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``number`` radian

*Example:*

.. code-block:: lua

    tImGui               =  require "ImGui"
    radian               = math.rad(90)

    function loop(delta)
        local label           = 'SliderAngle'
        local a_min           = 0
        local a_max           = 180
        local format          = "%.1f"
        local result, fRadian = tImGui.SliderAngle(label, radian, a_min, a_max, format)
        if result then
            radian = fRadian
        end
    end

.. figure:: _static/imgui_SliderAngle.png
   :align: center
   :figclass: align-center


TreeNode
^^^^^^^^


TreeNode
""""""""

.. data:: TreeNode(label)

    :param string: **label**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"

    function loop(delta)
        if tImGui.TreeNode("this is a tree node") then
            if tImGui.TreeNode("another one of those tree node...") then
                tImGui.Text("Some tree contents")
                tImGui.TreePop()
            end
            tImGui.TreePop()
        end
    end


.. figure:: _static/imgui_TreeNode.png
   :align: center
   :figclass: align-center

TreeNode
""""""""

.. data:: TreeNode(str_id,label)

    :param string: **str_id** (might be ``nil``)
    :param string: **label**
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"

    function loop(delta)

        if tImGui.TreeNode('id_0',"this is a tree node") then
            if tImGui.TreeNode('id_1',"another one of those tree node...") then
                tImGui.Text("Some tree contents")
                tImGui.TreePop()
            end
            tImGui.TreePop()
        end
    end


.. figure:: _static/imgui_TreeNode.png
   :align: center
   :figclass: align-center

TreeNodeEx
""""""""""

.. data:: TreeNodeEx(label, flags, str_id*)

    :param string: **label**
    :param number: **flags**
    :param string: **str_id** (might be omitted)
    :return: ``boolean`` - *result*

*Example:*

.. code-block:: lua

    tImGui       =  require "ImGui"

    function loop(delta)
        local flags = 0
        if tImGui.TreeNodeEx("this is a tree node Ex",flags) then
            if tImGui.TreeNodeEx("another one of those tree node Ex...",flags) then
                tImGui.Text("Some tree contents")
                tImGui.TreePop()
            end
            tImGui.TreePop()
        end
    end

.. figure:: _static/imgui_TreeNodeEx.png
   :align: center
   :figclass: align-center

TreePush
""""""""

.. data:: TreePush(str_id)

     ~Indent()+PushId(). Already called by TreeNode() when returning true, but you can call TreePush/TreePop yourself if desired.

    :param string: **str_id** (might be ``nil``)

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local str_id  =  '01'
    tImGui.TreePush(str_id)


TreePop
"""""""

.. data:: TreePop()

     ~Unindent()+PopId()

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    tImGui.TreePop()

TreeAdvanceToLabelPos
"""""""""""""""""""""

.. data:: TreeAdvanceToLabelPos()


*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    function loop(delta)
        tImGui.TreeAdvanceToLabelPos()
        if tImGui.TreeNode("this is a tree node") then
            if tImGui.TreeNode("another one of those tree node...") then
                tImGui.Text("Some tree contents")
                tImGui.TreePop()
            end
            tImGui.TreePop()
        end
    end

.. figure:: _static/imgui_TreeAdvanceToLabelPos.png
   :align: center
   :figclass: align-center

CollapsingHeader
""""""""""""""""


.. data:: CollapsingHeader(label, p_open, flags)

     When 'p_open' isn't ``false``, display an additional small close button on upper right of the header

    :param string: **label**
    :param boolean: **p_open**
    :param number: **flags**
    :return: ``boolean`` - *result*, ``boolean`` - *p_open*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    local label   = 'my label'
    local p_open  =  true
    local flags   =  0
    local ret, p_open = tImGui.CollapsingHeader(label, p_open, flags)


GetTreeNodeToLabelSpacing
"""""""""""""""""""""""""

.. data:: GetTreeNodeToLabelSpacing()

     Horizontal distance preceding label when using TreeNode * () or Bullet() == (g.FontSize + style.FramePadding.x * 2) for a regular unframed TreeNode

    :return: ``number`` - *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"

    tImGui.GetTreeNodeToLabelSpacing()

SetNextItemOpen
"""""""""""""""

.. data:: SetNextItemOpen(is_open, cond)

     Set next TreeNode/CollapsingHeader open state.

    :param boolean: **is_open**
    :param number: **cond**

*Example:*

.. code-block:: lua

    tImGui         =  require "ImGui"
    local is_open  = true
    local cond     =  0
    tImGui.SetNextItemOpen(is_open, cond)

VSlider
^^^^^^^


VSliderInt
""""""""""

.. data:: VSliderInt(label, size, value, v_min, v_max, format)

    :param string: **label**
    :param table: **size** ``{x,y}``
    :param number: **value**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui        =  require "ImGui"
    value         = 0

    function loop(delta)
        local label          = 'VSliderInt'
        local size           = {x=20,y=100}
        local v_min          = 0
        local v_max          = 5
        local format         = "%d"
        local result, iValue = tImGui.VSliderInt(label, size, value, v_min, v_max, format)
        if result then
            value = iValue
        end
    end

.. figure:: _static/imgui_VSliderInt.png
   :align: center
   :figclass: align-center

VSliderFloat
""""""""""""

.. data:: VSliderFloat(label, size, value, v_min, v_max, format, power)

    :param string: **label**
    :param table: **size** ``{x,y}``
    :param number: **value**
    :param number: **v_min**
    :param number: **v_max**
    :param string: **format**
    :param number: **power**
    :return: ``boolean`` - *result*, ``number`` *value*

*Example:*

.. code-block:: lua

    tImGui  =  require "ImGui"
    value   = 0

    function loop(delta)
        local label   = 'VSliderFloat'
        local size    = {x=30,y=100}
        local v_min   = 0
        local v_max   = 100
        local format  = "%.1f"
        local power   =  1.0
        local result, iValue = tImGui.VSliderFloat(label, size, value, v_min, v_max, format,power)
        if result then
            value = iValue
        end
    end

.. figure:: _static/imgui_VSliderFloat.png
   :align: center
   :figclass: align-center