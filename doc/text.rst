.. contents:: Table of Contents

.. _text:

Table text
==========

A table :ref:`text <text>` inherit from :ref:`renderizable <renderizable>` and implement all methods as usual.
A :ref:`text <text>` is created by a :ref:`font <font>` using the method :ref:`addText <addText>`.
An animation has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.
Also an animation for text does not change the frame (there is no this concept on :ref:`text <text>`), and instead of this, change how is drew the text on screen.
A :ref:`text <text>` can have more than one animation simultaneous (use :ref:`setWildCard <setWildCard>` to do it).

text methods
------------

text getSize
^^^^^^^^^^^^

.. data:: getSize(boolean* forceCalc)

   Get the size of :ref:`text <text>`, Do **NOT** consider the rotation.
   It considers the scale.

   :return: ``number`` *width*, ``number`` *height* - *dimension* of :ref:`text <text>`.

   *Example:*
.. code-block:: lua

   tArial15 = font:new('Arial15.fnt') -- load a file previously created by the engine.
   tText = tArial15:add('Some text here','2dw')
   local width, height = tText:getSize()
   print(width,height)

   local width, height = tText:getSize(true) -- force recalculate
   print(width,height)

.. Note::

   Only if the :ref:`text <text>` is ``3D`` there will be a depth value.


.. data:: getSize(string other_text)

   Get the size of the string sent as :ref:`text <text>`, Do **NOT** consider the rotation.
   It considers the scale.

   :param string: **text** to be calculated.
   :return: ``number`` *width*, ``number`` *height* - *dimension* of :ref:`text <text>`.

   *Example:*
.. code-block:: lua

   tArial15 = font:new('Arial15.fnt') -- load a file previously created by the engine.
   tText = tArial15:add('Some text here','2dw')
   local width, height = tText:getSize('Other text\nHere is a new line!')
   print(width,height)

.. Note::

   Only if the :ref:`text <text>` is ``3D`` there will be a depth value.


text getSizeText
^^^^^^^^^^^^^^^^

.. data:: getSizeText(text other_text)

   Get the size of :ref:`text <text>` **NOT** considering the rotation.
   It considers the scale.

   :param string: **text** to be calculated.
   :return: ``number`` *width*, ``number`` *height* - *dimension* of :ref:`text <text>`.

   *Example:*
.. code-block:: lua

   tArial15 = font:new('Arial15.fnt') -- load a file previously created by the engine.
   tText = tArial15:add('Some text here','2dw')
   local width, height = tText:getSizeText('Other other text\nHere is a new line!')
   print(width,height)

.. Note::

   Only if the :ref:`text <text>` is ``3D`` there will be a depth value.

.. _setWildCard:

text setWildCard
^^^^^^^^^^^^^^^^

.. data:: setWildCard(string wildCard)

   Set the wildCard to change  the animations in the same text.


   :param string: **wildCard** any character.

   *Example:*
.. code-block:: lua

   tArial15 = font:new('Arial15.fnt') -- load a file previously created by the engine.
   tText = tArial15:add('Hello world','2dw')
   tText:setWildCard('#')
   tText.text =  '#1#Hello #2#World!' --new text with wild-card.

.. Note::

   | In the example we are expecting the text has at least two animation.
   | :guilabel:`#1#` is the number for animation 'one'.
   | :guilabel:`#2#` is the number for animation 'two'.


text attributes
---------------

.. data:: text [read / write]

   Access the :ref:`text <text>` member for read / write.

   *Example:*
.. code-block:: lua

   tArial15 = font:new('Arial15.fnt') -- load a file previously created by the engine.
   tText = tArial15:add('Hello world','2dw')
   print(tText.text) -- Hello world
   tText.text =  'Welcome to my example!' --new text with
   print(tText.text) -- Welcome to my example!


.. data:: align [read / write]

   Access the align from :ref:`text <text>` member for read / write. The default is aligned to ``left``.

   The valid values are: ``left``, ``right`` and ``center``.

   *Example:*
.. code-block:: lua

   tArial15 = font:new('Arial15.fnt') -- load a file previously created by the engine.
   tText = tArial15:add('Hello world aligned!\nNew line','2dw')
   print(tText.align) -- left
   tText.align =  'center'

Creating a text programmatically
--------------------------------

To create a :ref:`text <text>` programmatically you need create a :ref:`font <font>`.

An example can be found :ref:`here  <creating_font>`.
