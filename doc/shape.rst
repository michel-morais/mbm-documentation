.. contents:: Table of Contents

.. _shape:

Table shape
===========

A table :ref:`shape <shape>` inherit from :ref:`renderizable <renderizable>` and implement all methods as usual.
Also each animation has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.

The main functionality of shape is to easily create a shape of circle, triangle or rectangle. Also is possible to create a free
shape using the methods :ref:`create <shape_create>` and :ref:`createIndexed <shape_createIndexed>`.

The table :ref:`shape <shape>` is also capable to render in real time using the method :ref:`onRender <onRenderShape>`. To do that the frame must be created as dynamic mode option.

.. Note::
  The shape is not capable to store runtime information.

  On lost device, the shape will lost its information, so, it must be reloaded on method :ref:`onRestore <onRestore>`.

  Also is lost its texture and callback for :ref:`onEndAnim <onEndAnim>` and :ref:`onEndFx <onEndFx>`.

shape methods
-------------

shape new
^^^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`shape <shape>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`shape <shape>` *table*.

   *Example:*
.. code-block:: lua

   tShape = shape:new('2dw')

.. _shape_create:

shape create
^^^^^^^^^^^^

.. data:: create(string type, number width, number * height, number * triangles, boolean * dynamic_buffer,  string * nick_name)

   Create a :ref:`shape <shape>` in runtime choosing among the pre defined option available.

   The :ref:`onRender <onRenderShape>` callback can be used if dynamic_buffer is ``true``.

   :param string: **type** pre defined.  ``circle``, ``rectangle`` or ``triangle``.
   :param number: **width** of shape.
   :param number: **height** of shape.
   :param number: **triangles** - default values ``circle: 18`` -  ``rectangle: 2``, ``triangle: 1``.
   :param boolean: **dynamic buffer** option to render in runtime the shape using :ref:`onRender <onRenderShape>` method (optional).
   :param string: **nick name** of shape (optional).
   :return: ``string`` *- nick_name*

   *Example:*
.. code-block:: lua


   tShapeCircle    = shape:new('2dw', -100)
   tShapeRectangle = shape:new('2dw',  0  )
   tShapeTriangle  = shape:new('2dw',  100)

   local nickNameCircle    = tShapeCircle:create('circle',100,100)
   local nickNameRectangle = tShapeRectangle:create('rectangle',100,100)
   local nickNameTriangle  = tShapeTriangle:create('triangle',100,100)

   if nickNameCircle then
      print('Circle ' .. nickNameCircle .. '  successfully created!')
   else
      print('Failed to create a circle')
   end

   if nickNameRectangle then
      print('Rectangle '.. nickNameRectangle .. ' successfully created!')
   else
      print('Failed to create a rectangle')
   end

   if nickNameTriangle then
      print('Triangle '.. nickNameTriangle .. ' successfully created!')
   else
      print('Failed to create a triangle')
   end

.. figure:: _static/shape_pre_defined.png
    :align: center
    :figclass: align-center

    Three shapes created as result.


.. _shape_create_circle:


   As is known, a shape is based on triangles and, to build a circle is not different. If is necessary to have a huge detail on circle, is recommended to create it using shader.


   *Example creating circle with different number of triangles*

.. code-block:: lua


   tShapeCircle_4    = shape:new('2dw', -200)
   tShapeCircle_8    = shape:new('2dw',  0)
   tShapeCircle_18   = shape:new('2dw',  200)

   local nickNameCircle_4   = tShapeCircle_4:create ('circle',200,200, 4)
   local nickNameCircle_8   = tShapeCircle_8:create ('circle',200,200, 8)
   local nickNameCircle_18  = tShapeCircle_18:create('circle',200,200, 18)

   if nickNameCircle_4 then
      print('Circle ' .. nickNameCircle_4 .. ' with 4 triangle  successfully created!')
   else
      print('Failed to create a circle')
   end

   if nickNameCircle_8 then
      print('Circle ' .. nickNameCircle_8 .. ' with 8 triangle  successfully created!')
   else
      print('Failed to create a circle')
   end

   if nickNameCircle_18 then
      print('Circle ' .. nickNameCircle_18 .. ' with 18 triangle  successfully created!')
   else
      print('Failed to create a circle')
   end

.. figure:: _static/shape_three_shape_circle.png
    :align: center
    :figclass: align-center

    Three circle created as result with different number of triangles.
    Although the first seems to be a square, it is a circle but with a minimum of triangles needed.

.. data:: create(table vertex, table* uv, table* normal, string * nick_name, string * mode_draw,string * mode_cull_face, string * mode_front_face_direction)

   Create a :ref:`shape <shape>` in runtime supplying native coordinates.

   :param table: **vertex** coordinates ``x,y`` or ``x,y,z``.
   :param table: **uv** coordinates ``u,v`` (optional).
   :param table: **normal** coordinates ``x,y,z`` (optional).
   :param string: **nick name** of shape (optional).
   :param string: **mode draw** of shape (optional). see :ref:`mode draw <ModeDraw>`.
   :param string: **mode cull face** of shape (optional). see :ref:`cull face <CullFace>`.
   :param string: **mode front face direction** of shape (optional). see :ref:`front face direction <front_face_direction>`.
   :return: ``string`` *- nick_name*


   *Example:*
.. code-block:: lua

   -- Example: 2d
   tShape = shape:new('2dw')

   --letś draw a square of 400x400
   --                    First Triangle                          Second Triangle
   local tVertex  = {-200,-200,  -200,200,  200,-200,       200,-200, -200,200, 200,200} -- (x,y) coordinates
   local tUv      = {0,1, 0,0, 1,1,                         1,1, 0,0, 1,0}
   local tNormal  = nil --let the normal be calculated automatically

   if tShape:create(tVertex,tUv,tNormal,'my-shape-nick-name') then
      print('Shape successfully created!')
   else
      print('Failed to create a shape')
   end

.. figure:: _static/shape_image_example_1.png
    :width: 127px
    :align: center
    :height: 106px
    :figclass: align-center

    An square shape without texture as result

.. _shape_create3d:

.. code-block:: lua

   -- Example: 3d
   tShape = shape:new('3d',0,0,500)

   --letś draw a square of 400x400x0
   --                    First Triangle                          Second Triangle
   local tVertex  = {-200,-200,0,  -200,200,0,  200,-200,0,       200,-200,0, -200,200,0, 200,200,0} -- (x,y) coordinates
   local tUv      = {0,1, 0,0, 1,1,                         1,1, 0,0, 1,0}
   local tNormal  = nil --let the normal be calculated automatically

   if tShape:create(tVertex,tUv,tNormal,'my-shape-nick-name','TRIANGLES', 'BACK','CW') then
      print('Shape successfully created!')
   else
      print('Failed to create a shape')
   end

.. _shape_createIndexed:

shape createIndexed
^^^^^^^^^^^^^^^^^^^

.. data:: createIndexed(table vertex, table index, table* uv, table* normal, string * nick_name, string * mode_draw,string * mode_cull_face, string * mode_front_face_direction)

   Create a :ref:`shape <shape>` in runtime supplying indexed native coordinates.

   :param table: **vertex** coordinates ``x,y`` or ``x,y,z``.
   :param table: **index** array referent to vertex (one based).
   :param table: **uv** coordinates ``u,v`` (optional).
   :param table: **normal** coordinates ``x,y,z`` (optional).
   :param string: **nick name** of shape (optional).
   :param string: **mode draw** of shape (optional). see :ref:`mode draw <ModeDraw>`.
   :param string: **mode cull face** of shape (optional). see :ref:`cull face <CullFace>`.
   :param string: **mode front face direction** of shape (optional). see :ref:`front face direction <front_face_direction>`.
   :return: ``string`` *- nick_name*

   *Example: 2d*
.. code-block:: lua

   tShape = shape:new('2dw')
   --letś draw a square of 400x400
   --                  1           2         3          4
   local tVertex  = {-200,-200,  -200,200,  200,-200,  200,200} -- (x,y) coordinates
   local tUv      = {0,1,           0,0,      1,1,       1,0  }
   local tIndex   = {1,2,3, 3,2,4 } -- one based
   local tNormal  = nil --let the normal be calculated automatically

   if tShape:createIndexed(tVertex,tIndex,tUv,tNormal,'my-shape-nick-name','TRIANGLES', 'BACK','CW') then
      print('Shape successfully created!')
   else
      print('Failed to create a shape')
   end


.. figure:: _static/shape_triangle_list.png
    :align: center
    :height: 447px
    :figclass: align-center

    Rendering using triangle list, Clockwise and CullFace mode back (default of the engine).


shape createDynamicIndexed
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: createDynamicIndexed(table vertex, table index, table* uv, table* normal, string * nick_name, string * mode_draw,string * mode_cull_face, string * mode_front_face_direction)

   Create a dynamic :ref:`shape <shape>` in runtime supplying indexed native coordinates.

   The :ref:`onRender <onRenderShape>` callback can be used after creating with this method.

   :param table: **vertex** coordinates ``x,y`` or ``x,y,z``.
   :param table: **index** array referent to vertex (one based).
   :param table: **uv** coordinates ``u,v`` (optional).
   :param table: **normal** coordinates ``x,y,z`` (optional).
   :param string: **nick name** of shape (optional).
   :param string: **mode draw** of shape (optional). see :ref:`mode draw <ModeDraw>`.
   :param string: **mode cull face** of shape (optional). see :ref:`cull face <CullFace>`.
   :param string: **mode front face direction** of shape (optional). see :ref:`front face direction <front_face_direction>`.
   :return: ``string`` *- nick_name*

   *Example: 2d*
.. code-block:: lua

   tShape = shape:new('2dw')
   --letś draw a square of 400x400
   --                  1           2         3          4
   local tVertex  = {-200,-200,  -200,200,  200,-200,  200,200} -- (x,y) coordinates
   local tUv      = {0,1,           0,0,      1,1,       1,0  }
   local tIndex   = {1,2,3, 3,2,4 } -- one based
   local tNormal  = nil --let the normal be calculated automatically

   if tShape:createDynamicIndexed(tVertex,tIndex,tUv,tNormal,'my-shape-nick-name','TRIANGLES', 'BACK','CW') then
      print('Shape successfully created!')
   else
      print('Failed to create a shape')
   end

setColor
^^^^^^^^

.. data:: setColor(number red,number green,number blue,number * alpha)

  Set a solid color as texture.

  :param number: **red** color. Values between 0.0 and 1.0
  :param number: **green** color. Values between 0.0 and 1.0
  :param number: **blue** color. Values between 0.0 and 1.0
  :param number: **alpha** color. Values between 0.0 and 1.0
  :return: ``boolean`` *- successfully* applied.

   *Example:*
.. code-block:: lua

   tShape = shape:new('2dw')
   tShape:create('RECTANGLE',100,100)
   local r,g,b, a = 1.0, 0.0, 0.0, 1.0
   local ret = tShape:setColor(r,g,b,a)


.. figure:: _static/shape_red.png
    :align: center
    :figclass: align-center

    Setting red color for the shape

.. _onRenderShape:

onRender
^^^^^^^^

.. data:: onRender(tShape,vertex,normal,uv,index_read_only)

  Shape is capable to render in run time if created with the option dynamic mode as mentioned at :ref:`shape:create <shape_create>` .

  The data from callback are organized in ``x, y, z`` for vertex, ``nx, ny, nz`` for normal and ``u, v`` for texture coordinates.

  Also each table has a property ``name`` to identify when return from render callback.

  The first argument is the shape itself following from vertex table, normal table, uv table and index_buffer.

  The index buffer is read only and one based. Do not need to return it.

  .. Note:: Passing ``nil`` to method :ref:`onRender <onRenderShape>` the callback is cancelled.

  :param table: **shape** instance of shape.
  :param table: **[x,  y,  x]** vertex table.
  :param table: **[nx, ny, nz]** normal table.
  :param table: **[u,  v]** texture coordinates.
  :param table: **[]** array index buffer read only. (one based)

  :return: ``table`` *vertex*, ``table`` *normal*, ``table`` *uv* - if ``nil`` is returned the callback is cancelled. Not all arguments are necessary, just the one edited.

   *Example how to access all members from callback:*
.. code-block:: lua

   mbm.setColor(0.7,0.7,0.7) --set background color to gray
   local numberTriangle = 1
   local isDynamic = true
   tShape = shape:new('2dw')
   nickName = tShape:create('Triangle',1000,1000,numberTriangle,isDynamic)
   print('Shape Nick Name:' .. nickName)

   function onRender(tShape,vertex,normal,uv,index_read_only)

      print("--------------------")
      print(" table (" .. vertex.name .. ')')
      for i=1,#vertex do
         print('x:' .. vertex[i].x .. ' y:' .. vertex[i].y .. ' z:' .. vertex[i].z)
      end

      print(" table (" .. normal.name .. ')')
      for i=1,#normal do
         print('nx:' .. normal[i].nx .. ' ny:' .. normal[i].ny .. ' nz:' .. normal[i].nz)
      end

      print(" table (" .. uv.name .. ')')
      for i=1,#uv do
         print('u:' .. uv[i].u .. ' v:' .. uv[i].v )
      end

      print("--------------------")

      -- if does not return or return nil the callback is cancelled
      return vertex,normal,uv
      --is possible to choose what return because of the name in the table which identify it
      -- example:
      -- return uv, vertex
      --
      -- if return nil or does not return the callback is cancelled
      -- example
      -- return nil
   end

   --set the callback
   tShape:onRender(onRender)


.. figure:: _static/shape_onRender_1.png
    :align: center
    :figclass: align-center

    The output should be like this.



.. Warning::
   | If return ``nil`` or does not return, the callback is cancelled for :guilabel:`onRender`.

