.. contents:: Table of Contents

.. _tile:

Table tile
==========

A table :ref:`tile <tile>` inherit from :ref:`renderizable <renderizable>` and implement all methods as usual.
Also the animation has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.

A tile is created using the :ref:`tile map editor <tile_map_editor>`


tile map definitions
--------------------

There are some definitions that are important to understand.

:ref:`Brick ID <brickID>` and :ref:`Tile ID <tileIdMap>`.

.. _brickID:

Brick ID
^^^^^^^^

Brick ID is a unique identifier for each brick.

In the editor the brick ID is just a number of itself:

.. figure:: _static/tile_map_7.png
   :align: center

   Brick ID on editor


Tile ID in the map
^^^^^^^^^^^^^^^^^^

.. _tileIdMap:

This is how *tile* are organized in the map.

- It is called '*tile*' when the brick it is in the map.
- The total of tile is *count width tile* x *count height tile*.
- The first tile is the *ID 1*, and the last one depends on map size.
- If we have for example *20x10* tiles we will have 200 tiles in total. 200th is the last one.
- Zero means *no tile* .
- See the figure to understand better:


.. figure:: _static/tile_map_6.png
   :align: center

   20x10 tiles = 200 tiles.



tile methods
------------

tile new
^^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`tile <tile>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`tile <tile>` *table*.

   *Example:*
.. code-block:: lua

   tTile = tile:new('2dw')


tile load
^^^^^^^^^

.. data:: load(string file_name)

   Load a :ref:`tile <tile>` from file (expected ``.tile`` extension).

   :param string: **file name** from file.

   *Example:*
.. code-block:: lua

   tTile = tile:new('2dW')
   if tTile:load('my_map.tile') then
      print('Successfully loaded tile map:','my_map.tile')
   else
      print('Failed to loaded tile map:','my_map.tile')
   end

.. Note::

   | The :guilabel:`load` method will search in all known path.
   | You can add a path to search by the method :ref:`addPath <addPath>`.


tile setLayerVisible
^^^^^^^^^^^^^^^^^^^^

.. data:: setLayerVisible(number layer, boolean value)

   Turn a layer visible or not indicating the number of it (one based).

   :param number: **layer** 1 to total of layer.
   :param boolean: **visible** ``true`` or ``false``.

   *Example:*
.. code-block:: lua

   tTile = tile:new('2dW')
   if tTile:load('my_map.tile') then
      tTile:setLayerVisible(1,false)
   else
      print('Failed to loaded tile map:','my_map.tile')
   end

.. Note::

   | Initially all layers are visible.

tile getTotalLayer
^^^^^^^^^^^^^^^^^^

.. data:: getTotalLayer()

   Retrieve the total of layer from tile map.

   :return: ``number`` - *total* of layer.

   *Example:*
.. code-block:: lua

   tTile = tile:new('2dW')
   if tTile:load('my_map.tile') then
      print('Total layer:',tTile:getTotalLayer())
   else
      print('Failed to loaded tile map:','my_map.tile')
   end


tile getTypeMap
^^^^^^^^^^^^^^^

.. data:: getTypeMap()

   Retrieve the type of the tile map (``isometric``, ``orthogonal``, ``hexagonal``)

   :return: ``string`` - *type* of tile map.

   *Example:*
.. code-block:: lua

   tTile = tile:new('2dW')
   if tTile:load('my_map.tile') then
      print('Type Tile Map:',tTile:getTypeMap())
   else
      print('Failed to loaded tile map:','my_map.tile')
   end

tile getTileID
^^^^^^^^^^^^^^

.. data:: getTileID(number x, number y, number layer)

   Retrieve the tile ID in the map given ``x``, ``y`` in ``2d screen`` coordinate and the layer number.

   :param number: **x** coordinate in ``2d screen``.
   :param number: **y** coordinate in ``2d screen``.
   :param number: **layer** 1 to total of layer.
   :return: ``number`` - *ID* of tile on tile map (1 to total of tile on map).

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')
      
   function onTouchMove(key,x,y)
      local layer = 1
      local ID = tTile:getTileID(x,y,layer)
      print('Tile ID:', ID)
   end


tile disableTile
^^^^^^^^^^^^^^^^

.. data:: disableTile(number x, number y, number * layer)

   Disable rendering of tile, given the position ``x``, ``y`` of the tile on the map in ``2d screen`` coordinate, and the layer number.

   :param number: **x** coordinate in ``2d screen``.
   :param number: **y** coordinate in ``2d screen``.
   :param number: **layer** 1 to total of layer. (Optional)
   :return: ``number`` - *position* of tile on tile map. (Zero means not found at ``x``, ``y`` coordinate).

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')
      
   function onTouchDown(key,x,y)
      local ID = tTile:disableTile(x,y)
      if ID ~= 0 then
         print ('Tile disabled ID ',ID)
      end
   end

.. Note:: 
   
   | See :ref:`tile ID <tileIdMap>` to understand better how tile ID works.
   | If -1 is supplied to layer, it will disable each tile for each layer at position.



tile enableTile
^^^^^^^^^^^^^^^

.. data:: enableTile(number x, number y, number * layer)

   Enable rendering of tile, given the position ``x``, ``y`` of the tile on the map in ``2d screen`` coordinate, and the layer number.

   :param number: **x** coordinate in ``2d screen``.
   :param number: **y** coordinate in ``2d screen``.
   :param number: **layer** 1 to total of layer. (Optional)
   :return: ``number`` - *position* of tile on tile map. (Zero means not found at ``x``, ``y`` coordinate).

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')
      
   function onTouchDown(key,x,y)
      local ID = tTile:enableTile(x,y)
      if ID ~= 0 then
         print ('Tile Enabled ID ',ID)
      end
   end

.. Note:: 
   
   | See :ref:`tile ID <tileIdMap>` to understand better how tile ID works.
   | If -1 is supplied to layer, it will enable each tile for each layer at position.


tile getPositionsFromBrickID
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: getPositionsFromBrickID(number idBrick,number layer)

   Return a ``table`` of ``x``, ``y`` positions, and ``layer`` of one or more tiles found on the map, according to the :ref:`brick ID <brickID>` and the number of layer (can be omitted).

   :param number: **brick** id.
   :param number: **layer** (one based).
   :return: ``table`` - ``{ x, y, layer}, { x, y, layer}, ...}``.

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')
   local brickID = 32 -- according to bricks
   local tPos = tTile:getPositionsFromBrickID(brickID)
   for i=1, #tPos do
      print(i,tPos[i].x,tPos[i].y,tPos[i],tPos[i].layer)
   end

.. Note:: 
   
   | If -1 is supplied to layer, it will retrieve each tile found for each layer.
   | Note that :ref:`brick ID <brickID>` id is different of :ref:`tile ID <tileIdMap>`.


tile getPositionFromTileID
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: getPositionFromTileID(number tileID)

   Return the ``x``, ``y`` tile position given the tile ID on map.

   :param number: **tile** id.
   :return: ``number`` - x, ``number`` - y

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')
   local tileID = 32
   local x,y = tTile:getPositionFromTileID(tileID)
   print('Center of tile 32:',x,y)

tile getNearPosition
^^^^^^^^^^^^^^^^^^^^

.. data:: getNearPosition(number x,number y, number layer)

   Return the near ``x``, ``y`` tile position given x,y in ``2d screen`` and layer.

   :param number: **x** position in ``2d screen``.
   :param number: **y** position in ``2d screen``.
   :param number: **layer** (one based) (if not supplied will get the first one).
   :return: ``number`` - x, ``number`` - y

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')
   
   local x,y = 10, 10
   local x,y = tTile:getNearPosition(x,y)
   


tile setTileID
^^^^^^^^^^^^^^

.. data:: setTileID(number x,number y, number brickID,number layer)

   Set a tile to render a brick given the :ref:`brick ID <brickID>` at ``x``, ``y`` position in ``2d screen``, and ``layer``.

   :param number: **x** position in ``2d screen``.
   :param number: **y** position in ``2d screen``.
   :param number: **brick** ID.
   :param number: **layer** (one based).
   :return: ``boolean`` - result of operation.

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   function onTouchDown(key,x,y)
      local brickID = 1
      local layer   = 1
      tTile:setTileID(x,y,brickID,layer)
   end
   
.. Note:: 
   
   | Note that :ref:`brick ID <brickID>` id is different of :ref:`tile ID <tileIdMap>`.


tile getTileID
^^^^^^^^^^^^^^

.. data:: getTileID(number x,number y, number brickID,number layer)

   Retrieve the :ref:`tile ID <tileIdMap>` and :ref:`brick ID <brickID>` (if there is one set) given ``x``, ``y`` position in ``2d screen``, and ``layer``.

   :param number: **x** position in ``2d screen``.
   :param number: **y** position in ``2d screen``.
   :param number: **layer** (one based).
   :return: ``number`` - tile ID, ``number`` - brick ID.

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   function onTouchDown(key,x,y)
      local layer   = 1
      local tileID, brickID = tTile:getTileID(x,y,layer)
      print('tileID', tileID,'brickID', brickID)
   end
   

tile getTileSize
^^^^^^^^^^^^^^^^

.. data:: getTileSize()

   Retrieve the default tile size considering the scale.

   :return: ``number`` - width, ``number`` - height.

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   local width,height = tTile:getTileSize()
   print('default size of tile:',width,height)

tile getMapSize
^^^^^^^^^^^^^^^

.. data:: getMapSize()

   Retrieve the map size considering the scale.

   :return: ``number`` - width, ``number`` - height.

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   local width,height = tTile:getMapSize()
   print('Map size:',width,height)
   

tile getProperties
^^^^^^^^^^^^^^^^^^

.. data:: getProperties(table * filter)

   Retrieve a array of a table with properties according to the filter.

   :param table: **filter** ``{name,owner,type,ID}`` (might be omitted).
   :return: ``table`` - ``{{name,value,owner,type}, ...}``

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   local tProperties = tTile:getProperties({owner = 'layer', type = 'string'})
   for i=1, #tProperties do
      local tProperty = tProperties[i]
      print(tProperty.name,tProperty.value,tProperty.owner,tProperty.type)
   end

.. Note:: The filter is optional and when present, one or more fields might be used.

tile getObjects
^^^^^^^^^^^^^^^

.. data:: getObjects(table * filter)

   Retrieve a array of a table with objects according to the filter.

   :param table: **filter** ``{name,type,ID}`` (might be omitted).
   :return: ``table`` - ``{{name,type, ...}, ...}``

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   local tRectangles = tTile:getObjects({type = 'rectangle'})
   for i=1, #tRectangles do
      local tObject = tRectangles[i]
      print(tObject.type,tObject.name,tObject.x,tObject.y,tObject.width,tObject.height)
   end

   local tCircles = tTile:getObjects({type = 'circle'})
   for i=1, #tCircles do
      local tObject = tCircles[i]
      print(tObject.type,tObject.name,tObject.x,tObject.y,tObject.ray)
   end

   local tTriangles = tTile:getObjects({type = 'triangle'})
   for i=1, #tTriangles do
      local tObject = tTriangles[i]
      print(tObject.type,tObject.name,tObject.p1.x,tObject.p1.y, tObject.p2.x,tObject.p2.y, tObject.p3.x,tObject.p3.y)
   end

   local tPoints = tTile:getObjects({type = 'point'})
   for i=1, #tPoints do
      local tObject = tPoints[i]
      print(tObject.type,tObject.name,tObject.x,tObject.y)
   end

   local tPoints = tTile:getObjects({type = 'line'})
   for i=1, #tPoints do
      local tObject = tPoints[i]
      print(' ' .. tObject.type,tObject.name)
      for j=1, #tObject.points do
         local point = tObject.points[j]
         print(point.x,point.y)
      end
   end

.. Note:: 
   | The filter is optional and when present, one or more fields might be used.
   | The table has different fields according to type. See table bellow:
   | Object might have repeated names.

Expected object types:

   +--------------------+------------------------------------------------+
   | Type               |    Fields                                      |
   +====================+================================================+
   |  ``rectangle``     |     ``x``                                      |
   |                    +------------------------------------------------+
   |                    |     ``y``                                      |
   |                    +------------------------------------------------+
   |                    |   ``width``                                    |
   |                    +------------------------------------------------+
   |                    |  ``height``                                    |
   +--------------------+------------------------------------------------+
   |  ``circle``        |     ``x``                                      |
   |                    +------------------------------------------------+
   |                    |     ``y``                                      |
   |                    +------------------------------------------------+
   |                    |   ``ray``                                      |
   +--------------------+------------------------------------------------+
   |  ``triangle``      | ``p1 = {x,y}``                                 |
   |                    +------------------------------------------------+
   |                    | ``p2 = {x,y}``                                 |
   |                    +------------------------------------------------+
   |                    | ``p1 = {x,y}``                                 |
   +--------------------+------------------------------------------------+
   |  ``point``         |     ``x``                                      |
   |                    +------------------------------------------------+
   |                    |     ``y``                                      |
   +--------------------+------------------------------------------------+
   |  ``line``          | ``points = {[1] = {x,y}, [2] = {x,y}, ...}``   |
   +--------------------+------------------------------------------------+

tile getPhysicsBrick
^^^^^^^^^^^^^^^^^^^^

.. data:: getPhysicsBrick(number brickID)

   Retrieve a array of a table with physics from brick as defined in the editor.

   :param number: **brickID**
   :return: ``table`` - ``{{name,type, ...}, ...}``

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   local brickID = 47
   local tPhysics = tTile:getPhysicsBrick(brickID)
   for i=1, #tPhysics do
      local tObject = tPhysics[i]
      print(tObject.type,tObject.name,tObject.x,tObject.y)
   end


.. Note:: A physic table might be the same table as object table for ``rectangle``, ``circle`` and ``triangle``.


tile createTileObject
^^^^^^^^^^^^^^^^^^^^^

.. data:: createTileObject(number tileID, number layer)

   Create a independent  :ref:`Tile Object <tile_object>` from an existent tile ID given the layer.

   The :ref:`Tile Object <tile_object>` will no long be controlled by tile map, however, if the tile map is destroyed, it is also destroyed.

   This :ref:`Tile Object <tile_object>` is inherited from :ref:`renderizable <renderizable>`.

   :param number: **tileID**
   :param number: **layer**
   :return: ``table`` - ``renderizable``

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   local tileID     = 144
   local layer      = 2
   tSaw             = tTile:createTileObject(tileID,layer)
   
   function loop(delta)
      tSaw.az = tSaw.az + math.rad(50)
   end

.. figure:: _static/tile_map_7.gif
   :align: center

   Using tile object as normal renderizable after building it.


tile setLayerZ
^^^^^^^^^^^^^^

.. data:: setLayerZ(number layer,number z)

   Set ``z`` position of ``layer``.

   :param number: **layer** (one based).
   :param number: **z** position.

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')
   local layer, z = 1, 0.5
   tTile:setLayerZ(layer,z)

tile getLayerZ
^^^^^^^^^^^^^^

.. data:: getLayerZ(number layer)

   Get ``z`` position from ``layer``.

   :param number: **layer** (one based).
   :return: ``number`` - ``z`` position of layer

   *Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')
   local layer = 1
   local z     = tTile:getLayerZ(layer)   

.. _tile_object:

tile object
-----------

   :ref:`Tile Object <tile_object>` is an independent object with its physics and own control of render. It inherits from :ref:`renderizable <renderizable>`.

   Although it is independent, if the tile map is destroyed, it is also destroyed.

tile setBrickID
^^^^^^^^^^^^^^^

.. data:: setBrickID(number brickID)

   Set a new :ref:`Brick ID <brickID>` based on existent in tile map.

   :param number: **brick** id.

*Example:*

.. code-block:: lua

   tTile = tile:new('2dW')
   tTile:load('my_map.tile')

   local tileID     = 144
   local layer      = 2
   tSaw             = tTile:createTileObject(tileID,layer)

   local brickID    = 25
   tSaw:setBrickID(brickID)
   
   function loop(delta)
      tSaw.az = tSaw.az + math.rad(50)
   end

.. figure:: _static/tile_map_8.gif
   :align: center

   Changing the brick ID from Tile Object