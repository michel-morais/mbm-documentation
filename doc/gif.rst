.. contents:: Table of Contents

.. _gif:

Table gif
=========

A table :ref:`gif <gif>` inherit from :ref:`renderizable <renderizable>` and implement all methods as usual.
Also the animation has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.

gif methods
-----------

gif new
^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`gif <gif>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`gif <gif>` *table*.

   *Example:*
.. code-block:: lua

   tGif = gif:new('2dw')


gif load
^^^^^^^^

For the :ref:`gif <gif>` example we will use the following gif:

.. figure:: _static/Lion-King.gif
   :align: center
   :figclass: align-center

   Lion-King gif animated

:download:`download Lion-King.gif <_static/Lion-King.gif>`

.. data:: load(string file_name)

   Load a :ref:`gif <gif>` from file.


   :param string: **file name** from file.

   *Example:*
.. code-block:: lua

   mbm.setColor(0.3,0.3,0.7) --blue color of background

   tGif = gif:new('2dw')
   if tGif:load('Lion-King.gif') then
      print('Successfully loaded gif:','Lion-King.gif')
   else
      print('Failed to loaded gif:','Lion-King.gif')
   end


.. figure:: _static/gif-from-gif.gif
   :align: center
   :figclass: align-center

   Lion-King gif running

.. Note::

   | The :guilabel:`load` method will search in all known path.
   | You can add a path to search by the method :ref:`addPath <addPath>`.


gif setSize
^^^^^^^^^^^

.. data:: setSize(number width, number height)

   Resize the frame size from :ref:`gif <gif>`.

   :param number: **width**  from frame.
   :param number: **height** from frame.

   *Example:*
.. code-block:: lua

   mbm.setColor(1,1,1) --White color of background

   tGif = gif:new('2dw')
   if tGif:load('Lion-King.gif') then
      tGif:setSize(100,100)
   else
      print('Failed to loaded gif:','Lion-King.gif')
   end

.. figure::  _static/Lion-King.gif
   :align: center
   :figclass: align-center
   :width: 100px
   :height: 100px

   Lion-King gif resized to 100 x 100

.. Note::

   Always the image gif will adapt to the frame size.




