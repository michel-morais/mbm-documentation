.. contents:: Table of Contents

.. _render2texture:

Table render2texture
====================

A table :ref:`render2texture <render2texture>` create a dynamic texture, inherit from :ref:`renderizable <renderizable>` and normally have one animation.
Also the animation has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.

A dynamic texture can be interpreted as a personal view and can be applied to any component through :ref:`setTexture <setTexture>`.
Also can be rendered in a single frame using the method :ref:`enableFrame <enableFrame>` as ``true`` (default).

render2texture methods
----------------------

render2texture new
^^^^^^^^^^^^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`render2texture <render2texture>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`render2texture <render2texture>` *table*.

   *Example:*
.. code-block:: lua

   tRender = render2texture:new('2dw') --note that render2texture inherits from renderizable



render2texture create
^^^^^^^^^^^^^^^^^^^^^

.. data:: create(number * width, number * height, boolean * alpha, string * nickName)

   Create a dynamic texture properly.
    - Width is the texture's width and should not be major than native width.   if it is then the native width will be used.
    - Height is the texture's height and should not be major than native height.  if it is then the native height will be used.
    - Alpha indicate if the texture will have alpha channel.
    - NickName is the texture name. It generate automatically if not supplied.


   :param number: **width** texture (optional).
   :param number: **height** texture (optional).
   :param boolean: **alpha** channel (optional).
   :param string: **nick name** from texture generated.
   :return: ``boolean`` - *result*, ``string`` - texture name, ``number`` - texture id

   *Example:*
.. code-block:: lua

   tRender = render2texture:new('2dw') --note that render2texture inherits from renderizable
   local ret, nickname, id = tRender:create()
   if ret then
      print('Dynamic texture created successfully')
      tRender:enableFrame(true) -- we are enabling render to frame to show our texture
      tex = texture:new('2dw')
      tex:load('#0000ff') --Blue texture
      tex:setSize(500,500) -- resize the frame
      tRender:add(tex)
   else
      print('Failed to create a dynamic texture')
   end

.. figure:: _static/render_2_texture_example_1.png
    :align: center
    :figclass: align-center

    Texture object being renderized in the render2texture object

Following other example doing the opposite way.
Setting the background color and applying it to the texture object:


.. code-block:: lua

   tRender = render2texture:new('2dw') --note that render2texture inherits from renderizable
   local ret, nickname, id = tRender:create()
   if ret then
      print('Dynamic texture created successfully')
      tRender:enableFrame(false) -- we are disabling render to frame
      tex = texture:new('2dw')
      tex:load('#0000ff') --Blue texture
      tex:setSize(500,500) -- resize the frame
      tRender:setColor(1,0,0) -- set the background color of render2texture to red
      tex:setTexture(nickname) -- will render the dynamic texture instead of blue because we just set

      --however anytime that we change background color of render2texture  will be applied to the texture
      --we will do it in the timer following
      tTimer = timer:new( 
         function (self)
               local r = math.random(0,1)
               local g = math.random(0,1)
               local b = math.random(0,1)

               tRender:setColor(r,g,b)
         end,
      0.2
      )
   else
      print('Failed to create a dynamic texture')
   end

.. figure:: _static/render_2_texture_example_2.gif
    :align: center
    :figclass: align-center

    render2texture being used as dynamic texture.

.. _enableFrame:

render2texture enableFrame
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: enableFrame(boolean enable)

   Enable the :ref:`render2texture <render2texture>` to render in a frame. The frame will have the same size as the texture. The frame is initially enabled.


   :param boolean: **enable** render the frame.


   *Example:*
.. code-block:: lua

   myMesh = mesh:new('2dw')-- create a object as 2d
   if myMesh:load('crate.msh') then
      tRender = render2texture:new('2dw')
      local ret, nickname, id = tRender:create(300,250) --similar to ratio current
      if ret then
         print('Dynamic texture created successfully')
         tRender:enableFrame(true) -- enable render to frame to show our object inside the frame
         tRender:setColor(0,1,0) --green color
         tRender:add(myMesh) -- add the object to our dynamic texture

      else
         print('Failed to create a dynamic texture')
      end
    else
        print('Failed to load crate.msh')
    end

.. figure:: _static/render_2_texture_example_3.png
    :align: center
    :figclass: align-center

    adding object to render2texture


.. _getCameraRender2Texture:

.. TODO: make sure the camera method are equal

render2texture getCamera
^^^^^^^^^^^^^^^^^^^^^^^^

.. data:: getCamera(string * world)

   Get a camera which gives you the control to the scene in the dynamic texture. See :ref:`camera <camera>` for more information.

   :param string: **world** can be ``'2d'`` or ``'3d'``. default is ``'2d'``.
   :return: :ref:`camera <camera>` *table* reference.

   *Example:*
.. code-block:: lua

   myMesh = mesh:new('3d')-- create a object as 3d
   if myMesh:load('crate.msh') then
      tRender = render2texture:new('2dw')
      local ret, nickname, id = tRender:create(300,250) --similar to ration current
      if ret then
         print('Dynamic texture created successfully')
         tRender:enableFrame(true) -- enable render to frame to show our any object inside the frame
         tRender:setColor(0,1,0) --green color
         tRender:add(myMesh) -- add the object to our dynamic texture
         camera3d = tRender:getCamera('3d')
         camera3d:setPos(0,0,-500)

         --using a timer to do a random movement using the camera
         tTimer = timer:new( 
               function (self)
                  local p = camera3d:getPos()
                  p.x = p.x + 10
                  p.y = p.y + 20
                  p.z = p.z - 1.5

                  if p.x > 300 then
                     p.x = -300
                  end
                  if p.y > 600 then
                     p.y = -600
                  end
                  if p.z > 500 then
                     p.z = -500
                  end
               end,
         0.02
         )
      else
         print('Failed to create a dynamic texture')
      end

   else
      print('Failed to load crate.msh')
   end

.. figure:: _static/render_2_texture_example_4.gif
    :align: center
    :figclass: align-center

    Accessing the camera from render_2_texture object


.. important::
   The camera method is supposed to be a normal camera but not all methods are implemented. TODO :)


render2texture add
^^^^^^^^^^^^^^^^^^

.. data:: add(renderizable mesh)

   Add a :ref:`renderizable <renderizable>` to render in the dynamic texture.

   :param renderizable: **mesh** to render in the frame.


   *Example:*
.. code-block:: lua

   myMesh = mesh:new('2dw')-- create a object as 2d
   if myMesh:load('crate.msh') then
      tRender = render2texture:new('2dw')
      local ret, nickname, id = tRender:create(300,250) --similar to ratio current
      if ret then
         print('Dynamic texture created successfully')
         tRender:enableFrame(true) -- enable render to frame to show our object inside the frame
         tRender:setColor(0,1,0) --green color
         tRender:add(myMesh) -- add the object to our dynamic texture
      else
         print('Failed to create a dynamic texture')
      end
    else
        print('Failed to load crate.msh')
    end

render2texture remove
^^^^^^^^^^^^^^^^^^^^^

.. data:: remove(renderizable mesh)

   Remove a :ref:`renderizable <renderizable>` from render 2 texture list.

   :param renderizable: **mesh** to remove from list.
   :return: ``boolean`` *found* - it was found to be removed?.


render2texture clear
^^^^^^^^^^^^^^^^^^^^

.. data:: clear()

   Remove all :ref:`renderizable <renderizable>` from render 2 texture list.

render2texture release
^^^^^^^^^^^^^^^^^^^^^^

.. data:: release()

   Remove all :ref:`renderizable <renderizable>` from render 2 texture list and destroy the dynamic texture created. The method ``create`` might be used again.


render2texture setColor
^^^^^^^^^^^^^^^^^^^^^^^

   Change the background color of dynamic texture.

.. data:: setColor(number r, number g, number b, number a)

   :param number: **r** red color, range 0  to 1.
   :param number: **g** green color, range 0  to 1.
   :param number: **b** blue color, range 0  to 1.
   :param number: **a** alpha color, range 0  to 1.


render2texture save
^^^^^^^^^^^^^^^^^^^

.. data:: save(string file_name, number * x, number * y, number * w, number * h)

   | Save the texture as ``png`` file.
   | If not supplied ``x`` and ``y`` will be zero.
   | If not supplied ``w`` and ``h`` will be the size of the back buffer.

   :param string: **file name** of texture to be saved.
   :param number: **x** origin to rect the texture.
   :param number: **y** origin to rect the texture.
   :param number: **width** of texture to be saved.
   :param number: **height** of texture to be saved.
   :return: ``boolean`` *result* of operation.


   *Example:*
.. code-block:: lua

   myMesh = mesh:new('2dw')-- create a object as 2d
   if myMesh:load('crate.msh') then
      tRender = render2texture:new('2dw')
      local ret, nickname, id = tRender:create(300,250) --similar to ratio current
      if ret then
         print('Dynamic texture created successfully')
         tRender:enableFrame(true) -- enable render to frame to show our object inside the frame
         tRender:setColor(0,1,0) --green color
         tRender:add(myMesh) -- add the object to our dynamic texture
     else
         print('Failed to create a dynamic texture')
     end
   else
      print('Failed to load crate.msh')
   end

   times_render = 0

   function loop(delta)
      if times_render == 1 then --we wait for the first render otherwise the texture will be empty
         if tRender:save('my_dynamic_texture.png') then
            print('Texture saved successfully ...')
         else
            print('Failed to save texture')
         end
      end
      times_render = times_render + 1
   end

.. important::
   | If the texture was never render, then when you save it will be just empty texture.
   | That is why we wait for the first cycle of render to save the texture.

