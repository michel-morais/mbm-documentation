.. contents:: Table of Contents

.. _line:

Table line
==========

A table :ref:`line <line>` inherit from :ref:`renderizable <renderizable>` and implement all methods as usual.
Also :ref:`line <line>` has a default animation with a particular :ref:`shader <shader>` table which can be accessed by :ref:`getShader <getShader>` method as usual.

This table allow to create lines dynamically. Each table :ref:`line <line>` might have one or more lines.

line methods
------------

line new
^^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`line <line>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`line <line>` *table*.

   *Example:*
.. code-block:: lua

   tLine = line:new('2dw')


line add
^^^^^^^^

.. data:: add(table[x,y,*z])

   Add new line. Optionally the table might have ``z`` coordinate. Each ``x,y,*z`` is a point in the line.

   :param table: **points** to line.
   :return: :literal:`number` - *index* from current line (1 based).

   *Example:*

.. code-block:: lua

    tLine = line:new('2dw')
    local tPoints = {0,0,  100,100} --minimum two points to be a line
    local index = tLine:add(tPoints)
    print('Line added index:',index)

.. figure:: _static/line_simple.png
    :align: center
    :figclass: align-center

    Draw a simple line at 2d world.

.. figure:: _static/line_simple_output.png
    :align: center
    :figclass: align-center

    Output.

*Note the difference to 2D screen. more* :ref:`detail <explaining_2ds_2dw_3d>`

.. code-block:: lua

    tLine = line:new('2ds')-- 2d screen
    local tPoints = {0,0,  100,100} --minimum two points to be a line
    local index = tLine:add(tPoints)

.. figure:: _static/line_simple_2ds.png
    :align: center
    :figclass: align-center



line set
^^^^^^^^

.. data:: set(table[x,y,*z], number index)

   Update the line passing the table and index (1 based).

   :param table: **points** to line.
   :param number: **index** to line.

   *Example:*
.. code-block:: lua

   tLine = line:new('2ds')
   local t = {0,0,  0,500, 500,500, 500, 0, 0,0 }
   local index = tLine:add(t)

   local tNewLine = {0,0,  0,250, 250,250, 250, 0, 0,0 }
   tLine:set(tNewLine,index)

.. _lineSetColor:

line setColor
^^^^^^^^^^^^^

.. data:: setColor(number r, number g, number b, number * a)

   Update the color from line passing the color RGB and optionally alpha.

   :param number: **r** red   color (0 to 1).
   :param number: **g** green color (0 to 1).
   :param number: **b** blue  color (0 to 1).
   :param number: **a** alpha color (0 to 1) (optional).

   *Example:*
.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   tLine = line:new('2dw')
   local tXY = {-50,-50,  -50,50,  50,50,  50,-50,  -50,-50 }
   local index = tLine:add(tXY)
   tLine:setColor(1,1,1) --white color

.. figure:: _static/line_set_color.png
    :align: center
    :figclass: align-center

    Set line color to white

.. Note::

   | This method is directly dependent from current (default) :ref:`shader <shader>` used on line.
   | This method look for a variable named ``color``.
   | The :ref:`shader <shader>` for line is available :ref:`here <lineShader>`.

line size
^^^^^^^^^

.. data:: size()

   Retrieve the size of lines added.

   :return: :literal:`number` - *size* of lines added.

   *Example:*
.. code-block:: lua

   tLine = line:new('2ds')
   local t = {0,0,  0,500, 500,500, 500, 0, 0,0 }
   local index = tLine:add(t)
   local size = tLine:size() -- 1


line drawBounding
^^^^^^^^^^^^^^^^^

.. data:: drawBounding(table renderizable,boolean * useAABB)

   Draw a bounding from a :ref:`renderizable <renderizable>`. The flag useAABB consider the rotation from a object.

   :param renderizable: **render** to set values from physics.
   :param boolean: **useAABB** is a flag enable AABB (default ``true``).

   *Example:*
.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   tTexture = texture:new('2dw') -- new texture
   tTexture:load('#ffffffff') -- load white color
   tTexture:setSize(150,50)

   tLine = line:new('2dw')
   tLine:drawBounding(tTexture)

.. figure:: _static/line_drawBounding.png
    :align: center
    :figclass: align-center

    Draw a simple bounding line.  Get the bound from texture.


*Disabled AABB:*

.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   tTexture = texture:new('2dw') -- new texture
   tTexture:load('#ffffffff') -- load white color
   tTexture:setSize(150,50)

   tLine = line:new('2dw')
   tLine:drawBounding(tTexture)

.. figure:: _static/line_drawBounding_without_AABB.png
    :align: center
    :figclass: align-center

    Draw a simple bounding line. Get the size from texture **not** considering AABB.

*Enabled AABB:*

.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   tTexture = texture:new('2dw') -- new texture
   tTexture:load('#ffffffff') -- load white color
   tTexture:setSize(150,50)

   tLine = line:new('2dw')
   tLine:drawBounding(tTexture,true)

.. figure:: _static/line_drawBounding_with_AABB.png
    :align: center
    :figclass: align-center

    Draw a simple bounding line. Get the size from texture considering AABB.


line setPhysics
^^^^^^^^^^^^^^^

.. data:: setPhysics(table renderizable,boolean * bDrawLines)

   Set physic from a :ref:`renderizable <renderizable>` (copy the values to itself). The flag ``bDrawLines`` instruct to draw the physics line. It is used in general for debugging proposal.

   :param renderizable: **render** to set values from physics.
   :param boolean: **bDrawLines** instruct to draw the line.

   *Example:*

.. code-block:: lua

   tShape = shape:new('2dw')
   tShape:create('rectangle',100,50)

   local bDrawLines = true
   tLine = line:new('2dw')
   tLine:setPhysics(tShape,bDrawLines)

.. figure:: _static/line_setPhysics_example_1.png
    :align: center
    :figclass: align-center

    Setting the physics and drawing it.


line setPhysics
^^^^^^^^^^^^^^^

.. data:: setPhysics(table tPhysics,boolean * bDrawLines)

   | Apply physics from a table. The flag ``bDrawLines`` instruct to draw the physics line.
   | It is very useful in general for debugging proposal.

   :param table: **info** to set values to physics.
   :param boolean: **bDrawLines** instruct to draw the line (default is ``false``).


   The following table describe the table available for the primitives 2d types:

   +---------------+-----------------+-----------------+-----------------+-----------------+
   | | type /      | | position x,y  | | width, height | | points        | | ray           |
   | | name        | | information ? | | information ? | | information ? | | information ? |
   +===============+=================+=================+=================+=================+
   |  ``rect``     |    ``yes``      |   ``yes``       |   no            |   no            |
   +---------------+-----------------+-----------------+-----------------+-----------------+
   | ``circle``    |    ``yes``      |   ``yes``       |   no            |   ``yes``       |
   +---------------+-----------------+-----------------+-----------------+-----------------+
   | ``ellipse``   |    ``yes``      |   ``yes``       |   no            |   ``yes``       |
   +---------------+-----------------+-----------------+-----------------+-----------------+
   |  ``triangle`` |    ``yes``      |   no            |   ``yes``       |   no            |
   +---------------+-----------------+-----------------+-----------------+-----------------+
   |  ``polygon``  |    ``yes``      |   no            |   ``yes``       |   no            |
   +---------------+-----------------+-----------------+-----------------+-----------------+


   *Example:*

   .. literalinclude:: example_modules/line_setPhysics_example_1.lua
     :language: lua
     :linenos:


   :download:`download <example_modules/line_setPhysics_example_1.lua>`.


   .. figure:: _static/line_setPhysics_example_1.gif
    :align: center
    :figclass: align-center

    Example creating physics from lua table.

.. _lineShader:

line shader
-----------

    The table :ref:`line <line>` has its own shader and can be accessed by the shortcut :ref:`setColor <lineSetColor>`.

*The equivalent using table shader is:*

.. code-block:: lua

    mbm.setColor(0,0,0)

    --common
    local r,g,b,a = 1,1,1,1
    local tXY = {-50,-50,  -50,50,  50,50,  50,-50,  -50,-50 }

    --using setColor to modify the line color 
    tLine_1 = line:new('2dw')
    local index = tLine_1:add(tXY)
    tLine_1:setColor(r,g,b,a) --set the variable color in the shader to white

    --using shader to modify the line color 
    tLine_2 = line:new('2dw',200)--200 to right (x)
    index = tLine_2:add(tXY)
    local tShader = tLine_2:getShader()
    tShader:setPSall('color',r,g,b,a)--set the variable color in the shader to white


.. figure:: _static/line_set_color_and_shader.png
    :align: center
    :figclass: align-center

    changing color using setcolor and shader directly.


line fragment shader
^^^^^^^^^^^^^^^^^^^^

    The fragment shader (pixel shader) used for the line is:

.. code-block:: glsl

    precision mediump float;
    uniform vec4 color;
    void main()
    {
        gl_FragColor =  color;
    }


line vertex shader
^^^^^^^^^^^^^^^^^^
    The vertex shader used for the line is:

.. code-block:: glsl

    attribute vec4 aPosition;
    uniform mat4 mvpMatrix;
    void main()
    {
        gl_Position = mvpMatrix * aPosition;
    }