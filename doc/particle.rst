.. contents:: Table of Contents

.. _particle:

Table particle
==============

A table :ref:`particle <particle>` inherit from :ref:`renderizable <renderizable>` and implement all methods as usual.

A particle has a '*stage*' which represent the animation. Each stage might have its own behavior.
Also each stage has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.

Particles have a peculiarity state of ``being born``, ``living`` and ``dying`` and then resurrecting (or not).
This is well known in all generator of particles and on this engine could not be different.
There are several combinations that provide a set of options for reach this.


particle methods
----------------

particle new
^^^^^^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`particle <particle>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`particle <particle>` *table*.

   *Example:*
.. code-block:: lua

   tParticle = particle:new('2dw') --note that particle inherits from renderizable



particle load
^^^^^^^^^^^^^

.. data:: load(string file_name,number * total_particle, string * shader_operator, string * aditional_code_shader)

   Load a :ref:`particle <particle>` from file (expected ``.plt`` extension) any of :ref:`images supported types <images_supported_types>`.


   :param string: **file name** can be a texture directly or an binary file ``.ptl``
   :param number: **total_particle** initial (optional).
   :param string: **shader operator** can be ``+``, ``-``, ``/``, ``*``,  (optional).
   :param string: **additional code** initial (optional).
   :return: ``boolean`` - result.

   *Example:*

.. code-block:: lua

   tParticle = particle:new('2d')
   if tParticle:load('particle.ptl',50,'*') then
      print('Successfully loaded particle:','particle.ptl')
   else
      print('Failed to load particle:','particle.ptl')
   end

.. Note::

   | The :guilabel:`load` method will search in all known path.
   | You can add a path to search by the method :ref:`addPath <addPath>`.


particle add
^^^^^^^^^^^^

.. data:: add(number total_particle,boolean * force_now)

   Add more particle to a :ref:`particle <particle>` system.

   :param number: **total_particle** to be added.
   :param boolean: **force now** means that all particle will appear instantaneously (optional).
   :return: ``boolean`` - result.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 4

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:add(1000) --add 1000 particles
   else
      print('Failed to load particle:','particle.ptl')
   end


particle Offset
^^^^^^^^^^^^^^^

.. data:: setMinOffset(number stage,number * x, number * y, number * z)

   Change the min offset from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **x** value (optional).
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   tParticle = particle:new('2dw')
   local stage = 1 --we only have one stage for this particle
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMinOffset(stage,-55,-150,0)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: setMinOffset(number stage,vec3 minOffset)

   Change the min offset from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **minOffset** value as :ref:`vec3 <vecthree>`.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 6

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local minOffset = vec3:new(-45,-100,0)
      tParticle:setMinOffset(stage,minOffset)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: getMinOffset(number stage)

   Get the min offset from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number``, ``number``, ``number`` - ``x``, ``y`` and ``z`` min offset.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local x,y,z = tParticle:getMinOffset(stage)
      print('Current min offset:',x,y,z)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: setMaxOffset(number stage,number * x, number * y, number * z)

   Change the max offset from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **x** value (optional).
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMaxOffset(stage,-55,-150,0)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: setMaxOffset(number stage,vec3 maxOffset)

   Change the max offset from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **maxOffset** value as :ref:`vec3 <vecthree>`.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 6

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local maxOffset = vec3:new(-35,-120,0)
      tParticle:setMaxOffset(stage,maxOffset)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: getMaxOffset(number stage)

   Get the max offset from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number``, ``number``, ``number`` - ``x``, ``y`` and ``z`` min offset.


   *Example:*

.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local x,y,z = tParticle:getMaxOffset(stage)
      print('Current max offset:',x,y,z)
   else
      print('Failed to load particle:','particle.ptl')
   end

particle Direction
^^^^^^^^^^^^^^^^^^

.. data:: setMinDirection(number stage,number * x, number * y, number * z)

   Change the min direction from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **x** value (optional).
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMinDirection(stage,-55,-150,0)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: setMinDirection(number stage,vec3 minDirection)

   Change the min direction from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **minDirection** value as :ref:`vec3 <vecthree>`.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 6

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local minDirection = vec3:new(-54,-154,0)
      tParticle:setMinDirection(stage,minDirection)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMinDirection(number stage)

   Get the min direction from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number``, ``number``, ``number`` - ``x``, ``y`` and ``z`` min direction.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local x,y,z = tParticle:getMinDirection(stage)
      print('Current min direction:',x,y,z)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: setMaxDirection(number stage,number * x, number * y, number * z)

   Change the max direction from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **x** value (optional).
   :param number: **y** value (optional).
   :param number: **z** value (optional).

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMaxDirection(stage,-55,-150,0)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: setMaxDirection(number stage,vec3 maxDirection)

   Change the max direction from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **vec3** value as :ref:`vec3 <vecthree>`.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 6

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local maxDirection = vec3:new(-55,-150,0)
      tParticle:setMaxDirection(stage,maxDirection)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMaxDirection(number stage)

   Get the max direction from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number``, ``number``, ``number`` - ``x``, ``y`` and ``z`` min direction.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local x,y,z = tParticle:getMaxDirection(stage)
      print('Current max direction:',x,y,z)
   else
      print('Failed to load particle:','particle.ptl')
   end

particle Speed
^^^^^^^^^^^^^^

.. data:: setMinSpeed(number stage,number min_speed)

   Change the min speed from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **min_speed** value.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMinSpeed(stage,500)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMinSpeed(number stage)

   Get the min speed from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number`` - *min* speed.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local min_speed = tParticle:getMinSpeed(stage)
      print('Current min speed:',min_speed)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: setMaxSpeed(number stage,number max_speed)

   Change the max speed from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **max_speed** value.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMaxSpeed(stage,150)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: getMaxSpeed(number stage)

   Get the max speed from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number`` - *max* speed.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local max_speed = tParticle:getMaxSpeed(stage)
      print('Current max speed:',max_speed)
   else
      print('Failed to load particle:','particle.ptl')
   end

particle Color
^^^^^^^^^^^^^^

.. data:: setMinColor(number stage,number * r, number * g, number * b)

   Change the min color from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **r** value 
   :param number: **g** value
   :param number: **b** value

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMinColor(stage,0,0.5,0)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: setMinColor(number stage,vec3 minColor)

   Change the min color from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **minColor** value as :ref:`vec3 <vecthree>`.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 6

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local minColor = vec3:new(1,1,0.8)
      tParticle:setMinColor(stage,minColor)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMinColor(number stage)

   Get the min color from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number``, ``number``, ``number`` - ``r``, ``g`` and ``b`` min color.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local r,g,b = tParticle:getMinColor(stage)
      print('Current min color:',r,g,b)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: setMaxColor(number stage,number * r, number * g, number * b)

   Change the max color from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **r** value
   :param number: **g** value
   :param number: **b** value

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMaxColor(stage,1,1,1)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: setMaxColor(number stage,vec3 maxColor)

   Change the max color from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **maxColor** value as :ref:`vec3 <vecthree>`.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 6

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local maxColor = vec3:new(1,1,1)
      tParticle:setMaxColor(stage,maxColor)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMaxColor(number stage)

   Get the max color from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number``, ``number``, ``number`` - ``r``, ``g`` and ``b`` min color.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local r,g,b = tParticle:getMaxColor(stage)
      print('Current max color:',r,g,b)
   else
      print('Failed to load particle:','particle.ptl')
   end


particle LifeTime
^^^^^^^^^^^^^^^^^

.. data:: setMinLifeTime(number stage,number * time)

   Change the min time from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **min** time of life in seconds for each new particle.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMinLifeTime(stage,6.5)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMinLifeTime(number stage)

   Get the min time from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number`` - ``min`` time of life in seconds.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local minTime = tParticle:getMinLifeTime(stage)
      print('Current min time:',minTime)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: setMaxLifeTime(number stage,number * time)

   Change the max time from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **max** time of life in seconds.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMaxLifeTime(stage,5.5)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMaxLifeTime(number stage)

   Get the max life time from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number``, **max** time of life in seconds.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local maxLifeTime = tParticle:getMaxLifeTime(stage)
      print('Current max time:',maxLifeTime)
   else
      print('Failed to load particle:','particle.ptl')
   end

particle Size
^^^^^^^^^^^^^

.. data:: setMinSize(number stage,number * size)

   Change the min size from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **min** size of the particle.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMinSize(stage,1)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMinSize(number stage)

   Get the min size from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number`` - ``min`` size of the particle.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local minSize = tParticle:getMinSize(stage)
      print('Current min size:',minSize)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: setMaxSize(number stage,number size)

   Change the max size from a :ref:`particle <particle>` system.

   :param number: **stage** to be modified.
   :param number: **max** size of the particle.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setMaxSize(stage,15.5)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getMaxSize(number stage)

   Get the max life size from a :ref:`particle <particle>` system.

   :param number: **stage** to retrieve.
   :return: ``number``, **max** size of the particle.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local maxSize = tParticle:getMaxSize(stage)
      print('Current max size:',maxSize)
   else
      print('Failed to load particle:','particle.ptl')
   end

particle Stage
^^^^^^^^^^^^^^

.. data:: getStage()

   Get the current stage from :ref:`particle <particle>` system.

   :return: ``number`` - ``stage`` current.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 4

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local stage = tParticle:getStage()
      print('Current stage:',stage)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: getTotalStage()

   Get the total stage from :ref:`particle <particle>` system.

   :return: ``number`` - ``total`` stage.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 4

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local iTotalStage = tParticle:getTotalStage()
      print('Total stage:',iTotalStage)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: setStage(number stage)

   Set the current stage to a :ref:`particle <particle>` system.

   :param number: **stage** to be set.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setStage(stage)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: addStage()

   Add a stage for a :ref:`particle <particle>` system.

   :return: ``number`` - ``index`` stage added (1 based).


   *Example:*
.. code-block:: lua
   :emphasize-lines: 4

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local stage = tParticle:addStage()
      print('Stage added at:',stage)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: setStageTime(number stage,number time)

   Set time for the stage in a :ref:`particle <particle>` system.

   :param number: **stage** to be set.
   :param number: **time** in seconds to change the stage.


   *Example:*

.. code-block:: lua
   :emphasize-lines: 4

   local stage = 1
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      tParticle:setStageTime(stage,2) --2 seconds to change to next stage
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: getStageTime(number stage,number time)

   Set time for the stage in a :ref:`particle <particle>` system.

   :param number: **stage** to be set.
   :return: ``number`` - ``time`` from stage.


   *Example:*

.. code-block:: lua
   :emphasize-lines: 4

   local stage = 1
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      local iTime = tParticle:getStageTime(stage)
      print('Time in the stage: ' .. iTime)
   else
      print('Failed to load particle:','particle.ptl')
   end


particle Arise
^^^^^^^^^^^^^^

   | Arise time is the time to show up since the particle system starts.
   | For example, if is added 50 particle and the time is one second, then all 50 particle should be appear in the max one second.

.. data:: setAriseTime(number stage, number time)

   Set the current arise time to a :ref:`particle <particle>` system.

   :param number: **stage** to be set.
   :param number: **arise** time to be set.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      tParticle:setAriseTime(stage,0.88)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getAriseTime(number stage)

   Get the current arise time from :ref:`particle <particle>` system.

   :param number: **stage** to be get.
   :return: ``number`` - ``arise time`` from particle.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local ariseTime = tParticle:getAriseTime(stage)
      print('Current arise time:',ariseTime)
   else
      print('Failed to load particle:','particle.ptl')
   end

particle Invert color
^^^^^^^^^^^^^^^^^^^^^

.. data:: setInvertedColor(number stage, boolean r, boolean g, boolean b, boolean a)

   Set individual inverted color to the current stage on a :ref:`particle <particle>` system.

   :param number: **stage** to be set.
   :param number: **red** color to be set.
   :param number: **green** color to be set.
   :param number: **blue** color to be set.
   :param number: **alpha** color to be set.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 4

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      tParticle:setInvertedColor(stage,true,false,true,false)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getInvertedColor(number stage)

   Get the current arise time from :ref:`particle <particle>` system.

   :param number: **stage** to be get.
   :return: ``boolean``, ``boolean``, ``boolean``, ``boolean`` - ``r, g, b, a`` indicating if is inverted.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   local stage = 1 --we only have one stage for this particle
   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local ir,ig,ib,ia = tParticle:getInvertedColor(stage)
      print(string .format('Inverted color R:%s, G:%s, B:%s, A:%s:',ir,ig,ib,ia))
   else
      print('Failed to load particle:','particle.ptl')
   end

particle Total
^^^^^^^^^^^^^^

.. data:: getTotalAlive()

   Retrieve the total of particles alive.

   :return: ``number`` - ``total`` particle alive from system.

   *Example:*
.. code-block:: lua
   :emphasize-lines: 4

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local total = tParticle:getTotalAlive()
      print('Total particles alive in the system:',total)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getTotalParticle()

   Get the total particle capacity from :ref:`particle <particle>` system.

   :return: ``number`` - ``total`` particle (dead + alive from system).


   *Example:*
.. code-block:: lua
   :emphasize-lines: 4

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local total = tParticle:getTotalParticle()
      print('Total particles in the system:',total)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: getTotalParticle(number stage)

   Get the total particle available per stage from :ref:`particle <particle>` system.

   :param number: **stage** to be get.
   :return: ``number`` - ``total`` particle available to the stage indicated.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local stage = 1
      local total = tParticle:getTotalParticle(stage)
      print('Total particles in from stage selected:',total)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: setTotalParticle(number stage,number total)

   Set the total particle available per stage to :ref:`particle <particle>` system.

   :param number: **stage** to be set.
   :param number: **total** particle to the selected stage.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 5

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local stage = 1
      tParticle:setTotalParticle(stage,100)
   else
      print('Failed to load particle:','particle.ptl')
   end   

particle getTexture
^^^^^^^^^^^^^^^^^^^

.. data:: getTexture()

   Get the current texture from :ref:`particle <particle>` system.

   :return: ``string`` - ``file name`` from texture.


   *Example:*
.. code-block:: lua
   :emphasize-lines: 4

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      print('Successfully loaded particle:','particle.ptl')
      local texture_file_name = tParticle:getTexture()
      print('Current texture used at particle:',texture_file_name)
   else
      print('Failed to loaded particle:','particle.ptl')
   end

particle attributes
-------------------

.. data:: alpha [read / write]

   Enable or disable the alpha flag for a :ref:`particle <particle>` from current stage.

   *Example:*
.. code-block:: lua

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      tParticle.alpha = false
      print('alpha:',tParticle.alpha)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: revive [read / write]

   Enable or disable the revive flag for a :ref:`particle <particle>` from current stage.

   *Example:*
.. code-block:: lua

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      tParticle.revive = false
      print('revive:',tParticle.revive)
   else
      print('Failed to load particle:','particle.ptl')
   end

.. data:: grow [read / write]

   Enable or disable the grow flag for a :ref:`particle <particle>` from current stage.

   *Example:*
.. code-block:: lua

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      tParticle.grow = false
      print('grow:',tParticle.grow)
   else
      print('Failed to load particle:','particle.ptl')
   end


.. data:: segmented [read / write]

   Enable or disable the segmented flag for a :ref:`particle <particle>` from current stage.

   *Example:*
.. code-block:: lua

   tParticle = particle:new('2dw')
   if tParticle:load('particle.ptl',70,'*') then
      tParticle.segmented = false
      print('segmented:',tParticle.segmented)
   else
      print('Failed to load particle:','particle.ptl')
   end



Creating particle programmatically
----------------------------------

   Follow some examples how to create particles programmatically.

   For the first example, we will use a solid texture in red color.


.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   tParticle = particle:new('2dw')
   tParticle:load('#FFFF0000') --red solid texture (alpha 1 red 1, green 0 blue)
   tParticle:add(50)

.. _sample_particle_solid_texture_red:

.. figure:: _static/sample_particle_solid_texture_red.gif
    :align: center
    :figclass: align-center

    Simple particle with solid color red as texture.


Now, we will use the follow texture:

   .. figure:: _static/particle_flame.png
    :align: center
    :width: 200px
    :height: 200px
    :figclass: align-center

:download:`download particle_flame.png <_static/particle_flame.png>`

    *Particle using texture:*

.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   tParticle = particle:new('2dw')
   tParticle:load('particle_flame.png')
   tParticle:add(50)


.. figure:: _static/particle_using_texture_1.gif
    :align: center
    :figclass: align-center

    Simple particle using texture.


*Particle using texture and changing the direction and speed:*

.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   local stage = 1 --we only have one stage for this particle

   tParticle = particle:new('2dw')
   tParticle:load('particle_flame.png')

   tParticle:setMinDirection(stage,-0.2,1,0)
   tParticle:setMaxDirection(stage,0.2,1,0)

   tParticle:setMinSpeed(stage,10)
   tParticle:setMaxSpeed(stage,1000)

   tParticle:add(1000)


.. figure:: _static/particle_using_texture_2.gif
    :align: center
    :figclass: align-center

    Changing the direction, speed and amount of particle.


*Particle with 2 stages:*

.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   local stage_1 = 1

   --Set up for the first stage
   tParticle:add(500) --500 particle for the first stage

   tParticle:setMinSpeed(stage_1,10)
   tParticle:setMaxSpeed(stage_1,1000)

   tParticle:setMinLifeTime(stage_1,1)--at least one second for the life of particle

   tParticle:setStageTime(stage_1,1)--one second of duration for the first stage

   --Set up for the second stage
   local stage_2 = tParticle:addStage()

   tParticle:setMinDirection(stage_2,-0.2,1,0)
   tParticle:setMaxDirection(stage_2,0.2,1,0)

   tParticle:setMinSpeed(stage_2,10)
   tParticle:setMaxSpeed(stage_2,1000)

   tParticle:add(1000)--500 particle for the second stage

   tParticle:restartAnim()--restart the animation since we have modified it


.. figure:: _static/particle_using_texture_3.gif
    :align: center
    :figclass: align-center

    Particle with two stages

.. _Particleoperator:

particle operator
^^^^^^^^^^^^^^^^^

*Particle with different operator in the shader:*

.. code-block:: lua

   mbm.setColor(0,0,0) --set background color to black

   local stage = 1 --we only have one stage for this particle
   local total_particle = 300
   local operator = { '+', '-', '/', '*'}
   local x = {-300, -150, 0, 150}

   tParticleArray = {} -- just to store 4 particle table

   for i =1, #operator do
      local tParticle = particle:new('2dw',x[i])

      print('operator:',operator[i] )

      tParticle:load('particle_flame.png',0,operator[i])

      tParticle:setMinDirection(stage,-0.2,1,0)
      tParticle:setMaxDirection(stage,0.2,1,0)

      tParticle:setMinSpeed(stage,10)
      tParticle:setMaxSpeed(stage,1000)

      tParticle:add(total_particle)

      table.insert(tParticleArray,tParticle)
   end


.. figure:: _static/particle_using_different_operator.gif
    :align: center
    :figclass: align-center

    Four operator, ``+``, ``-``, ``/``, ``*``


.. _particleShader:

particle fragment shader
^^^^^^^^^^^^^^^^^^^^^^^^
    | The fragment shader (pixel shader) for particle is created in the moment of load.
    | See how the options affect the shader:

.. code-block:: glsl

    precision mediump float;
    uniform vec4 color;
    uniform float enableAlphaFromColor;
    varying vec2 vTexCoord;
    uniform sampler2D sample0;

    void main()
    {
      vec4 texColor;
      vec4 outColor;
      texColor = texture2D( sample0, vTexCoord );
      if(enableAlphaFromColor > 0.5)
         outColor.a = color.a;
      else
         outColor.a = texColor.a;

      //The operator is used in the next line. the default if '*'
      outColor.rgb = color.rgb * texColor.rgb;

      //
      //this line is the aditional code if supplied.
      //

      gl_FragColor = outColor;
    }

particle vertex shader
^^^^^^^^^^^^^^^^^^^^^^
    The vertex shader used for the particle is:

.. code-block:: glsl

    attribute vec4 aPosition;
    attribute vec2 aTextCoord;
    uniform mat4 mvpMatrix;
    varying vec2 vTexCoord;
    void main()
    {
       gl_Position = mvpMatrix * aPosition;
       vTexCoord = aTextCoord;
    }

Saving particle to a binary file
--------------------------------

   It is possible to save any mesh with help of :ref:`meshDebug <meshDebug>`. For that it is necessary to prepare the :ref:`meshDebug <meshDebug>` based on our :ref:`renderizable <renderizable>`.

   * First, we have to create a temporary particle and set the values that we expect to have.
   * Then we create a :ref:`meshDebug <meshDebug>`, add at least one frame and set it to ``particle`` type.
   * Next, we copy the animations and shaders effect using the function :ref:`copyAnimationsFromMesh <copyAnimationsFromMesh>`.
   * Finally we just need specify the file name to save through the function :ref:`save <meshDebugSave>`,.

   *Follow the example how to save a particle to a binary  file:*

   .. literalinclude:: example_modules/saving_particle_to_binary_file.lua
     :language: lua
     :linenos:


   :download:`download <example_modules/saving_particle_to_binary_file.lua>`.

