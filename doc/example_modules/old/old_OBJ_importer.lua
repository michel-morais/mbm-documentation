local OBJ_importer = {_v1 = vec3:new(),_v2= vec3:new(),_v3 = vec3:new(), G = vec3:new(), tMaterial = {},
						cp1 = vec3:new(),cp2 = vec3:new(),A = vec3:new(),B = vec3:new(),C = vec3:new()}

mbm.include('E:\\Michel\\mini-mbm\\lua-scripts\\util\\util.lua')
mbm.addPath('E:\\Michel\\Download\\85-cottage_obj\\')
mbm.addPath('E:\\Michel\\Download\\66-obj\\obj\\')
mbm.addPath('E:\\Michel\\Download\\66-obj\\obj\\textures\\')
mbm.addPath('E:\\Michel\\Download\\Apartment_Building_17_obj')
mbm.addPath('E:\\Michel\\Download')


OBJ_importer.parser = function (self,sFileNameOBJ,bTwoSides)

	sFileNameOBJ = mbm.getFullPath(sFileNameOBJ)
	local fp = io.open(sFileNameOBJ,"r")
	if (fp == nil) then
		print("Could not open the file:", sFileNameOBJ)
		return false
	end

	local tPositions = {}
	local tCoordinates = {}
	local tNormals = {}

	local tFrame = {}
	local tSubset = {}

	for line in fp:lines() do
		
		if not line:find('^%s*#') then --it is a comment, discard
			if line:find('^%s*v%s') then --vertex position
				local x,y,z = line:match('^%s*v%s+(-?[%d.,eE%+]*)%s+(-?[%d.,eE%+]*)%s+(-?[%d.,eE%+]*)')
				if (x and y and z) then
					table.insert(tPositions,{x=tonumber(x),y=tonumber(y),z=tonumber(z)})
				else
					print('Something wrong at line (v):',line)
					return false
				end
			elseif line:find('^%s*vt%s') then --texture coordinates
				local u,v = line:match('^%s*vt%s+(-?[%d.,eE%+]*)%s+(-?[%d.,eE%+]*)')
				if (u and v) then
					table.insert(tCoordinates,{u=tonumber(u),v=tonumber(v)})
				else
					print('Something wrong at line (vt):',line)
					return false
				end
			elseif line:find('^%s*vn%s') then --normal coordinates
				local nx,ny,nz = line:match('^%s*vn%s+(-?[%d.,eE%+]*)%s+(-?[%d.,eE%+]*)%s+(-?[%d.,eE%+]*)')
				if (nx and ny and nz) then
					table.insert(tNormals,{nx=tonumber(nx),ny=tonumber(ny),nz=tonumber(nz)})
				else
					print('Something wrong at line (vn):',line)
					return false
				end
			elseif line:find('^%s*g%s') then --normal coordinates
				if #tSubset > 0 then
					table.insert(tFrame,tSubset)
					tSubset = {}
				end
			elseif line:find('^%s*f%s+') then --faces
				local tLines = line:split('%s+')
				-- f 4611 4657 4656 4612 										#faces
				-- f 4--9 3--8 2--6 1--3 										#faces and normals
				-- f 400--900 300--800 200--600 100--300 						#faces and normals
				-- f 1/1 2/2 3/3 4/4     										#faces and texture
				-- f 100/100 200/200 300/300 400/400     						#faces and texture
				-- f 4/5/9 3/88/8 2/9889/6 10/10/3 								#faces ,texture and  normals
				-- f 421/521/921 321/882/821 2121/9889/6121 102121/10212/3212 	#faces ,texture and  normals
				if #tLines < 4 then
					print('Expected at least 3 vertex coordinates (face):[' .. line .. ']')
					return false
				else

					local tVertexList = {}
					for i=2,#tLines do --skip f
						local sLine = tLines[i]
						if not sLine:find('^%s+$') and sLine:len() > 0 then --empty 
							local tFace = {}
							if sLine:find('%d+%/%d+%/%d+') then --vertex ,texture and normal
								local v,t,n = sLine:match('(%d+)%/(%d+)%/(%d+)')
								tFace.index_buffer 	= tonumber(v)
								tFace.index_uv 		= tonumber(t)
								tFace.index_normal 	= tonumber(n)
								if tFace.index_uv == nil or tFace.index_normal == nil then
									print('Something wrong at line (f,vtn): [' .. line .. ']' .. ' [' .. sLine .. ']')
									return false
								end
							elseif sLine:find('%d+%/%/%d+') then -- vertex and normal
								local v,n = sLine:match('(%d+)%/%/(%d+)')
								tFace.index_buffer 	= tonumber(v)
								tFace.index_normal 	= tonumber(n)
								if tFace.index_normal == nil then
									print('Something wrong at line (f,vn): [' .. line .. ']' .. ' [' .. sLine .. ']')
									return false
								end
							elseif sLine:find('%d+%/%d+') then -- vertex and texture
								local v,t = sLine:match('(%d+)%/(%d+)')
								tFace.index_buffer 	= tonumber(v)
								tFace.index_uv 		= tonumber(t)
								if tFace.index_uv == nil then
									print('Something wrong at line (f,vt): [' .. line .. ']' .. ' [' .. sLine .. ']')
									return false
								end
							elseif sLine:find('%d+') then --vertex 
								local v = sLine:match('(%d+)')
								tFace.index_buffer 	= tonumber(v)
							elseif sLine:find('^#') then --comment
								break
							end
							if tFace.index_buffer == nil or tFace.index_buffer == 0 then
								print('Something wrong at line (f,v): [' .. line .. ']' .. ' [' .. sLine .. ']')
								return false
							end
							table.insert(tVertexList,tFace)
						end
					end

					--since the engine only works with triangle list (each 3 we have one triangle)
					--while #tVertexList % 3 ~= 0 do
					--	table.insert(tVertexList,tVertexList[#tVertexList])
					--end
					if #tVertexList > 3 then
						for i=1, #tVertexList - 2  do
							if i > 1 then
								local tTrianglePrev = {tVertexList[i-1],tVertexList[i],tVertexList[i+1]}
								local tTriangleCur  = {tVertexList[i],tVertexList[i+1],tVertexList[i+2],tVertexList[i-1]}
								local a,b,c = self:findNextTriangle(tTrianglePrev,tTriangleCur,tPositions)
								table.insert(tSubset,a)
								table.insert(tSubset,b)
								table.insert(tSubset,c)
								if bTwoSides then
									table.insert(tSubset,c)
									table.insert(tSubset,b)
									table.insert(tSubset,a)
								end
							else
								table.insert(tSubset,tVertexList[i])
								table.insert(tSubset,tVertexList[i+1])
								table.insert(tSubset,tVertexList[i+2])
								if bTwoSides then
									table.insert(tSubset,tVertexList[i+2])
									table.insert(tSubset,tVertexList[i+1])
									table.insert(tSubset,tVertexList[i])
								end
							end
						end
					elseif #tVertexList == 3 then
						table.insert(tSubset,tVertexList[1])
						table.insert(tSubset,tVertexList[2])
						table.insert(tSubset,tVertexList[3])
						if bTwoSides then
							table.insert(tSubset,tVertexList[3])
							table.insert(tSubset,tVertexList[2])
							table.insert(tSubset,tVertexList[1])
						end
					else
						print('Error, unexpected size of face different of 3')
						return false
					end
				end
			elseif line:find('^%s*$') then --blank space untill the end
			elseif line:find('^%s*[ols]%s') then -- object, line, smoth shading (TODO: implement)
			elseif line:find('^%s*mtllib%s*%w') then
				local s,e = line:find('^%s*mtllib%s+')
				local newmtl_name = line:sub(e+1)
				self:loadNewMtl(newmtl_name)
			elseif line:find('^%s*usemtl%s%w') then
				local tLines = line:split('%s+')
				local material = tLines[2]
				tSubset.sTexture = self.tMaterial[material]
				if tSubset.sTexture == nil then
					print('Material not found:['..material .. ']')
					tUtil:print_recursive_table(self.tMaterial)
				end
			else
				print('ignored:', line)
			end
		end
	end

	if #tSubset > 0 then
		table.insert(tFrame,tSubset)
	end

	tSubset = nil

	fp:close()

	--tUtil:print_recursive_table(tSubset)
	for i=1,#tFrame do
		local tSubset = tFrame[i]
		local tVertexBuffer = {}
		local tIndexBuffer  = {}
		for j=1,#tSubset do
			local iPosition = tSubset[j].index_buffer
			local iTexCoord = tSubset[j].index_uv or 0
			local iNormal   = tSubset[j].index_normal or 0
			
			local index = self:addVertex(iPosition,iTexCoord,iNormal,tPositions,tCoordinates,tNormals,tVertexBuffer)
			table.insert(tIndexBuffer,index)
		end
		tFrame[i] = {tIndexBuffer =  tIndexBuffer,tVertexBuffer = tVertexBuffer, sTexture = tSubset.sTexture  }
	end
	--print('vertex size:',#tVertexBuffer)
	--print('index  size:',#self.tIndexBuffer)

	--tUtil:print_recursive_table(tFrame)

	return true, tFrame

end

OBJ_importer.addVertex = function (self,iPosition,iTexCoord,iNormal,tPositions,tCoordinates,tNormals,tVertexBuffer)
	
	--Yeh, it is slow but we have to search in each element
	local tVertexFound = nil
	local tVertex = tPositions[iPosition]
	if iTexCoord > 0 and iNormal > 0 then
		local tNormal = tNormals[iPosition] or {nx=0,ny=0,nz=0}
		local tUV = tCoordinates[iPosition] or {u=0,v=0}
		for i=1,#tVertexBuffer do
			local tVB = tVertexBuffer[i]
			if 	tVB.x  == tVertex.x and tVB.y   == tVertex.y and tVB.z   == tVertex.z and 
				tVB.u  == tUV.u     and tVB.v   == tUV.v and 
				tVB.nx == tNormal.nx and tVB.ny == tNormal.ny and tVB.nz == tNormal.nz then
				return i
			end
		end
	elseif iTexCoord > 0 then
		local tUV = tCoordinates[iPosition] or {u=0,v=0}
		for i=1,#tVertexBuffer do
			local tVB = tVertexBuffer[i]
			if 	tVB.x  == tVertex.x and tVB.y  == tVertex.y and tVB.z  == tVertex.z and 
				tVB.u  == tUV.u     and tVB.v  == tUV.v then
				return i
			end
		end
	elseif iNormal > 0 then
		local tNormal = tNormals[iPosition] or {nx=0,ny=0,nz=0}
		for i=1,#tVertexBuffer do
			local tVB = tVertexBuffer[i]
			if 	tVB.x  == tVertex.x and tVB.y   == tVertex.y  and tVB.z  == tVertex.z and 
				tVB.nx == tNormal.nx and tVB.ny == tNormal.ny and tVB.nz == tNormal.nz then
				return i
			end
		end
	else
		for i=1,#tVertexBuffer do
			local tVB = tVertexBuffer[i]
			if 	tVB.x  == tVertex.x and tVB.y   == tVertex.y  and tVB.z  == tVertex.z then
				return i
			end
		end
	end

	--Not found, lets add then

	local index = #tVertexBuffer + 1
	if iTexCoord > 0 and iNormal  > 0 then
		local tNormal = tNormals[iPosition] or {nx=0,ny=0,nz=0}
		local tUV = tCoordinates[iPosition] or {u=0,v=0}
		local tVB = {	x  = tVertex.x,		y  = tVertex.y, 	z  = tVertex.z,
						nx = tNormal.nx,	ny = tNormal.nx, 	nz = tNormal.nz,
						u  = tUV.u,  		v  = tUV.v  }
		tVertexBuffer[index] = tVB
	elseif iTexCoord  > 0 then
		local tUV = tCoordinates[iPosition] or {u=0,v=0}
		local tVB = {	x  = tVertex.x,		y  = tVertex.y, 	z  = tVertex.z,
						u  = tUV.u,  		v  = tUV.v  }
		tVertexBuffer[index] = tVB
	elseif iNormal  > 0 then
		local tNormal = tNormals[iPosition] or {nx=0,ny=0,nz=0}
		local tVB = {	x  = tVertex.x,		y  = tVertex.y, 	z  = tVertex.z,
						nx = tNormal.nx,	ny = tNormal.nx, 	nz = tNormal.nz}
		tVertexBuffer[index] = tVB
	else
		local tVB = {	x  = tVertex.x,		y  = tVertex.y, 	z  = tVertex.z}
		tVertexBuffer[index] = tVB
	end

	return index
end

OBJ_importer.findNextTriangle = function(self,tTrianglePrev,tTriangleCur,tPositions)
	--tUtil:print_recursive_table(tTriangleCur)
	local tAllCombination = {
		1,2,3,
		1,3,2,
		2,1,3,
		2,3,1,
		3,1,2,
		3,2,1,
		--4 -> 1
		4,2,3,
		4,3,2,
		2,4,3,
		2,3,4,
		3,4,2,
		3,2,4,
        --4 -> 2
		1,4,3,
		1,3,4,
		4,1,3,
		4,3,1,
		3,1,4,
		3,4,1,
		--4 -> 3
		1,2,4,
		1,4,2,
		2,1,4,
		2,4,1,
		4,1,2,
		3,2,1,
	}

	local pa = tPositions[tTrianglePrev[1].index_buffer]
	local pb = tPositions[tTrianglePrev[2].index_buffer]
	local pc = tPositions[tTrianglePrev[3].index_buffer]

	local clockwise = self:isClockWise(pa,pb,pc)
	--local clockwise = true

	--local cx,cy,cz = self:centerOfTriangle(pa,pb,pc)


	for i=1, #tAllCombination, 3 do
		local a = tAllCombination[i]
		local b = tAllCombination[i+1]
		local c = tAllCombination[i+2]

		local ia = tTriangleCur[a].index_buffer
		local ib = tTriangleCur[b].index_buffer
		local ic = tTriangleCur[c].index_buffer

		local va = tPositions[ia]
		local vb = tPositions[ib]
		local vc = tPositions[ic]

		if self:isClockWise(va,vb,vc) == clockwise then
			--print('isClockWise:',ia,ib,ic)
			--if not self:isPointInTriangle({x=cx,y=cy,z=cz}, va,vb,vc) then
			--if not self:isPointInTriangleByArea({x=cx,y=cy,z=cz}, va,vb,vc) then

			if not self:isPointInTriangleByArea({x=pa.x*0.9,y=pa.y*0.9,z=pa.z*0.9}, va,vb,vc) and 
				not self:isPointInTriangleByArea({x=pb.x*0.9,y=pb.y*0.9,z=pb.z*0.9}, va,vb,vc) and 
				not self:isPointInTriangleByArea({x=pc.x*0.9,y=pc.y*0.9,z=pc.z*0.9}, va,vb,vc) 
				then
				
				print('Point is NOT in triangle')
			--if not self:isPointInTriangle(va,vb,vc,cx,cy,clockwise) then
			--if not self:isPointOnTriangle(va,vb,vc,cx,cy) then
				--print('cx,cy is NOT in triangle',ia,ib,ic)
				return tTriangleCur[a], tTriangleCur[b],tTriangleCur[c]
			else
				print('Point is in triangle')
			end
		end
	end

	return tTriangleCur[1], tTriangleCur[2],tTriangleCur[3]
end

OBJ_importer.isClockWise = function(self,va,vb,vc)
	self._v1:set(vb.x - va.x,vb.y - va.y,vb.z - va.z);
	self._v2:set(vc.x - va.x,vc.y - va.y,vc.z - va.z);
	self._v3:cross(self._v1,self._v2)
	if(self._v3.z < 0.0) then
		return true
	end
	return false
end

OBJ_importer.centerOfTriangle = function(self,va,vb,vc)
	local x = (va.x + vb.x + vc.x) / 3
	local y = (va.y + vb.y + vc.y) / 3
	local z = (va.z + vb.z + vc.z) / 3
	return x,y,z
end
--
OBJ_importer.isPointOnTriangle = function(self,A,B,C,x,y)
	local G = self.G
	G:set(x,y,0)
	if self:isClockWise(A,B,C) then--inverted?
		if not self:isClockWise(A,B,G) then
			return false;
		end
		if not self:isClockWise(B,C,G) then
			return false;
		end
		if not self:isClockWise(C,A,G) then
			return false;
		end
		return true;
	else
		if self:isClockWise(A,B,G) then
			return false;
		end
		if self:isClockWise(B,C,G) then
			return false;
		end
		if self:isClockWise(C,A,G) then
			return false;
		end
		return true;
	end
end

--OBJ_importer.isPointInTriangle = function(self,va,vb,vc, cx,cy, clockwise)
--    if (clockwise) then
--      	if (((vb.y - va.y) * (cx - va.x) - (vb.x - va.x) * (cy - va.y)) >= 0 and
--         	((vc.y - vb.y) * (cx - vb.x) - (vc.x - vb.x) * (cy - vb.y)) >= 0 and
--         	((va.y - vc.y) * (cx - vc.x) - (va.x - vc.x) * (cy - vc.y)) >= 0) then
--           	return true
--         end
--    else
--      	if (((vb.y - va.y) * (cx - va.x) - (vb.x - va.x) * (cy - va.y)) <= 0 and
--        	((vc.y - vb.y) * (cx - vb.x) - (vc.x - vb.x) * (cy - vb.y)) <= 0 and
--         	((va.y - vc.y) * (cx - vc.x) - (va.x - vc.x) * (cy - vc.y)) <= 0) then
--           	return true
--       	end
--    end
--    return false
--end

OBJ_importer.isSameSide = function(self,p1,p2, a,b)
    self.A:set(b.x  - a.x, b.y -  a.y, b.z - a.z)
    self.B:set(p1.x - a.x, p1.y - a.y, p1.z - a.z)
    self.C:set(p2.x - a.x, p2.y - a.y, p2.z - a.z)

    self.cp1:cross(self.A,self.B)
    self.cp2:cross(self.A,self.C)
    if self.cp1:dot(self.cp2) >= 0 then
    	return true
    else
    	return false
    end
end

OBJ_importer.isPointInTriangle = function(self,p, a,b,c)
    if self:isSameSide(p,a, b,c) and self:isSameSide(p,b, a,c) and self:isSameSide(p,c, a,b) then 
    	return true
    else 
    	return false
    end
end


OBJ_importer.isPointInTriangleByArea = function(self,p, a,b,c)

  local function heron(a,b,c)
      local s = (a + b + c) * 0.5   
      area = math.pow((s*(s-a) * (s-b)*(s-c)), 0.5)
      return area
  end

  local function distance3d(x1,y1,z1,x2,y2,z2)
      local a = math.pow((x1-x2),2) + math.pow((y1-y2),2) + math.pow((z1-z2),2)
      local d = math.pow(a , 0.5)
      return d  
  end

  local function areatriangle3d(a,b,c)
      local x1,y1,z1,x2,y2,z2,x3,y3,z3 = a.x,a.y,a.z,b.x,b.y,b.z,c.x,c.y,c.z
      local A = distance3d(x1,y1,z1,x2,y2,z2)  
      local B = distance3d(x2,y2,z2,x3,y3,z3)  
      local C = distance3d(x3,y3,z3,x1,y1,z1)  
      local area = heron(A,B,C)
      return area
  end

  -- get the area of the triangle
  local  areaOrig = areatriangle3d(a,b,c)

  -- get the area of 3 triangles made between the point
  -- and the corners of the triangle
  local area1 =   areatriangle3d(a,b,p)
  local area2 =   areatriangle3d(a,c,p)
  local area3 =   areatriangle3d(c,b,p)

  print('areaOrig',areaOrig,area1, area2 , area3)
  -- if the sum of the three areas equals the original,
  -- we're inside the triangle!
  if (area1 + area2 + area3 == areaOrig) then
  	print('areaOrig',areaOrig,area1 + area2 + area3)
    return true;
  else
  	print('areaOrig',areaOrig,area1 + area2 + area3)
    return false;
   end
end


OBJ_importer.loadNewMtl = function(self,newmtl_name)
	print('Loading new mtl',newmtl_name)--cottage_obj.mtl
	local newmtl_file_name = mbm.getFullPath(newmtl_name)
	local fp = io.open(newmtl_file_name,"r")
	if (fp == nil) then
		print("Could not open the file:", newmtl_file_name)
		return false
	end
	local material_name = nil
	for line in fp:lines() do
		if line:find('^%s*newmtl%s%w') then
			material_name = line:match('^%s*newmtl%s(%w.*)')
		elseif line:find('^%s*Ka%s') then -- ignore ambiemt color
		elseif line:find('^%s*Kd%s') then -- use diffuse color
			local r,g,b = line:match('^%s*Kd%s+(-?[%d.,eE%+]*)%s+(-?[%d.,eE%+]*)%s+(-?[%d.,eE%+]*)')
			if (r and g and b) then
				local rv = math.floor(tonumber(r) * 255)
				local gv = math.floor(tonumber(g) * 255)
				local bv = math.floor(tonumber(b) * 255)
				local hr = string.format('%02X', rv)
				local hg = string.format('%02X', gv)
				local hb = string.format('%02X', bv)
				self.tMaterial[material_name] = '#FF' .. hr .. hg .. hb 
			else
				print('Failed to get diffuse color from file')
			end
		elseif line:find('^%s*map_Kd%s*') then -- Texture
			local file_name_texture = line:match('^%s*map_Kd%s+(%g+)')
			self.tMaterial[material_name] = file_name_texture
			print('Texture found : [' .. file_name_texture .. ']')
		elseif line:find('^%s*Ks%s') then -- ignore specular color
		elseif line:find('^%s*Ns%s') then -- ignore specular color attribute
		elseif line:find('^%s*d%s')  then -- ignore transparency (dissolved (opaque = 1))
		elseif line:find('^%s*Tr%s') then -- ignore transparency (Tr = 0 == opaque) (Tr = 1 - d)
		elseif line:find('^%s*illum%s') then -- ignore illumination mode
		end
	end
	fp:close()

end


return OBJ_importer