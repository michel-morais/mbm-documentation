local bVariables = true --enable all variables
local filter     = 'ps' --put nil to print them all
local bMin       = true --enable min values
local bMax       = true --enable max values
local bCode      = true --enable code

local tShaders   = mbm.getShaderList(bVariables,filter,bMin,bMax,bCode)

for i=1, # tShaders do
    local tShader = tShaders[i]
    print('\n')
    local name = tShader.name:split('%.')[1]
    print(string.format('%s', name))
    print(string.rep("^",name:len()))
    print(string.format(':name: ``%s``',tShader.name))
    for k,v in pairs(tShader.min) do
        print(string.format(':variable: ``%s``\n', k))
        if #tShader.min[k] == 1 then --number, boolean
            print(string.format('   :min: :guilabel:`%g`',tShader.min[k][1]))
            print(string.format('   :max: :guilabel:`%g`',tShader.max[k][1]))
            print(string.format('   :default: :guilabel:`%g`',tShader.var[k][1]))
        elseif #tShader.min[k] == 2 then --xy 2
            print(string.format('   :min: :guilabel:`%g`, :guilabel:`%g`',tShader.min[k][1],tShader.min[k][2]))
            print(string.format('   :max: :guilabel:`%g`, :guilabel:`%g`',tShader.max[k][1],tShader.max[k][2]))
            print(string.format('   :default: :guilabel:`%g`, :guilabel:`%g`',tShader.var[k][1],tShader.var[k][2]))
        elseif #tShader.min[k] == 3 then --xyz 3, rgb
            print(string.format('   :min: :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`',tShader.min[k][1],tShader.min[k][2],tShader.min[k][3]))
            print(string.format('   :max: :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`',tShader.max[k][1],tShader.max[k][2],tShader.max[k][3]))
            print(string.format('   :default: :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`',tShader.var[k][1],tShader.var[k][2],tShader.var[k][3]))
        elseif #tShader.min[k] == 4 then --xyz 4, rgba
            print(string.format('   :min: :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`',tShader.min[k][1],tShader.min[k][2],tShader.min[k][3],tShader.min[k][4]))
            print(string.format('   :max: :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`',tShader.max[k][1],tShader.max[k][2],tShader.max[k][3],tShader.max[k][4]))
            print(string.format('   :default: :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`, :guilabel:`%g`',tShader.var[k][1],tShader.var[k][2],tShader.var[k][3],tShader.var[k][4]))
        end

    end
    print('\n:shader code:')
    print('\n.. code-block:: glsl\n')
    local tShaderCode = tShader.code:split('\n')
    for i =1, #tShaderCode do
        local line = tShaderCode[i]
        print(string.format('    %s', line))
    end
end