mbm.setColor(1,1,1) --set background color to white

function createFace()

   local tFace = {  {x=-50, y= -50},
                    {x=-50, y=  50},
                    {x= 50, y= -50},
                    {x= 50, y=  50}}
   local tIndex = {1,2,3, 3,2,4}
   local sTextureFileName = 'HB_smile.png'

   return tFace, tIndex, sTextureFileName
end

function saveMeshToBinaryFile(fileName,tVertex,tIndex,sTexture)

    --meshDebug is used to create dynamically mesh in the engine.
    --For sprite it has to have at least one frame to be able to generate the sprite
    local stride      = 2 --stride only can be 3 or 2. it means (x,y,z) or (x,y)
    local tMesh       = meshDebug:new() --new mesh debug to store the information about our sprite
    local nFrame      = tMesh:addFrame(stride) -- Add one frame with stride 2 (x,y)
    local indexFrame  = nFrame   --(meshDebug uses 1 based index)
    local indexSubset = 1 --first subset (1 based index)
   
   
    --To add vertex, first we need to add a subset
    local nSubset     = tMesh:addSubSet(indexFrame) --add one subset for the first frame

    --we are adding vertex to frame (next)
    --this vertex list has to have at least 3 vertex (one triangle) to be valid
    -- The table expected is : {{x,y},{x,y},{x,y}, ...}
    if not tMesh:addVertex(indexFrame,indexSubset,tVertex) then 
        print("Error on add vertex buffer")
        return false
    end
    
    if not tMesh:addIndex(indexFrame,indexSubset,tIndex) then 
        print("Error on add index buffer")
        return false
    end

    --apply the texture to frame / subset
    if not tMesh:setTexture(indexFrame,indexSubset,sTexture) then
        print("Error on set texture!")
        return false
    end

    tMesh:setType('sprite')  -- set it to sprite type

    local calcNormal,calcUv = false,true --Instruct to do not calculate normal but calcule UV
    if tMesh:save(fileName,calcNormal,calcUv) then
        print("Sprite created successfully ")
        return true
    else
        print("Failed to create sprite!")
        return false
    end
    
end


function onInitScene()

    tSprite = sprite:new('2DW')-- our object which will load from binary file

    local sFileNameSprite = 'smile.spt'

    local tFaceVertex, tFaceIndex, sTextureFileName = createFace()--create our face 

    if saveMeshToBinaryFile(sFileNameSprite,tFaceVertex, tFaceIndex, sTextureFileName) then
        tSprite:load(sFileNameSprite) --all coordinate already in place
    else
        print('Failed to create ' .. sFileNameSprite)
        mbm.quit()
    end

end

