mbm.setColor(0.3,0.3,0.7) --blue color of background

if not mbm.get('USE_EDITOR_FEATURES') then
	mbm.messageBox('Missing features','Is necessary to compile using USE_EDITOR FEATURES to run this example','ok','error',0)
	mbm.quit()
end

function saveBinaryFont(tText,fileName)

   local tMeshDebug = meshDebug:new()

   if tMeshDebug:loadFromMemory(tText) then -- require to be compiled using the editor flag: USE_EDITOR_FEATURES

      if tMeshDebug:copyAnimationsFromMesh(tText) then --copy animations created

         if tMeshDebug:save(fileName) then

            print("Sucessfully saved font!")
            return true
         else
            print("Failed to save font!")
         end
      else
         print("Failed to apply shader and animations!")
      end
   else
      print("Failed to load font!")
   end
   return false
end


function onInitScene()

   local height = 50 --the font height 
   local space_x = 2 --space between characters in points coordinates
   local space_y = 1 --space between new line characters in points coordinates
   local save_image_as_png = true --will save a local image as 'font_name-50.png'
   local base_name = 'Carlito-Regular'

   tMyFont = font:new(base_name .. '.ttf',height,space_x,space_y,save_image_as_png)
   
   --add one text to view
   local tText = tMyFont:addText('Hello \n Font !\n 0123456789','2dw')

   --increase the space width
   local xSpace,ySpace = tMyFont:getSizeLetter(' ')
   tMyFont:setSizeLetter(' ',10,ySpace) --10 it is good space  for this font size (I am guessing)
   
   --add one font color as animation
   tText:addAnim('red font animation')
   
   --get the shader of font to change it
   local shader = tText:getShader()
   shader:load('font.ps') --just pixel shader, vertex shader is not necessary
   
   shader:setPSall('colorFont',1,0,0) --change color font to red for min, max and current
   
   local fileNameBinary = base_name .. '.fnt'

   if saveBinaryFont(tText,fileNameBinary) then
      tText:destroy() --destroy the existing one to reload from binary file previously created

      tMyFont = font:new(fileNameBinary) --load from binary font

      tText = tMyFont:addText('Hello world \n Font !\n 0123456789 \n #2# Second Animation','2dw')
      tText:setWildCard('#')--instruct to look a number between '#' that indicate the animation
   end

end