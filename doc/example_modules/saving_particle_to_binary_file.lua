
mbm.setColor(0,0,0) --set background color to black

function createParticle()

   local tParticle = particle:new('2dw')
   tParticle:load('particle_flame.png')

   local stage_1 = 1 
   
   --Set up for the first stage 
   tParticle:add(500) --500 particle for the first stage

   tParticle:setMinSpeed(stage_1,10,10,0)
   tParticle:setMaxSpeed(stage_1,1000,1000,0)

   tParticle:setMinLifeTime(stage_1,1)--minimun alive for one second
   tParticle:setMaxLifeTime(stage_1,2)--maximun alive for two seconds

   tParticle:setStageTime(stage_1,1)--One second of duration

   --Set up for the second stage 
   local stage_2 = tParticle:addStage()

   tParticle:setMinDirection(stage_2,-0.2,1,0)
   tParticle:setMaxDirection(stage_2,0.2,1,0)

   tParticle:setMinSpeed(stage_2,10,10,0)
   tParticle:setMaxSpeed(stage_2,1000,1000,0)

   tParticle:add(1000)--500 particle for the second stage

   tParticle:restartAnim()--restart the animation since we have modified it
   return tParticle
end

function saveParticleToFile(fileName,tParticle)

   --meshDebug is used to create dynamically the mesh in the engine.
   --For particle it has to have at least one frame to be able to generate the mesh
   local stride      = 2 --stride only can be 3 or 2. it means (x,y,z) or (x,y)
   local tMesh       = meshDebug:new() --new mesh debug to store the information about our particle
   local nFrame      = tMesh:addFrame(stride) -- Add one frame with stride 2 (x,y)
   local indexFrame  = nFrame  --(meshDebug uses 1 based index)
   local indexSubset = 1 --first subset
   local totalVertex = 3 --minimun 3  to build one triangle (otherwise the internal check of meshDebug will fail)
   
   --To add vertex, first we need to add a subset
   local nSubset     = tMesh:addSubSet(indexFrame) --add one subset for the first frame

   --we have to add one frame to set the texture and be able to save the mesh
   --this frame has to have at least 3 vertex (one triangle)
   if not tMesh:addVertex(indexFrame,indexSubset,totalVertex) then 
      print("Error on add vertex")
      return false
   end
   
   --apply the texture from particle
   if not tMesh:setTexture(indexFrame,indexSubset,tParticle:getTexture()) then
      print("Error on set texture")
      return false
   end

   tMesh:setType('particle')  -- set it to particle type 

   if tMesh:copyAnimationsFromMesh(tParticle) then --copy animations and shaders, include stages.
      local calcNormal,calcUv = false,false --dont wanna normal (neither recalc it) and recalc UV at all
      if tMesh:save(fileName,calcNormal,calcUv) then
         print("Particle created successfully ")
         return true
      else
         print("Failed to create particle!")
         return false
      end
   else
      print("Failed to copy animations - stages, shaders...")
      return false
   end
end


local tParticle = createParticle()

if saveParticleToFile('particle.ptl',tParticle) then
   tParticleFromFile = particle:new('2dw')
   tParticleFromFile:load('particle.ptl') --all behavior already in place
end

tParticle:destroy()