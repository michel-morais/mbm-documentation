mbm.setColor(1,1,1) --set background color to white

function createCube(width,height,depth)
--[[
k-uv=1,1
    uv=0,0-f________________g-uv=1,0, i-uv=0,1
           /               /|
          /               / |
uv=0,1-b /_______________/c-uv=1,1
        |   |           |   |
        |   |           |   |
        |   |   back    |   |
    uv=0,1-e|___________|___|h-uv=1,1  j-uv=0,0
 l-uv=1,1  /            |  /
        | /             | /
        |/______________|/
 uv=0,0-a   front       d-uv=1,0

]]--

    local halfSizeCube = { x = width / 2, y = height / 2, z = depth / 2}

    local  pa = {x = -halfSizeCube.x,  y = -halfSizeCube.y,  z = -halfSizeCube.z, u = 0, v = 0};
    local  pb = {x = -halfSizeCube.x,  y =  halfSizeCube.y,  z = -halfSizeCube.z, u = 0, v = 1};
    local  pc = {x =  halfSizeCube.x,  y =  halfSizeCube.y,  z = -halfSizeCube.z, u = 1, v = 1};
    local  pd = {x =  halfSizeCube.x,  y = -halfSizeCube.y,  z = -halfSizeCube.z, u = 1, v = 0};
    local  pe = {x = -halfSizeCube.x,  y = -halfSizeCube.y,  z =  halfSizeCube.z, u = 0, v = 1};
    local  pf = {x = -halfSizeCube.x,  y =  halfSizeCube.y,  z =  halfSizeCube.z, u = 0, v = 0};
    local  pg = {x =  halfSizeCube.x,  y =  halfSizeCube.y,  z =  halfSizeCube.z, u = 1, v = 0};
    local  ph = {x =  halfSizeCube.x,  y = -halfSizeCube.y,  z =  halfSizeCube.z, u = 1, v = 1};

    --extra because of the texture coordinates
    local  pi = {x =  halfSizeCube.x,  y =  halfSizeCube.y,  z =  halfSizeCube.z, u = 0, v = 1};
    local  pj = {x =  halfSizeCube.x,  y = -halfSizeCube.y,  z =  halfSizeCube.z, u = 0, v = 0};

    local  pk = {x = -halfSizeCube.x,  y =  halfSizeCube.y,  z =  halfSizeCube.z, u = 1, v = 1};
    local  pl = {x = -halfSizeCube.x,  y = -halfSizeCube.y,  z =  halfSizeCube.z, u = 1, v = 0};

    local  tCube = {pa,pb,pc,pd,pe,pf,pg,ph,pi,pj,pk,pl};

    local a,b,c,d,e,f,g,h,i,j,k,l = 1,2,3,4,5,6,7,8,9,10,11,12

    local tIndex = { a,b,d, d,b,c, -- front
                     d,c,i, d,i,j, -- right
                     a,l,k, k,b,a, -- left
                     h,g,e, e,g,f, -- back
                     b,f,c, c,f,g, -- top
                     a,d,e, h,e,d  -- bellow
                    }
    local sTextureFileName = 'crate.png'

   return tCube, tIndex, sTextureFileName
end

function saveMeshToBinaryFile(fileName,tVertex,tIndex,sTexture)

    --meshDebug is used to create dynamically mesh in the engine.
    --For mesh it has to have at least one frame to be able to generate the mesh
    local stride      = 3 --stride only can be 3 or 2. it means (x,y,z) or (x,y)
    local tMesh       = meshDebug:new() --new mesh debug to store the information about our mesh
    local nFrame      = tMesh:addFrame(stride) -- Add one frame with stride 3 (x,y,z)
    local indexFrame  = nFrame  --(meshDebug uses 1 based index)
    local indexSubset = 1 --first subset (1 based index)
   
   
    --To add vertex, first we need to add a subset
    local nSubset     = tMesh:addSubSet(indexFrame) --add one subset for the first frame

    --we are adding vertex to frame (next)
    --this vertex list has to have at least 3 vertex (one triangle) to be valid
    -- The table expected is : {{x,y,z},{x,y,z},{x,y,z}, ...}
    if not tMesh:addVertex(indexFrame,indexSubset,tVertex) then 
        print("Error on add vertex buffer")
        return false
    end
    
    if not tMesh:addIndex(indexFrame,indexSubset,tIndex) then 
        print("Error on add index buffer")
        return false
    end

    --apply the texture to frame / subset
    if not tMesh:setTexture(indexFrame,indexSubset,sTexture) then
        print("Error on set texture!")
        return false
    end

    tMesh:setType('mesh')  -- set it to mesh type

    local calcNormal,calcUv = true,false --Instruct to calculate normal
    if tMesh:save(fileName,calcNormal,calcUv) then
        print("Mesh created successfully ")
        return true
    else
        print("Failed to create Mesh!")
        return false
    end
    
end


function onInitScene()

    tMesh = mesh:new('3D')-- our object which will load from binary file

    local sFileNameMesh = 'crate.msh'

    local width, height, depth = 100, 100, 100

    local tCubeVertex, tCubeIndex, sCubeTextureFileName = createCube(width, height, depth)--create our cube 

    if saveMeshToBinaryFile(sFileNameMesh,tCubeVertex, tCubeIndex, sCubeTextureFileName) then
        tMesh:load(sFileNameMesh) --all coordinate already in place
    else
        print('Failed to create ' .. sFileNameMesh)
        mbm.quit()
    end


    --set up camera
    camera3d = mbm.getCamera('3D')
    camera3d:setPos(0,0,-500)
    camera3d:setFocus(0,0,0)

    --need to rotate our object
    tMouse = {x=0,y=0,clicked = false}
end

function onTouchDown(key,x,y)
    tMouse.clicked = true
    tMouse.x = x
    tMouse.y = y
end

function onTouchMove(key,x,y)
    if tMouse.clicked then
        local diff_X = tMouse.x - x
        local diff_Y = tMouse.y - y
        tMouse.x = x
        tMouse.y = y

        --simple rotation based on mouse
        tMesh.ay = tMesh.ay + math.rad(diff_X)
        tMesh.ax = tMesh.ax + math.rad(diff_Y)

    end
end

function onTouchUp(key,x,y)
    tMouse.clicked = false
end

