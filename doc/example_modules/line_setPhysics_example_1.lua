
function onInitScene()

   mbm.setColor(0,0,0) --set background color to black

   local bDrawLines = true --instruct to draw the lines
   local tRect      = {type = 'rect',   width=100,  height=100,  x=0,  y=0}
   local tCircle    = {type = 'circle', ray = 50,  x=0,  y=0}
   local tEllipse   = {type = 'ellipse', width=100,  height=70,  x=0,  y=0}
   local tTriangle  = {type = 'triangle',  x=0,  y=0,  points={[1]={x=-50,y=-50},--must have 3 points
                                                               [2]={x=0,y=50},
                                                               [3]={x=50,y=-50}}}
   local tPolygon   = {type = 'polygon',  x=0,  y=0,  points={ --each 3 points
                                                               [1]={x=-50,y=-50},
                                                               [2]={x=0,y=0},
                                                               [3]={x=40,y=-40},
                                                               [4]={x=50,y=50},
                                                               [5]={x=0,y=0},
                                                               [6]={x=-50,y=50},
                                                               [7]={x=-250,y=250},--this point will be ignored
                                                            }}

   
   tLineRect = line:new('2dw')
   tLineRect:setPhysics(tRect,bDrawLines)
   tLineRect.x = -200

   tLineCircle = line:new('2dw')
   tLineCircle:setPhysics(tCircle,bDrawLines)
   tLineCircle.x = -100

   tLineEllipse = line:new('2dw')
   tLineEllipse:setPhysics(tEllipse,bDrawLines)
   tLineEllipse.x = 0

   tLineTriangle = line:new('2dw')
   tLineTriangle:setPhysics(tTriangle,bDrawLines)
   tLineTriangle.x = 100

   tLinePolygon = line:new('2dw')
   tLinePolygon:setPhysics(tPolygon,bDrawLines)
   tLinePolygon.x = 200

   tAllObjs = {}
   table.insert(tAllObjs,tLineRect)
   table.insert(tAllObjs,tLineCircle)
   table.insert(tAllObjs,tLineEllipse)
   table.insert(tAllObjs,tLineTriangle)
   table.insert(tAllObjs,tLinePolygon)

end


function loop(delta)
   for i=1, #tAllObjs do
      local tObj = tAllObjs[i]
      tObj:rotate('z',math.rad(90))
   end
end