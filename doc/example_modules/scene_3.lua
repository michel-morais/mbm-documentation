--example of scene using coroutine class style
local tMyClass = {}

tMyClass.onInitScene = function(self)
    self.tTextures = {}
    local iTotal = 91 --we put 91 just to understand how onProgress works (91 represent the end of the loop (100%))
    for i=1,iTotal do
        local x = i * 10
        local y = i * 10
        local tTexture = texture:new('2ds',x,y) --tTexture is local
        tTexture:load('crate.png') --just load the texture (does not care if not found the texture)
        tTexture:setScale(0.5,0.5) --adjust the scale
        table.insert(self.tTextures,tTexture) --save tTexture to a table (will not be destroyed anymore)
        --call coroutine.yield informing step and total of step (iStep , iTotal). our case (1,91), (2,91), (3,91), ... 
        --each loop will call onProgress where it could do some animation there.
        --could be coroutine.yield() but in this case, will not call onProgress method
        coroutine.yield(i,iTotal)
    end
end

--This function only exist for class style and using coroutine
--This function is called each step if passed the progress to coroutine.yield (current and total steps)
tMyClass.onProgress = function(self,percent)
    print(string.format('loaded: %.0f%%',percent))
end

tMyClass.onTouchMove = function(self,key,x,y)
    self.tTextures[#self.tTextures]:setPos(x,y) --move according to mouse or touch (we are ignoring the key)
end

tMyClass.onTouchDown = function(self,key,x,y)
    local tTexture = texture:new('2ds',x,y)-- create a new local texture.
    tTexture:load('crate.png') --Load it
    tTexture:setScale(0.5,0.5)--adjust the scale
    tTexture.az = self.tTextures[#self.tTextures].az -- adjust the rotation on z angle
    self.tTextures[#self.tTextures].z = tTexture.z -1 --bring to front
    table.insert(self.tTextures,tTexture)
end

tMyClass.loop = function(self,delta)
    for i=1,#self.tTextures do
        local tTexture = self.tTextures[i]
        tTexture:rotate('z',math.rad(30)) -- rotate 30 degree per second on z angle
    end
end 

--[[
    important return the class otherwise won't work
]]

return tMyClass
