mbm.setColor(1,1,1) --set background color to white

function createOneFrame(sTexture)

   local tVertex = {  {x=-50, y= -50, u=0, v=1},
                    {x=-50, y=  50, u=0, v=0},
                    {x= 50, y= -50, u=1, v=1},
                    {x= 50, y=  50, u=1, v=0}}
   local tIndex = {1,2,3, 3,2,4}

   return {tVertex = tVertex, tIndex = tIndex, sTexture = sTexture }
end

function saveMeshToBinaryFile(fileName,tFrames)

    --meshDebug is used to create dynamically mesh in the engine.
    --For sprite it has to have at least one frame to be able to generate the sprite
    local stride      = 2 --stride only can be 3 or 2. it means (x,y,z) or (x,y)
    local tMesh       = meshDebug:new() --new mesh debug to store the information about our sprite
    
    --First we must add the frames
    for i = 1, #tFrames do
        local tFrame = tFrames[i]
        local indexFrame  = tMesh:addFrame(stride) -- Add one frame with stride 2 (x,y)
        --To add vertex, first we need to add a subset
        local indexSubset = tMesh:addSubSet(indexFrame) --add one subset for the first frame
        --we are adding vertex to frame (next)
        --this vertex list has to have at least 3 vertex (one triangle) to be valid
        -- The table expected is : {{x,y},{x,y},{x,y}, ...}
        if not tMesh:addVertex(indexFrame,indexSubset,tFrame.tVertex) then 
            print("Error on add vertex buffer")
            return false
        end
    end
    
    --Then we add the index buffer
    for indexFrame = 1, #tFrames do
        local tFrame = tFrames[indexFrame]
        local indexSubset = 1
        if not tMesh:addIndex(indexFrame,indexSubset,tFrame.tIndex) then 
            print("Error on add index buffer")
            return false
        end
        --apply the texture to frame / subset
        if not tMesh:setTexture(indexFrame,indexSubset,tFrame.sTexture) then
            print("Error on set texture!")
            return false
        end
    end

    tMesh:setType('sprite')  -- set it to sprite type

    --animation
    local animation_name   = 'first_animation'
    local initialFrame     = 1
    local finalFrame       = 4
    local timeBetweenFrame = 0.3
    local typeAnimation    = mbm.GROWING_LOOP

    local index = tMesh:addAnim(animation_name,initialFrame,finalFrame,timeBetweenFrame,typeAnimation)
    print('Animation :', index)


    local calcNormal,calcUv = false,false --Instruct to do not calculate normal and UV
    if tMesh:save(fileName,calcNormal,calcUv) then
        print("Sprite created successfully ")
        return true
    else
        print("Failed to create sprite!")
        return false
    end
    
end


function onInitScene()

    tSprite = sprite:new('2DW')-- our object which will load from binary file

    local sFileNameSprite = 'smile.spt'

    local tFrames = {
        [1] = createOneFrame('HB_smile.png'),
        [2] = createOneFrame('HB_sad.png'),
        [3] = createOneFrame('HB_neutral.png'),
        [4] = createOneFrame('HB_dead.png') 
    }

    if saveMeshToBinaryFile(sFileNameSprite,tFrames) then
        tSprite:load(sFileNameSprite) --all coordinate already in place
    else
        print('Failed to create ' .. sFileNameSprite)
        mbm.quit()
    end

end

