mbm.setColor(1,1,1) --set background color to white

function addBirdFrame(x,y)

    local sTexture = 'Bird.png'
    local w_size_texture, h_size_texture = 512, 512 -- we know the size of texture
    local pw = 1.0 / w_size_texture * (w_size_texture/3) --we calculate the 3 birds in the x
    local ph = 1.0 / h_size_texture * (h_size_texture/3) --we calculate the 3 birds in the y

    local tVertex = {  {x=-50, y= -50, u= (x-1) * pw, v= (y  ) * ph},
                       {x=-50, y=  50, u= (x-1) * pw, v= (y-1) * ph},
                       {x= 50, y= -50, u= (x  ) * pw, v= (y  ) * ph},
                       {x= 50, y=  50, u= (x  ) * pw, v= (y-1) * ph}}
   local tIndex = {1,2,3, 3,2,4}

   return {tVertex = tVertex, tIndex = tIndex, sTexture = sTexture }
end

function saveMeshToBinaryFile(fileName,tFrames)

    --meshDebug is used to create dynamically mesh in the engine.
    --For sprite it has to have at least one frame to be able to generate the sprite
    local stride      = 2 --stride only can be 3 or 2. it means (x,y,z) or (x,y)
    local tMesh       = meshDebug:new() --new mesh debug to store the information about our sprite
    
    --First we must add the frames
    for i = 1, #tFrames do
        local tFrame = tFrames[i]
        local indexFrame  = tMesh:addFrame(stride) -- Add one frame with stride 2 (x,y)
        --To add vertex, first we need to add a subset
        local indexSubset = tMesh:addSubSet(indexFrame) --add one subset for the first frame
        --we are adding vertex to frame (next)
        --this vertex list has to have at least 3 vertex (one triangle) to be valid
        -- The table expected is : {{x,y},{x,y},{x,y}, ...}
        if not tMesh:addVertex(indexFrame,indexSubset,tFrame.tVertex) then 
            print("Error on add vertex buffer")
            return false
        end
    end
    
    --Then we add the index buffer
    for indexFrame = 1, #tFrames do
        local tFrame = tFrames[indexFrame]
        local indexSubset = 1
        if not tMesh:addIndex(indexFrame,indexSubset,tFrame.tIndex) then 
            print("Error on add index buffer")
            return false
        end
        --apply the texture to frame / subset
        if not tMesh:setTexture(indexFrame,indexSubset,tFrame.sTexture) then
            print("Error on set texture!")
            return false
        end
    end

    tMesh:setType('sprite')  -- set it to sprite type

    --animation
    local animation_name   = 'bird flying'
    local initialFrame     = 1
    local finalFrame       = #tFrames
    local timeBetweenFrame = 0.2
    local typeAnimation    = mbm.GROWING_LOOP

    local index = tMesh:addAnim(animation_name,initialFrame,finalFrame,timeBetweenFrame,typeAnimation)
    print('Animation :', index)


    local calcNormal,calcUv = false,false --Instruct to do not calculate normal and UV
    if tMesh:save(fileName,calcNormal,calcUv) then
        print("Sprite created successfully ")
        return true
    else
        print("Failed to create sprite!")
        return false
    end
    
end


function onInitScene()

    tSprite = sprite:new('2DW')-- our object which will load from binary file

    local sFileNameSprite = 'Bird.spt'

    local tFrames = {
        [1] = addBirdFrame(1,1),
        [2] = addBirdFrame(1,2),
        [3] = addBirdFrame(1,3),

        [4] = addBirdFrame(2,1),
        [5] = addBirdFrame(2,2),
        [6] = addBirdFrame(2,3),

        [7] = addBirdFrame(3,1),
        [8] = addBirdFrame(3,2),
        [9] = addBirdFrame(3,3),        
    }

    if saveMeshToBinaryFile(sFileNameSprite,tFrames) then
        tSprite:load(sFileNameSprite) --all coordinate already in place
    else
        print('Failed to create ' .. sFileNameSprite)
        mbm.quit()
    end

end

