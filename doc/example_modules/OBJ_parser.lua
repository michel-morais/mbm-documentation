
--[[
    OBJ_parser.lua module (work together mbm engine)
    Simple parser OBJ (Wave front)

    Necessary method to parser:    'OBJ_parser.parse_file'
    ------------------------------------------------------------------------------------------------
    Result of parser:
        boolean (true/false)    -> result of parser
        table Frame             -> contain one or more subset
            each subset contains: vertex buffer, index buffer and Material information


    ------------------------------------------------------------------------------------------------
    Example:

    OBJ_parser = require "OBJ_parser"
    local bResult , tFrame = OBJ_parser:parse_file('some_file.obj')

    if bResult then 
        print('Success parsed OBJ file!')

        --Data available:
        for i=1,#tFrame do
            local tSubset = tFrame[i]
            print('Subset ' .. tostring(i) .. ' Size of vertex buffer:' .. tostring(#tSubset.tVertex))
            print('Subset ' .. tostring(i) .. ' Size of index buffer :' .. tostring(#tSubset.tIndex))
            print('Subset ' .. tostring(i) .. ' texture suggested    :' .. OBJ_parser:get_texture_from_material(tSubset.tMaterial))
        end
    else
        print('Failed to parse OBJ file!')
    end

    
    ------------------------------------------------------------------------------------------------
    You can adjust u/v on parse_file:
    
    Example:

    OBJ_parser = require "OBJ_parser"
    OBJ_parser.bInvert_u = false
    OBJ_parser.bInvert_v = false

    local bResult , tFrame = OBJ_parser:parse_file('some_file.obj')
    ...

    ------------------------------------------------------------------------------------------------

  ]]--


local OBJ_parser = {
    version = '1.0',
    bInvert_u = false,
    bInvert_v = true,
    A = vec3:new(),
    B = vec3:new(),
    C = vec3:new(),
    U = vec3:new(),
    V = vec3:new(),
    magnitude = vec3:new(),
    dot_a = vec3:new(),
    dot_b = vec3:new(),
    aux_normal = vec3:new(),
    aux_cross_a = vec3:new(),
    aux_cross_b = vec3:new(),
    cp1 = vec3:new(), 
    cp2 = vec3:new(),
    }


OBJ_parser.failed = function(self,msg)
    print(msg)
    if self.fp then
        self.fp:close()
        self.fp = nil
    end
    return false
end

OBJ_parser.has_normal = function (self,tFrame)

    local has = #tFrame > 0 and #tFrame[1].tVertex > 0 and tFrame[1].tVertex[1].nx ~= nil
    return has
end

OBJ_parser.has_texture = function (self,tFrame)

    local has = #tFrame > 0 and #tFrame[1].tVertex > 0 and tFrame[1].tVertex[1].u  ~= nil
    return has
end

OBJ_parser.vertex = function (self)

    return {x=0,y=0,z=0}
end

OBJ_parser.get_texture_from_material = function(self,tMaterial)

    local sTexture = '#FFFFFFFF'
    if tMaterial then
        sTexture = tMaterial.map_Kd
        if sTexture == nil or sTexture:len() == 0 then
            sTexture = tMaterial.map_Ks
        end
        if sTexture == nil or sTexture:len() == 0 then
            sTexture = tMaterial.Kd.hex_color
        end
        if sTexture == nil or sTexture:len() == 0 then
            sTexture = tMaterial.Ks.hex_color
        end
        if sTexture == nil or sTexture:len() == 0 then
            sTexture = '#FFFFFFFF'
        end
    end
    return sTexture
end

OBJ_parser.clone_vertex = function (self,other)
    
    local vertex = {x=other.x,y=other.y,z=other.z}
    if other.nx then
        vertex.nx = other.nx
        vertex.ny = other.ny
        vertex.nz = other.nz
    end

    if other.u then
        vertex.u = other.u
        vertex.v = other.v
    end
    return vertex
end

OBJ_parser.is_xyz_equal = function (self,a,b)

    if a.u and b.u then
        return a.x == b.x and a.y == b.y and a.z == b.z and a.u == b.u and a.v == b.v
    end    
    return a.x == b.x and a.y == b.y and a.z == b.z
end

OBJ_parser.material_info = function (self)

    return {
        name='',
        Ka = {r=0,g=0,b=0, hex_color = nil},
        Kd = {r=0,g=0,b=0, hex_color = '#FFFFFFFF'},
        Ks = {r=0,g=0,b=0, hex_color = nil},
        Ns = 0,
        Ni = 0,
        d = 0,
        illum = 0,
        map_Ka = '',
        map_Kd = '',
        map_Ks = '',
        map_Ns = '',
        map_d = '',
        map_bump = '',
    }
end


OBJ_parser.subset = function (self, _tVertex,  _tIndex)

    return
    {
        tVertex = _tVertex or {},
        tIndex  = _tIndex or {},
        sName   = 'unnamed',
        tMaterial = nil
    }
end

OBJ_parser.is_same_side = function(self,p1,p2, a,b)

    self.A:set(b.x  - a.x, b.y -  a.y, b.z - a.z)
    self.B:set(p1.x - a.x, p1.y - a.y, p1.z - a.z)
    self.C:set(p2.x - a.x, p2.y - a.y, p2.z - a.z)

    self.cp1:cross(self.A,self.B)
    self.cp2:cross(self.A,self.C)
    if self.cp1:dot(self.cp2) >= 0 then
        return true
    else
        return false
    end
end

-- Generate a cross product normal for a triangle
OBJ_parser.generate_normal = function(self,t1,t2,t3)

    self.V:set(t3.x - t1.x, t3.y - t1.y, t3.z - t1.z)
    self.U:set(t2.x - t1.x, t2.y - t1.y, t2.z - t1.z)
    self.aux_normal:cross(self.U,self.V)

    return {x = self.aux_normal.x, y = self.aux_normal.y, z = self.aux_normal.z}
end

OBJ_parser.get_magnitude = function(self, v)

    self.magnitude:set(v.x,v.y,v.z)
    return self.magnitude:length()
end

OBJ_parser.dot = function(self, a, b)

    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z)
end

-- Angle between 2 Vector
OBJ_parser.angle_between_vector = function(self,a, b)

    local angle = self:dot(a, b)
    local r = self:get_magnitude(a) * self:get_magnitude(b)
    if r == 0 then 
        return 0 
    end
    angle = angle / r
    return math.acos(angle)
end

OBJ_parser.projction_vector = function (self,a,b)

    local m  = self:get_magnitude(b)
    local bn = {x = b.x / m, y = b.y / m, z = b.z / m}
    local d  = self:dot(a, bn)
    return {x = bn.x * d, y = bn.y * d, z = bn.z * d}
end

-- Check to see if a Vector Point is within a Triangle
OBJ_parser.is_point_in_triangle = function(self,point, tri1, tri2, tri3)

    -- Test to see if it is within an infinite prism that the triangle outlines.
    local within_tri_prisim = self:is_same_side(point, tri1, tri2, tri3) and 
                              self:is_same_side(point, tri2, tri1, tri3) and 
                              self:is_same_side(point, tri3, tri1, tri2)

    -- If it isn't it will never be on the triangle
    if not within_tri_prisim then
        return false
    end

    -- Calculate Triangle's Normal
    local n = self:generate_normal(tri1, tri2, tri3)

    -- Project the point onto this normal
    local proj = self:projction_vector(point, n)

    -- If the distance from the triangle to the point is 0
    -- it lies on the triangle
    if self:get_magnitude(proj) == 0 then
        return true
    else
        return false
    end
end

OBJ_parser.get_element = function(self,elements,index)

    if index < 0 then
        index = #elements + index + 1
    end
    return elements[index]
end

OBJ_parser.parse_file = function (self,sFileNameOBJ)

    sFileNameOBJ = mbm.getFullPath(sFileNameOBJ)
    self.fp = io.open(sFileNameOBJ,"r")
    if (self.fp == nil) then
        return self:failed("Could not open the file:" .. sFileNameOBJ)
    end

    print('\n OBJ importer version ' .. OBJ_parser.version)
    print('File name:',sFileNameOBJ)
    print('Option invert u/v:',tostring(self.bInvert_u) .. '/' .. tostring(self.bInvert_v) .. '\n')

    local tFrame        = {}
    local tAllVertices  = {}
    local tIndex        = {}
    local tMaterial     = {}

    local tPositions = {}
    local tCoordinates = {}
    local tNormals = {}

    local Vertices = {} 
    local Indices = {}

    local bFirstGroup = true

    local sGroupName = ''
    local sObjectName = ''
    local sUsemtl = ''

    
    for line in self.fp:lines() do
        
        if not line:find('^%s*#') then --it is a comment, discard

            if line:find('^%s*[og]%s') then -- Generate a subset Object or Prepare for an object to be created

                local sGroupOrObject = ''
                local bGroup = line:find('^%s*(g)%s*%w+')
                
                if bFirstGroup then
                    bFirstGroup = false
                    sGroupOrObject = line:match('^%s*[og]%s*(%w+)') or 'unnamed'
                    if string.len(sGroupOrObject)  == 0 then
                        sGroupOrObject = "unnamed"
                    end
                else
                    -- Generate a subset and  put it into in the frame
                    if #Indices > 0 and #Vertices > 0 then
                        -- Create subset
                        local tSubset = self:subset(Vertices, Indices)

                        --Insert subset
                        table.insert(tFrame,tSubset)

                        -- Cleanup
                        Vertices = {}
                        Indices  = {}
                        sGroupOrObject = line:match('^%s*[og]%s+(%w+)') or 'unnamed'
                        if sGroupOrObject == nil or string.len(sGroupOrObject)  == 0 then
                            sGroupOrObject = "unnamed"
                        end
                        if bGroup then
                            tSubset.sGroupName = sGroupOrObject
                        else
                            tSubset.sName = sGroupOrObject
                        end

                        if sUsemtl then
                            local material = tMaterial[sUsemtl]
                            if material then
                                tSubset.tMaterial = material
                            end
                        end
                    else
                        if line:find('^%s*[og]%s') then
                            sGroupOrObject = line:match('^%s*[og]%s+(%w+)')
                        else
                            sGroupOrObject = "unnamed"
                        end
                    end
                end
                if bGroup then
                    sGroupName = sGroupOrObject
                else
                    sObjectName = sGroupOrObject
                end
            elseif line:find('^%s*v%s') then --vertex position

                local x,y,z = line:match('^%s*v%s+(-?[%d%.,eE%+%-]+)%s+(-?[%d%.,eE%+%-]+)%s+(-?[%d%.,eE%+%-]+)')
                if (x and y and z) then
                    table.insert(tPositions,{x=tonumber(x),y=tonumber(y),z=tonumber(z)})
                else
                    return self:failed('Something wrong at line (v):' .. line,x,y,z)
                end
            elseif line:find('^%s*vt%s') then --texture coordinates

                local u,v = line:match('^%s*vt%s+(-?[%d%.,eE%+%-]+)%s+(-?[%d%.,eE%+%-]+)')
                if (u and v) then
                    table.insert(tCoordinates,{u=tonumber(u),v=tonumber(v)})
                else
                    return self:failed('Something wrong at line (vt):' .. line)
                end
            elseif line:find('^%s*vn%s') then --normal coordinates

                local nx,ny,nz = line:match('^%s*vn%s+(-?[%d%.,eE%+%-]+)%s+(-?[%d%.,eE%+%-]+)%s+(-?[%d%.,eE%+%-]+)')
                if (nx and ny and nz) then
                    table.insert(tNormals,{nx=tonumber(nx),ny=tonumber(ny),nz=tonumber(nz)})
                else
                    return self:failed('Something wrong at line (vn):' .. line)
                end
            elseif line:find('^%s*f%s+') then --faces

                local tVertexTmp = {}
                if not self:generate_vertices_from_raw_obj(tVertexTmp, tPositions, tCoordinates, tNormals, line) then
                    return self:failed('Something went wrong into generate_vertices_from_raw_obj:' .. line)
                end
                
                -- Add Vertices
                for i=1,#tVertexTmp do
                    table.insert(Vertices,tVertexTmp[i])
                    table.insert(tAllVertices,tVertexTmp[i])
                end

                local iIndices = {}
                
                if not self:triangulation(iIndices, tVertexTmp) then
                    return self:failed('Something wrong at triangulation:' .. line)
                end
                
                --Add the indexes
                for i=1,#iIndices do
                    local index_raw = #Vertices - #tVertexTmp + iIndices[i]
                    table.insert(Indices,index_raw)

                    index_raw = #tAllVertices - #tVertexTmp + iIndices[i]
                    table.insert(tIndex,index_raw)
                end

            elseif line:find('^%s*usemtl%s+.+') then

                local sMaterial = line:match('^%s*usemtl%s+(.+)')
                if sMaterial then
                    local material = tMaterial[sMaterial]
                    if material then
                        sUsemtl = sMaterial
                        if #tFrame > 0 then
                            local tSubset = tFrame[#tFrame]
                            if tSubset.tMaterial == nil then
                                tSubset.tMaterial = material
                            end
                        end
                    end
                end
            elseif line:find('^%s*mtllib%s*%w') then

                local newmtl_name = line:match('^%s*mtllib%s+([%g ]+)')
                print('newmtl_name',newmtl_name)
                self:loadNewMtl(newmtl_name,tMaterial)

            end
        end
    end

    self.fp:close()
    self.fp = nil

    -- last subset
    if #Indices > 0 and #Vertices > 0 then
        
        local tSubset      = self:subset(Vertices, Indices)
        tSubset.sGroupName = sGroupName
        tSubset.sName      = sObjectName

        if sUsemtl then
            local material = tMaterial[sUsemtl]
            if material then
                tSubset.tMaterial = material
            end
        end

        table.insert(tFrame,tSubset)
    end

    if #tFrame  > 0 then
        print('File successfully parsed...')
    else
        print('Failed to parse obj...')
    end

    return #tFrame  > 0 , tFrame
end

OBJ_parser.loadNewMtl = function(self,newmtl_name,tMaterial)
    
    local newmtl_file_name = mbm.getFullPath(newmtl_name)
    local fp = io.open(newmtl_file_name,"r")
    if (fp == nil) then
        print('Could not open the material file:' .. newmtl_file_name)
        return false
    end

    local function write_color(r,g,b,component)

        component.r = tonumber(r)
        component.g = tonumber(g)
        component.b = tonumber(b)
        local rv = math.floor(component.r * 255)
        local gv = math.floor(component.g * 255)
        local bv = math.floor(component.b * 255)
        local hr = string.format('%02X', rv)
        local hg = string.format('%02X', gv)
        local hb = string.format('%02X', bv)
        component.hex_color  = '#FF' .. hr .. hg .. hb

    end

    local material_name = nil
    for line in fp:lines() do

        if line:find('^%s*newmtl%s.+%s?') then

            material_name = line:match('^%s*newmtl%s+(%S+.*)%s*$')
            tMaterial[material_name] = self:material_info()

        elseif line:find('^%s*K[ads]%s') then -- color

            local ads, r,g,b = line:match('^%s*K([ads])%s+(-?[%d.,eE%+]+)%s+(-?[%d.,eE%+]+)%s+(-?[%d.,eE%+]+)')
            if (ads and r and g and b) then
                local material = tMaterial[material_name]
                local sTableName = 'K' .. ads
                local tK = material[sTableName]
                write_color(r,g,b,tK)
            else
                print('Failed to get color from file:',newmtl_name)
            end

        elseif line:find('^%s*map_K[ads]%s') then -- Texture map_Ka, map_Kd, map_Ks

            local map_K, file_name = line:match('^%s*map_K([ads])%s+([%g ]+)')
            if (map_K and file_name) then
                local material = tMaterial[material_name]
                local sTableName = 'map_K' .. map_K
                local map_Kads = material[sTableName]
                if map_Kads then
                    material[sTableName] = file_name
                end
            else
                print('Failed to get Texture map_Ka, map_Kd, map_Ks from file:',newmtl_name)
            end

        elseif line:find('^%s*map_Ns%s') then -- Texture map_Ns

            local map_Ns, file_name = line:match('^%s*map_Ns%s+([%g ]+)')
            if (map_Ns and file_name) then
                local material = tMaterial[material_name]
                material[sTableName].map_Ns = file_name
            else
                print('Failed to get Texture map_Ns from file:',newmtl_name)
            end

        elseif line:find('^%s*map_d%s') then -- Texture map_d

            local map_d, file_name = line:match('^%s*map_d%s+([%g ]+)')
            if (map_d and file_name) then
                local material = tMaterial[material_name]
                material[sTableName].map_d = file_name
            else
                print('Failed to get Texture map_d from file:',newmtl_name)
            end

        elseif line:find('^%s*map_bump%s') then -- Texture map_bump

            local map_bump, file_name = line:match('^%s*map_bump%s+([%g ]+)')
            if (map_bump and file_name) then
                local material = tMaterial[material_name]
                material[sTableName].map_bump = file_name
            else
                print('Failed to get Texture map_bump from file:',newmtl_name)
            end

        elseif line:find('^%s*Ns%s') then -- Specular Exponent

            local ns = line:match('^%s*Ns%s+(-?[%d.,eE%+]+)')
            if ns then
                local material = tMaterial[material_name]
                material.Ns = tonumber(ns)
            else
                print('Failed to get Specular Exponent from file:',newmtl_name)
            end

        elseif line:find('^%s*Ni%s') then -- Optical Density

            local ni = line:match('^%s*Ni%s+(-?[%d.,eE%+]+)')
            if ni then
                local material = tMaterial[material_name]
                material.Ni = tonumber(ni)
            else
                print('Failed to get Optical Density from file:',newmtl_name)
            end

        elseif line:find('^%s*d%s')  then -- transparency (dissolved (opaque = 1))

            local d = line:match('^%s*d%s+(-?[%d.,eE%+]+)')
            if d then
                local material = tMaterial[material_name]
                material.d = tonumber(d)
            else
                print('Failed to get dissolved from file:',newmtl_name)
            end

        elseif line:find('^%s*Tr%s') then -- transparency (Tr = 0 == opaque) (Tr = 1 - d)

            local Tr = line:match('^%s*Tr%s+(-?[%d.,eE%+]+)')
            if Tr then
                local material = tMaterial[material_name]
                material.Tr = tonumber(Tr)
            else
                print('Failed to get transparency from file:',newmtl_name)
            end

        elseif line:find('^%s*illum%s') then -- illumination mode

            local illum = line:match('^%s*illum%s+(-?[%d.,eE%+]+)')
            if illum then
                local material = tMaterial[material_name]
                material.illum = tonumber(illum)
            else
                print('Failed to get illumination mode from file:',newmtl_name)
            end

        end
    end

    fp:close()

    print('Loaded new material',newmtl_name)
end

OBJ_parser.generate_vertices_from_raw_obj = function(self,tVertexOut,iPositions,iTCoords,iNormals,line)
    
    local tLines = line:split('%s+')
    -- f 4611 4657 4656 4612                                        #faces
    -- f 4//9 3//8 2//6 1//3                                        #faces and normals
    -- f 400//900 300//800 200//600 100//300                        #faces and normals
    -- f 1/1 2/2 3/3 4/4                                            #faces and texture
    -- f 100/100 200/200 300/300 400/400                            #faces and texture
    -- f 4/5/9 3/88/8 2/9889/6 10/10/3                              #faces ,texture and  normals
    -- f 421/521/921 321/882/821 2121/9889/6121 102121/10212/3212   #faces ,texture and  normals
    if #tLines < 4 then
        return self:failed('Expected at least 3 vertex coordinates (face):[' .. line .. ']')
    end

    for i=2,#tLines do --skip f

        local sLine = tLines[i]

        if not sLine:find('^%s+$') and sLine:len() > 0 then --empty 

            local tVertex = self:vertex()

            if sLine:find('%-?%d+%/%-?%d+%/%-?%d+') then --vertex ,texture and normal

                local v,t,n = sLine:match('(%-?%d+)%/(%-?%d+)%/(%-?%d+)')
                local index_buffer  = tonumber(v)
                local index_uv      = tonumber(t)
                local index_normal  = tonumber(n)

                if index_uv == nil or index_normal == nil then
                    return self:failed('Something wrong at line (f,vtn): [' .. line .. ']' .. ' [' .. sLine .. ']')
                end

                local tPosition                    = self:get_element(iPositions,   index_buffer)
                local tCoordinates                 = self:get_element(iTCoords,     index_uv)
                local tNormal                      = self:get_element(iNormals,     index_normal)
                tVertex.x,  tVertex.y,  tVertex.z  = tPosition.x, tPosition.y, tPosition.z
                tVertex.nx, tVertex.ny, tVertex.nz = tNormal.nx,  tNormal.ny, tNormal.nz
                if self.bInvert_u then
                    tVertex.u                      = 1.0 - tCoordinates.u
                else
                    tVertex.u                      = tCoordinates.u
                end
                if self.bInvert_v then
                    tVertex.v                      = 1.0 - tCoordinates.v
                else
                    tVertex.v                      = tCoordinates.v
                end
                table.insert(tVertexOut,tVertex)

            elseif sLine:find('%-?%d+%/%/%-?%d+') then -- vertex and normal

                local v,n           = sLine:match('(%-?%d+)%/%/(%-?%d+)')
                local index_buffer  = tonumber(v)
                local index_normal  = tonumber(n)

                if index_normal == nil then
                    return self:failed('Something wrong at line (f,vn): [' .. line .. ']' .. ' [' .. sLine .. ']')
                end

                local tPosition                    = self:get_element(iPositions,  index_buffer)
                local tNormal                      = self:get_element(iNormals,    index_normal)
                tVertex.x,  tVertex.y,  tVertex.z  = tPosition.x, tPosition.y, tPosition.z 
                tVertex.nx, tVertex.ny, tVertex.nz = tNormal.nx,  tNormal.ny, tNormal.nz 
                table.insert(tVertexOut,tVertex)

            elseif sLine:find('%-?%d+%/%-?%d+') then -- vertex and texture

                local v,t           = sLine:match('(%-?%d+)%/(%-?%d+)')
                local index_buffer  = tonumber(v)
                local index_uv      = tonumber(t)

                if index_uv == nil then
                    return self:failed('Something wrong at line (f,vt): [' .. line .. ']' .. ' [' .. sLine .. ']')
                end
                
                local tPosition                   = self:get_element(iPositions,   index_buffer)
                local tCoordinates                = self:get_element(iTCoords,     index_uv)
                tVertex.x,  tVertex.y,  tVertex.z = tPosition.x, tPosition.y, tPosition.z 
                if self.bInvert_u then
                    tVertex.u                      = 1.0 - tCoordinates.u
                else
                    tVertex.u                      = tCoordinates.u
                end
                if self.bInvert_v then
                    tVertex.v                      = 1.0 - tCoordinates.v
                else
                    tVertex.v                      = tCoordinates.v
                end
                table.insert(tVertexOut,tVertex)

            elseif sLine:find('%-?%d+') then --vertex

                local v             = sLine:match('(%-?%d+)')
                local index_buffer  = tonumber(v)
                local tPosition                   = self:get_element(iPositions,   index_buffer)
                tVertex.x,  tVertex.y,  tVertex.z = tPosition.x, tPosition.y, tPosition.z 
                table.insert(tVertexOut,tVertex)

            elseif sLine:find('^#') then --comment
                break
            end
        end
    end
    return true
end

OBJ_parser.triangulation = function(self,tIndexOut,tVertex)

    -- If there are 2 or less vertices
    -- no one triangle can be created,
    -- so exit
    if #tVertex < 3 then
        return false
    end
    -- If it is a triangle no need to triangulate it
    if #tVertex == 3 then
        table.insert(tIndexOut,1)
        table.insert(tIndexOut,2)
        table.insert(tIndexOut,3)
        return true
    end

    table.insert(tIndexOut,1)
    table.insert(tIndexOut,2)
    table.insert(tIndexOut,3)

    local t1 = 1
    local t3 = 3

    for i=4, #tVertex do
        table.insert(tIndexOut,t1)
        t1 = tIndexOut[#tIndexOut]
        table.insert(tIndexOut,t3)
        table.insert(tIndexOut,i)
        t3 = tIndexOut[#tIndexOut]
    end
    return true
end

return OBJ_parser