mbm.setColor(1,1,1) --set background color to white

function createCubeFace()

   local tCube = {  {x=-50,y=-50,z=0}, 
                    {x=-50,y=50,z=0}, 
                    {x=50,y=-50,z=0}, 
                    {x=50,y=50,z=0}}
   local tIndex = {1,2,3, 3,2,4}
   local sTextureFileName = 'crate.png'

   return tCube, tIndex, sTextureFileName
end

function saveMeshToBinaryFile(fileName,tVertex,tIndex,sTexture)

    --meshDebug is used to create dynamically mesh in the engine.
    --For mesh it has to have at least one frame to be able to generate the mesh
    local stride      = 3 --stride only can be 3 or 2. it means (x,y,z) or (x,y)
    local tMesh       = meshDebug:new() --new mesh debug to store the information about our mesh
    local nFrame      = tMesh:addFrame(stride) -- Add one frame with stride 3 (x,y,z)
    local indexFrame  = nFrame   --(meshDebug uses 1 based index)
    local indexSubset = 1 --first subset (1 based index)
   
   
    --To add vertex, first we need to add a subset
    local nSubset     = tMesh:addSubSet(indexFrame) --add one subset for the first frame

    --we are adding vertex to frame (next)
    --this vertex list has to have at least 3 vertex (one triangle) to be valid
    -- The table expected is : {{x,y,z},{x,y,z},{x,y,z}, ...}
    if not tMesh:addVertex(indexFrame,indexSubset,tVertex) then 
        print("Error on add vertex buffer")
        return false
    end
    
    if not tMesh:addIndex(indexFrame,indexSubset,tIndex) then 
        print("Error on add index buffer")
        return false
    end

    --apply the texture to frame / subset
    if not tMesh:setTexture(indexFrame,indexSubset,sTexture) then
        print("Error on set texture!")
        return false
    end

    tMesh:setType('mesh')  -- set it to mesh type

    local calcNormal,calcUv = true,true --Instruct to calculate normal and UV
    if tMesh:save(fileName,calcNormal,calcUv) then
        print("Mesh created successfully ")
        return true
    else
        print("Failed to create Mesh!")
        return false
    end
    
end


function onInitScene()

    tMesh = mesh:new('3D')-- our object which will load from binary file

    local sFileNameMesh = 'crate.msh'

    local tCubeVertex, tCubeIndex, sCubeTextureFileName = createCubeFace()--create our cube face 

    if saveMeshToBinaryFile(sFileNameMesh,tCubeVertex, tCubeIndex, sCubeTextureFileName) then
        tMesh:load(sFileNameMesh) --all coordinate already in place
    else
        print('Failed to create ' .. sFileNameMesh)
        mbm.quit()
    end


    --set up camera
    camera3d = mbm.getCamera('3D')
    camera3d:setPos(0,0,-500)
    camera3d:setFocus(0,0,0)

    --need to rotate our object
    tMouse = {x=0,y=0,clicked = false}
end

function onTouchDown(key,x,y)
    tMouse.clicked = true
    tMouse.x = x
    tMouse.y = y
end

function onTouchMove(key,x,y)
    if tMouse.clicked then
        local diff_X = tMouse.x - x
        local diff_Y = tMouse.y - y
        tMouse.x = x
        tMouse.y = y

        --simple rotation based on mouse
        tMesh.ay = tMesh.ay + math.rad(diff_X)
        tMesh.ax = tMesh.ax + math.rad(diff_Y)

    end
end

function onTouchUp(key,x,y)
    tMouse.clicked = false
end

