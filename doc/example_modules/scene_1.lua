--example of scene

function onInitScene()
    tTexture = texture:new('2ds') --tTexture is global for the scene
    tTexture:load('crate.png') --just load the texture (does not care if not found the texture)
    tTexture:setScale(0.5,0.5) --adjust the scale
end

function onTouchMove(key,x,y)
    tTexture:setPos(x,y) --move according to mouse or touch (we are ignoring the key)
end

function onTouchDown(key,x,y)
    local tTextureTmp = texture:new('2ds',x,y)-- create a new local texture. ***this will be collected by GC***
    tTextureTmp:load('crate.png') --Load it
    tTextureTmp:setScale(0.5,0.5)--adjust the scale
    tTextureTmp.az = tTexture.az -- adjust the rotation on z angle
    tTexture.z = tTextureTmp.z -1 --bring to front
end

function loop(delta)
    tTexture:rotate('z',math.rad(30)) -- rotate 30 degree per second on z angle
end 