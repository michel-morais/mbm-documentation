--example of scene class style


local tMyClass = {}

tMyClass.onInitScene = function (self) 
    self.tMyTexture = texture:new('2ds') --texture is part of tMyClass now
    self.tMyTexture:load('crate.png') -- just load the texture (does not care if not found the texture)
    self.tMyTexture:setScale(0.5,0.5) --adjust the scale
    
end
    
tMyClass.onTouchMove = function(self,key, x,y)
    self.tMyTexture:setPos(x,y) --move according to mouse or touch (we are ignoring the key)
end

tMyClass.loop= function(self,delta)
    self.tMyTexture:rotate('z',math.rad(30)) -- rotate 30 degree per second on z angle
end


tMyClass.onTouchDown = function(self,key, x,y)
    local tTextureTmp = texture:new('2ds',x,y)-- create a new local texture. ***this will be collected by GC***
    tTextureTmp:load('crate.png') --Load it
    tTextureTmp:setScale(0.5,0.5)--adjust the scale
    tTextureTmp.az = self.tMyTexture.az -- adjust the rotation on z angle
    self.tMyTexture.z = tTextureTmp.z -1 --bring to front
end

local w, h = mbm.getSizeScreen()

--[[
    important return the class otherwise won't work
]]

return tMyClass
