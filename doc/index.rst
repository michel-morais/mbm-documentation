.. documentation master file, created by
   sphinx-quickstart on Mon Jul 23 17:50:14 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to documentation!
=========================

.. TODO: describe and check each index 1 based


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   introduction
   scene
   mbm
   camera
   font
   gif
   line
   mesh
   meshDebug
   particle
   render2texture
   renderizable
   shader
   shape
   sprite
   text
   texture
   tile
   timer
   vec2
   vec3
   audio
   editors
   modules
   box2d
   imgui
   lsqlite3


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
