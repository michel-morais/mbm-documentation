.. contents:: Table of Contents

Editors
=======
**MBM** has some :guilabel:`LUA` based *editors*. This page intent to show each of them and its main functionality.

Those editor is meant to facilitate to build games/application. 

We are talking about :guilabel:`sprite`, :guilabel:`mesh`, :guilabel:`particle`, :guilabel:`font/text`, :guilabel:`shader`, etc...

File Format
-----------

All editors can save the current edit status, however, the main objective is to create a final binary file that the engine is able to read in the optimized way.

Here the expected format for each editor:

+---------------------+-------------------+---------------------+--------------------------------+
| Editor              |  Editor Format    |    Binary Format    |   Type mesh                    |
+=====================+===================+=====================+================================+
| Sprite Maker        |  ``.sprite``      |     ``.spt``        |   :ref:`sprite <sprite>`       |
+---------------------+-------------------+---------------------+--------------------------------+
| Font Maker          |  ``.ttf``         |     ``.fnt``        |   :ref:`font <font>`           |
+---------------------+-------------------+---------------------+--------------------------------+
| Particle Maker      |  ``.particle``    |     ``.ptl``        |   :ref:`particle <particle>`   |
+---------------------+-------------------+---------------------+--------------------------------+


.. _sprite_maker_editor:

Sprite Maker
------------

**Sprite Maker** meant to make animations based on image(s).

One sprite might have one or more images, one or more animations, one or more frames.

The animations are based on frames interpolation  and also might be repeated (you might want to make some effect on it).

The application is written in :guilabel:`LUA` script and the file name is :guilabel:`sprite_maker.lua`.

It is dependent on :ref:`ImGui Plugin <ImGui>` and :ref:`EDITOR_FEATURES <flag_compilation>` flag of compilation in the engine.


First Steps With Sprite Maker Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For this example it will be used those six images:

.. figure:: _static/ninja_run.png
   :align: center


Open :guilabel:`Sprite Maker` indicating the script and typing the desired width and height:

.. prompt:: bash $

    mini-mbm sprite_maker.lua -w 1280 -h 800 --nosplash

.. Note:: You might need to specify the whole path if it is not in your environment.

.. figure:: _static/sprite_maker_1.png
   :align: center


From menu insert image and choose *All images* needed:

.. figure:: _static/sprite_maker_2.png
   :align: center

From menu in *Image(s) Selector* select *All Images*:

.. figure:: _static/sprite_maker_3.png
   :align: center

From menu in *Add Frame* click in *Add all* and one frame for each image will be added:

.. figure:: _static/sprite_maker_4.png
   :align: center


From menu *Animations* click in *Edit Animations* and choose the frames, type, interval and time between frames:

.. figure:: _static/sprite_maker_5.gif
   :align: center

Also it is possible to create frame from sprite sheet. This allows to crop a lot of frames from one image:

.. figure:: _static/sprite_maker.png
   :align: center
   :figclass: align-center


Now a *sprite* could be loaded using :ref:`sprite <sprite>`.

Font Maker
----------

**Font Maker** or should be better called **Font Parser**, is a way to create font from *truetype font*.

It is a simple editor which import and parse *truetype font* and allow to make some desired animations and then, save as binary file to be used in the engine.

The application is written in :guilabel:`LUA` script and the file name is :guilabel:`font_maker.lua`.

It is dependent on :ref:`ImGui Plugin <ImGui>` and :ref:`EDITOR_FEATURES <flag_compilation>` flag of compilation in the engine.



First Steps With Font Maker Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   For this example it will be used the **Carlito-Regular** *truetype font*.

:download:`download Carlito-Regular.ttf <_static/Carlito-Regular.ttf>`

From command line type:

.. prompt:: bash $

   mini-mbm --scene font_maker.lua -w 1280 -h 800 --nosplash

.. Note:: You might need to specify the whole path if it is not in your environment.

.. figure:: _static/font_maker_1.png
   :align: center
   :figclass: align-center


From menu, choose the desired height of font (default is 50) and click in *Load Font*:

.. figure:: _static/font_maker_2.png
   :align: center
   :figclass: align-center


Change/add animation, change positions, space, size, colors as desired and click in *Save Binary Font*:


.. figure:: _static/font_maker_3.gif
   :align: center
   :figclass: align-center

.. Note:: For each font size, a ``png`` file will be created. The ``png`` is the image itself as a sprite sheet and the binary file contains the information (animation, shaders, position etc.) to be used in the engine. These two files are required.

.. figure:: _static/Carlito-Regular-50.png
   :align: center
   :figclass: align-center

   Carlito-Regular png generated

:download:`download Carlito-Regular-50.png <_static/Carlito-Regular-50.png>`.

Now a *font* could be loaded using :ref:`font <font>`.

Particle Editor
---------------

**Particle Editor** is a editor to create particle behavior.

One particle might have one image (texture) and one or more stage(s) (like animations but with extra control).

The shader is specific for :ref:`particle <particle>`  (see :ref:`shader particle detail <particleShader>` ).

The application is written in :guilabel:`LUA` script and the file name is :guilabel:`particle_editor.lua`.

It is dependent on :ref:`ImGui Plugin <ImGui>` and :ref:`EDITOR_FEATURES <flag_compilation>` flag of compilation in the engine.


First Steps With Particle Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   For this example, we will use the folowing texture:

   .. figure:: _static/particle_flame.png
    :align: center
    :width: 200px
    :height: 200px
    :figclass: align-center

:download:`download particle_flame.png <_static/particle_flame.png>`

From command line type:

.. prompt:: bash $

   mini-mbm --scene particle_editor.lua -w 800 -h 600 --nosplash

.. Note:: You might need to specify the whole path if it is not in your environment.

.. figure:: _static/particle_editor_1.png
   :align: center
   :figclass: align-center

From menu, choose the *File* select *New Particle*

.. figure:: _static/particle_editor_2.gif
   :align: center
   :figclass: align-center


From menu, choose the *Image* select *Add Image* and set *particle_flame.png*.

Change the *Number of particle*, *Direction* and other options from *Particle Options* as desired.

.. figure:: _static/particle_editor_3.gif
   :align: center
   :figclass: align-center

From menu, choose the *File* select *Save Particle* and select the file name.

Now a *particle* could be loaded using :ref:`particle <particle>`.



Physic Editor
-------------

**Physic Editor** is a simple editor to be able to edit physics of :ref:`sprite <sprite>` or :ref:`mesh <mesh>`.

The editor is dependent on :ref:`ImGui Plugin <ImGui>` and :ref:`EDITOR_FEATURES <flag_compilation>` flag of compilation in the engine.

This editor is **TODO** for 3D objects:

.. figure:: _static/physics_editor_1.png
   :align: center


Simple load a :ref:`sprite <sprite>` or :ref:`mesh <mesh>` and edit, then save it.


To run it from command line type:

.. prompt:: bash $

   mini-mbm --scene physic_editor.lua -w 800 -h 600 --nosplash

Shader Editor
-------------

**Shader Editor** is a editor to change/add :ref:`shader <shader>` effect. Also is possible to add or change :ref:`animation <animation>` .

This editor support all binaries types of :guilabel:`mesh` created by the engine.

The application is written in :guilabel:`LUA` script and the file name is :guilabel:`shader_editor.lua`.

It is dependent on :ref:`ImGui Plugin <ImGui>` and :ref:`EDITOR_FEATURES <flag_compilation>` flag of compilation in the engine.


First Steps With Shader Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   For this example, create a :ref:`sprite <sprite>` using :ref:`sprite maker <sprite_maker_editor>` with the folowing texture:

   .. figure:: _static/crate.png
      :align: center
      :width: 200
      :height: 200
      :figclass: align-center

:download:`download crate.png <_static/crate.png>`

From command line type:

.. prompt:: bash $

   mini-mbm --scene shader_editor.lua -w 800 -h 600 --nosplash

.. Note:: You might need to specify the whole path if it is not in your environment.

.. figure:: _static/shader_editor_1.png
   :align: center
   :figclass: align-center

From menu, choose the *File* select *Open Mesh* and select the sprite created.

.. figure:: _static/shader_editor_2.png
   :align: center
   :figclass: align-center


From menu *Shader Options* select *Pixel Shader* -> *bloom.ps* and change the type for ``RECURSIVE_LOOP``.

Change others parameters as desired.

.. figure:: _static/shader_editor_3.gif
   :align: center
   :figclass: align-center

From menu, choose the *File* select *Save Mesh* and select the file name.

Now the *mesh* will have the shader configured in the binary file. Whenever be load, automatically will be set the shader with the configurations set in the editor.

Scene Editor
------------

The **Scene Editor** is a simple 2D editor, meant to create :ref:`scene <scene>` in visual mode.

This editor support all binaries types of :guilabel:`mesh` created by the engine.

The application is written in :guilabel:`LUA` script and the file name is :guilabel:`scene_editor2d.lua`.

It is dependent on :ref:`ImGui Plugin <ImGui>`, :ref:`Box 2d <box2d>` and :ref:`EDITOR_FEATURES <flag_compilation>` flag of compilation in the engine.


First Steps With Scene Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   For this example, create a :ref:`sprite <sprite>` using :ref:`sprite maker <sprite_maker_editor>` with the folowing texture:

   .. figure:: _static/crate.png
      :align: center
      :width: 200
      :height: 200
      :figclass: align-center

:download:`download crate.png <_static/crate.png>`

Then, after that, from command line type:

.. prompt:: bash $

   mini-mbm --scene scene_editor2d.lua -w 800 -h 600 --nosplash

.. Note:: You might need to specify the whole path if it is not in your environment.

.. figure:: _static/scene2d_editor_1.png
   :align: center
   :figclass: align-center

From menu, choose the *Mesh* select *Added Mesh* and select the sprite created.

.. figure:: _static/scene2d_editor_2.png
   :align: center
   :figclass: align-center

Edit the position, scale, rotation or physics as you wish...

.. figure:: _static/scene2d_editor_3.png
   :align: center
   :figclass: align-center

From menu *File* select *Save Scene* -> *my_scene.lua*.

.. figure:: _static/scene2d_editor_4.png
   :align: center
   :figclass: align-center

Now you have the scene with all mesh(es) added. 

You can use the scene as point of start and do the necessary logic for your game.

Here an example using the scene:

.. code-block:: lua
   
   tScene = require "my_scene"

   function onInitScene()
      mbm.setColor(1,1,1)
      tScene:load()
   end

   function onTouchDown(key,x,y)
      -- we are sure that box.spt exists in the file my_scene.lua
      local tMesh = tScene:addMesh('box.spt')
      tMesh.x, tMesh.y = mbm.to2dw(x,y)
   end

   function loop(delta)
      -- your logic here    
   end


.. figure:: _static/scene2d_editor_5.gif
   :align: center
   :figclass: align-center

* Remarks
   
   - It was added only one :ref:`sprite <sprite>` using the editor. 
   - Therefore, when ``tScene:load()`` is called, only one :ref:`sprite <sprite>` is added to the :ref:`scene <scene>`. 
   - The others :ref:`sprites <sprite>` are dynamic added as you can see in the code.
   - Besides the ``tMesh`` is local, the scene will hold the instance in the table ``tScene.tMeshesLoaded``. That is why the object is not destroyed by gc from :guilabel:`LUA`.
   - When is added an object using ``tScene:addMesh``, all configuration (*position*, *scale*, *etc*) are applied to the object added.



Asset Packager
--------------

The **Asset Packager** create asset from any type of files like image file, script file, music file, etc. 

The idea is to be able to make your assets in one unique file as mentioned before. Some people or company want to make the assets not so easy to access for many reasons. This editor might help on that.

The application is written in :guilabel:`LUA` script and the file name is :guilabel:`asset_packager.lua`.

It is dependent on :ref:`ImGui Plugin <ImGui>` and :ref:`SQLite <lsqlite3>` module.


First Steps With Packager
^^^^^^^^^^^^^^^^^^^^^^^^^

From command line type:

.. prompt:: bash $

   mini-mbm --scene asset_packager.lua -w 800 -h 600 --nosplash

.. Note:: You might need to specify the whole path if it is not in your environment.

From menu *File* select *New Asset (from folder)* and add it.

.. figure:: _static/sql_editor_1.png
   :align: center
   :figclass: align-center


Then from menu *File* select *Save asset (to Database)* and it is done!

You can find more information about how to use your asset at :ref:`SQLite <lsqlite3>` module topic.



.. _tile_map_editor:

Tile Map Editor
---------------

**Tile Map Editor** is a 2D level editor meant to help you develop the content of your game by little bricks (tiles).

One tile can be easily mapped through the world (in the size that you need) forming a 2D map.

It is possible create layers and add some information as physics or objects/paths.

The final map is an unique file + images ready to be used in the engine.

The editor is dependent on :ref:`ImGui Plugin <ImGui>` and :ref:`EDITOR_FEATURES <flag_compilation>` flag of compilation in the engine.


First Steps With Tile Map Editor
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For this example it will be used  a tileset from `kenney <https://www.kenney.nl/assets/simplified-platformer-pack>`__ :

.. figure:: _static/platformPack_tilesheet.png
   :align: center


Open :guilabel:`Tile Map` indicating the script and typing the desired width and height:

.. prompt:: bash $

    mini-mbm tilemap_editor.lua -w 1280 -h 800 --nosplash

.. Note:: You might need to specify the whole path if it is not in your environment.

.. figure:: _static/tile_map_1.png
   :align: center

From *Tile Map Options* menu, click at *Load Image(s)* and select the tileset.

Adjust the size of tile. In this example the tile size is 64x64 Pixel with no margin/ space.

.. figure:: _static/tile_map_2.png
   :align: center

Click in create button and got to the layer tab.

Create a new layer and select the bricks (tiles) to make a layer like this:

.. figure:: _static/tile_map_3.png
   :align: center


Then, create a new layer and try to do something like this:

.. figure:: _static/tile_map_4.png
   :align: center

The final result should be something like this:

.. figure:: _static/tile_map_5.png
   :align: center


Save it and now the *tilemap* could be loaded using :ref:`tile <tile>`.

.. _texture_packer_editor:

Texture Packer Editor
---------------------

**Texture Packer Editor** is a 2D level editor meant to merge textures in a single file (sprite sheet)

The editor is dependent on :ref:`ImGui Plugin <ImGui>` and :ref:`EDITOR_FEATURES <flag_compilation>` flag of compilation in the engine.

This is a simple editor and the following image shows the editor making a single texture from severals images:

.. figure:: _static/texture_packer.png
   :align: center


After that you just have to specify the file name to save the final image.