.. contents:: Table of Contents

.. _sprite:

Table sprite
============

A table :ref:`sprite <sprite>` inherit from :ref:`renderizable <renderizable>` and implement all methods as usual..
Each animation on sprite has an initial frame and final frame which represent its cycle.
Also each animation has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.

sprite methods
--------------

sprite new
^^^^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`sprite <sprite>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`sprite <sprite>` *table*.

   *Example:*
.. code-block:: lua

   tSpt = sprite:new('2dw') --note that sprite inherits from renderizable


sprite load
^^^^^^^^^^^

.. data:: load(string file_name)

   Load a :ref:`sprite <sprite>` from file (expected ``.spt`` extension).

   :param string: **file name** from file.

   *Example:*
.. code-block:: lua

   tSprite = sprite:new('3d')
   if tSprite:load('mario.spt') then
      print('Successfully loaded sprite:','mario.spt')
   else
      print('Failed to loaded sprite:','mario.spt')
   end

.. Note::

   | The :guilabel:`load` method will search in all known path.
   | You can add a path to search by the method :ref:`addPath <addPath>`.


Creating a sprite programmatically
----------------------------------

   Here an example how to create an sprite programmatically using the engine to do that.

   It is possible to save any mesh with help of :ref:`meshDebug <meshDebug>`. For that it is necessary to prepare the :ref:`meshDebug <meshDebug>` based on our :ref:`renderizable <renderizable>`.

   * First, we have to create coordinates of vertex buffer.
   * We also need to create index buffer (1 index based) for the vertex.
   * Then we create a :ref:`meshDebug <meshDebug>` which will store all the information.
   * The UV and normal coordinates are not mandatory. For this example we will not fill it.
   * Next we create a frame informing the stride (for sprite will be 2) and add a new subset. 
   * One frame can have one or more subsets. Each subset has its own texture.
   * Next we add the vertex buffer and index buffer.
   * The vertex buffer can have normal buffer (optional) and texture coordinates (optional).
   * Next we set the ``sprite`` type (others types are ``mesh``, ``particle``, ``font``, ...).
   * Finally we just need to specify the file name to save through the function :ref:`save <meshDebugSave>`.

Sprite to a binary  file
^^^^^^^^^^^^^^^^^^^^^^^^

   For the first example we will use the following texture:

.. figure:: example_modules/HB_smile.png
   :align: center
   :height: 200px
   :figclass: align-center

   smile.png

:download:`download <example_modules/HB_smile.png>`.


*Follow the first example how to save a sprite to a binary file:*

   .. literalinclude:: example_modules/saving_sprite_to_binary_file_example_1.lua
     :language: lua
     :linenos:


   :download:`download <example_modules/saving_sprite_to_binary_file_example_1.lua>`.


.. figure:: _static/sprite_creating_example_1.png
   :align: center
   :figclass: align-center

   Example 1 creating sprite



*Follow other example how to save a sprite to a binary file setting uv coordinates:*

   .. literalinclude:: example_modules/saving_sprite_to_binary_file_example_2.lua
     :language: lua
     :linenos:


   :download:`download <example_modules/saving_sprite_to_binary_file_example_2.lua>`.


.. figure:: _static/sprite_creating_example_2.png
   :align: center
   :figclass: align-center

   Example 2 setting uv to create a sprite

*Follow other example how to save a sprite to a binary file setting uv coordinates upside down:*

   .. literalinclude:: example_modules/saving_sprite_to_binary_file_example_3.lua
     :language: lua
     :linenos:


   :download:`download <example_modules/saving_sprite_to_binary_file_example_3.lua>`.


.. figure:: _static/sprite_creating_example_3.png
   :align: center
   :figclass: align-center

   Example 3 setting uv upside down to create a sprite


Sprite animated to a binary  file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   Next one example how to create animated sprite.

*We will use four different image for this example:*

.. figure:: example_modules/HB_smile.png
   :align: center
   :height: 50px

   smile

:download:`download HB_smile.png <example_modules/HB_smile.png>`

.. figure:: example_modules/HB_sad.png
   :align: center
   :height: 50px

   sad

:download:`download HB_sad.png <example_modules/HB_sad.png>`


.. figure:: example_modules/HB_neutral.png
   :align: center
   :height: 50px

   neutral

:download:`download HB_neutral.png <example_modules/HB_neutral.png>`


.. figure:: example_modules/HB_dead.png
   :align: center
   :height: 50px

   dead

:download:`download HB_dead.png <example_modules/HB_dead.png>`


   .. literalinclude:: example_modules/saving_sprite_to_binary_file_example_4.lua
     :language: lua
     :linenos:


   :download:`download <example_modules/saving_sprite_to_binary_file_example_4.lua>`


.. figure:: _static/sprite_creating_example_4.gif
   :align: center
   :figclass: align-center

   Example how to create animated sprite using different images.



Next example we will use a unique image with 9 animations inside:

.. figure:: example_modules/Bird.png
   :align: center
   :height: 300px

   Bird

:download:`download Bird.png <example_modules/Bird.png>`


   .. literalinclude:: example_modules/saving_sprite_to_binary_file_example_5.lua
     :language: lua
     :linenos:

:download:`download <example_modules/saving_sprite_to_binary_file_example_5.lua>`


.. figure:: _static/sprite_creating_example_5.gif
   :align: center
   :figclass: align-center

   Example how to create animated sprite using unique image and different coordinates by frame.
