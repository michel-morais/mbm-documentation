.. contents:: Table of Contents

.. _texture:

Table texture
=============

A table :ref:`texture <texture>` inherit from :ref:`renderizable <renderizable>` and normally have one animation.
Also the animation has a :ref:`shader <shader>` table by default which can be accessed by :ref:`getShader <getShader>` method.

texture methods
---------------

texture new
^^^^^^^^^^^

.. data:: new(string * world, number * x, number * y, number * z)

   Create a new instance of a :ref:`texture <texture>` passing the world desired (:ref:`detail <explaining_2ds_2dw_3d>`) and position.

   :param string: **world** can be ``2ds``, ``2dw`` or ``3d``.
   :param number: **x** position (optional).
   :param number: **y** position (optional).
   :param number: **z** position (optional).
   :return: :ref:`texture <texture>` *table*.

   *Example:*
.. code-block:: lua

   tTex = texture:new('2dw') --note that texture inherits from renderizable
   tTex:setPos(100,100,-1)


texture load
^^^^^^^^^^^^

.. data:: load(string file_name, number * width, number * height)

   Load a :ref:`texture <texture>` from file at any format listed at :ref:`images supported types <images_supported_types>`.

   The number ``width`` and  ``height`` (optional) are the frame size.

   :param string: **file name** from texture.
   :param number: **width** frame (optional).
   :param number: **height** frame (optional).
   :param boolean: **alpha** force to use or not (default is ``true``)(optional).
   :return: *boolean* result.

   *Example:*
.. code-block:: lua

   tTexture = texture:new('2ds')
   if tTexture:load('mario.png') then
      print('Successfully loaded texture:','mario.png')
   else
      print('Failed to loaded texture:','mario.png')
   end

.. Note::

   | The :guilabel:`load` method will search in all known path.
   | You can add a path to search by the method :ref:`addPath <addPath>`.


texture setSize
^^^^^^^^^^^^^^^

.. data:: setSize(number width, number height)

   Resize the frame size from :ref:`texture <texture>`.

   :param number: **width**  from frame.
   :param number: **height** from frame.

   *Example:*
.. code-block:: lua

   tTex = texture:new('2dw') --note that texture inherits from renderizable
   tTex:setPos(100,100,-1)
   tTex:setSize(50,50)

.. Note::

   Always the image will adapt to the frame size.


Creating a texture programmatically
-----------------------------------

   Here an example how to create a texture programmatically using the engine to do that.


   Please check the page :ref:`creating texture <creating_texture>`
