.. contents:: Table of Contents

.. _modules:

Modules
=======

This section has a lot of :ref:`modules <modules>` available and where I hope would be useful for you!

Even if you are learning LUA this is a interesting topic.

Have fun!

What is a Module?
^^^^^^^^^^^^^^^^^
Module is like a library that can be loaded using ``require`` and has a single global name containing a table. 

All functions and variables are wrapped in to a table which acts as a namespace. 
Also, a well behaved module has necessary provisions to return this table on ``require``.

Module can be an ``.lua`` , ``.Dll`` or ``.so`` extension. 

A module can be an external extension for the project or not.

For example, we can consider to build a module that will use some resources from this engine like a :ref:`vec3 <vecthree>`, :ref:`particle <particle>` system, :ref:`blend operation <blend_operation>` , etc.

In another words, a :ref:`modules <modules>` can be an extension about what we have.

The next topics will present some :ref:`modules <modules>` available.

Creating and using Modules
--------------------------

The way for LUA 5.2 create module is described bellow:

First, we will create a file ``mymodule.lua`` with the following content:

.. code-block:: lua

    local mymodule = {}

    function mymodule.foo()
        print("Hello World from module!")
    end

    return mymodule

This is the basic concept of :ref:`modules <modules>`.

Now to use this new module in the interactive interpreter:

.. code-block:: lua

    mymodule = require "mymodule"
    mymodule.foo() -- Hello World from module!

For more information you can consult `ModulesTutorial <http://lua-users.org/wiki/ModulesTutorial>`__.

Parsing OBJ wavefront
^^^^^^^^^^^^^^^^^^^^^

Next we will demonstrate how to import from `OBJ <https://en.wikipedia.org/wiki/Wavefront_.obj_file>`__ format using a :ref:`modules <modules>` written in LUA.

`OBJ <https://en.wikipedia.org/wiki/Wavefront_.obj_file>`__ (or ``.OBJ``) is a geometry definition file format first 
developed by Wavefront Technologies for its Advanced Visualizer animation package. 
The file format is open and has been adopted by other 3D graphics application vendors. 

The parser implemented here might NOT be the fully implementation but at least for this example works.

To do that we will use the following module: ``OBJ_parser.lua``

:download:`You can download OBJ_parser.lua here <example_modules/OBJ_parser.lua>`.

The following program will use the ``OBJ_parser.lua`` to parse an `OBJ <https://en.wikipedia.org/wiki/Wavefront_.obj_file>`__ format and 
generate a mesh ``my_mesh.msh``:

   .. literalinclude:: example_modules/importing_obj_to_mesh.lua
     :language: lua

:download:`download <example_modules/importing_obj_to_mesh.lua>`.

   .. figure:: _static/importing_OBJ_example_1.gif
      :align: center
      :figclass: align-center

      Mesh 3D from OBJ

   :download:`download OBJ <obj/Mech.obj>`.
   :download:`download material <obj/Mech.mtl>`.
   :download:`download texture <obj/Mech.png>`.