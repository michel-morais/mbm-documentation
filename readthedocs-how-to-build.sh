

how_to_build_fn()
{
git clone --depth 1 https://bitbucket.org/michel-morais/mbm-documentation/src/master .
# git fetch origin --force --prune --prune-tags --depth 50 refs/heads/master:refs/remotes/origin/master
# git checkout --force origin/master
git clean -d -f -f
#cat .readthedocs.yaml
#asdf global python 3.9.18
mkdir /tmp/READTHEDOCS
export READTHEDOCS_VIRTUALENV_PATH=/tmp/READTHEDOCS
/tmp/READTHEDOCS/bin/python -mvirtualenv $READTHEDOCS_VIRTUALENV_PATH
/tmp/READTHEDOCS/bin/python -m pip install --upgrade --no-cache-dir pip setuptools
/tmp/READTHEDOCS/bin/python -m pip install --upgrade --no-cache-dir sphinx readthedocs-sphinx-ext
/tmp/READTHEDOCS/bin/python -m pip install --exists-action=w --no-cache-dir -r doc/requirements.txt

cd ~/mbm-doc-test/doc
/tmp/READTHEDOCS/bin/python -m sphinx -T -b html -d _build/doctrees -D language=en . html
}